# 1.微信小程序账号和工具

1. 在线文档：<https://developers.weixin.qq.com/miniprogram/dev/framework/>
2. 官方例子 https://github.com/wechat-miniprogram/miniprogram-demo，可通过以下来查看官方实例

![1581233276982](images/1581233276982.png)

3.登录地址 https://mp.weixin.qq.com/

# 2.文件结构

##   2.1 文件结构

![](images/目录结构.png)

## 2.2 文件类型

微信小程序与html类型对比

| 微信小程序 | html | 区别                                                         |
| :--------: | :--: | ------------------------------------------------------------ |
|    wxml    | html | 1.微信小程序封装了自己的标签                                 |
|    wxss    | css  | 1.wx引用class时不需要加点如：.class  <view class="class">  2.不支持*样式 |
|     js     |  js  | 1.使用时类似vue                                              |
|    json    |      | 1.wx作为配置文件使用                                         |
|            |      |                                                              |

## 2.3 配置文件常用属性

app.json作为配置文件使用，可以用来对全局进行配置，页面路径，超时时间，设置多tab等。

![1581235480474](images/1581235480474.png)

**tabBar：**

![1581246363947](images/1581246363947.png)



# 3.常用组件使用方法

组件文档：https://mp.weixin.qq.com/debug/wxadoc/dev/component/

## 3.1常用标签

| wx标签      | 对应html标签 |                        |
| ----------- | ------------ | ---------------------- |
| <view>      | <div>        |                        |
| <text>      | <span>       |                        |
| <image>     | <img>        |                        |
| <navigator> | <a>          |                        |
| <block>     |              | 区块标签不会渲染到页面 |

## 3.2 常用组件

```
<button></button>
<input type="text" />				
<checkbox />
<radio/>
```

### **3.21 轮播图组件(swiper)**



```
/* pages/demo10/demo10.wxss */
swiper {
  width: 100%;
  /* height: calc(100vw * 352 /  1125); */
  height: 31.28vw;
}
image {
  width: 100%;
}
```



```
<!-- 
  1 轮播图外层容器 swiper
  2 每一个轮播项 swiper-item
  3 swiper标签 存在默认样式
    1 width 100%
    2 height 150px    image 存在默认宽度和高度 320 * 240 
    3 swiper 高度 无法实现由内容撑开 
  4 先找出来 原图的宽度和高度 等比例 给swiper 定 宽度和高度
    原图的宽度和高度  1125 * 352 px
    swiper 宽度 / swiper  高度 =  原图的宽度  /  原图的高度
    swiper  高度  =  swiper 宽度 *  原图的高度 / 原图的宽度
    height: 100vw * 352 /  1125
  5 autoplay 自动轮播
  6 interval 修改轮播时间
  7 circular 衔接轮播  通过该属性能够解决无缝对接的问题
  8 indicator-dots 显示 指示器 分页器 索引器 
  9 indicator-color 指示器的未选择的颜色 
  10 indicator-active-color 选中的时候的指示器的颜色 
 -->
<swiper autoplay interval="1000" circular indicator-dots indicator-color="#0094ff" indicator-active-color="#ff0094">
    <swiper-item> <image mode="widthFix" src="//gw.alicdn.com/imgextra/i1/44/O1CN013zKZP11CCByG5bAeF_!!44-0-lubanu.jpg" /> </swiper-item>
    <swiper-item> <image mode="widthFix" src="//aecpm.alicdn.com/simba/img/TB1CWf9KpXXXXbuXpXXSutbFXXX.jpg_q50.jpg" /> </swiper-item>
    <swiper-item> <image mode="widthFix" src="//gw.alicdn.com/imgextra/i2/37/O1CN01syHZxs1C8zCFJj97b_!!37-0-lubanu.jpg" /> </swiper-item>
</swiper>

```

| 属性名                        | 类型    | 默认值         | 说明                 |
| ----------------------------- | ------- | -------------- | -------------------- |
| indicator-dots indicator-dots | Boolean | false          | 是否显⽰⾯板指⽰点   |
| indicator-color               | Color   | rgba(0,0,0,.3) | 指⽰点颜⾊           |
| indicator-active-color        | Color   | #000000        | 当前选中的指⽰点颜⾊ |
| autoplay                      | Boolean | false          | 是否⾃动切换         |
| interval                      | Number  | 5000           | ⾃动切换时间间隔     |
| circular                      | Boolean | false          | s是否循环轮播        |

### 3.22 rich-text (富文本编辑器)

可将字符串解析成对应的标签

```
<!-- 
  rich-text 富文本标签
  1 nodes属性来实现
    1 接收标签字符串 
    2 接收对象数组 
 -->
 <rich-text nodes="{{html}}"></rich-text>
```

```
// pages/demo12/demo12.js
Page({
  data: {
    // 1 标签字符串 最常用的
    // html:'<div class="tpl-wrapper" data-tpl-id="m_h_v31icon_1" style="margin-top: -10px;"><div view-name="DFrameLayout" style="display: flex; overflow: hidden; height: 160px; width: 375px; place-self: flex-start; position: relative;"><div view-name="DView" style="display: flex; overflow: hidden; background-color: rgb(255, 255, 255); margin-top: 9px; height: 100%; width: 100%; top: 0px; left: 0px; position: absolute;"></div><div view-name="HImageView" style="display: flex; overflow: hidden; height: 100%; width: 100%; position: absolute;"><div style="width: 100%; height: 100%; background-image: url(&quot;https://gw.alicdn.com/tps/TB155AUPpXXXXajXVXXXXXXXXXX-1125-480.png_.webp&quot;); background-repeat: no-repeat; background-position: center center; background-size: contain;"></div></div><div view-name="DLinearLayout" aria-label="天猫" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 10px; margin-top: 13px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB1Wxi2trsrBKNjSZFpXXcXhFXa-183-144.png_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">天猫</div></div><div view-name="DLinearLayout" aria-label="聚划算" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 83.5px; margin-top: 13px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://img.alicdn.com/tfs/TB10UHQaNjaK1RjSZKzXXXVwXXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">聚划算</div></div><div view-name="DLinearLayout" aria-label="天猫国际" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 157px; margin-top: 13px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB11rTqtj7nBKNjSZLeXXbxCFXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">天猫国际</div></div><div view-name="DLinearLayout" aria-label="外卖" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 230.5px; margin-top: 13px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tps/TB1eXc7PFXXXXb4XpXXXXXXXXXX-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">外卖</div></div><div view-name="DLinearLayout" aria-label="天猫超市" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 304px; margin-top: 13px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB1IKqDtpooBKNjSZFPXXXa2XXa-183-144.png_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">天猫超市</div></div><div view-name="DLinearLayout" aria-label="充值中心" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 10px; margin-top: 84px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB1o0FLtyMnBKNjSZFoXXbOSFXa-183-144.png_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">充值中心</div></div><div view-name="DLinearLayout" aria-label="飞猪旅行" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 83.5px; margin-top: 84px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB15nKhtpkoBKNjSZFEXXbrEVXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">飞猪旅行</div></div><div view-name="DLinearLayout" aria-label="领金币" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 157px; margin-top: 84px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB1BqystrZnBKNjSZFrXXaRLFXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">领金币</div></div><div view-name="DLinearLayout" aria-label="拍卖" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 230.5px; margin-top: 84px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB1CMf4tlnTBKNjSZPfXXbf1XXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">拍卖</div></div><div view-name="DLinearLayout" aria-label="分类" style="display: flex; overflow: hidden; width: 61px; height: 67px; margin-left: 304px; margin-top: 84px; -webkit-box-orient: vertical; flex-direction: column; top: 0px; left: 0px; position: absolute;"><div view-name="HGifView" style="display: flex; overflow: hidden; width: 61px; height: 48px;"><div style="width: 100%; height: 100%; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(&quot;https://gw.alicdn.com/tfs/TB18P98tyQnBKNjSZFmXXcApVXa-183-144.png?getAvatar=1_.webp&quot;);"></div></div><div view-name="DTextView" style="display: inline-block; overflow: hidden; font-size: 11px; height: auto; margin-top: 5px; text-align: center; color: rgb(102, 102, 102); width: 61px; text-overflow: ellipsis; white-space: nowrap; line-height: 14px;">分类</div></div></div></div>'
    // 2 对象数组
    html:[
      {
        // 1 div标签 name属性来指定
        name:"div",
        // 2 标签上有哪些属性
        attrs:{
          // 标签上的属性 class  style
          class:"my_div",
          style:"color:red;"
        },
        // 3 子节点 children 要接收的数据类型和 nodes第二种渲染方式的数据类型一致 
        children:[
          {
            name:"p",
            attrs:{},
            // 放文本
            children:[
              {
                type:"text",
                text:"hello"
              }
            ]
          }
        ]
      }
    ]
  }
})
```



## 3.3 数据传递

通过数据绑定的方式进行数据传递

```
<!--wxml-->
<view> {{message}} </view>
```

```
// page.js
Page({
  data: {
    message: 'Hello MINA!'
  }
})
```

**注意事项：**

 	1.换括号和引号之前不能有空格

​        2.可以在括号内做简单的运算 列子：{{1+1}} ，{{false?1:2}}

## 3.4 wx:for

可以通过wx:for标签来遍历数组和对象

1. **遍历数组方式**

```
<!--wxml-->
 <view wx:for="{{list}}" wx:key="name"  wx:for-item="item" wx:for-index="index">
```

```
// page.js
Page({
  data: {
    list:[
        {"age":11,name:"sf1"},
        { "age": 11, name: "sf2" },
        { "age": 11, name: "sf3" }
      ]
  }
})
```

**2.遍历对象**

```
<view>
  <view wx:for="{{persion}}" wx:key="name"  wx:for-item="item" wx:for-index="index">
   name:{{index}} -- age:{{item}}
  </view>
</view>
```

```
 Page({ data: {
      persion:{
        age:14,
        height:11,
        weight:55,
        name:"张三"
      }
  }})
```

**遍历对象和数组是的区别：**

**1.数组**：

wx:for="{{数组}}" wx:for-item="循环内容" wx:for-index="循环项的索引"

**2.对象**

 wx:for="{{数组}}  wx:for-item="对象的值" wx:for-index="对象的属性"

在使用时如何过线嵌套循环：item，index不能重复

**wx:key 使用**

可以提高列表渲染时排序的效率

wx:key 的值以两种形式提供

1.字符串：代表for循环中array中的某个属性，该属性必须是列表中的唯一，且不能动态的改变

2.*this 代表for循环中的item本身，这种表示需要item本身是一个唯一的字符串或者数字，一般用于数组[1,2,4,5,6,7]

## 3.4 wx:if 

```
<!--wxml-->
<view wx:if="{{length >= 80}}"> 优秀 </view>
<view wx:elif="{{length >= 60}}"> 良好 </view>
<view wx:else> 加油 </view>
```

```
// page.js
Page({
  data: {
    length: '95'
  }
})
```

wx:if 和hidden 区别

wx:if 是否渲染  hidden是否隐藏，一般wx;if 会比 hidden 消耗更多的资源，如果频繁切换的情况下，使用hidden更好

## 3.5 自定义组件

可以将公用的部分抽取出来，达到复用的作用

### 3.5.1 创建组件步骤

![1581257973299](images/1581257973299.png)

创建好之后结构如下，目录结构与普通页面相同

![1581258651246](images/1581258651246.png)

### 3.5.1 申明组件

![1581258769749](images/1581258769749.png)



### 3.5.2 编写组件内容

**1.页面**

组件中可以像正常编写页面一样，组件中提供了slot 占位便签，使用了该标签后，可以在引用页面编写自己需要的内容

```
  <view class="tabs_content">
    <!-- 
      slot 标签 其实就是一个占位符 插槽
      等到 父组件调用 子组件的时候 再传递 标签过来 最终 这些被传递的标签
      就会替换 slot 插槽的位置 
     -->
    <slot></slot>
  </view>
```

注意：组件中不应该用id选择器，属性选择器，和标签名选择器 （这样容易对其他页面造成影响）

**2.js编写**

在组件的js文件中，需要使用Component（） 来注册组件，并提供组件的属性定义，内部数据和自定义方法。

```
// components/Tabs/Tabs.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
      tabs:{
        type:Array,
        value:[]
      }
  },
  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表 ,该方法可以在父页面中使用
   */
  methods: {
    hanlderItemTap(e){
       const {index}=e.currentTarget.dataset;
       this.triggerEvent("itemChange",{index});
     }
  }
})

```

### 3.5.3 页面使用组件

1.首先需要在使用页面的json中引入组件

![1581295253593](images/1581295253593.png)

2.引用成功后就可以在页面中使用组件了

![1581295328163](images/1581295328163.png)

### 3.5.4 组件属性

| 定义段     | 类型      | 是 否 必 填 | 描述                                                         |
| ---------- | --------- | ----------- | ------------------------------------------------------------ |
| properties | ObjectMap | 否          | 组件的对外属性，是属性名到属性设置的映射表，参⻅下⽂         |
| data       | Object    | 否          | 组件的内部数据，和 properties ⼀同⽤于组件的模板渲染         |
| observers  | Object    | 否          | 组件数据字段监听器，⽤于监听properties和data的变化，参⻅数据监听器 |
| methods    | Object    | 否          | 组件的⽅法，包括事件响应函数和任意的⾃定义⽅法，关于事件响应函数的使⽤，参⻅组件事件 |
| created    | Function  | 否          | 组件⽣命周期函数，在组件实例刚刚被创建时执⾏，注意此时不能调⽤ setData ，参⻅组件⽣命周期 |
| attached   | Function  | 否          | 组件⽣命周期函数，在组件实例进⼊⻚⾯节点树时执⾏，参⻅组件⽣命周期 |
| ready      | Function  | 否          | 组件⽣命周期函数，在组件布局完成后执⾏，参⻅组件⽣命周期     |
| moved      | Function  | 否          | 组件⽣命周期函数，在组件实例被移动到节点树另⼀个位置时执⾏，参⻅组件⽣命周期 |
| detached   | Function  | 否          | 组件⽣命周期函数，在组件实例被从⻚⾯节点树移除时执⾏，参⻅组件⽣命周期 |

### 3.5.5 组件与使用页面传值

**1.组件和页面之间如何传值**

![1581295781162](../../../AppData/Local/Temp/1581295781162.png)

页面中通过{{需要传的值}}这种方法可以将值传到组件中，组件使用以下方式进行接收

![1581295851092](images/1581295851092.png)

在properties中指定接收的参数类型，和参数的初始值。指定完成后就可以在组件的方法中使用了。

**2.组件中调用页面中的方法**

父组件

![1581296514567](images/1581296514567.png)

子页面

![1581296560161](images/1581296560161.png)

# 4.事件

## 4.1事件类型

常用的类型如下

| 类型      | 触发条件                                                     |
| --------- | ------------------------------------------------------------ |
| tap       | 手指触摸后马上离开                                           |
| longpress | 手指触摸后，超过350ms再离开，如果指定了事件回调函数并触发了这个事件，tap事件将不被触发 |

```
<!--wxml-->
<view data-index="自定义属性" bindtap="tapHandle"> 点我触发事件 </view>
```

```
// page.js
Page({
  tapHandle: function(event) {
     console.log( event.target.dataset.index );  // 输出标签自定义属性上的index值
  }
})
```

## 4.2 事件注意事项

1.绑定事件时不能带参数，以下为错误的写法

```
<input bindinput="handleInput(100)" />
```

2.事件的传值 通过标签自定义属性的范式和value

```
<input bindinput="handleInput" data-item="100" />
```

data- 为固定写法，后面的item自定义，设置之后值会在detail域中

3.事件触发时获取数据

```
handleInput: function(e) {
// {item:100}
console.log(e.currentTarget.dataset)
// 输入框的值
console.log(e.detail.value);
}
```

## 4.3 Promise

<<http://es6.ruanyifeng.com/#docs/promise>>

# 5.尺寸转换

**rpx是小程序中的尺寸单位，它有以下特征：**

小程序的屏幕宽固定为750rpx（即750个物理像素），在所有设备上都是如此
1rpx=（screenWidth / 750）px，其中screenWidth为手机屏幕的实际的宽度（单位px），

例如iphone6的screenWidth=375px，则在iphone6中1rpx=0.5px
由上可知，在不同设备上rpx与px的转换是不相同的，但是宽度的rpx却是固定的，所以可以使用rpx作为单位，来设置布局的宽高


```
  //一般设置图片的时候，都是height或者width固定一个，另一个100%，这样可以起到按比例进行拉伸
  /*
    先找出来 原图的宽度和高度 等比例 给swiper 定 宽度和高度
    原图的宽度和高度  1125 * 352 px
    swiper 宽度 / swiper  高度 =  原图的宽度  /  原图的高度
    swiper  高度  =  swiper 宽度 *  原图的高度 / 原图的宽度
    height: 100vw * 352 /  1125
  */
  width: 100%;
  height: calc(100vw * 352 /  1125);  
  //height: 31.28vw;
```

# 6.基础概念

## **1.单位**

- 小程序中，窗口宽度固定为100vw，将窗口宽度平均分成100份，1份是1vw
- 小程序中，窗口高度固定为100vh ，将窗口高度平均分成100份，1份是1vh

## **2.常用的一些标签**

### 2.1 display

具体效果可参考<https://www.runoob.com/css3/css3-flexbox.html>

 display: flex; 弹性盒子，该属性为css3新属性，弹性子元素通常在弹性盒子内一行显示，默认每个容器只有一行。

#### 2.1.1 flex

flex属性用于设置或检索弹性盒模型对象的子元素如何分配空间。

flex属性是flex-grow，flex-shrink和flex-basis属性的简写属性。

如果设置为-> flex:1 等比例分

### **2.2.justify-content标签**

 内容对齐（justify-content）属性应用在弹性容器上，把弹性项沿着弹性容器的主轴线（main axis）对齐。

各个值解析:

- flex-start：

  弹性项目向行头紧挨着填充。这个是默认值。第一个弹性项的main-start外边距边线被放置在该行的main-start边线，而后续弹性项依次平齐摆放。

- flex-end：

  弹性项目向行尾紧挨着填充。第一个弹性项的main-end外边距边线被放置在该行的main-end边线，而后续弹性项依次平齐摆放。

- center：

  弹性项目居中紧挨着填充。（如果剩余的自由空间是负的，则弹性项目将在两个方向上同时溢出）。

- space-between：

  弹性项目平均分布在该行上。如果剩余空间为负或者只有一个弹性项，则该值等同于flex-start。否则，第1个弹性项的外边距和行的main-start边线对齐，而最后1个弹性项的外边距和行的main-end边线对齐，然后剩余的弹性项分布在该行上，相邻项目的间隔相等。

- space-around：

  弹性项目平均分布在该行上，两边留有一半的间隔空间。如果剩余空间为负或者只有一个弹性项，则该值等同于center。否则，弹性项目沿该行分布，且彼此间隔相等（比如是20px），同时首尾两边和弹性容器之间留有一半的间隔（1/2*20px=10px）。

![1581345714913](images/1581345714913.png)

### 2.3 box-sizing

自动计算盒子padding，border边框

## 3 其他一些概念

### 1.var、let、const用法与区别

**let：**

​	ES6 新增了let命令，用来声明变量。它的用法类似于var，但是所声明的变量，只		在let命令所在的代码块内有效。

**const**：

​	const声明一个只读的常量。一旦声明，常量的值就不能改变

**var:**

​	全局变量