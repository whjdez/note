package com.whj.opencv.service;



import org.opencv.core.CvType;
import org.opencv.core.Mat;


import java.net.URL;

import static org.opencv.highgui.HighGui.imshow;
import static org.opencv.highgui.HighGui.waitKey;
import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2GRAY;
import static org.opencv.imgproc.Imgproc.cvtColor;


/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/25 11:09
 * @updateDate 2020/12/25 11:09
 **/
public class Test {

    public static void main(String[] args) {
        // 解决awt报错问题
        System.setProperty("java.awt.headless", "false");
        System.out.println(System.getProperty("java.library.path"));
        // 加载动态库
        URL url = ClassLoader.getSystemResource("lib/opencv/opencv_java3413.dll");
        System.load(url.getPath());
        // 读取图像
        Mat image = imread("D:\\images\\hello.jpg");
        if (image.empty()) {
            try {
                throw new Exception("image is empty");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        imshow("Original Image", image);

        // 创建输出单通道图像
        Mat grayImage = new Mat(image.rows(), image.cols(), CvType.CV_8SC1);
        // 进行图像色彩空间转换
        cvtColor(image, grayImage, COLOR_RGB2GRAY);

        imshow("Processed Image", grayImage);
        imwrite("D:\\hello.jpg", grayImage);
        waitKey();

    }
}
