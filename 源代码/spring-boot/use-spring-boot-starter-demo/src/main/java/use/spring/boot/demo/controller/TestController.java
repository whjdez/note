package use.spring.boot.demo.controller;

import com.whj.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/1 13:15
 * @updateDate 2020/12/1 13:15
 **/
@RestController
public class TestController {

    @Autowired
    private ServiceUtil  serviceUtil;

    /**
     * 获取配置信息
     * @return
     */
    @RequestMapping("test")
    public String test(){

        return serviceUtil.getPro();

    }
}
