package com.whj;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/1 11:55
 * @updateDate 2020/12/1 11:55
 **/
@Configuration
@EnableConfigurationProperties(AutoConfigruationProperties.class)
public class AutoConfigrutionClass  {
    /**
     * 生成工具类
     * @return
     */
    @Bean
    public ServiceUtil serviceUtil() {
        return new ServiceUtil();
    }
}
