package com.whj;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/1 12:25
 * @updateDate 2020/12/1 12:25
 **/
@Component
public class ServiceUtil {

    @Autowired
    private AutoConfigruationProperties autoConfigruationProperties;

    /**
     * 获取了配置信息
     * @return
     */
    public String getPro(){

        return "姓名："+autoConfigruationProperties.getUserName()+"年龄："+autoConfigruationProperties.getAge();
    }

}
