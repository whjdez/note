package com.whj;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/1 13:39
 * @updateDate 2020/12/1 13:39
 **/
public class MyImport implements ImportSelector {
    /**
     * 导入类
     * @param annotationMetadata
     * @return
     */
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{AutoConfigrutionClass.class.getName()};
    }
}
