package com.whj;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * 自动配置类
 *
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/1 11:54
 * @updateDate 2020/12/1 11:54
 **/
@ConfigurationProperties(prefix = "com.whj")
public class AutoConfigruationProperties {

    private String userName;

    private String age;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
