package com.whj.nio.v1.client;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class NioClient {

    public static void main(String[] args) {
        new Thread(new NioClientHandler("localhost", 8099), "nioClient-001").start();
    }
}
