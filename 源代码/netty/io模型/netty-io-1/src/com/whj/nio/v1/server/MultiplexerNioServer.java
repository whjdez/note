package com.whj.nio.v1.server;

import com.sun.org.apache.bcel.internal.generic.Select;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class MultiplexerNioServer implements Runnable{


    //通道
    private ServerSocketChannel serverSocketChannel;

    //多路复用器，事件轮训器
    private Selector selector;
    private volatile boolean stop=false;

    /**
     *
     * @param port
     */
    public MultiplexerNioServer(int port){
        try {
            //获得一个ServerChannel
            serverSocketChannel=ServerSocketChannel.open();
            //获取一个多路复用器
            selector=Selector.open();
            //设置为阻塞（nio可以设置阻塞或非阻塞）
            serverSocketChannel.configureBlocking(false);
            //绑定一个端口和等待队列长度
            serverSocketChannel.socket().bind(new InetSocketAddress(port),1024);
            //把selector注册到channel，关注注册事件
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void run() {
        while (!stop){
            try{
                //遍历各种事件，如果没有事件发生 则当前线程会阻塞
                //不设置超时事件为线程阻塞，但是IO上支持多个文件描述符就绪
                int client=selector.select();
                if(client==0){
                    continue;
                }
                //获取连接事件 相对于epoll返回的结果
                Set<SelectionKey> selectionKeys=selector.selectedKeys();
                Iterator<SelectionKey> it=selectionKeys.iterator();
                SelectionKey key=null;
                while (it.hasNext()){
                    key=it.next();
                    it.remove();
                    try{
                        handle(key);
                    }catch (Exception e){
                        if(key!=null){
                            key.channel();
                            if(key.channel()!=null){
                                key.channel().close();
                            }
                        }
                    }
                }


            }catch (Exception e){

            }finally {

            }
        }
    }

    private void handle(SelectionKey key) throws IOException {
        //判断是否有效
        if(key.isValid()){
            //连接事件 每一次接收客户端都会先触发
            if(key.isAcceptable()){
                //获取连接事件
                ServerSocketChannel ssc=(ServerSocketChannel)key.channel();
                //3次握手
                SocketChannel sc=ssc.accept();
                sc.configureBlocking(false);
                //建立连接后关注读事件
                System.out.println("注册读事件s=======");
                sc.register(selector,SelectionKey.OP_READ);
            }
            //读事件
            if (key.isReadable()) {
                SocketChannel sc=(SocketChannel)key.channel();
                //设置读入事件
                ByteBuffer readbuffer=ByteBuffer.allocate(1024);
                int readBytes=sc.read(readbuffer);
                if(readBytes>0){
                    //读写模式反转
                    readbuffer.flip();
                    //设置读入大小
                    byte[]bytes=new byte[readbuffer.remaining()];
                    readbuffer.get(bytes);
                    String body=new String(bytes,"utf-8");
                    System.out.println(" input is "+body);

                    res(sc,body);
                }
            }
        }
    }

    /**
     * 返回
     * @param channel
     * @param response
     * @throws IOException
     */
    private void res(SocketChannel channel, String response) throws IOException {
        if(response!=null&&response.length()>0){
            byte[]bytes=response.getBytes();
            ByteBuffer writeBuffer=ByteBuffer.allocate(bytes.length);
            writeBuffer.put(bytes);
            writeBuffer.flip();
            channel.write(writeBuffer);
            System.out.println("res end");
        }
    }
}
