package com.whj.nio.v1.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class Server123 {
    static int port = 10001;
    static Selector selector = null;
    static ServerSocketChannel ssc = null;

    public static void main(String[] args) throws IOException {

        ssc = ServerSocketChannel.open();
        //是否为阻塞线程
        ssc.configureBlocking(false);
        ssc.bind(new InetSocketAddress(port));
        selector = Selector.open();
        ssc.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("===服务开始监听====");
        handler();
    }

    public static void handler() {
        while (true){
            try {
                int event=selector.select();
                if(event>0){
                    Iterator<SelectionKey> selectionKeys =selector.selectedKeys().iterator();
                    while (selectionKeys.hasNext()){
                        SelectionKey key=selectionKeys.next();
                        selectionKeys.remove();;
                        //if(key.isAcceptable()){
                            accept(key);
                        //}
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     *处理请求
     * @param key
     */
    private static void accept(SelectionKey key) throws IOException {
        ServerSocketChannel server=null;
        SocketChannel client=null;
        String receiveText;
        String sendText;
        //测试该通道是否已经准备好接受新的套接字连接
        if(key.isAcceptable()){
            //返回此键的通道
            server=(ServerSocketChannel)key.channel();
            //接受此通道套接字连接
            //返回套接字通道（如果有）将处于阻塞模式
            client=server.accept();
            //配置为非阻塞
            client.configureBlocking(false);
            //注册到selector 等待连接
            client.register(selector,SelectionKey.OP_READ);
        }else if(key.isReadable()){
          //
            client=(SocketChannel) key.channel();
          //
        }

    }
}
