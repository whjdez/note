package com.whj.nio.v1.server;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class NioServer {

    public static void main(String[] args) {
        int port=8099;
        MultiplexerNioServer nioServer=new MultiplexerNioServer(8099);
        new Thread(nioServer,"nioServer-001").start();
    }
}
