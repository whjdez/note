package com.whj.blocking.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 工具类
 */
public class Utils {

    /**
     * 读取返回值
     *
     * @param in
     * @return
     * @throws IOException
     */
    public static String read(InputStream in) throws IOException {
        int length=0;
        byte[] by=new byte[1024];
        StringBuffer bf=new StringBuffer();
        while ((length=in.read(by))!=-1){
            bf.append(new String(by));
        }
        //in.close();
        return bf.toString();
    }

    public static void out(OutputStream out,String msg) throws IOException {
        out.write(msg.getBytes());
    }
}
