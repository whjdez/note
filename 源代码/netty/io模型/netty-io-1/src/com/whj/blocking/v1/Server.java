package com.whj.blocking.v1;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author w
 *
 * 同步阻塞io，当处于处理状态时 ，不能处理其他请求
 */
public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(10001);
        System.out.println("启动服务"+serverSocket.getLocalPort());
        Socket socket = serverSocket.accept();
        InputStream in = socket.getInputStream();
        int length = 0;
        byte[] by = new byte[1024];
        StringBuilder str = new StringBuilder();
        while ((length = in.read(by)) != -1) {
            System.out.println(new String(by));
        }
        System.out.println(str.toString());
        in.close();
        socket.close();
    }
}
