package com.whj.blocking.v2;

import com.whj.blocking.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Handler;

/**
 * 服务器
 * @author w
 */
public class Server {

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        ServerSocket serverSocket = server.startServerSocket(10001);
        while (true) {
            Socket socket = serverSocket.accept();
            new Thread(new Handle(socket)).start();
        }
    }


    /**
     * 启动serverSocket
     *
     * @param port
     * @return
     * @throws IOException
     */
    public ServerSocket startServerSocket(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        return serverSocket;
    }

}

class Handle implements Runnable {
    /**
     * socket
     */
    private Socket socket;

    private InputStream in;

    private OutputStream out;

    /**
     * 构造方法
     *
     * @param socket
     */
    public Handle(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            in = socket.getInputStream();
            String msg = Utils.read(in);
            System.out.println(msg + "=======后台收到消息了==========");
            if (msg != null && "end".equals(msg)) {
                in.close();
                out.close();
                socket.close();
            }
            out = socket.getOutputStream();
            out.write("我收到消息了:".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
