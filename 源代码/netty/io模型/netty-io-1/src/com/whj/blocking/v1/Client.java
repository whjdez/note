package com.whj.blocking.v1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author w
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket=new Socket("localhost", 10001);
        OutputStream out=socket.getOutputStream();
        out.write("客户端==》发送了一个消息".getBytes());
        out.flush();
        out.close();
    }
}
