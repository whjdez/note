package com.whj.blocking.v2;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * 客户端
 * @author w
 */
public class Client {
    private Socket socket;

    public static void main(String[] args) throws IOException {
        Client client=new Client();
        client.start("127.0.0.1",10001);
        OutputStream  out = client.socket.getOutputStream();
    }
    /**
     * 启动客户端
     * @param ip
     * @param port
     * @throws IOException
     */
    public void start(String ip ,int port) throws IOException {
            socket=new Socket(ip,port);
    }
}
