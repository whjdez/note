package whj.thread.interrupte;

import java.util.concurrent.locks.LockSupport;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class ParkTest {
    public static void main(String[] args) throws InterruptedException {
       Thread t1=new Thread(()->{
            System.out.println("======1==========");
            if(Thread.currentThread().isInterrupted()){
                System.out.println("唤醒了===========");
            }
           LockSupport.park();
           System.out.println("======2==========");
        });

        t1.start();;
        Thread.sleep(5000);
        LockSupport.unpark(t1);
        //也可将park阻塞的唤醒
        // t1.interrupt();
    }
}
