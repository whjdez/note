package whj.thread.create.Executor.test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ExecutorServiceUtil {
    private static final int corePoolSize = 5;
    private static final int maximumPoolSize = 50;
    private static final int keepAliveTime = 1000;
    private static final BlockingQueue<Runnable> queue
            = new LinkedBlockingQueue<>
            (1000);

    //private static final ThreadFactory factory = new CustomThreadFactoryBuilder().setDaemon(false).setNamePrefix("这是线程：").build();
    private static volatile ExecutorService executor;
// =new ThreadPoolExecutor
//                                    (corePoolSize, maximumPoolSize, keepAliveTime,
//                                            TimeUnit.SECONDS,queue,factory,new  ThreadPoolExecutor.AbortPolicy());;

    private ExecutorServiceUtil() {

    }

    public static ExecutorService instanceExcutorService() {
        ThreadFactory factory = new CustomThreadFactoryBuilder().setDaemon(false).setNamePrefix("这是线程：").build();


        if (executor == null) {
            synchronized (ExecutorServiceUtil.class) {
                if (executor == null) {
                    executor =
                            new ThreadPoolExecutor
                                    (corePoolSize, maximumPoolSize, keepAliveTime,
                                            TimeUnit.SECONDS, queue, new NamedThreadFactory("这是线程："));
                }
            }
        }

        return executor;
    }

    public static void shutdown() {
        if (executor != null) {
            executor.shutdown();
        }
    }

    private static class CustomThreadFactoryBuilder {

        //线程名称
        private String namePrefix = null;

        //是否是守护线程
        private boolean daemon = false;
        //线程优先度
        private int priority = Thread.NORM_PRIORITY;


        public CustomThreadFactoryBuilder setNamePrefix(String namePrefix) {
            if (namePrefix == null) {
                throw new NullPointerException();
            }
            this.namePrefix = namePrefix;
            return this;
        }

        public CustomThreadFactoryBuilder setPriority(int priority) {
            if (priority < Thread.MIN_PRIORITY) {
                throw new IllegalArgumentException(String.format("Thread priority (%s) must be >= %s", priority, Thread.MIN_PRIORITY));
            }

            if (priority > Thread.MAX_PRIORITY) {
                throw new IllegalArgumentException(String.format("Thread priority (%s) must be >= %s", priority, Thread.MAX_PRIORITY));
            }
            this.priority = priority;
            return this;
        }

        public CustomThreadFactoryBuilder setDaemon(boolean daemon) {
            this.daemon = daemon;
            return this;
        }

        public ThreadFactory build() {
            return build(this);
        }

        private ThreadFactory build(CustomThreadFactoryBuilder builder) {
            final String namePrefix = builder.namePrefix;
            final Boolean daemon = builder.daemon;
            final Integer priority = builder.priority;
            final AtomicLong count = new AtomicLong(0);

            return (runnable) -> {
                Thread thread = new Thread(runnable);
                if (namePrefix != null) {
                    thread.setName(namePrefix + "-" + count.getAndIncrement());
                }
                if (daemon != null) {
                    thread.setDaemon(daemon);
                }
                return thread;
            };
        }
    }

}
