package whj.thread.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/9/27 10:45
 * @updateDate 2020/9/27 10:45
 **/
public class LockTest1 {

    public static void main(String[] args) {
        ReentrantLock lock=new ReentrantLock();
        for (int i = 0; i < 3 ; i++) {
            new Thread(
                    ()-> {
                        System.out.println(Thread.currentThread().getName() + "  进入了该方法");
                        lock.lock();
                        try {
                            System.out.println("处理业务方法");
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            lock.unlock();
                        }
                    }
            ).start();

        }
    }
}

