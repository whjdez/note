package whj.thread.create.Executor.test;


class SingleTon {
    private static SingleTon singleTon = new SingleTon();
    public static int count1;
    public static int count2 = 99;

    public SingleTon() {
        System.out.println("========SingleTon执行了=============="+count2);
        count1++;
        count2++;
    }

    public static SingleTon getInstance() {
        System.out.println("===========getInstance==============");
        return singleTon;
    }
}

public class Test {
    public static void main(String[] args) {
        SingleTon singleTon = SingleTon.getInstance();
        System.out.println("count1=" + SingleTon.count1);
        System.out.println("count2=" + SingleTon.count2);
    }

}