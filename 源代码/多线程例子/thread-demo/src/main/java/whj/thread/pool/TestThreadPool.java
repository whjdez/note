package whj.thread.pool;

import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TestThreadPool {



    private BlockingQueue<Runnable> queue=new ArrayBlockingQueue<Runnable>(10);

    private HashSet<Working> work=new HashSet<Working>();

    private  volatile  boolean isDown=true;

    private  int coolSize;

    public TestThreadPool(int size){
        this.coolSize =size;
        init(size);
    }

    private void init(int size) {
        for(int i=0;i<size;i++){
            Working working=new Working(queue);
            work.add(working);
        }
    }

    public void execut(Runnable run){
        queue.add(run);
    }

    class Working{
        public Working(BlockingQueue queue){
            init(queue);
        }
        private void init(final BlockingQueue <Runnable>queue) {
            new Thread(()-> {
                    while (isDown){
                        Runnable runnable= null;

                        try {
                            runnable = queue.take();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runnable.run();
                        System.out.println("当前线程是=》"+Thread.currentThread().getName());
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
            }).start();
        }

    }

    public static void main(String[] args) {
        TestThreadPool pool=new TestThreadPool(5);


        for (int i=0;i<5;i++){
            RunTest t=new RunTest();
            pool.execut(t);
        }
    }


}
