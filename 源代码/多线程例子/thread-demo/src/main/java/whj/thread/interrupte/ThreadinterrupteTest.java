package whj.thread.interrupte;

/**
 * 功能描述
 *
 * isInterrupted 的作用：
 *
 * 多线程之间进行通讯，可以通过标识位中断线程进行通讯，中断触发后会 唤醒 seelp，wait，park 使用时要注意
 *
 *
 * @author WangHaiJing
 * @date
 */
public class ThreadinterrupteTest {

    private static int i;
    public static void main(String[] args) {
        Thread thread=new Thread(()->{
           while (true){
               i++;
               System.out.println(i);
               try {
                   Thread.sleep(5000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
//               //判断标识位 执行完成后会清除该标识位,方法体内方法只会执行一次
//               if(Thread.interrupted()){
//                   System.out.println("================");
//                   //break;
//               }

               //判断标识位，执行完成后不会清除标识位，方法体内的方法会多次执行
               if(Thread.currentThread().isInterrupted()){
                   System.out.println("=========");
               }
           }
        });
        thread.start();
        //只是修改中断标识为true，并不会真正的中断线程，如果在线程内没有Thread.currentThread().isInterrupted() 方法进行判断处理，则没有效果
        thread.interrupt();
    }
}
