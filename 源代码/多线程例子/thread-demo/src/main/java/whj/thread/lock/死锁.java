package whj.thread.lock;

public class 死锁 {

    private Object ob=new Object();
    public static void main(String[] args) {
        final 死锁 t=new 死锁();
        new Thread(){
            @Override
            public void run() {
                try {
                    t.send1();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        new Thread(){
            @Override
            public void run() {
                    t.send2();
            }
        }.start();
    }
    public synchronized  void send1() throws InterruptedException {
        System.out.println("======进入send方法==========");
        Thread.sleep(1000);
        synchronized (ob){
            System.out.println("send1====阻塞=============");
        }
    }

    public void send2(){
        synchronized (ob){
            System.out.println("进入send2方法");
            synchronized (this){
                System.out.println("send2====阻塞=============");
            }
        }
    }
}
