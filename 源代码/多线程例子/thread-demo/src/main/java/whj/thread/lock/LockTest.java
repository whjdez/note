package whj.thread.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockTest {



    static class Product{
        String name;
    }

    public static void main(String[] args) {

      //  ThreadPoolExecutor


        final  Lock lock=new ReentrantLock();
        final Condition a=lock.newCondition();
        final Condition b=lock.newCondition();
        final List <Product>list=new ArrayList();
        new Thread(()-> {
                int i=0;
                while (true){
                    lock.lock();
                    if(list.size()==0){
                        Product p=new Product();
                        p.name=i+"";
                        i++;
                        list.add(p);
                        System.out.println( "生成->"+ list.get(0).name);
                        b.signal();
                    }
                    try {
                        a.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    lock.unlock();
                }
        }).start();


        new Thread(()-> {
                while (true){
                    lock.lock();

                    if(list.size()>0){
                        System.out.println( "消费->"+ list.get(0).name);
                        list.remove(0);
                        a.signal();
                    }
                    try {
                        b.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    lock.unlock();
                }
        }).start();
    }

}
