package whj.thread.create.Executor;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 参数解释
 * <p>
 * <p>
 * corePoolSize
 * 核心线程数：当新增一个请求时，如果线程池中的线程数不大于该值，无论是否空闲都会新增一个线程来处理
 * <p>
 * maximumPoolSize
 * 最大线程数：当请求线程数大于corePoolSize 时，并且 队列满了，线程池中的线程数小于该参数时，会创建新的线程来处理
 * <p>
 * keepAliveTime
 * 当线程池中处理空闲状态时，大于coreSize的线程就会被回收
 * TimeUnit
 * 时间单位
 * BlockingQueue
 * 大于coreSize时存线程的队列，如果该队列是无界队列可能会让maxnumPoolSize失效，失败策略也不起作用
 * <p>
 * ThreadFactory
 * 线程工厂：因为线程池使用的是池中的线程来处理任务，在单个任务中设置的一部分值是不起作用的（比如线程名称，线程的优先级，是否守护线程）
 * 可以在工厂内设置线程名称，线程的优先级，和是否守护线程
 * <p>
 * RejectedExecutionHandler
 * 拒绝策略：当线程池和队列满后会使用该策略，来处理新的请求
 * 系统中默认有四种拒绝策略，这四种拒绝策略被封装在ThreadPoolExecutor中，
 * AbortPolicy：该策略是线程池中的默认策略。使用该策略时，如果线程池队列满了丢掉这个任务并且抛出RejectedExecutionException异常
 * DiscardPolicy：这个策略和AbortPolicy的slient版本，如果线程池队列满了，会直接丢掉这个任务并且不会有任何异常
 * DiscardOldestPolicy：这个策略从字面上也很好理解，丢弃最老的。也就是说如果队列满了，会将最早进入队列的任务删掉腾出空间，再尝试加入队列。
 * 因为队列是队尾进，队头出，所以队头元素是最老的，因此每次都是移除对头元素后再尝试入队。
 * CallerRunsPolicy：使用此策略，如果添加到线程池失败，那么主线程会自己去执行该任务，不会等待线程池中的线程去执行。就像是个急脾气的人，我等不到别人来做这件事就干脆自己干。
 * 、
 * <p>
 * <p>
 * 执行时有以下两种方式：
 * executor.submit  ：如果是CallAble  可获取返回信息 Future<?> ,如果是其他类型runable，和thread 则放回null，内部实现了FutrueTask，如果是非callable 不建议使用
 * executor.execute ： 没有返回值
 *
 * @author w
 */


public class ExecutorServiceDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int corePoolSize = Runtime.getRuntime().availableProcessors() * 2;
        //核心线程数
        int maximumPoolSize = 1;
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue(2);
        ThreadFactory threadFactory = new NameTreadFactory();
        RejectedExecutionHandler handler = new MyIgnorePolicy();

        //ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,1000,TimeUnit.MILLISECONDS,queue,threadFactory,handler);

        //默认的 线程工厂
        Executors.defaultThreadFactory();
        //自己创建的线程工厂
        CustomThreadFactoryBuilder customThreadFactoryBuilder;

        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                1000,
                TimeUnit.MILLISECONDS,
                queue,
                new CustomThreadFactoryBuilder().
                        setNamePrefix("我的线程").
                        setDaemon(false).
                        setPriority(1).
                        build(), new ThreadPoolExecutor.
                AbortPolicy());
        for (int i = 0; i < 10; i++) {
            executor.execute(new ThreadDeom());
            Future f = executor.submit(new ThreadDeom());
            System.out.println("  future => " + f.get());
            System.out.println();
        }

        executor.shutdown();
    }
}

class NameTreadFactory implements ThreadFactory {
    private final AtomicInteger mThreadNum = new AtomicInteger(1);


    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, "my-thread-" + mThreadNum.getAndIncrement());
        System.out.println(t.getName() + " has been created");
        return t;
    }
}

class MyIgnorePolicy implements RejectedExecutionHandler {

    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {

        System.out.println(r.toString() + " rejected");
    }
}

class ThreadDeom implements Runnable {

    public ThreadDeom() {
        Thread.currentThread().setName("线程名称666");
    }

    @Override
    public void run() {
        System.out.println("线程执行了-----------------" + Thread.currentThread().getName());
    }
}


class CustomThreadFactoryBuilder {

    private String namePrefix = null;
    private boolean daemon = false;
    private int priority = Thread.NORM_PRIORITY;

    public CustomThreadFactoryBuilder setNamePrefix(String namePrefix) {
        if (namePrefix == null) {
            throw new NullPointerException();
        }
        this.namePrefix = namePrefix;
        return this;
    }

    public CustomThreadFactoryBuilder setDaemon(boolean daemon) {
        this.daemon = daemon;
        return this;
    }

    public CustomThreadFactoryBuilder setPriority(int priority) {
        if (priority < Thread.MIN_PRIORITY) {
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be >= %s", priority, Thread.MIN_PRIORITY));
        }

        if (priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be <= %s", priority, Thread.MAX_PRIORITY));
        }

        this.priority = priority;
        return this;
    }

    public ThreadFactory build() {
        return build(this);
    }

    private static ThreadFactory build(CustomThreadFactoryBuilder builder) {
        final String namePrefix = builder.namePrefix;
        final Boolean daemon = builder.daemon;
        final Integer priority = builder.priority;
        final AtomicLong count = new AtomicLong(0);
         /*
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable);
                if (namePrefix != null) {
                    thread.setName(namePrefix + "-" + count.getAndIncrement());
                }
                if (daemon != null) {
                    thread.setDaemon(daemon);
                }
                if (priority != null) {
                    thread.setPriority(priority);
                }
                return thread;
            }
        };*/
        //jdk8中还是优先使用lamb表达式
        return (Runnable runnable) -> {
            Thread thread = new Thread(runnable);
            if (namePrefix != null) {
                thread.setName(namePrefix + "-" + count.getAndIncrement());
            }
            if (daemon != null) {
                thread.setDaemon(daemon);
            }
//                /*
//                    thread.setPriority(priority);
//                */
            return thread;
        };
    }
}
