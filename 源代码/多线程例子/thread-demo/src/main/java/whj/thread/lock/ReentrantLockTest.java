package whj.thread.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {
    public static void main(String[] args) {
        ReentrantLock transt = new ReentrantLock();
        Condition c= transt.newCondition();
        Condition d= transt.newCondition();
        for (int i=0;i<10;i++){
            X t=new X(transt,i,c);
            Thread thread = new Thread(t);
            thread.start();

        }

        Z y=new Z(transt,0,c);
        Thread thread1 = new Thread(y);
        thread1.start();
    }
}

class X implements Runnable{

    private ReentrantLock t=null;
    Condition condition;
    private int i;
    public X(ReentrantLock t,int i,Condition condition){
        this.t = t;
        this.i=i;
        this.condition=condition;
    }
    @Override
    public void run() {
        System.out.println("进入X方式------》"+i);
        t.lock();
        try {
            System.out.println("进入X方式synchronized------》"+i);
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("X 被阻塞了 wait------》"+i);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            t.unlock();
        }


        System.out.println("X 出了synchronized------》"+i);
    }
}

class Z implements Runnable {

    private ReentrantLock t=null;
    private ReentrantLock locklock=new ReentrantLock();
    private int i;
    Condition c;
    public Z(ReentrantLock t,int i, Condition c){
        this.t = t;
        this.i=i;
        this.c=c;
    }
    @Override
    public void run() {
        System.out.println("进入Z方式------》"+i);
        locklock.lock();
        try {
            System.out.println("进入Z方式synchronized------》"+i);
            c.signalAll();
            System.out.println("Z 被阻塞了 wait------》"+i);
        } finally {
            locklock.unlock();
        }

        System.out.println("Y 出了synchronized------》"+i);
    }
}