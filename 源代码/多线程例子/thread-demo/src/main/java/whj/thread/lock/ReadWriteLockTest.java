package whj.thread.lock;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLockTest {

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void test(){
        //lock.writeLock().newCondition().await();
        lock.writeLock().newCondition().signal();
        lock.writeLock();
        lock.readLock();
    }
}
