package whj.thread.pool;

import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TestThreadPool1 {
    private volatile  boolean isShotDown=false;

    private BlockingQueue<Runnable> blockingQueue=null;

    private HashSet<Working> workings=new HashSet<Working>();

    private int count;

    private int coreSize;


    public TestThreadPool1(int coreSize){
        this.coreSize=coreSize;
        count=0;
        blockingQueue=new LinkedBlockingQueue<Runnable>();
    }


    public void  exeute(Runnable run){
        blockingQueue.add(run);
        if(count<=coreSize){
            this.Consume();
        }
    }

    public void ShotDown(){
        isShotDown=true;
    }
    public void Consume(){
        new Thread(new Working()).start();
        count++;
    }
    class Working implements Runnable{
        public void run() {
            while (!isShotDown){
                System.out.println(Thread.currentThread().getName()+"=====执行》");
                Runnable run= null;
                try {
                    run = blockingQueue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                run.run();
            }
            System.out.println(Thread.currentThread().getName()+"=====退出》");
        }
    }

    public static void main(String[] args) {

        TestThreadPool1 pool=new TestThreadPool1(10);
        for (int i=0;i<100;i++){
            pool.exeute( new RunTest());
        }
        pool.ShotDown();
    }
}
