package whj.thread.lock;

/**
 * synchronized 使用时需要使用对象当作锁。
 * 锁可以时任意对象，也可以是类如：Object.class
 *
 *
 */
public class SynchronizedDemoTest {


    public static void main(String[] args) {
        SynchronizedDemoTest test = new SynchronizedDemoTest();

        for (int i=0;i<10;i++){
            T t=new T(test,i);
            Thread thread = new Thread(t);
            thread.start();
        }

        Y y=new Y(test,0);
        Thread thread1 = new Thread(y);
        thread1.start();
    }
}

class T implements Runnable{

    private SynchronizedDemoTest t=null;
    private int i;
    public T(SynchronizedDemoTest t, int i){
        this.t = t;
        this.i=i;
    }
    @Override
    public void run() {
        System.out.println("进入T方式------》"+i);
        synchronized (SynchronizedDemoTest.class){
            System.out.println("进入T方式synchronized------》"+i);

            try {
               // t.wait();
                SynchronizedDemoTest.class.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("T 被阻塞了 wait------》"+i);
        }
        System.out.println("T 出了synchronized------》"+i);
    }
}

class Y implements Runnable {

    private SynchronizedDemoTest t=null;
    private int i;
    public Y(SynchronizedDemoTest t, int i){
        this.t = t;
        this.i=i;
    }
    @Override
    public void run() {
        System.out.println("进入Y方式------》"+i);
        synchronized (SynchronizedDemoTest.class){
            System.out.println("进入Y方式synchronized------》"+i);
         try {
                //t.wait();
//                t.notifyAll();
             SynchronizedDemoTest.class.notifyAll();
             SynchronizedDemoTest.class.wait();

            } catch (InterruptedException e) {
               e.printStackTrace();
           }
            System.out.println("Y 被阻塞了 wait------》"+i);
        }
        System.out.println("Y 出了synchronized------》"+i);
    }
}