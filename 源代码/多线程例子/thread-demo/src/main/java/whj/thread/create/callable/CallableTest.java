package whj.thread.create.callable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CallableTest {
    public static void main(String[] args) {

        // char []a=new char[];
        CallableDemo r1=new CallableDemo("一",new Date());
        CallableDemo r2=new CallableDemo("二",new Date());
        //1.执行 Callable 方式，需要 FutureTask 实现类的支持，用于接收运算结果。
        //可以中断获取执行状态太
        FutureTask<String> result = new FutureTask<String>(r1);
        new Thread(result).start();
        //.接收线程运算后的结果

        String sum = null;  //FutureTask 可用于 闭锁 类似于CountDownLatch的作用，在所有的线程没有执行完成之后这里是不会执行的
        try {
            //result.isDone() //是否执行完成
            //result.cancel(true); //中断运行如果子任务已经开始执行了，但是还没有执行结束，根据mayInterruptIfRunning的值，如果mayInterruptIfRunning = true，那么会中断执行任务的线程，然后返回true，如果参数为false，会返回true，不会中断执行任务的线程
            sum = result.get();
            System.out.println(sum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

            System.out.println("------------------------------------");

    }

}

/*
 * 一、创建执行线程的方式三：实现 Callable 接口。 相较于实现 Runnable 接口的方式，方法可以有返回值，并且可以抛出异常。
 *
 * 二、执行 Callable 方式，需要 FutureTask 实现类的支持，用于接收运算结果。  FutureTask 是  Future 接口的实现类
 */
class  CallableDemo implements Callable{

    private String name;
    private Date date;

    public CallableDemo(String name,Date date) {
        this.date=date;
        this.name=name;
    }

    @Override
    public String call() throws Exception {
        Thread.sleep(10000);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
        return "传入时间"+ sdf.format(date)+"  。线程名称："+name;
    }
}
