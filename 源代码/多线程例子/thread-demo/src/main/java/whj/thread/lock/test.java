package whj.thread.lock;

/**
 *  锁是针对对象来使用的，一个对象就是一个锁，多个线程同时操作一个对象时锁才会有用
 */
public class test {
    public synchronized  void send1() throws InterruptedException {
        System.out.println("进入send1方法-");
        Thread.sleep(100000);
        System.out.println("=======1============="+Thread.currentThread().getName());
    }

    private  Object o=new Object();

    public  void send2(){
        System.out.println("进入send2方法-");
        synchronized(o) {
            System.out.println("=======2=============" + Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final test t = new test();
        for (int i = 0; i < 100; i++) {
        Thread t1=    new Thread() {
                @Override
                public void run() {
                    try {
                        t.send1();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

        Thread t2=  new Thread() {
                @Override
                public void run() {
                    t.send2();

                }
            };
        t1.start();
        t2.start();
//        t1.join();
//        t2.join();
        }
    }
}
