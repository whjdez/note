package whj.thread.threadloal;

public class ThreadLocalDemo {

    public static ThreadLocal<String> LOCAL_DATA=new ThreadLocal<String>();


    public void add(String str){
        LOCAL_DATA.set(str);
    }

    public String get(){
        return LOCAL_DATA.get();
    }

    public static void main(String[] args) {
        ThreadLocalDemo threadLocalDemo=new ThreadLocalDemo();
        new Thread(()->{
            threadLocalDemo.add(Thread.currentThread().getName());
            threadLocalDemo.add("111");
            threadLocalDemo.add("222");
            final String s = threadLocalDemo.get();
            System.out.println(s);

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();


        new Thread(()->{
            final String s = threadLocalDemo.get();
            System.out.println(s);

        }).start();

    }
}
