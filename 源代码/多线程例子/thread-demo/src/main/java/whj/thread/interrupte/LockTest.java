package whj.thread.interrupte;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class LockTest {
    public static void main(String[] args) {

        ReentrantLock lock= new ReentrantLock();

        int i=0;
        for (int j = 0; j <10 ; j++) {
            Thread t1=new Thread(()->{
                 lock.lock();
                try{
                    System.out.println("被锁了");
                }finally {
                    lock.unlock();
                }
            });

        }


    }
}
