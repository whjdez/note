package whj.thread.join;

import java.util.Objects;

public class JoinDemo {
    public static void main(String[] args) throws InterruptedException {
//        Thread a = new Thread(() -> {
//            System.out.println("线程1");
//        });
//        Thread b = new Thread(() -> {
//            System.out.println("线程2");
//        });
//        Thread c = new Thread(() -> {
//            System.out.println("线程3");
//        });
//        Thread d = new Thread(() -> {
//            System.out.println("线程4");
//        });
//
//        d.start();
//        d.join();
//
//        c.start();
//        c.join();
//
//
//        b.start();
//        b.join();
//
//
//        a.start();
//        a.join();
        for (int i = 0; i < 10; i++) {
            final Thread thread3 = new Thread(new DemoRunable());
            final Thread thread2 = new Thread(new DemoRunable(thread3));
            final Thread thread1 = new Thread(new DemoRunable(thread2));
            thread1.setName("t1");
            thread2.setName("t2");
            thread3.setName("t3");
            thread1.start();
            thread2.start();
            thread3.start();

            System.out.println("========================================================");
            Thread.sleep(1000);
        }

    }
}

class DemoRunable implements Runnable {

    public DemoRunable() {
    }

    private Thread priorityThread;

    public DemoRunable(Thread prorityThread) throws InterruptedException {

        this.priorityThread = prorityThread;
    }

    @Override
    public void run() {
        System.out.println("execute=>"+Thread.currentThread().getName());
        if (Objects.nonNull(priorityThread)) {
            try {
                System.out.println("join=" + priorityThread.getName());
                priorityThread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("run threadName = " + Thread.currentThread().getName());
    }
}
