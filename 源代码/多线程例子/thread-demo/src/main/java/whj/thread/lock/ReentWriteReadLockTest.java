package whj.thread.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁的列子，
 * 列子读使用读锁，
 * 写使用写锁，
 * 多个线程进行写，多个线程进行读
 * 程序运行的结果：当程序写时会对读的方法
 */
public  class ReentWriteReadLockTest {

    public static void main(String[] args) {
        RwUtils rw = new RwUtils(0);
        for(int i = 0;i< 2; i++){
            final int n=i;
            new Thread(()->{
                rw.setNum(n);
            }).start();
        }
        for(int i = 0;i< 10; i++){
            final int n=i;
            new Thread(()->{
                rw.getNum(n);
            }).start();
        }
    }


}


class RwUtils{

    private ReentrantReadWriteLock  readWriteLock=new ReentrantReadWriteLock();

    //必须在方法体内才能获取锁，否则会报错
    //Lock readLock=readWriteLock.readLock();
    //Lock writeLock=readWriteLock.writeLock();
    //    Condition w=writeLock.newCondition();
    //    Condition r=readLock.newCondition();
    private int num=0;

    private int i;

    public RwUtils(int i){
        this.i = i;
    }
    public int getNum(int i){
        System.out.println("=============进入了获取值方法======================================");

        readWriteLock.readLock().lock();
        try {
            if(i==0){
               // try {
                    //System.out.println("=============读开始阻塞了======================================");
                    //Thread.sleep(100000000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
            System.out.println("==============="+i+"==获取"+num);
            return i;
        }finally {
            readWriteLock.readLock().unlock();
        }
    }

    public void setNum(int num){
        readWriteLock.writeLock().lock();
        try {
            this.num = num;
            System.out.println("================="+i+"   修改值=>"+num);
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }
}