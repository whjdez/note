package whj.thread.create.Executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

//        1,newCachedThreadPoo
//        创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
//        2,newFixedThreadPool
//        创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
//        3,newScheduledThreadPool
//        创建一个定长线程池，支持定时及周期性任务执行。
//        4,newSingleThreadExecutor
//        创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行。
public class ExecutorDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();

        List<Future> li=new ArrayList<Future>();
        for(int i=0;i<100;i++){
          Future<Map> p= executor.submit(new Tesk(i,"name:"+i));
            li.add(p);
        }
        executor.shutdown();
        executor.submit(new Tesk(100,"name:"+100));

      for(int i=0;i<li.size();i++){
          System.out.println( li.get(i).get());
      }
    }
}

class Tesk implements Callable {
    private Integer i;
    private String name;
    public Tesk(Integer i,String name){
         this.i=i;
         this.name=name;
     }

    public Map<String,Integer> call() throws Exception {
        i++;
        Map<String,Integer> m=new HashMap<String,Integer>();
        m.put(name,i);
        return m;
    }
}