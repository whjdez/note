package whj.thread.lock;

public class test2 {
    public static void main(String[] args) {
        Object o=new Object();
        for(int i=0;i<100;i++){
            new Thread(new Thread1(i,o)).start();
        }
    }

}

class Thread1 implements  Runnable{

    private Integer i=0;
    private Object o;
    public Thread1(int i,Object o){
        this.i=i;
        this.o=o;
    }

    public  void run() {
        synchronized (o) {
            System.out.println(i + "====进入run====" + Thread.currentThread().getName());
            try {
                Thread.sleep(100000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i + "====结束run====" + Thread.currentThread().getName());
        }
    }
}
