package whj.create;

public class Factory implements Runnable{
    Object lock;
    public Factory(Object lock){
        this.lock=lock;
    }
    public void run() {
        synchronized(lock) {
            while (true) {
                System.out.println("==生产===");
                lock.notify();
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
              //  System.out.println("==生产==释放=");
                try {
                    lock.wait();
                  //  System.out.println("==生产==等待=");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class  Consume implements  Runnable{

    public Object obj;

    public Consume(Object obj){
        this.obj=obj;
    }

    public void run() {
        synchronized(obj){
            while (true) {
                System.out.println("消费");
                obj.notify();
               // System.out.println("==消费==释放=");
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                try {
                    obj.wait();
                   // System.out.println("==消费==等待=");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Object o=new Object();
        Thread a=new Thread(new Factory(o));
        Thread b=new Thread(new Consume(o));
        a.start();
        b.start();
    }
}