package whj.countDownLatchorcyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier 与 CountDownLatch 区别
 *
 * CountDownLatch 是一次性的，CyclicBarrier 是可循环利用的
 * CountDownLatch 参与的线程的职责是不一样的，有的在倒计时，有的在等待倒计时结束。CyclicBarrier 参与的线程职责是一样的。
 * 链接：https://www.jianshu.com/p/333fd8faa56e
 *
 * 类似于
 * 长途汽车站提供长途客运服务。
 * 当等待坐车的乘客到达20人时，汽车站就会发出一辆长途汽车，让这20个乘客上车走人。
 * 等到下次等待的乘客又到达20人是，汽车站就会又发出一辆长途汽车。
 *
 * 当调用await 后 ，方法后面代码会阻塞，当前线程阻塞，当执行够构造函数中 数量后 ，开始放开阻塞，继续执行
 */
public class CyclicBarrierDemo {
    public static void main(String[] args) {
        final CyclicBarrier cyclicBarrier=new CyclicBarrier(10);
        for (int i=0;i<5;i++){
            final int index=i;
            new Thread(){
                @Override
                public void run() {
                    System.out.println(index+"======进入方法调用awite=======");
                    try {
                        cyclicBarrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                    System.out.println(index+"======结束执行=======");

                }
            }.start();
        }
        System.out.println("========主线程未开始================");
        try {
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println("========主线程================");
    }
}
