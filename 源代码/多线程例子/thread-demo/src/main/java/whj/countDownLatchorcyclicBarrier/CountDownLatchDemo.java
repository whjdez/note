package whj.countDownLatchorcyclicBarrier;

import java.util.concurrent.CountDownLatch;

/**
 * 用于对线程计数，
 * 比如有一个任务A，它要等待其他4个任务执行完毕之后才能执行
 * 此时就可以利用CountDownLatch来实现这种功能了。
 *
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch latch=new CountDownLatch(2);
        new Thread(){
            @Override
            public void run() {
                System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
                latch.countDown(); //计数器减一
            }
        }.start();

        new Thread() {
            public void run() {
                try {
                    System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                    Thread.sleep(3000);
                    System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
        }.start();

        System.out.println("等待2个子线程执行完毕...");
        latch.await(); //调用await()方法的线程会被挂起，它会等待直到count值为0才继续执行
        System.out.println("2个子线程已经执行完毕");
        System.out.println("继续执行主线程");

    }
}
