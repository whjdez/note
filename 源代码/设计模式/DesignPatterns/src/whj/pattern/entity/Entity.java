package whj.pattern.entity;

import javax.swing.border.EmptyBorder;
import java.util.Map.Entry;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class Entity {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
