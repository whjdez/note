package whj.pattern.proxy;

import whj.pattern.proxy.jdk.JdkProxy;

import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {

        DztService dztService = new DztServiceImpl();

        DztService proxy = (DztService) Proxy.newProxyInstance(Test.class.getClassLoader(),
                new Class[]{DztService.class},
                new JdkProxy(dztService));
        proxy.doSomething("66", new Entity());
    }
}
