package whj.pattern.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 *
 * jdk 代理类型
 *
 * @author w
 */
public class JdkProxy implements InvocationHandler {


    private Object obj;

    /**
     *
     * @param obj
     */
    public JdkProxy(Object obj){
        this.obj=obj;
    }



    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName()+" : 我被代理了");
        Object o= method.invoke(obj, args);
        return o;
    }
}
