package whj.pattern.proxy;

/**
 * 吊炸天的服务
 */
public interface DztService {

    /**
     * 做不可描述的事
     *
     * @param str
     * @param entity
     * @return
     */
    String doSomething(String str,Entity entity);
}
