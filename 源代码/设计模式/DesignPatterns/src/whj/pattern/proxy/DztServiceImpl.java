package whj.pattern.proxy;

/**
 * @author w
 */
public class DztServiceImpl implements DztService{

    /**
     * 做不可描述的事
     *
     * @param str
     * @param entity
     * @return
     */
    @Override
    public String doSomething(String str, Entity entity){

        return str+" 人过留名 雁过留声"+entity.getName();

    }
}
