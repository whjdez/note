package whj.pattern.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 工具类
 */
public class Utils {

    /**
     * 序列化
     * @param obj
     * @return
     */
    public static byte[] serializa(Object obj){
        ObjectOutputStream objectOutputStream=null;
        ByteArrayOutputStream byteArrayOutputStream=null;
        try{
            byteArrayOutputStream=new ByteArrayOutputStream();
            objectOutputStream=new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            objectOutputStream.flush();
            objectOutputStream.close();

            return byteArrayOutputStream.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 反序列化
     * @param bytes
     * @param <T>
     * @return
     */
    public static <T> T deSerializa(byte[]bytes){
        ByteArrayInputStream byteArrayInputStream=null;
        try{

            byteArrayInputStream=new ByteArrayInputStream(bytes);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
           return (T) objectInputStream.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
