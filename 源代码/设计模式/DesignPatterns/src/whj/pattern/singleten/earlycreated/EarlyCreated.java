package whj.pattern.singleten.earlycreated;

/**
 * 饿汉模式
 * @author w
 * 缺点 一开始就加载数据，会消耗一定的性能
 */
public class EarlyCreated {

    private static EarlyCreated earlyCreated=new EarlyCreated();

    private EarlyCreated(){

    }

    /**
     * 获取单例对象
     * @return
     */
    public static EarlyCreated getEarlyCreated(){
        return earlyCreated;
    }
}
