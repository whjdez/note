package whj.pattern.singleten.lazy;

import java.io.Serializable;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 *
 * 在编写时要注意线程安全问题
 *
 */
public class LazySingleten implements Serializable {

    private LazySingleten(){}

    /**
     *
     */
    private volatile static LazySingleten lazySingleten;

    /**
     *
     * 双检索的作用，最外城非空判断视为了非首次调用时 都加锁，提高性能。
     *
     * @return
     */
    public static LazySingleten getLazySingleten(){
        if(lazySingleten==null){
            synchronized (LazySingleten.class){
                if(lazySingleten==null){
                    //加valatile为了防止程序重排序[jvm在不保证错误的情况下，会对执行顺序进行调优]
                    //new 是分为三部分的，先定义引用 A a
                    //第二步在堆中开辟内存空间
                    //第三步将堆中地址赋为引用 在这过程中 2,3是可能顺序颠倒的，先赋值再初始化，这样就会导致 返回的是一个空对象
                    lazySingleten=new LazySingleten();
                }
            }
        }
        return lazySingleten;
    }

    /**
     * 反序列化工具
     * @return
     */
    public Object readResolve(){
        return (Object) lazySingleten;
    }
}
