package whj.pattern.singleten.enums;

/**
 * 能够保证线程安全
 */
public enum  EnumsSingleten {

    FRONT("前") {
        @Override
        public void show() {
            System.out.println("前");
        }
    };

    public abstract void show();

    private String name;

    private EnumsSingleten(String name) {
        this.name = name;
    }

    public String getString() {
        return name;
    }

}
