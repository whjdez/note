package whj.pattern.singleten.enums;

/**
 * 线程安全，还能防止序列化
 *
 * 推荐使用
 */
public enum Singleton {
    INSTANCE;
    public void say(){
        System.out.println("hello world");
    }



    public static void main(String[] args) {
        Singleton instance = Singleton.INSTANCE;
       Singleton instance2= Singleton.INSTANCE;
        System.out.println(instance==instance2);

        instance.say();
    }
}
