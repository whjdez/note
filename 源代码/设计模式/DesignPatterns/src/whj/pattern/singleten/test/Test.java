package whj.pattern.singleten.test;

import whj.pattern.singleten.earlycreated.EarlyCreated;
import whj.pattern.singleten.enums.EnumsSingleten;
import whj.pattern.singleten.enums.Singleton;
import whj.pattern.singleten.lazy.LazySingleten;
import whj.pattern.util.Utils;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

/**
 * @author w
 */
public class Test {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //如果不使用双检索的话很容易就会造成多个实例，测试出现了最少五个实例 懒汉模式
//        test(()->LazySingleten.getLazySingleten(),100);
//        //=====================恶汉模式=================================================================
//        //不用做处理 可以实现线程安全
//        test(()->EarlyCreated.getEarlyCreated(),100);
//        //======================枚举类==========================================================
//        test(()->EnumsSingleten.FRONT,100);
        //=======================反射测试============================================================

        //=======================反射生成懒汉单例对象======存疑为什么使用实例调用后产生的对象一样==========================================
//        test(() -> {
//            Class clazz = null;
//            Object newInstance =null;
//            try {
//                clazz = Class.forName("whj.pattern.singleten.lazy.LazySingleten");
//                Constructor constructor = clazz.getDeclaredConstructor();
//                constructor.setAccessible(true);
//                newInstance = constructor.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return newInstance;
//        }, 100);
        //=======================序列化==============================================================================
        LazySingleten lazySingleten =  LazySingleten.getLazySingleten();
        test(()->{
            byte[] bytes = Utils.serializa(lazySingleten);
            System.out.println(bytes);
            return  Utils.deSerializa(bytes);
        },100);
    }


    /**
     * 多个线程生成
     *
     * @param supplier
     * @param size
     * @return
     */
    public static Map<Object, ?> test(Supplier<?> supplier, int size) {
        ConcurrentHashMap map = new ConcurrentHashMap<>(20);

        for (int i = 0; i < size; i++) {
            map.put(supplier.get(), 1);
        }
        System.out.println(map);
        return map;
    }


}
