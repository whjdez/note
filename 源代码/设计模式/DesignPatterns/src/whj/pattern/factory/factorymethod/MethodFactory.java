package whj.pattern.factory.factorymethod;


import com.sun.xml.internal.ws.util.StringUtils;

public class MethodFactory {

    /**
     * 生成指定类对象
     * @param className
     * @param <T>
     * @return
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public <T> T createObj(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        T t = null;
        if (!"".equals(className) && className != null) {
            Class cls = Class.forName(className);
            t = (T) cls.newInstance();
        }
        return t;
    }

}
