package whj.pattern.factory.abstractfactory.factory;

import whj.pattern.factory.abstractfactory.Human;
import whj.pattern.factory.abstractfactory.impl.MaleBlackHuman;
import whj.pattern.factory.simpleFactory.impl.YellowHuman;

/**
 *
 * @author w
 */
public class MaleFactory implements HumanFactory {
    @Override
    public Human createYellowHuman() {

        return null;
    }

    @Override
    public Human createWhiteHuman() {

        return null;
    }

    @Override
    public Human createBlackHuman() {

        return new MaleBlackHuman();
    }
}
