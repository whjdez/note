package whj.pattern.factory.abstractfactory.impl;

/**
 * @author w
 */
public class FemaleBlackHuman extends AbstractBlackHuman{
    @Override
    public void sex() {
        System.out.println("我是黑人女性");
    }
}
