package whj.pattern.factory.abstractfactory.factory;

import whj.pattern.factory.abstractfactory.Human;
import whj.pattern.factory.abstractfactory.impl.FemaleBlackHuman;

/**
 * 申城
 */
public class FemaleFactory implements HumanFactory {
    @Override
    public Human createYellowHuman() {
        return null;
    }

    @Override
    public Human createWhiteHuman() {
        return null;
    }

    @Override
    public Human createBlackHuman() {
        return new FemaleBlackHuman();
    }
}
