package whj.pattern.factory.abstractfactory.impl;

import whj.pattern.factory.abstractfactory.Human;

/**
 * 黑人抽象类
 * @author w
 */
public abstract class AbstractBlackHuman implements Human {

    @Override
    public void getColor() {
        System.out.println("我是黑色的");
    }

    @Override
    public void sayHello() {
        System.out.println("guala guala");
    }

}
