package whj.pattern.factory.abstractfactory.impl;

/**
 * 黑人实现类
 * @author w
 */
public class MaleBlackHuman extends AbstractBlackHuman {
    @Override
    public void sex() {
        System.out.println("我是黑人男性");
    }
}
