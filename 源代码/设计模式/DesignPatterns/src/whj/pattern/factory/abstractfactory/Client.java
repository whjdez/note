package whj.pattern.factory.abstractfactory;

import whj.pattern.factory.abstractfactory.factory.FemaleFactory;
import whj.pattern.factory.abstractfactory.factory.HumanFactory;
import whj.pattern.factory.abstractfactory.factory.MaleFactory;

/**
 * @author w
 *
 * 抽象工厂与普通工厂的区别
 *
 * 普通工厂:在一个create方法中根据传入类型返回需要的对象
 * 好处：实现简单，逻辑清晰
 * 缺点：如果需要增加新的类型就需要对create方法进行修改，
 *      如果需要新增新的类和方法就需要在基类的接口中添加，
 *      导致所有产品实现都得添加
 * 抽象工厂可以修正部分问题
 *  解决思路：在基类工厂中创建多个产品工厂，每个工厂按照普通工厂的方式进行实现，每个工厂返回的对象必须保证是同一类型，这样就能在使用时同一的进行处理，
 *          当需要新增一个新的产品时只需要在工厂中添加一个新的产品，不对原有的产品产生任何影响
 *
 *
 *
 *
 *
 *
 */
public class Client {
    public static void main(String[] args) {
        //男工厂
        HumanFactory maleHumanFactory=new MaleFactory();
        //女工厂
        HumanFactory femaleHumanFactory=new FemaleFactory();
        //黑人小哥哥
        Human blackHuman = maleHumanFactory.createBlackHuman();

        //黑人小姐姐
        Human blackFemaleHumanFactory=femaleHumanFactory.createBlackHuman();

        System.out.println("============小姐姐的特性===============");
        blackFemaleHumanFactory.getColor();
        blackFemaleHumanFactory.sex();
        blackFemaleHumanFactory.sayHello();

    }
}
