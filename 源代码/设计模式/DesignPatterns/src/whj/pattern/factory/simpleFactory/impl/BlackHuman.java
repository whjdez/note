package whj.pattern.factory.simpleFactory.impl;


import whj.pattern.factory.simpleFactory.Human;

public class BlackHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("我是黑色的");
    }

    @Override
    public void sayHello() {
        System.out.println("guala guala");
    }
}
