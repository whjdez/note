package whj.pattern.factory.simpleFactory;

import whj.pattern.factory.simpleFactory.impl.BlackHuman;
import whj.pattern.factory.simpleFactory.impl.WhiteHuman;
import whj.pattern.factory.simpleFactory.impl.YellowHuman;

/**
 * 工厂类
 */
public class HumanFactory extends AbstractHumanFactory{
    @Override
    public <T extends Human> T createHuman(String humanType) {
        switch (humanType){
            case "BlackHuman":
                return  (T)new BlackHuman();
            case "WhiteHuman":
                return (T)new WhiteHuman();
            case "YellowHuman":
                return (T)new YellowHuman();
            default:
                break;
        }
        return null;
    }
}
