package whj.pattern.factory.simpleFactory;

public class Test {
    public static void main(String[] args) {
        HumanFactory humanFactory=new HumanFactory();
        Human human=humanFactory.createHuman("WhiteHuman");
        System.out.println(human);
    }
}
