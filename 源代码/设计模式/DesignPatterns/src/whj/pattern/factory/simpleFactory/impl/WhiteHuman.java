package whj.pattern.factory.simpleFactory.impl;


import whj.pattern.factory.simpleFactory.Human;

public class WhiteHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("我是白色的");
    }

    @Override
    public void sayHello() {
        System.out.println("hello");
    }
}
