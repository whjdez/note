package whj.pattern.factory.simpleFactory;

/**
 * 抽象工厂
 * @author w
 */
public abstract class AbstractHumanFactory {

    public abstract <T extends Human> T createHuman(String humanType);
}
