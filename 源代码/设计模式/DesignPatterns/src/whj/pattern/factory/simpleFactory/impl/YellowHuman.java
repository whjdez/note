package whj.pattern.factory.simpleFactory.impl;


import whj.pattern.factory.simpleFactory.Human;

public class YellowHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("我是黄色的");
    }

    @Override
    public void sayHello() {
        System.out.println("您好");
    }
}
