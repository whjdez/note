package binary.search;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @Author wanghaijing
 * @Description TODO
 * @Date 2024/5/1 16:48
 * @Version 1.0
 */

public class BinarySearch {

    public static int binarySearch(int[] arr, int target) {

        if (arr == null || arr.length == 0) {
            return -1;
        }
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {
            int mid = right + left >>> 1;
            if (arr[mid] < target) {
                left = mid + 1;
            } else if (arr[mid] > target) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    public static int binarySearch1(int[] arr, int target) {

        if (arr == null || arr.length == 0) {
            return -1;
        }
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            int mid = right + left >>> 1;
            if (arr[mid] < target) {
                left = mid;
            } else if (arr[mid] > target) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    /**
     * 折半查找
     * @param arr
     * @param target
     * @return
     * 1 2 3 4 5 6 7 8 9
     * i                 j
     * mid=(i+j)/2
     * i为起点，j为长度，在数组外部
     */
    public static int binarySearchBalance(int[] arr, int target) {
        int i = 0, j = arr.length;
        while (1 < j - i) {
            int mid = i + j >>> 1;
            if (arr[mid] > target) {
                j = mid;
            } else {
                i = mid;
            }
        }

        return arr[i] == target ? i : -1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int target = 5;
        assertEquals(6, binarySearchBalance(arr, 7));
        System.out.println();
        assertEquals(2, binarySearchBalance(arr, 7));

//        int index = binarySearch(arr, target);
//        System.out.println(index);
    }
}
