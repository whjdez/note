package com.whj.lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 功能描述
 *
 * postProcessBeforeInitialization，
 * postProcessAfterInitialization方法
 * 使用时要返回正确的值，
 * 否则会导致bean工厂中没有对应的bean
 *
 * @author WangHaiJing
 * @date
 */
//@Component
public class BeanPostProcessorDemo implements BeanPostProcessor {


    /**
     * 该方法在bean对象的初始化方法调用之前回调
     * （在initlizeBean方法前调用，内部值已经注入完成）
     *
     * 等会儿看下源码，为什么返回null，还可以继续注入
     * @param o beanfactory 存储的对象
     * @param s beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        System.out.println(" o==="+o);
        System.out.println(" s==="+s);
        System.out.println("bean 初始化之前 ");
        if("taskService".equals(s)){
            // ((TaskService)o).sayHello();
           // System.out.println(o);
           // return new String ("dfdg");
        }
        return null;
    }

    /**
     *
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
//        System.out.println(" o==="+o);
//        System.out.println(" s==="+s);
//        System.out.println("bean 初始化之后 ");
//        if("taskService".equals(s)){
//
//        }
        return o;
    }
}
