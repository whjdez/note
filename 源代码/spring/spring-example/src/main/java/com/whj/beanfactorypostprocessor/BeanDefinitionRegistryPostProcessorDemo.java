package com.whj.beanfactorypostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * 功能描述
 *
 * 先执行 postProcessBeanDefinitionRegistry方法 再执行postProcessBeanFactory
 *
 * @author WangHaiJing
 * @date
 */
// @Component
public class BeanDefinitionRegistryPostProcessorDemo implements BeanDefinitionRegistryPostProcessor {


    /**
     *
     *该方法可以将bean注册到bean工厂中
     *
     * @param beanDefinitionRegistry
     * @throws BeansException
     */
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {

         // System.out.println(beanDefinitionRegistry);
    }

    /**
     *
     *
     * @param configurableListableBeanFactory
     * @throws BeansException
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

       // System.out.println(configurableListableBeanFactory);
    }
}
