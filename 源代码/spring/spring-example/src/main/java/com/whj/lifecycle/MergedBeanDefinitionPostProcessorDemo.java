package com.whj.lifecycle;

import com.whj.service.OrderService;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;

//@Component
public class MergedBeanDefinitionPostProcessorDemo implements MergedBeanDefinitionPostProcessor {


    /**
     *
     * @param rootBeanDefinition
     * @param aClass
     * @param s
     */
    @Override
    public void postProcessMergedBeanDefinition(RootBeanDefinition rootBeanDefinition, Class<?> aClass, String beanName) {
        if ("taskService".equals(beanName)) {
            // setOrderService 注入对象要求有set方法
            rootBeanDefinition.getPropertyValues().add("orderService", new OrderService());
        }
    }
}
