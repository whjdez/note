package com.whj.lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

//@Component
public class BeanFactoryPostProcessorDemo implements BeanFactoryPostProcessor {

    /**
     * 我们可以在postProcessBeanFactory()方法中对BeanFactory进行加工
     *
     * @param configurableListableBeanFactory
     * @throws BeansException
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        System.out.println(configurableListableBeanFactory.getBeanDefinitionNames());
    }
}
