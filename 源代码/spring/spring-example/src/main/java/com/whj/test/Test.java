package com.whj.test;

import com.whj.service.TaskService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Locale;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringTest.class);
        TaskService taskService = applicationContext.getBean(TaskService.class);
        taskService.sayHello();

//        LookupDemo lookupDemoService = applicationContext.getBean(LookupDemo.class);
//        lookupDemoService.sayHello();
//
//        LookupDemo lookupDemoService1 = applicationContext.getBean(LookupDemo.class);
//        lookupDemoService1.sayHello();
//        System.out.println(lookupDemoService);
//        System.out.println(lookupDemoService1);
        //taskService

      //  message(applicationContext);
    }

    /**
     * 国际化使用
     *
     * @param context
     */
    public static  void message(ApplicationContext context){
//        String message = context.getMessage("test", null, new Locale("en_CN"));
//        System.out.println("消息："+message);
        //未指定Locale，此时系统会取默认的locale对象，本地默认的值中文【中国】，即：zh_CN
        System.out.println(context.getMessage("name", null, null));
        System.out.println(context.getMessage("name", null, Locale.CHINA)); //CHINA对应：zh_CN
        System.out.println(context.getMessage("name", null, Locale.UK)); //UK对应en_GB
        //未指定Locale，此时系统会取默认的，本地电脑默认的值中文【中国】，即：zh_CN
        /*
         * 动态参数使用
         * 注意配置文件中的personal_introduction，个人介绍，比较特别，
         * 包含了{0},{1},{0}这样一部分内容，这个就是动态参数，
         * 调用getMessage的时候，通过第二个参数传递过去，来看一下用法
         */
        System.out.println(context.getMessage("personal_introduction", new String[]{"spring高手", "java高手"}, Locale.CHINA)); //CHINA对应：zh_CN
        System.out.println(context.getMessage("personal_introduction", new String[]{"spring", "java"}, Locale.UK)); //UK对应en_GB
    }
    void test() {
//        AnnotationConfigApplicationContext context = new  AnnotationConfigApplicationContext(SpringTest.class);
//         // 生成一个BeanDefinition对象，并设置beanClass为User.class，并注册到ApplicationContext中
//        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
//        beanDefinition.setBeanClass(SpringTest.class);
//        context.registerBeanDefinition("SpringTest", beanDefinition);
//        System.out.print ln(context.getBean("SpringTest"));
    }
}
