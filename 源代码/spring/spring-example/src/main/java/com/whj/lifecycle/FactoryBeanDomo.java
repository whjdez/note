package com.whj.lifecycle;

import com.whj.service.OrderService;
import org.springframework.beans.factory.FactoryBean;

//@Component
public class FactoryBeanDomo implements FactoryBean {


    /**
     * 使用factory定义的bean是不经历生命周期的
     * @return
     * @throws Exception
     */
    @Override
    public Object getObject() throws Exception {
        return new OrderService();
    }

    @Override
    public Class<?> getObjectType() {
        return OrderService.class;
    }
}
