package com.whj.lifecycle;

import org.springframework.beans.factory.InitializingBean;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
//@Component
public class InitiaizingBeanDemo implements InitializingBean {
    
    @Override
    public void afterPropertiesSet() throws Exception {
        // System.out.println("===========InitiaizingBeanDemo=============");
    }
}
