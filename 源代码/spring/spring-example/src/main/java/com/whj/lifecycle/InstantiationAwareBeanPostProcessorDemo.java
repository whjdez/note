package com.whj.lifecycle;

import com.whj.service.TaskService;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class InstantiationAwareBeanPostProcessorDemo implements InstantiationAwareBeanPostProcessor {

    /**
     * bean还没有生成，只有原始的类信息
     * 在实例化前会直接返回一个由我们所定义的taskServic对象。如果是这样，
     * 表示不需要Spring来实例化了，并且后续的Spring依赖注入也不会进行了，会跳过一些步骤，直接执
     * 行初始化后这一步。
     * @param beanClass
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if ("taskService".equals(beanName)) {
            // System.out.println("实例化前");
           // return new TaskService();
        }
        return null;
    }

    /**
     * 实例化之后,可以对生成的bean进行操作,还没有进行注入
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if ("taskService".equals(beanName)) {
            TaskService userService = (TaskService) bean;
            // userService.sayHello();
        }

//        System.out.println(bean);
        return true;
    }

    /**
     *
     * @param pvs
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        System.out.println(pvs);

        if ("taskService".equals(beanName)) {
            System.out.println(pvs.getPropertyValues());
           // pvs.getPropertyValue("").setAttribute("orderService",new OrderService());
        }
        return null;
    }
}