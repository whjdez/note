package com.whj.service;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
@Component
public class TaskService {

    //@Autowired
    @Resource
    private OrderService orderService;

//    @PostConstruct
//    public void postConstruct(){
//        System.out.println("====================PostConstruct========================");
//    }

    public  TaskService(){

        // System.out.println("============初始化TestService==========");
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void sayHello(){
        orderService.sayHello();
        System.out.println("=============说 hello==========================");
    }
}
