package com.whj.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class CommonConfig {

    /**
     * 国际化文件命名格式：名称_语言_地区.properties
     * 这个文件名称没有指定Local信息，当系统找不到的时候会使用这个默认的(messages.properties)
     * spring中注册国际化的bean
     * 注意必须是MessageSource类型的，bean名称必须为messageSource
     * @return
     */
//    @Bean
//    public MessageSource messageSource(){
//        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//        // 可以指定国际化化配置文件的位置
//        // 格式：路径/文件名称,注意不包含【语言_国家.properties】含这部分
//      //   messageSource.setBasename("messageSource.setBasename 多文件怎么设置\n");
//     //   messageSource.setBasename("classpath:i18n/messages, classpath:i18n/errors");
//        messageSource.setBasename("i18n/messages");
//        messageSource.setDefaultEncoding("UTF-8");
//        return messageSource;
//    }
//    @Bean
//    public MessageSource messageSource() {
//        ReloadableResourceBundleMessageSource result = new ReloadableResourceBundleMessageSource();
//        result.setBasenames("i18n/messages");
//        result.setDefaultEncoding("UTF-8");
//        //设置缓存时间1000毫秒
////        -1：表示永远缓存
////        0：每次获取国际化信息的时候，都会重新读取国际化文件
////        大于0：上次读取配置文件的时间距离当前时间超过了这个时间，重新读取国际化文件
//        result.setCacheMillis(1000);
//        return result;
//    }

//    @Bean
//    public MessageSource messageSource(){
//        return new MessageSourceFromDb();
//    }
}
