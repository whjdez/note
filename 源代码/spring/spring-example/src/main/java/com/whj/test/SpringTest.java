package com.whj.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
@ComponentScan("com.whj")
@Configuration
public class SpringTest {
}
