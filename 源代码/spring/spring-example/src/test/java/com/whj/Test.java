package com.whj;

import com.whj.common.CommonConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Test {

    @org.junit.Test
    public void test3() throws InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(CommonConfig.class);
        context.refresh();
        //输出2次
        for (int i = 0; i < 2; i++) {
            System.out.println(context.getMessage("name", null, Locale.CHINA));
            TimeUnit.SECONDS.sleep(5);
        }

    }

    @org.junit.Test
    public void test4() throws IOException {
        SimpleMetadataReaderFactory simpleMetadataReaderFactory = new
                SimpleMetadataReaderFactory();
//构造一个MetadataReader
        MetadataReader metadataReader = simpleMetadataReaderFactory.getMetadataReader("com.whj.service.TaskService");
//得到一个ClassMetadata，并获取了类名
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        System.out.println(classMetadata.getClassName());

// 获取一个AnnotationMetadata，并获取类上的注解信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        for (String annotationType : annotationMetadata.getAnnotationTypes()) {
            System.out.println(annotationType);
        }
    }

}


