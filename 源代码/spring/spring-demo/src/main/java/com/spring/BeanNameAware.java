package com.spring;

/**
 * @author wangj
 */
public interface BeanNameAware {

    void setBaneName(String beanName);
}
