package com.spring;

import java.lang.annotation.*;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Lazy  {
}
