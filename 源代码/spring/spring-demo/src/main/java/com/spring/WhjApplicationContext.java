package com.spring;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class WhjApplicationContext {

    private Class cls;

    private Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    private Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(256);

    public WhjApplicationContext(Class cls) throws ClassNotFoundException {
        this.cls = cls;
        ComponentScan scan = (ComponentScan) cls.getAnnotation(ComponentScan.class);
        String scanClassValue = scan.value();
        //扫描类
        scan(scanClassValue);
        //创建非懒加载单例bean
        createNoLazySingletonBean();
        System.out.println(beanDefinitionMap);
    }

    private void createNoLazySingletonBean() {
        for (String beanName : beanDefinitionMap.keySet()) {
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if (beanDefinition.getScope().equals("singleton") && !beanDefinition.isLazy()) {
                Object bean = createBean(beanDefinition,beanName);
                singletonObjects.put(beanName, bean);
            } else if (beanDefinition.getScope().equals("prototype")) {

            }
        }
    }

    private Object createBean(BeanDefinition beanDefinition,String beanName) {
        //创建对象
        Class classBean = beanDefinition.getBeanClass();
        Object instance = null;
        try {
            instance = classBean.getDeclaredConstructor().newInstance();
            //给属性填充值
            Field[] declaredFields = classBean.getDeclaredFields();
            for (Field f : declaredFields) {
                if (f.isAnnotationPresent(Autowired.class)) {
                    //获取属性类型 byType
                    //获取属性值 tyName
                    Object bean = getBean(f.getName());
                    f.setAccessible(true);
                    f.set(instance, bean);
                }
            }

            //设置bean名称
            if(instance instanceof BeanNameAware){
                ((BeanNameAware)instance).setBaneName(beanName);
            }
            //

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //注入对象

        return instance;
    }


    /**
     * 扫描某个包下的所有类
     *
     * @param scanClassValue
     */
    private void scan(String scanClassValue) throws ClassNotFoundException {
        ClassLoader classLoader = WhjApplicationContext.class.getClassLoader();

        scanClassValue = scanClassValue.replace(".", "/");
        URL resource = classLoader.getResource(scanClassValue);
        File file = new File(resource.getFile());
        for (File f : file.listFiles()) {
            BeanDefinition beanDefinition = new BeanDefinition();
            String path = f.getAbsolutePath();
            if (path.endsWith(".class")) {
                path = path.substring(path.indexOf("classes\\") + 8, path.indexOf(".class"));
                path = path.replace("\\", ".");
                Class resClass = classLoader.loadClass(path);
                if (resClass.isAnnotationPresent(Component.class)) {
                    //如果被该注解修饰，说明是个bean
                    Component component = (Component) resClass.getAnnotation(Component.class);
                    String beanName = component.value();
                    if (resClass.isAnnotationPresent(Lazy.class)) {
                        beanDefinition.setLazy(true);
                    }
                    beanDefinition.setBeanClass(resClass);
                    if (resClass.isAnnotationPresent(Scope.class)) {
                        Scope scope = (Scope) resClass.getAnnotation(Scope.class);
                        String value = scope.value();
                        beanDefinition.setScope(value);
                    } else {
                        //单例
                        beanDefinition.setScope("singleton");
                    }
                    beanDefinitionMap.put(beanName, beanDefinition);
                }
                System.out.println(resClass.getName());
            }
        }
    }

    /**
     * 获取bean信息
     *
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) {
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new NullPointerException();
        } else {
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if (beanDefinition.getScope().equals("singleton")) {
                Object object = singletonObjects.get(beanName);

                if (object == null) {
                    object = createBean(beanDefinition,beanName);
                    singletonObjects.put(beanName, object);
                }
                return object;
            } else if (beanDefinition.getScope().equals("prototype")) {
                return createBean(beanDefinition,beanName);
            }

        }

        return null;
    }
}
