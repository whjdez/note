package com.whj.service;


import com.spring.Autowired;
import com.spring.BeanNameAware;
import com.spring.Component;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
@Component("userService")
public class UserService implements BeanNameAware {


    @Autowired
    private OrderService orderService;

    /**
     *
     * @return
     */
    public String  sayHello(){

        System.out.println(orderService);
        return "你好";
    }

    @Override
    public void setBaneName() {

    }
}
