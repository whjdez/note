package com.whj;

import com.spring.ComponentScan;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
@ComponentScan("com.whj.service")
public class WhjConfig {
}
