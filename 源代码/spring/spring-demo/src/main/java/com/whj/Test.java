package com.whj;

import com.spring.WhjApplicationContext;
import com.whj.service.UserService;

/**
 * 功能描述
 *
 * @author WangHaiJing
 * @date
 */
public class Test {
    public static void main(String[] args) throws ClassNotFoundException {
            WhjApplicationContext appilcation=new WhjApplicationContext(WhjConfig.class);
            UserService userService= (UserService)appilcation.getBean("userService");
            System.out.println(userService.sayHello());
    }
}
