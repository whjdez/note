package classloader;

import java.io.FileInputStream;
import java.lang.reflect.Method;

/**
 *
 *
 *
 *
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/4/12 17:35
 * @updateDate 2021/4/12 17:35
 **/
public class MyClassLoaderTest {
    static class  MyClassLoader  extends ClassLoader{
        private String classPath;

        public MyClassLoader(String classPath) {
            this.classPath = classPath;
        }

        private byte[] loadByte(String name) throws Exception {
            name = name.replaceAll("\\.", "/");
            FileInputStream fis = new FileInputStream(classPath + "/" + name
                    + ".class");
            int len = fis.available();
            byte[] data = new byte[len];
            fis.read(data);
            fis.close();
            return data;
        }

        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            System.out.println("=================================1=====================================================");
            try {
                byte[] data = loadByte(name);
                System.out.println("======================2================================");
                System.out.println(data);
                System.out.println("======================3================================");
                //defineClass将一个字节数组转为Class对象，这个字节数组是class文件读取后最终的字节数组。
                return defineClass(name, data, 0, data.length);
            } catch (Exception e) {
                e.printStackTrace();
                throw new ClassNotFoundException();
            }
        }
        //测试时不要在当前项目中建同名类 ，否则在加载时会优先加载当前目录（因为自定义的类类加载器继承与appclassload，当未指定类加载器时会使用该类加载器加载）
        public static void main(String args[]) throws Exception {
            //初始化自定义类加载器，会先初始化父类ClassLoader，其中会把自定义类加载器的父加载器设置为应用程序类加载器AppClassLoader
            //路径到包为止
            MyClassLoader classLoader = new MyClassLoader("C:\\Users\\w\\Desktop\\");
            //D盘创建 test/com/tuling/jvm 几级目录，将User类的复制类User1.class丢入该目录
            Class clazz = classLoader.loadClass("classloader.entity.User");
            Object obj = clazz.newInstance();
            Method method = clazz.getDeclaredMethod("sout", null);
            method.invoke(obj, null);
            System.out.println(clazz.getClassLoader().getClass().getName());
        }
    }

}
