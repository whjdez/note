package loading.order;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/4/19 11:36
 * @updateDate 2021/4/19 11:36
 * 验证类的加载顺序，
 * 1. 父类的静态变量
 * 2. 父类的静态代码块
 * 3. 子类的静态变量
 * 4. 子类的静态代码块
 * 5. 父类的代码块
 * 6. 父类成员变量
 * 7. 父类的构造方法
 * 8. 子类的代码块
 * 9. 子类构成员变量
 * 10. 子类的构造方法
 *      注意：
 *      1. 成员变量创建当前对象时会报错：StackOverflowError
 *         private ClassOrder classOrder = new ClassOrder();
 *      2. 静态变量也可以被继承
 *      3. 子类中如果重写了父类中的任何方法和成员变量，在子类中使用时都会使用子类的
 *
 **/

//  验证类的加载顺序，
//  1. 父类的静态变量
//  2. 父类的静态代码块
//  3. 子类的静态变量
//  4. 子类的静态代码块
//  5. 父类的代码块
//  6. 父类成员变量
//  7. 父类的构造方法
//  8. 子类的代码块
//  9. 子类构成员变量
//  10. 子类的构造方法
//     注意：
//     1. 成员变量创建当前对象时会报错：StackOverflowError
//        private ClassOrder classOrder = new ClassOrder();
//     2. 静态变量也可以被继承
//     3. 子类中如果重写了父类中的任何方法和成员变量，在子类中使用时都会使用子类的

public class ClassOrder extends ClassOrderParent {

    public int p = 1;

    //private ClassOrder classOrder = new ClassOrder();

    //    private ClassOrder classOrder=new ClassOrder();
    public ClassOrder() {
        System.out.println("ClassOrder 静态值： a=" + a);
        System.out.println("ClassOrder 成员变量 b=" + b);
        //new ClassOrderParent();
    }

    public static int a = 123;

    static {
        System.out.println("===============children====静态代码块==================");
        System.out.println("==================================================a=" + a);
        System.out.println("=================================getStatic=" + getStatic());
        System.out.println("===============children====静态代码块==================");
    }

    @Override
    public void test() {
        System.out.println("=======children-test==============================");
    }

    public static int getStatic() {
        return a;
    }

    public static void main(String[] args) {
        //System.out.println("================================="+ ClassOrder.a);
        ClassOrder classOrder = new ClassOrder();
//        //System.out.println(classOrder.p);
////        ClassOrderParent classOrder=new ClassOrderParent();
////        classOrder.print();
////        ClassOrderParent classOrder=new ClassOrder();
//        classOrder.print();
//        System.out.println(ClassOrder.a);
         classOrder.test2();
    }
}
