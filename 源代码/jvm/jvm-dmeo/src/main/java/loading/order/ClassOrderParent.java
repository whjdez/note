package loading.order;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/4/19 11:43
 * @updateDate 2021/4/19 11:43
 **/
public class ClassOrderParent {
    static int a = 456;
    public int p = 2;
    int b = 0;

    static {
        System.out.println("=======ClassOrderParent===========静态代码块======a=" + a);
    }

    public ClassOrderParent() {
        System.out.println("=======ClassOrderParent===构造方法==============a=" + a + ",p=" + p);
    }

    public void print() {
        System.out.println("=======parent===========print方法======");
        this.test();
    }


    /**
     * 验证子类如果继承方法中调用的成员变量后，会使用哪个
     */
    public void test2() {
        System.out.println(" 父类：test2，a=" + a);
    }

    public void test() {
        System.out.println("=======parent-test=====================");
    }
}
