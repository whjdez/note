package reload.classloader.test;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import sun.applet.AppletClassLoader;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/4/14 14:15
 * @updateDate 2021/4/14 14:15
 **/
public class ConfigWatcher {
    //监控新增文件夹及其子文件夹
//    public static void WatchNewDir(Path newDir) {
//        try {
//            newDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY,
//                    StandardWatchEventKinds.ENTRY_DELETE,StandardWatchEventKinds.OVERFLOW);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        File[] files = newDir.toFile&#40;&#41;.listFiles();
//        for (int i = 0; i &lt; files.length; i++) {
//            if(files[i].isDirectory())
//                WatchNewDir(files[i].toPath());
//        }
//    }


    public static void makeWatch(Path targetPath){
        try {
            WatchService watchService = targetPath.getFileSystem().newWatchService();
            Files.walkFileTree(targetPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException
                {
                    dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
                    return FileVisitResult.CONTINUE;
                }
            });
            ExecutorService es1 = Executors.newSingleThreadExecutor();
            es1.execute(()->{
                WatchKey watchKey = null;
                while (true) {
                    try {
                        watchKey = watchService.take();
                        List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
                        for (final WatchEvent<?> event : watchEvents) {
                            WatchEvent<Path> watchEvent = (WatchEvent<Path>) event;
                            WatchEvent.Kind<Path> kind = watchEvent.kind();
                            if(kind == StandardWatchEventKinds.ENTRY_CREATE){
                                Path watchable = ((Path) watchKey.watchable()).resolve(watchEvent.context());
//                                System.out.println("====================================name============"+watchable.toAbsolutePath());
//                                System.out.println("====================================context============"+watchable.getFileName());
                                if(Files.isDirectory(watchable)){
                                    watchable.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
                                }
                            }
                            Path watchable = ((Path) watchKey.watchable()).resolve(watchEvent.context());
                            System.out.println("====================================絕對路徑============"+watchable.toAbsolutePath());
                            System.out.println("====================================name============"+kind.name());
                            System.out.println("====================================context============"+watchEvent.context());
                        }
                        //consumer.accept(targetPath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }finally {
                        if(watchKey != null){
                            watchKey.reset();
                        }
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static WatchService watchService;

    @PostConstruct
    public void init() {
        System.out.println("启动配置文件监控器");
        try {
            watchService = FileSystems.getDefault().newWatchService();
            URL url = ConfigWatcher.class.getResource("/");
            System.out.println("======================监控地址============"+url.toURI());
            Path path = Paths.get("D:\\个人信息\\学习笔记\\node\\源代码\\jvm\\jvm-dmeo\\src\\main\\java");
            path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        /**
         * 启动监控线程
         */
        Thread watchThread = new Thread(new WatchThread());
        watchThread.setDaemon(true);
        watchThread.start();

        /**注册关闭钩子*/
        Thread hook = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    watchService.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        Runtime.getRuntime().addShutdownHook(hook);
    }

    public class WatchThread implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    // 尝试获取监控池的变化，如果没有则一直等待
                    WatchKey watchKey = watchService.take();
                    for (WatchEvent<?> event : watchKey.pollEvents()) {
                        String editFileName = event.context().toString();
                        System.out.println(editFileName);
                        /**
                         * 重新加载配置
                         */
                    }
                    watchKey.reset();//完成一次监控就需要重置监控器一次
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
       // ConfigWatcher watcher=new ConfigWatcher();
        Path path = Paths.get("D:\\个人信息\\学习笔记\\node\\源代码\\jvm\\jvm-dmeo\\src\\main\\java");
        ConfigWatcher.makeWatch(path);
        //watcher.
        // watcher.init();
        Thread.sleep(10000000);
    }
}
