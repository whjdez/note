package reload.classloader;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/4/13 17:29
 * @updateDate 2021/4/13 17:29
 **/
public interface BaseManager {
     void logic();
}
