package whj.demo.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/21 11:47
 * @updateDate 2020/12/21 11:47
 **/
@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        System.out.println("============================================================================"+passwordEncoder.encode("123456"));
        System.out.println("=============================================================================");
        UserDetails userDetails= User.withUsername("whj")
                //配置下面后就不用配password的is
                //@Bean
                //public PasswordEncoder passwordEncoder(){ return new BCryptPasswordEncoder(); }
                //.password("{noop}123456")
                .password(passwordEncoder.encode("123456"))
                .authorities("admin")
                .build();

        return userDetails;
    }
}
