package whj.demo.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/21 11:42
 * @updateDate 2020/12/21 11:42
 **/
@RestController
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/demo")
    public String demo() {
        return "spring security demo";
    }
}
