package com.whj.hwod;

import java.util.Scanner;

/**
 * @ClassName Code1
 * @Description
 * @Author WangHaiJing
 * @Date 2024/12/30
 */

//题目描述
//存在一种虚拟IPv4地址，由4小节组成，每节的范围为0~255，以#号间隔，虚拟IPv4地址可以转换为一个32位的整数，例如：
//
//        128#0#255#255，转换为32位整数的结果为2147549183（0x8000FFFF）
//
//        1#0#0#0，转换为32位整数的结果为16777216（0x01000000）
//
//现以字符串形式给出一个虚拟IPv4地址，限制第1小节的范围为1128，即每一节范围分别为(1128)#(0255)#(0255)#(0~255)，要求每个IPv4地址只能对应到唯一的整数上。如果是非法IPv4，返回invalid IP
//
//输入描述
//输入一行，虚拟IPv4地址格式字符串
//
//        输出描述
//输出一行，按照要求输出整型或者特定字符
//
//        示例1
//输入
//
//100#101#1#5
//        1
//输出
//
//1684340997
//        1
//说明
//
//        示例2
//输入
//
//1#2#3
//        1
//输出
//
//invalid IP
//1
//说明
//
// 解题思路
//虚拟IPv4地址由四个小节组成，每个小节用#号分隔。在这个虚拟版本中用#替代。每个小节代表一个整数，范围从0到255，但题目中特别指出第一小节的范围应为1到128。地址的正确形式应该是四部分，例如 1#2#3#4。如果格式不正确或数值不在指定范围内，则视为非法IPv4，输出“invalid IP”。
//
//解题注意事项
//异常处理：
//
//确保输入的每一部分（小节）都是数字。
//确保没有空的小节，例如1##3#4。
//处理任何非数字字符，例如a#b#c#d。
//检查是否每个部分都严格为数字，并且没有前导零（除了单独的0），例如01#01#01#01应被视为非法。
//范围验证：
//
//第一小节必须在1到128之间。
//其余三小节必须在0到255之间。
//任何超出这些范围的值都应该导致输出“invalid IP”。
//格式正确性：

// 确保地址严格由四个数字小节组成，多于或少于四部分都应视为无效。
public class Code1 {
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
//        while (true) {
//            System.out.println("请输入虚拟IPv4地址格式字符串：[0~255]#[0~255]#[0~255]#[1~128]");
//            String input = scanner.nextLine();
//            if(input.equals("end")){
//                break;
//            }
//
//        }
        Code1 code1 = new Code1();
        // 1684340997
        System.out.println(code1.getIPv4Address("100#101#1#5"));
        // 2147549183
        System.out.println(code1.getIPv4Address("128#0#255#255"));
        // 16777216
        System.out.println(code1.getIPv4Address("1#0#0#0"));
        // invalid IP
        System.out.println(code1.getIPv4Address("1#2#3"));
        System.out.println(code1.getIPv4Address("1#2# #4"));
        System.out.println(code1.getIPv4Address("1#2#3#4#5"));
    }


    public Long getIPv4Address(String ipv4Address) {
        String[] parts = ipv4Address.split("#");
        long result = -1;
        if (parts.length != 4) {
            System.err.println("invalid IP");
            return result;
        }
        // 验证每部分是否为数字
        for (int i = 0; i < parts.length - 1; i++) {
            String part = parts[i];
            // 验证是否为数字
            if (!part.matches("\\d+")) {
                System.err.println("invalid IP");
                return result;
            }
            // 验证是否有前导零
            if (part.length() > 1 && part.startsWith("0")) {
                System.err.println("invalid IP");
                return result;
            }
            if (i > 0) {
                // 验证范围
                if (Integer.parseInt(part) < 0 || Integer.parseInt(part) > 255) {
                    System.err.println("invalid IP");
                    return result;
                }
            } else {
                // 验证范围
                if (Integer.parseInt(part) < 1 || Integer.parseInt(part) > 128) {
                    System.err.println("invalid IP");
                    return result;
                }
            }
        }
        // 转成32位整数
        result = 0;
        for (String s : parts) {
            long part = Long.parseLong(s);
            result = result * 256 + part;
        }
        return result;
    }


}
