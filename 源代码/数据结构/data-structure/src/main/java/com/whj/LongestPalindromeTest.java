package com.whj;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/21 9:15
 * @updateDate 2020/12/21 9:15
 **/
public class LongestPalindromeTest {

    /**
     * 给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。
     * <p>
     * 示例 1：
     * <p>
     * 输入: "babad"
     * 输出: "bab"
     * 注意: "aba" 也是一个有效答案。
     * 示例 2：
     * <p>
     * 输入: "cbbd"
     * 输出: "bb"
     * <p>
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/longest-palindromic-substring
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     *
     * @param s
     * @return
     */
    public static String longestPalindrome(String s) {

        char[] chs = s.toCharArray();

        int startIndex = 0;
        int endIndex = 0;

        String str1 = "", str2 = "";

        boolean flag = false;
        for (int i = 0; i < chs.length; i++) {
            if(flag){
                break;
            }
            endIndex = chs.length - 1;
            for (int j = i; j < chs.length; j++) {
                //
                if (j >= endIndex) {
                    flag = true;
                    startIndex = i;
                    endIndex = chs.length - 1;
                    break;
                }
                if (chs[j] == chs[endIndex]) {
                    endIndex--;
                }else{
                    break;
                }
            }
        }
        if (flag) {
            str1 = s.substring(startIndex, endIndex+1);
            flag=false;
        }

        for (int i = chs.length - 1; i >= 0; i--) {
            if(flag){
                break;
            }
            startIndex = 0;
            for (int j = i; j >= 0; j--) {
                if (startIndex >= j) {
                    endIndex = i;
                    startIndex = 0;
                    flag=true;
                    break;
                }
                if (chs[j] == chs[startIndex]) {
                    startIndex++;
                }else{
                    break;
                }
            }
        }
        return str1.length() > endIndex - startIndex ? str1 : s.substring(startIndex, endIndex+1);
    }

    public static void main(String[] args) {
        System.out.println(longestPalindrome("dfgabaaaghjghj"));
    }
}
