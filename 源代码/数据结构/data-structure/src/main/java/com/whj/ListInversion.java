package com.whj;

import java.util.Stack;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2021/7/13 14:36
 * @updateDate 2021/7/13 14:36
 * <p>
 * 描述
 * 输入一个链表，反转链表后，输出新链表的表头。
 * 示例1
 * 输入：
 * {1,2,3}
 * 返回值：
 * {3,2,1}
 **/
public class ListInversion {


    public static ListNode ReverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode lastNode = null;
        ListNode nowNode = head;
        while (head != null && head.next != null) {
            head = head.next;
            nowNode.next = lastNode;
            lastNode = nowNode;
            nowNode = head;
        }
        nowNode.next = lastNode;
        return nowNode;
    }

    static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        listNode1.next = listNode2;
        listNode1.next.next = listNode3;
        ReverseList(listNode1);
    }

}


