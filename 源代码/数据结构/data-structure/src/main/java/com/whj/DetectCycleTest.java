package com.whj;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author WangHaiJing
 * @version 1.0
 * @description
 * @updateRemark
 * @updateUser
 * @createDate 2020/12/15 8:49
 * @updateDate 2020/12/15 8:49
 **/
public class DetectCycleTest {
    /**
     * 给定一个链表，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。
     * <p>
     * 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。
     * <p>
     * 如果 pos 是 -1，则在该链表中没有环。注意，pos 仅仅是用于标识环的情况，并不会作为参数传递到函数中。
     * <p>
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/linked-list-cycle-ii
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
        public ListNode detectCycle(ListNode head) {
            Map<ListNode,Object> map=new HashMap(16);
            ListNode rt=null;
            if (head == null) {
                return null;
            }
            while(head.next!=null&&head.next.next!=null){
                map.put(head,null);
                head=head.next;
                if(map.containsKey(head)){
                    return head;
                }
            }
            return null;
        }
    }
class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }
}