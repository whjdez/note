[TOC]

# idea快捷键

ctrl+n  查找类

shirt+ctrl+r 查找字符

shirt+ctrl+空格 提示

ctrl+alt + 左右键  返回上一步或者下一步

Step Over (F8)：步过，一行一行地往下走，如果这一行上有方法不会进入方法。

 Step Into (F7)：步入，如果当前行有方法，可以进入方法内部，一般用于进入自定义方法内，不会进入官方类库的方法

Show Execution Point (Alt + F10)：如果你的光标在其它行或其它页面，点击这个按钮可跳转到当前代码执行的行。

 Force Step Into (Alt + Shift + F7)：强制步入，能进入任何方法，查看底层源码的时候可以用这个进入官方类库的方法。

 Step Out (Shift + F8)：步出，从步入的方法内退出到方法调用处，此时方法已执行完毕，只是还没有完成赋值。

 Drop Frame (默认无)：回退断点，后面章节详细说明。

 Run to Cursor (Alt + F9)：运行到光标处，你可以将光标定位到你需要查看的那一行，然后使用这个功能，代码会运行至光标行，而不需要打断点。

 Evaluate Expression (Alt + F8)：计算表达式

ctrl+alt+h:方法引用更详细的调用关系图

ctrl+f7:方法调用关系图

# 数据结构

## 1.算法复杂度计算

### 1.1 时间复杂度 

一个算法语句总的执行次数是关于问题规模N的某个函数，记为f(N)，N称为问题的规模。语句总的执行次数记为T(N)，当N不断变化时，T(N)也在变化，算法执行次数的增长速率和f(N)的增长速率相同。则有T(N) = O(f(N))，这称作算法的渐进时间复杂度，简称时间复杂度。

#### 1.1.1 时间复杂度计算方式

1.找出算法中的基本语句。算法中执行次数最多的那条语句就是基本语句，通常是最内层循环的循环体

2.计算基本语句的执行次数

只需计算基本语句执行次数的数量及（忽略所有低次幂和最高次幂的系数）

**例子：**

```
void aFunc(int n) {
    for(int i = 0; i < n; i++) {          
        printf("Hello, World!\n");  //t(n)=n+1+1=n+2   所以 时间复杂度为 O(N)     
    }
}


void aFunc(int n) {
    for(int i = 0; i < n; i++) {          
        for(int j = 0; j < n; j++) {        
            printf("Hello, World!\n");    //T(n)=n+n^2+2+n^2=2n^2+2
            							//O（n^2）
        }
    }
}

void aFunc(int n) {
    if (n >= 0) {
         for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                printf("输入数据大于等于零\n"); //O(n^2)
            }
        }
    } else {
         for(int j = 0; j < n; j++) {
            printf("输入数据小于零\n");
        }
    }
}

void aFunc(int n) {
    for (int i = 2; i < n; i++) {
        i *= 2;
        printf("%i\n", i);        //T(n)=2log
        						//O(log)
    }
}

```

常见排序算法时间复杂度

![](images/常见算法时间复杂度.png)



### 1.2 空间复杂度

个程序的空间复杂度是指运行完一个程序所需内存的大小，利用程序的空间复杂度，可以对程序的运行所需要的内存多少有个预先估计。

## 2.数组与链表

### 2.1 线性表

  由一维数据组成，java中常见线性表有ArrayList linkedList。

### 2.2 链表

链表是一种物理上地址非连续、非顺序的存储结构

数据元素的逻辑顺序是通过链表中的指针链接次序实现

链表由一系列结点组成

每个结点包括两个部分：一个是存储[数据元素](https://baike.baidu.com/item/%E6%95%B0%E6%8D%AE%E5%85%83%E7%B4%A0)的数据域，另一个是存储下一个结点地址的[指针](https://baike.baidu.com/item/%E6%8C%87%E9%92%88/2878304)域



个人模拟实现：

```
public class ArrayList<E> implements List<E> {

	private static final int DEFAULT_CAPACITY = 10;

	private E[] data;
	private int size;

	public ArrayList(int capacity) {
		this.size = 0;
		data = (E[]) new Object[capacity];
	}

	public ArrayList(E[] arr) {
		data = (E[]) new Object[arr.length];
		for (int i = 0; i < arr.length; i++)
			data[i] = arr[i];
		this.size = arr.length;
	}

	public ArrayList() {
		this(DEFAULT_CAPACITY);
	}

	// O(1)
	@Override
	public int getSize() {
		return size;
	}

	// O(1)
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	// O(n)
	@Override
	public boolean contains(E o) {
		for (int i = 0; i < size; i++) {
			if (data[i].equals(o))
				return true;
		}

		return false;
	}

	// O(n)
	@Override
	public int indexOf(E e) {
		for (int i = 0; i < size; i++) {
			if (data[i].equals(e))
				return i;
		}

		return -1;
	}

	// O(1)
	@Override
	public E get(int index) {
		if (index < 0 || index >= size)
			throw new IllegalArgumentException("数组小标越界...");

		return data[index];
	}

	// O(1)
	@Override
	public void set(int index, E e) {
		if (index < 0 || index >= size)
			throw new IllegalArgumentException("数组小标越界...");

		data[index] = e;
	}

	// O(n)
	@Override
	public void add(int index, E e) {
		if (index < 0 || index > size) {
			throw new IllegalArgumentException("数组小标越界...");
		}

		if (size == data.length) {
			grow(2 * data.length);
		}

		for (int i = size - 1; i >= index; i--) {
			data[i + 1] = data[i];
		}

		data[index] = e;

		size++;

	}

	// 均摊时间复杂度 O(1)
	public void addLast(E e) {
		add(size, e);
	}

	// O(n)
	@Override
	public E remove(int index) {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("数组小标越界...");
		}

		E val = data[index];

		for (int i = index + 1; i < size; i++) {
			data[i - 1] = data[i];
		}

		size--;

		data[size] = null;

		if (size < (data.length >> 1)) {
			grow(data.length / 2);
		}

		return val;
	}

	private void grow(int newCapacity) {
		/*
		 * E[] newData = (E[]) new Object[newCapacity]; for(int i=0; i<data.length; i++)
		 * newData[i] = data[i];
		 * 
		 * data = newData;
		 */
		data = Arrays.copyOf(data, newCapacity);
	}

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();

		for (int i = 0; i < 100; i++)
			list.add(i, i);

		for (int i = 0; i < 100; i++) {
			System.out.println("The " + i + "th element is: " + list.get(i));
		}

		for (int i = 0; i < 50; i += 8) {
			list.remove(i);
		}

		for (int i = 0; i < list.getSize(); i++) {
			System.out.println("After removing, the " + i + "th element is: " + list.get(i));
		}
	}
}
```

```
package com.yyschool.demo.ds.list;

public class LinkedList<E> implements List<E> {

	private class Node {
		private E data; //数据域
		private Node next; //指针域，指向下一个Node
		
		public Node(E data, Node next) {
			this.data = data;
			this.next = next;
		}
		
		public Node(E data) {
			this(data, null);
		}
		
		public String toString() {
			return data.toString();
		}
	}
	
	
	private Node head;
	//private Node tail;
	private int size;
	
	public LinkedList() {
		head = null;
		size = 0;
	}
	
	
	//O(1)
	@Override
	public int getSize() {
 		return size;
	}

	//O(1)
	@Override
	public boolean isEmpty() {
 		return size == 0;
	}

	//O(n)
	@Override
	public boolean contains(E o) {
		Node p = head;
		while(p != null) {
			if(p.data.equals(o))
				return true;
			p = p.next;
		}
 		return false;
	}

	//O(n)
	@Override
	public int indexOf(E e) {
		
		int result = -1;
		Node p = head;
		int i = 0;
		while(p != null) {
			if(p.data.equals(e))
			{
				result = i;
				break;
			}
 			p = p.next;
			i++;
		}
 		return result;
	}

	//O(n)
	@Override
	public E get(int index) {
		if(index<0 || index >= size) {
			throw new IllegalArgumentException("非法下标...");
		}
		
		Node p = head;
		for(int i=0; i<index; i++)
			p = p.next;
		
		return p.data;
	}

	//O(n)
	@Override
	public void set(int index, E e) {
		if(index<0 || index >= size) {
			throw new IllegalArgumentException("非法下标...");
		}
		
		Node p = head;
		for(int i=0; i<index; i++)
			p = p.next;
		
		 p.data = e;		
	}

	//O(n)
	@Override
	public void add(int index, E e) {
		if(index < 0 || index > size) {
			throw new IllegalArgumentException("下标越界.....");
		}
		
		//插到链表头部
		if(index == 0) {
			addFirst(e);
		}else if(index == size) {
			addLast(e);
		}else {
			Node prev = head;
			for(int i=0; i<index; i++) {
				prev = prev.next;
			}
			
			prev.next = new Node(e, prev.next);
			
			size++;
		}
 		
	}
	
	//O(1)
	public void addFirst(E e) {
		Node node = new Node(e, head);
		head = node;
		
		size++;
	}
	
	//O(n)
	public void addLast(E e) {
		Node node = new Node(e, null);
		
		//链表为空
		if(head == null) {
			head = node;
		}else {
			Node prev = head;
			while(prev.next != null) {
				prev = prev.next;
			}
			
			prev.next = node;
		}
		
		size++;
	}

	//O(n)
	@Override
	public E remove(int index) {
		
		if(index<0 || index >= size) {
			throw new IllegalArgumentException("非法下标...");
		}
		
		if(index == 0) {
			return removeFirst();
		}else if (index == size -1) {
			return removeLast();
		}else{
			
			Node prev = head;
			for(int i=0; i<index-1; i++)
				prev = prev.next;
			
			Node tmp = prev.next;
			prev.next = tmp.next;
			tmp.next = null;
			size--;
			
			return tmp.data;
					
			
		}
 	}
	
	//O(1)
	public E removeFirst() {
		if(head == null)
			return null;
		
		E result = head.data;
		head = head.next;
		size--;
		
		return result;
	}
	
	//O(n)
	public E removeLast() {
		if(head == null)
			return null;
		
		E result;
		
		//链表只有一个节点
		if(head.next == null) {
			result = head.data;
			head = null;
		}else {
			
			Node prev = head;
			while(prev.next.next != null)
				prev = prev.next;
			
			result = prev.next.data;
			prev.next = null;
		}
		
		size--;
		return result;

	}
	
	public static void main(String[] args) {
		List<Integer> list = new LinkedList<>();
		
		for(int i=0; i<10; i++)
			list.add(i, i);
		
		for(int i=0; i<10; i++) {
			System.out.println("The " + i + "th element is: " + list.get(i));
		}
		
		list.remove(5);
		
		for(int i=0; i<list.getSize(); i++) {
			System.out.println("After removing, the " + i + "th element is: " + list.get(i));
		}
		
	}

}

```

## 3 常见运算及应用

### 3.1 按位与运算符（&）

参与运算的两个数据，按二进制位进行“与”运算

运算规则 ：0&0=0 0&1=0 1&0=0 1&1=1

即两位同时为"1",结果才为1 否则为0

**实际应用案例：**

1.hashmap 中通过&运算将具体的key hash值定位到数组下标上，在计算的过程中可以与%运算互换结论如下

　**当 lenth = 2n 时，X % length = X & (length - 1)**

　　也就是说，长度为2的n次幂时，模运算 % 可以变换为按位与 & 运算。

　　比如：9 % 4 = 1，9的二进制是 1001 ,4-1 = 3,3的二进制是 0011。 9 & 3 = 1001 & 0011 = 0001 = 1

　　再比如：12 % 8 = 4,12的二进制是 1100,8-1 = 7,7的二进制是 0111。12 & 7 = 1100 & 0111 = 0100 = 4

　　上面两个例子4和8都是2的n次幂，结论是成立的，那么当长度不为2的n次幂呢？



**2.判断int型变量a是奇数还是偶数** 

a&1 = 0 偶数  

a&1 = 1 奇数

**3.求平均值**

比如有两个int类型变量x、y,首先要求x+y的和，再除以2，但是有可能x+y的结果会超过int的最大表示范围，所以位运算就派上用场啦。  
(x&y)+((x^y)>>1);

对于一个大于0的整数，判断它是不是2的几次方  
((x&(x-1))==0)&&(x!=0)；

**4.与取模运算的装换（1中有）**

取模运算，采用位运算实现：  

a % (2^n) 等价于 a & (2^n - 1)

### 3.2 按位或运算符（|）

运算规则：0|0=0 , 0|1=1, 1|0=1 ,1|1=1

参与运算的只要有一个是1，其值为1

### 3.3 异或运算符（^）

运算规则：0^0=0;0^1=1;1^0=1;1^1=0;

参与运算的两个对象，如果两个数不同则结果是1

**应用**

**1.交换值**

比如有两个int类型变量x、y,要求两者数字交换，位运算的实现方法：性能绝对高效  

x ^= y;  

y ^= x;  

x ^= y;



### 3.4 取反运算符（~）

10

### 3.5 左移运算（<<）

将一个运算对象的各二进制位全部左移移若干位

列子:a=a<<2 将a的二进制位左移2位，右补0

左移1位后a=a*2;

若左移时舍弃的高位不包括1，则每左移一位相当于该数乘以2.

**应用**

乘法运算 采用位运算实现  

a * (2^n) 等价于 a << n

### 3.6 右移运算符(>>)

将一个数的各二进制全部右移若干位，正数左补0，负数左补1，右边丢弃

操作数每右移一位，相当于该数除以2.

例如：a = a>> 2 将a的二进制位右移2位，

应用

**1.求绝对值**

int abs( int x )  

{  

int y ;  

y = x >> 31 ;  

return (x^y)-y ; //or: (x+y)^y  

}

2.运算转换

除法运算转化成位运算  

a / (2^n) 等价于 a>> n

## 4 栈与队列

#### 4.1 栈

##### 4.1.1栈的结构

栈是一种线性表的特殊表现形式，按照后进先出的原则处理数据。

栈的操作基本只有两个：

1.入栈（push）即将数据保存到栈顶。进行该操作前，先修改栈顶指针，使其向上移动一个元素位置，然后将数据保存到栈顶指针所指的位置

2.出栈（pop）即将栈顶的数据弹出，然后修改栈顶指针，使其指向栈中的下一个元素

##### 4.1.2 栈的基本操作和实现

![](images/栈的基本操作.png)

### 4.2 队列

#### 4.2.1 队列结构

队列是一种特殊的线性表（先进先出）

只允许在表的前端进行删除操作，在表的后端进行插入操作

进行查处操作的端称为队尾，进行删除的操作的端称为队头

## 5.排序算法

### 5.1 冒泡排序

1.每次两两比较交换位置，选出剩余组里最大（最小的元素放在数组尾部）

![](images/冒泡排序.png)

代码实现如下

```
public int [] bubbleSort2(int[] nums){
        int s=0;
        if(nums.length==0){
            return nums;
        }
        for(int i=0;i<nums.length;i++){
            for(int j=0;j<nums.length-1-i;j++){
                ++s;
                if(nums[j+1]<nums[j]){
                    int temp=nums[j+1];
                    nums[j+1]=nums[j];
                    nums[j]=temp;
                }
            }
        }
        return nums;
    }
```

### 5.2 插入排序

```
* 插入排序（Insertion-Sort）的算法描述是一种简单直观的排序算法。它的工作原理是通过构建有序序列，对于未排序数据，
* 在已排序序列中从后向前扫描，找到相应位置并插入。插入排序在实现上，通常采用in-place排序（即只需用到O(1)的额外空间的排序），
* 因而在从后向前扫描过程中，需要反复把已排序元素逐步向后挪位，为最新元素提供插入空间。
```

代码实现

```
   public int[] insertSort(int []arr)
        if(arr.length<=1){
            return arr;
        }
        int current;
        for(int i=0;i<arr.length-1;i++){
            current=arr[i+1];    //待比较数
         
            int preIndex=i; //当前下标
            
            //使用当前下标的数据和current进比较，如果比较数小于当前数说明，需要交换，则将比当前数大的
            //向后移动，重复执行以上操作
            while (preIndex>=0&&current<arr[preIndex]){
                arr[preIndex+1]=arr[preIndex];
                preIndex--;
            }
            //跳出循环时说明，已经找到数据的位置
            arr[preIndex+1]=current;
        }

        return arr;
    }
```

### 5.3 选择排序

```
每一趟从待排序的记录中选出最小的元素，顺序放在已排好序的序列最后，直到全部记录排序完毕
```

代码实现如下

```
    public int [] optionSort2(int [] arr){
        if(arr.length==0){
            return arr;
        }
        for(int i=0;i<arr.length;i++){
            int minIndex=i;
            for(int j=i;j<arr.length;j++){
                if(arr[j]<arr[minIndex]){
                    minIndex=j;
                }
            }
            int temp=arr[minIndex];
            arr[minIndex]=arr[i];
            arr[i]=temp;
        }
        return arr;
    }
```

### 5.4 归并排序

代码形式 ，自己调用自己，内部需要停止条件否则，会出现永远不会停止的问题。执行顺序类似与栈

后进先出列子；

```

    public static  int test(int couot,int index ){
        if(index <= 0){
            return 0;
        }
        int s=test(couot+1,  index-1);
        System.out.println("执行次数=》"+couot+"====执行顺序=="+index);
        return s;
    }
===============================================================================================    
执行次数=》9====执行顺序==1
执行次数=》8====执行顺序==2
执行次数=》7====执行顺序==3
执行次数=》6====执行顺序==4
执行次数=》5====执行顺序==5
执行次数=》4====执行顺序==6
执行次数=》3====执行顺序==7
执行次数=》2====执行顺序==8
执行次数=》1====执行顺序==9
执行次数=》0====执行顺序==10
```

从以上执行结果可以看出，执行顺序是按照栈的方式来执行的，只有达到结束条件后，方法体之后的内容才可执行。





1.先将每个子数组有序，再使得子数组间有序

2.将已有序的子数组合并

```
   public int[] sort(int [] arr){
        if(arr==null||arr.length<=1){
            return arr;
        }
        //提前设置临时数组，节省空间
        int[] tmp=new int[arr.length];

        //将数据进行分割
        sortHelper(arr,0,arr.length-1,tmp);
        return arr;
    }

    private void sortHelper(int[] arr, int start, int end, int[] tmp) {
        if(start>=end){
            return;
        }
        int mid=(start+end)/2;

        //通过递归的方式将数据分割成左右两部分，         
        sortHelper(arr,start,mid,tmp);
        sortHelper(arr,mid+1,end,tmp);

        //分割完成后进行合并（真正的排序在内部进行）
        merge(arr,start,mid,mid+1,end,tmp);


    }

    private void merge(int[] arr, int s1, int e1, int s2, int e2, int[] tmp) {

        //设置临时变量，k为合并后的数量
        int i=s1,j=s2,k=0;

        //对左右两部分数据进行合并(分别从左右两边数组中获取数据，
        // 如果左边小，就将左边数据放入tmp中，反之亦然)
        while (i <= e1 && j <= e2){
            //
            if(arr[i] <= arr[j]){
                tmp[k++]=arr[i];
                i++;
            }else{
                tmp[k++]=arr[j];
                j++;
            }
        }
        //比较完后如果左边还剩数据，则将左边数据放入临时数组中
        while (i<=e1){
            tmp[k++]=arr[i];
            i++;
        }

        //比较完后如果右边还剩数据，则将右边数据放入临时数组中
        while (j<=e2){
            tmp[k++]=arr[j];
            j++;
        }

        //将临时数据放入原数组
        for(int z=0;z<k;z++){
            arr[s1+z]=tmp[z];
        }
    }

    public static void main(String[] args) {
        int[]arr={3,4,6,1,2,3,14,333,123,0,-1};
        MergeSorter s=new MergeSorter();
        s.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
```

### 5.5 快速排序



快速排序的基本思想如下：

从数组中取出一个数作为中轴(pivot)

划分数组：将比这个数大的放到它的右边，小于或等于它的数放到它的左边

再对左右区间重复上述步骤，直到各个区间只有一个数

实现方式 一般有三种：1，同向的两个指针，2 头尾各一个指针 3 头尾 中三个指针

![](images/快速排序.png)

```
public static void sort(int [] arr){

    quickSort(arr,0,arr.length-1);
}

private static void quickSort(int[] arr, int low, int high) {
    if(low>=high){
        return;
    }
    int mid = getmidle(arr,low,high);
    quickSort(arr,low, mid );
    quickSort(arr, mid+1,high);

}

private static int getmidle(int[] arr, int low, int high) {
    int temp=arr[low];

 while(low < high){
        while (low < high&&arr[high]>=temp){
           high--;
        }
        arr [low] = arr [high];
        while (  low < high&&arr [low] <= temp){
            low++;
        }
        arr[high]=arr[low];
    }

    arr [low]=temp;
    return low;

}

public static void main(String[] args) {
    int [] arr={
            23,1,3,456,3,2,6,4,-3,0
    };
    sort(arr);
    System.out.println(Arrays.toString(arr));
}
```





1.同向指针实现

设定两个指针 i ,j

```
public class QuickSorter {
    public int[] sort(int [] arr){
        if(arr==null||arr.length<=1){
            return arr;
        }
        sortHelper(arr,0,arr.length-1);
        return arr;
    }

    /**
     *  两个同向
     * @param arr
     * @param start
     * @param end
     */
    private void sortHelper(int[] arr, int start, int end) {
        if(start>=end){
            return;
        }

        //找基准数据 通过头部 中部 尾部 找出 中间数，防止 找到最大 或者最小数
        int medianIndex=medianof3Nnms(arr,start,(end-start)/2,end);

        //将开始数与中位数交换,为之后使用
        swap(arr,start,medianIndex);
        //进行查找替换
        int index=partition(arr,start,end);

        //将数组左右两边进行分组
        sortHelper(arr,start,index-1);
        sortHelper(arr,index+1,end);
       
    }

    private int medianof3Nnms(int[] arr, int lo, int center, int hi) {

        if(arr[lo]<arr[center]){
            if(arr[center]<arr[hi]){
                return center;
            }else{
                return (arr[lo]<arr[hi])?hi:lo;
            }
        }else{
            if(arr[hi]<arr[center]){
                return center;
            }else{
                return (arr[hi]>arr[lo])?hi:lo;
            }
        }
    }

    private int partition(int[] arr, int start, int end) {

        //基准数，用该数进行比较，比较完成后处于左边比该数小，右边大
            int pivot=arr[start];

            //第一个指针 ，用来当做小数的指针
            int i=start+1;

            //第二个指针，用来当做大数的指针，当该指针选择的数比中位数小时会和小数指针交换
            for(int j=start+1;j<=end;j++){

                if(arr[j]<=pivot){
                    swap(arr,i,j);
                    i++;
                }

            }
            //当比较完成后，会将第一个中位数与i-1交换，一般i-1位中心
            swap(arr,start,i-1);

            return i-1;
    }

    private void swap(int[] arr, int i, int j) {
        int tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
    }

    public static void main(String[] args) {
        QuickSorter q=new QuickSorter();

        int[]arr={3,4,6,1,2,3,14,333,123,0,-1};
        q.sort(arr);
        System.out.println(Arrays.toString(arr));

    }
```

## 6.B树和B+树

### 6.1  2-3树

1.2-3 树时一种多路查找树：2,3的意思是 包含两个节点：

2节点包含一个元素和两个孩子（或者没有孩子）

2.1 左子树包含的节点的元素小于该节点的元素值。，右节点包含的节点的元素值大于该节点的元素值

2.2 2节点要不有两个节点，要不没有孩子，不允许有一个孩子

3   3节点包含一大一小两个元素和三个孩子（或者没有孩子）。两个元素按大小排列好：

3.1左子树包含的节点的元素值小于该节点较小的元素值，右子树包含的节点大元素大于该节点较大的元素值，中间子树包含的节点的元素值介于这两个元素之间

3.2 3节点要不有三个孩子，要不没有孩子，不允许有一个或者两个孩子

4. 2-3树所有的叶子节点都在同一层

![](images/QQ图片20190911194308.png)



## 6.2 B树



​	

b树是一种平衡的多路查找树，2-3树都是b树的特例，我们把树中节点最大的孩子数称为b树的阶。通常记为m。一个m阶b树或为空树，或为满足如下特性的m叉树。

1.树中每个节点至多有m颗子树（即至多含m-1个关键字）两颗子树指针夹着一个关键字。

2.若根节点不是终端节点，则至少有两棵子树（至少一个关键字）

3.除根节点外的所有非叶子节点最少有m/2+1 棵子树。



5.所有的叶子节点出现在同一层次上，不带信息

**重点：2,3** 

### 6.2.1 查找

用查找的数据与关键字进行比较，等于则查找成功，如果小于则去左边下一个节点找，大于则去右边下一个节点去找，直到叶子节点

### 6.2.2  插入

![](images/QQ截图20190911205658插入列子.png)

![](images/20190911210033插入2.png)

![](images/20190911210509插入3.png)

![](images/QQ截图20190911210741插入4.png)

从上面的列子来总结b树的构建过程

1.先确定树的阶，确定好之后就可以确定每一层可以存放几个元素 （例：3阶     3/2+1   每一层最少放一个，最多放3-1 个元素）

2.确定好之后就可以放数据，数据从叶子节点开始，有序插入，当元素数量超过规定的阶数时就会分裂（分裂过程：1.先确定要分裂的元素：m/2  例如 3/2=1 从从0 开始取中间的元素，该元素会留下，其他元素向下分 。根节点像下分裂，非根节点先向上合并，如果上级节点合并后不合符再进行分裂）









### 6.2.3 删除

https://www.bilibili.com/video/av36069871?from=search&seid=11823354691771280849

#### 6.2.3.1 终端节点删除

#### 6.2.3.2 非终端节点删除

## 6.3 B+树

b+树的的构成与b树基本相同也是一种多路树。不同点在于b树中非叶子节点会存储数据，b+树非叶子节点不会存储数据存储的是索引，数据都会在叶子节点体现如下

![](images/QQ截图20190913193910b+树.png)

叶子节点在存储值时使用链表结构来存储数据，叶子节点的值如果等于 则存到右边。

## 7.平衡二叉树

https://www.cnblogs.com/ricklz/p/10016050.html

产生原因：为了避免像全是左树或者全是右树的结构出现

平衡二叉树的特点：

1.可以是空树

2.假如不是空树，任何一个结点的左子树与右子树都是平衡二叉树，并且高度之差绝对不会超过1

### 7.1 失衡后的调整

1.最小失衡树：在新插入的节点向上找，以第一个平衡因子绝对值超过1的节点为根的子树。找到最小失衡树后判断是以下哪种类型，然后根据类型进行旋转使其平衡（**使用树高来作为参数**）

#### 7.1.1  左子树过高

- LL形

  首先找到最小不平衡子树，再根据其结构点右旋（

  向右旋转后，相当于右边的子树树高增加了1，而左边的子树降低了1

  ）

  ![1569908249415](images/1569908249415.png)


- LR

  对于LR形要分为两步进行旋转，旋转之后（原来根节点的做孩子的右还在未做新的根节点）

  第一步:先找到最小不平衡树然后将其节点与右节点交换，变成LL形

  第二步：以新插入节点为根进行右旋

  ![1569908624645](images/1569908624645.png)

#### 7.1.2  右子树过高

- RR形

 旋转后 “雨来的根节点的右字数作为新的根节点”

![1569911946982](images/1569911946982.png)

- RL型

  **第一步** ：将插入节点与父节点进行交换，转换成RR型

  **第二步**：以新节点为根节点左旋

## 8.红黑树

<https://blog.csdn.net/v_JULY_v/article/details/6105630>

https://www.cs.usfca.edu/~galles/visualization/Algorithms.html

红黑树的主要特点：**

- 1.每个节点要么是黑色，要么是红色（非红即黑）
- 2.根节点是黑色
- 3.每个叶子节点都是黑色
- 4.如果一个节点是红色，那么他的子节点必须是黑色（父子节点不能同时为红色）
- 5.从一个节点到该节点的子孙节点所有路径上包含的相同的黑节点数

如果不符合以上条件，就会出现变色旋转(节点插入时默认为红色)

![1569892026169](images/1569892026169.png)

**![1569891992612](images/1569891992612.png)**

![1569892216988](images/1569892216988.png)

![1569892357400](images/1569892357400.png)

![1569892510581](images/1569892510581.png)

![1569892611376](images/1569892611376.png)

![1569892633343](images/1569892633343.png)



## 9.集合框架

### 9.1 hashMap

1. 高并发下出现的问题

​        1.7的死链问题产生原因如下 ：

![1570599304814](images/1570599304814.png)



以上代码为将数据重新hash后放入新数组的逻辑实现：该逻辑为hash后如果当前位置有值则将当前位置的值作为新值得naxt节点，然后将新值放入，这种插入方法会导致节点进行反转。

产生死链的两个条件：1.多线程下进行resize操作      2某个位置是链表结构并且重新hash后还在一个位置

例如 ：如下图所示，当a，b线程同时插入时，会导致resize。当a线程执行到下图圆圈时，线程执行权被b线程获取这时 a线程值：e=a,e.next=b。b线程正常执行执行完成后变成了    b -> a。执行完成后a线程获取执行权进行操作，会执行 a.next=b  newTable[1]=a ,这时就形成了死链。

![1570600055391](images/1570600055391.png)

![1570599920859](images/1570599920859.png)

https://blog.csdn.net/qq_36520235/article/details/82417949

# Jvm

https://www.cnblogs.com/andy-zhou/p/5327288.html

## 1 虚拟机简介

### 1.1 虚拟机作用

​	1.jvm是java字节码执行的引擎，还能优化java字节码，使之转换成效率更高的机器指令

​	2.jvm类的装载是通过类加载器实现的

​	3.不同的平台对应的不同的jvm，在执行字节码时，jvm负责将每一条要执行的字节码送给解释器，解释器再将其翻译成特定平台的机器指令

​	4.jvm是jre的一部分。它是一个虚构出来的计算机，是通过在实际的计算机上仿真模拟各种计算机的功能

### 1.2 工作原理

JVM在整个JDK中处于最底层，负责与操作系统的交互。windows操作系统装入jvm是通过JDK中的java.exe来实现的，具体步骤如下：	

a、创建JVM装载环境和配置；	

b、装载jvm.dll;	

c、初始化jvm.dll;	

d、调用JNIEnv实例装载并处理class类；	

e、运行java程序 

### 1.3 内存划分

粗略分，jvm的内部体系结构分为三部分，分别是：**类加载器（ClassLoader）子系统**，

**运行时数据区**和**执行引擎**

#### 1.3.1 类加载器 

##### 1.3.1.1 类加载过程

类加载分为三步：加载，链接，初始化

  **加载**：读取类文件产生二进制流。并转换成特定的数据结构，初步校验cafe魔法数，常量池，文件长度，是否有父类等，然后创建对应的java.class

**链接**：包括三个阶段

验证 ：验证数据的合法性如，final 是够合规，类型是否正确，静态变量是否合理等。

准备 ： 为静态变量分配内存，并设定默认值

解析 ：解析类和方法确保类与类之间的相互引用的正确性，完成内存布局

  **符号引用**（Symbolic Reference）：符号引用以一组符号来描述所引用的目标，符号引用可以是任何形式的字面量，符号引用与虚拟机实现的内存布局无关，引用的目标并不一定已经在内存中。

​    **直接引用**（Direct Reference）：直接引用可以是直接指向目标的指针、相对偏移量或是一个能间接定位到目标的句柄。直接引用是与虚拟机实现的内存布局相关的，同一个符号引用在不同的虚拟机实例上翻译出来的直接引用一般都不相同，如果有了直接引用，那引用的目标必定已经在内存中存在。



**初始化**：为静态变量初始化，执行静态代码块，如果赋值运算是通过其他类的静态方法来完成的，那么会解析另一个类

##### 1.3.1.2 类加载器

类是通过类加载器进行加载的，有三种类加载器

- **bootstrap**：在jvm启动时创建，负责加载最核心的java类，如object，system，String等通常由与操作系统相关的语言实现  bin/lib/
- **PlatFrom ClassLoader**（1.9中修改了，以前为extension ClassLoader）:用来加载扩展的类如xml，加密，压缩相关的功能类 /bin/ext
- **Application ClassLoader** ：主要用于加载用户定义的classpath路径下的类

##### 1.3.1.3 类加载的时机

类按需来加载，一般只会加载一次，加载时机有以下几种：

- 创建类的实例（也就是new对象的时候，只有new对象时才会调用构造方法，其他方式加载类都不会调用）

- 访问类的静态变量（除常量【被final修饰的静态变量】常量是一种特殊的变量，因为编译器把他们当作值而不是域来对待，如果你的代码中用到了常变量(constant variable)，编译器并不会生成字节码来从对象中载入域的值，而是直接把这个值插入到字节码中。这是一种很有用的优化，但是如果你需要改变final域的值那么每一块用到那个域的代码都需要重新编译）

- 访问类的静态方法

- 反射（class.formName）

- 当初始化一个类式发现一个类还没有初始化，则先初始化

- 虚拟机启动时，定义了main方法的类先初始化


  ##### 1.3.1.4 类初始化

  ```
  public class Fu {
       int a=1;
      public static int b=1;
      private int c=show1();
      public Fu(){
          show();
      }
      public  void show(){
          System.out.println("=====fu======a=="+a+"==b=="+b+"=c="+c);
      }
      public int show1(){
          return 1;
      }
  }
  class Zi extends Fu{
      private int a=2;
      private static int b=2;
      private int c=show1();
      public Zi(){
          super();
      }
      public  void show(){
          System.out.println("=====zi======a=="+a+"==b=="+b+"=c="+c);
      }
      public int show1(){
          return 2;
      }
      public static void main(String[] args) {
        Fu f= new Zi();
          System.out.println(f.a);
      }
  }
  ```

![1570093348282](images/1570093348282.png)

通过以上代码来分析可以得出：

1.当存在父子继承关系时先加载父类再加载子类

2.当存在重写的方法时，在父类中调用会调用子类重写的方法

3.把子类对象指向父类时，调用方法执行的是子类的，如果调用时成员变量则是父类的



通过类加载顺序来分析，上述代码执行顺序“：

1.执行new Zi（）时调用zi类的构造方法，因为存在继承关系则调用父类的构造方法，这时候会先对父类进行实例化

2.如果是第一次调用会初始化父类和子类静态变量

3.在堆中为父类，子类 成员变量分配内存空间，完成后会先设置初始值



##### 1.3.1.5 类加载器使用方式 



#### 1.3.2 运行时数据区

![jvm结构](images/20160417014617165)

![1570241902614](images/1570241902614.png)

​								      		1.8 结构图

##### 1.3.2.1 方法区

 方法区域存放了所加载的类的信息（名称、修饰符等）、类中的静态变量、类中定义为final类型的常量、类中的Field信息、类中的方法信息

1.7及之前Perm（永久区）在启动时固定大小，可用-xx：maxPermSize=1280m

1.7以后（metaSpace）元空间替换了perm，元内存在本地分配，可直接操作本地内存

##### 1.3.2.2 堆

​	线程共享的，用来存储java对象的，由垃圾回收器回收。可用-Xms256M -xmx256

来指定最大值和最小值	。

堆分为两大块：新生代和老年代。新生代=一个eden区+2个Survivor。对象产生之初在新生代，步入暮年在老年代，老年代也会接纳新生代无法容纳的超大对象。

​	绝大多数对象在eden生成，当eden区实行清除策略时，没有引用对象会被直接回收，依旧存活的会移动到survivor区。survivor分为s0和s1两块内存，每次复制时会将存活的对象复制到为使用的那块。如果YGC要移动的大于s的上限则直接移动到老年代。每个对象有一个计数器，每次YGC都会加1 -xx：MaxTenuringThreshold参数可以设置阈值。默认值是15 也就是在survivor区交换14次后就会晋升为老年代。

##### 1.3.2.3 栈

调用方法时会为方法分配栈，用来存放局部变量。是一个后进先出的结构。线程私有的

栈的大小通过-Xss来设置

**当一个方法被调用时会进入压栈例如：**

```
 public  void a1(){}
    public void a2(){}
    public void a3(){}
    public static void main(String[] args) {

        Test t=new Test();
        t.a1();
        t.a2();
        t.a3();

    }
```

当调用main 方法时 进行压栈，jvm开始分配栈内存，初始化变量，当调用 a1 时a1 入栈执行，执行完毕后弹栈，a2入栈 执行完后弹栈a3同上。当方法执行完后 main也弹栈方法执行完成

##### 1.3.2.4 本地方法栈

存放本地方法的。线程私有

##### 1.3.2.5 程序计数器

程序计数器保存着当前线程所执行的字节码位置，每个线程工作时都有一个独立的计数器。程序计数器为执行java方法服务，执行native方法时，程序计数器为空。

##### 1.3.2.6 对象头中信息

对象头中信息如下图

![image-20230409162256425](images\image-20230409162256425.png)

1. 当对象初始化后，虚拟机开始为对象设置信息：如对应的class类元信息，分带年龄等对象头信息。对象分为三部分 1. mark word 2.kclass pointer（指向元空间中的类元信息）3.实例数据
2. 



##### 1.3.15 分析例子

以下会通过一个程序来分析从执行java **.class 到运行完main方法，jvm执行了哪些步骤

- 代码

  

```java
package com.whj;

public class User {

    private Integer age;
    int sex;
    protected  String name;

}

```

```java
package com.whj;

public class Main {
    private int id;


    private  static User user=new User();

    private static String name;

    public  void  compute(){

        int a=1;
        int b =2;
        System.out.println(a+b);

    }
    public static void main(String[] args) {
        Main main=new Main();
        main.compute();
        System.out.println("Hello world!");
    }
}
```

通过javap -v xxx.class ->math.txt

javap -c xxx.class ->math.txt

可以获取到类信息

- 运行流程

  - 执行java main.class

  - 启动流程：jvm.exe开启进程，加载引导类加载器，接着加载扩展类加载器，最后加载app类加载器，进行对该类加载（加载过程中使用双亲委派）

  - 加载字节码流程：(黑体部分为链接阶段)

    - 加载：将字节码信息加载到jvm中  
    -  **验证：验证字节码头部信息是否是cafe** 
    - **准备：验证通过后开始将类信息加载到元空间中，为静态变量赋初始值（int=0，char=0）**
      - 当加载main对象时会先将user设置null，name设置为null
    - **解析：将符号引用变成静态引用（将一些静态的变量变成内存中对象的地址，如：代码中Main m=0x8600）**
    - 初始化：为静态变量初始化值
      - 先在堆空间中创建user对象，将该对象引用赋值给元空间中的user
      - *在字符常量池中创建name，将地址赋值给name* ？
  
  - 执行代码流程
  
    - 分配栈空间：运行main方法后会在栈中开辟一块空间用来存放main线程（在此线程中运行的方法都会在该main线程栈中创建栈帧）
  
    - 执行代码阶段：
  
      - 执行new Main()时会先在 常量池中检查是否已经被加载，如果未加载则执行加载流程。加载流程完毕后 在**堆内存中开辟空间**存放main对象的局部变量（在eden中存储，如果该对象特别大则会直接进入老年代，如果垃圾回收时第一次不会清除则会将该对象移动到survive中，当多次清理都没有清理掉，则会移动到老年代中）并将main对象地址赋值给栈中的Main m变量
  
      - 调用m.compute（），会在main线程栈中的开辟一块空间创建compute栈帧，栈帧中存放了：局部变量表，操作栈，动态链接和方法出口
  
        - 局部变量表：用来存放局部变量
        - 操作栈：进行赋值和计算的零时空间
          - 当执行compute时，先执行a，a会先进入操作栈，然后在执行赋值操作，赋值完成后将变量放入局部变量表，变量b的执行相同
          - 执行加法时会将变量a,b从局部变量表中拿出来放到操作栈中进行运算
        - 动态链接：存放执行方法的地址
        - 程序出口：存放完成后需要继续执行main方法中哪行代码
  

**加载过程中几个注意点**

          1. 方法区中加载了哪些信息
                    1. 类型信息
                    2. 字段信息
                    3. 方法信息
                    4. 对应class实例的引用
                              1.  会在heap中创建对象，对象头中的kclassPointer指向类的元信息
                              2. 当使用反射时，会在堆中生成一份独立的类对象信息，可以通过反射获取到对象信息
                    5. 类加载器的引用
                              1. 这个类到类加载器实例的引用
                    6. 运行常量池信息
          2. 方法区的对象都会与哪些空间进行交互
                    1. 

#### 1.3.3 执行引擎：它或者在执行字节码，或者执行本地方法

#### 1.3.4 垃圾回收

##### 1.3.4.1 标记为回收方法

###### 1.引用计数法

给对象添加一个引用计数器，每当一个地方引用时，数据器+1；当引用失效时-1；计数器为0时回收

缺点：不好解决循环依赖

优点：实现简单效率高

###### 2.根搜索算法

通过一系列名为GC root的对象作为起始点，从这些节点开始向下搜索，搜索所有走过的路径称为引用链。当一个对象到GC ROOT 没有任何引用链时回收

##### 1.3.4.2 垃圾回收方法

###### 1.标记清除

它的思想就是标记那些要被回收的对象，然后统一回收。这种方法很简单，但是会有两个主要问题：
1.效率不高，标记和清除的效率都很低；
2.会产生大量不连续的内存碎片，导致以后程序在分配交大的对象时，由于没有充足的连续内存而提前触发一次GC动作。

###### 2.标记整理

该算法是为了解决标记-清楚，产生大量内存碎片的问题；当对象存活率较高时，也解决了复制算法的效率问题。它的不同之处就是在清除对象的时候现将可回收的对象移动一端，然后清除掉端边界以外的对象，这样就不会产生内存碎片。

###### 3.复制算法

复制算法将可用内存按容量划分相等的两部分，然后每次只使用其中的一块，当第一块内存用完时，就将还存活的对象复制到第二块内存上，然后一次性清除完第一块内存，在将第二块上的对象复制到第一块。但是这种方式，内存的代价太高，每次基本上都要浪费一块内存。
于是将该算法进行了改进，内存区域不再是按照1：1去划分，而是将内存划分为8：1：1三部分，较大的那份内存叫Eden区，其余两块较小的内存叫Survior区。每次都会先使用Eden区，若Eden区满，就将对象赋值到第二块内存上，然后清除Eden区，如果此时存活的对象太多，以至于Survivor不够时，会将这些对象通过分配担保机制赋值到老年代中。（java堆又分为新生代和老年代）。

###### 4.分代回收

根据对象的存活周期的不同将内存划分为几块，一般就分为新生代和老年代，根据各个年代的特点采用不同的收集算法。新生代（少量存活）用复制算法，老年代（对象存活率高）“标记-清理”算法

**垃圾回收时的名词**

（1）对新生代的对象的收集称为minor GC；

（2）对老年代的对象的收集称为Full GC；

（3）程序中主动调用System.gc()强制执行的GC为Full GC。





##### 1.3.4.3 垃圾回收器

###### 1.CMS

CMS：以获取最短回收停顿时间为目标的收集器，基于并发“标记清理”实现
过程：
1、初始标记：独占PUC，仅标记GCroots能直接关联的对象
2、并发标记：可以和用户线程并行执行，标记所有可达对象
3、重新标记：独占CPU(STW)，对并发标记阶段用户线程运行产生的垃圾对象进行标记修正
4、并发清理：可以和用户线程并行执行，清理垃圾
优点:
并发，低停顿
缺点：
1、对CPU非常敏感：在并发阶段虽然不会导致用户线程停顿，但是会因为占用了一部分线程使应用程序变慢
2、无法处理浮动垃圾：在最后一步并发清理过程中，用户县城执行也会产生垃圾，但是这部分垃圾是在标记之后，所以只有等到下一次gc的时候清理掉，这部分垃圾叫浮动垃圾
3、CMS使用“标记-清理”法会产生大量的空间碎片，当碎片过多，将会给大对象空间的分配带来很大的麻烦，往往会出现老年代还有很大的空间但无法找到足够大的连续空间来分配当前对象，不得不提前触发一次FullGC，为了解决这个问题CMS提供了一个开关参数，用于在CMS顶不住，要进行FullGC时开启内存碎片的合并整理过程，但是内存整理的过程是无法并发的，空间碎片没有了但是停顿时间变长了
CMS 出现FullGC的原因：
1、年轻带晋升到老年带没有足够的连续空间，很有可能是内存碎片导致的
2、在并发过程中JVM觉得在并发过程结束之前堆就会满，需要提前触发FullGC

######   cms配置参数

###### 2.G1 

G1 是一款面向服务端应用的垃圾收集器

特点：
1、并行于并发：G1能充分利用CPU、多核环境下的硬件优势，使用多个CPU（CPU或者CPU核心）来缩短stop-The-World停顿时间。部分其他收集器原本需要停顿Java线程执行的GC动作，G1收集器仍然可以通过并发的方式让java程序继续执行。
2、分代收集：分代概念在G1中依然得以保留。虽然G1可以不需要其它收集器配合就能独立管理整个GC堆，但它能够采用不同的方式去处理新创建的对象和已经存活了一段时间、熬过多次GC的旧对象以获取更好的收集效果。也就是说G1可以自己管理新生代和老年代了。
3、空间整合：由于G1使用了独立区域（Region）概念，G1从整体来看是基于“标记-整理”算法实现收集，从局部（两个Region）上来看是基于“复制”算法实现的，但无论如何，这两种算法都意味着G1运作期间不会产生内存空间碎片。
4、可预测的停顿：这是G1相对于CMS的另一大优势，降低停顿时间是G1和CMS共同的关注点，但G1除了追求低停顿外，还能建立可预测的停顿时间模型，能让使用这明确指定一个长度为M毫秒的时间片段内，消耗在垃圾收集上的时间不得超过N毫秒。


与其它收集器相比，G1变化较大的是它将整个Java堆划分为多个大小相等的独立区域（Region），虽然还保留了新生代和来年代的概念，但新生代和老年代不再是物理隔离的了它们都是一部分Region（不需要连续）的集合。同时，为了避免全堆扫描，G1使用了Remembered Set来管理相关的对象引用信息。当进行内存回收时，在GC根节点的枚举范围中加入Remembered Set即可保证不对全堆扫描也不会有遗漏了。

如果不计算维护Remembered Set的操作，G1收集器的运作大致可划分为以下几个步骤：
1、初始标记（Initial Making）
2、并发标记（Concurrent Marking）
3、最终标记（Final Marking）
4、筛选回收（Live Data Counting and Evacuation）
看上去跟CMS收集器的运作过程有几分相似，不过确实也这样。初始阶段仅仅只是标记一下GC Roots能直接关联到的对象，并且修改TAMS（Next Top Mark Start）的值，让下一阶段用户程序并发运行时，能在正确可以用的Region中创建新对象，这个阶段需要停顿线程，但耗时很短。并发标记阶段是从GC Roots开始对堆中对象进行可达性分析，找出存活对象，这一阶段耗时较长但能与用户线程并发运行。而最终标记阶段需要吧Remembered Set Logs的数据合并到Remembered Set中，这阶段需要停顿线程，但可并行执行。最后筛选回收阶段首先对各个Region的回收价值和成本进行排序，根据用户所期望的GC停顿时间来制定回收计划，这一过程同样是需要停顿线程的，但Sun公司透露这个阶段其实也可以做到并发，但考虑到停顿线程将大幅度提高收集效率，所以选择停顿。

**最后 可总结如下：**

并行收集器：串行收集器使用一个单独的线程进行收集，GC 时服务有停顿时间

串行收集器：次要回收中使用多线程来执行 

CMS 收集器是基于“标记—清除”算法实现的，经过多次标记才会被清除  并发，低停顿

G1 从整体来看是基于“标记—整理”算法实现的收集器，从局部（两个 Region 之间） 上来看是基于“复制”算法实现的  并行于并发

**Minor GC 与 Full GC 分别在什么时候发生？**

 新生代内存不够用时候发生 MGC 也叫 YGC，JVM 内存不够的时候发生 FGC 

**几种常用的内存调试工具：jmap、jstack、jconsole、jhat**

 jstack 可以看当前栈的情况，jmap 查看内存，jhat 进行 dump 堆的信息 mat

##### 垃圾收集底层算法实现

###### 三色标记 

#### 1.3.5  JVM中名词解释

**1.内存逃逸**

https://blog.csdn.net/Dear_UU/article/details/96458126

不在Heap Area分配对象，不需要GC来释放对象。

主要是在Stack中来分配，Stack中回收。

对象定义在方法体内部，又被外部引用使用，则为内存逃逸。

如果程序判定为非内存逃逸对象则可以使用以上优点

-XX:+DoEscapeAnalysis 开启逃逸分析、
-XX:+PrintEscapeAnalysis 开启逃逸分析后，可通过此参数查看分析结果。
-XX:+EliminateAllocations 开启标量替换
-XX:+EliminateLocks 开启同步消除
-XX:+PrintEliminateAllocations 开启标量替换后，查看标量替换情况。



**2.双亲委派**

即一个类加载器在加载类时，先把这个请求委托给自己的父类加载器去执行，如果父类加载器还存在父类加载器，就继续向上委托，直到顶层的启动类加载器。如果父类加载器能够完成类加载，就成功返回，如果父类加载器无法完成加载，那么子加载器才会尝试自己去加载。
这种双亲委派模式的好处，一个可以避免类的重复加载，另外也避免了java的核心API被篡改。

破坏双亲委派的方式：tomcat 破坏了双亲委派

22 GC部分：分代回收的思想和依据，以及不同垃圾回收算

#### 1.3.6 jvm优化

https://www.jianshu.com/p/30e8ff0f7dd9

https://www.jianshu.com/p/30e8ff0f7dd9

Java堆大小设置，Xms 和 Xmx设置为老年代存活对象的3-4倍，即FullGC之后的老年代内存占用的3-4倍

~~永久代 PermSize和MaxPermSize~~(元空间)设置为老年代存活对象的1.2-1.5倍。

年轻代Xmn的设置为老年代存活对象的1-1.5倍。

老年代的内存大小设置为老年代存活对象的2-3倍。

| 参数名称 | 说明                         | 默认值         | 备注                                                         |
| -------- | ---------------------------- | -------------- | ------------------------------------------------------------ |
| -Xms     | 初始堆大小，也可称最小堆大小 | 物理内存的1/64 | 默认空余堆内存小于40%时，JVM就会增大堆，直到-Xmx的最大限制   |
| -Xmx     | 最大堆大小                   | 物理内存的1/4  | 默认空余堆内存大于70%时，JVM会减少堆直到-Xms的最小限制       |
| -Xss     | 每个线程的堆栈大小           |                | JDK5.0以后每个线程大小为1M，以前每个线程堆栈大小为256K。根据应用的线程所需内存大小调整。在相同物理内存大小下，减小这个值能生成更多的线程。但是操作系统对一个进程内的线程数还是有限制的，不能无限生成，经验值在3000~5000左右。一般小的应用，如果栈不是很深，应该是128K够用的，大的应用建议使用256K。`这个选项对性能影响比较大，需要严格的测试` |
| -Xmn     | 堆中新生代的大小             |                |                                                              |

## 其他相关参数

| 参数名称                           | 说明                              | 默认值 | 备注                                                         |
| ---------------------------------- | --------------------------------- | ------ | ------------------------------------------------------------ |
| -XX:AggressiveOpts                 | 加快编译                          |        |                                                              |
| -XX:PermSize                       | 设置持久代(perm gen)初始值        |        |                                                              |
| -XX:MaxPermSize                    | 设置持久代最大值                  |        |                                                              |
| -XX:DisableExplicitGC              | 关闭System.gc()                   |        | `这个参数需要严格的测试`                                     |
| -XX:MaxTenuringThreshold           | 垃圾最大年龄                      |        | 如果设置为0的话，则年轻代对象不经过Survivor区，直接进入年老代。对于年老代比较多的应用，可以提高效率。如果将此值设置为一个较大值，则年轻代对象会在Survivor区进行多次复制，这样可以增加对象在年轻代的存活时间，增加在年轻代即被回收的概率，该参数只有在串行GC时才有效 |
| -XX:+UseConcMarkSweepGC            | 使用CMS内存收集                   |        | 测试中配置这个以后，-XX:NewRatio=4的配置失效了，原因不明。`所以，此时年轻代大小最好用-Xmn设置`。 |
| -XX:+UseParNewGC                   | 设置年轻代为并行收集              |        | 可与CMS收集同时使用。`JDK5.0以上，JVM会根据系统配置自行设置，所以无需再设置此值` |
| -XX:+UseBiasedLocking              | 锁机制的性能改善                  |        |                                                              |
| -XX:+CMSParallelRemarkEnabled      | 降低标记停顿                      |        |                                                              |
| -XX:+UseCMSCompactAtFullCollection | 在FULL GC的时候， 对年老代的压缩  |        |                                                              |
| -XX:+UseFastAccessorMethods        | 原始类型的快速优化                |        |                                                              |
| -XX:+UseCMSInitiatingOccupancyOnly | 使用手动定义初始化定义开始CMS收集 |        | 禁止hostspot自行触发CMS GC                                   |

以下为一部分配置：

16G内存 JDK8 生产服务器配置未验证，先丢完整配置

JAVA_OPTS="-server -Xmx4g -Xms4g -Xmn256m -Xss256k -XX:+DisableExplicitGC  -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 -Duser.timezone=GMT+8"

-server//服务器模式
-Xmx4g //JVM最大允许分配的堆内存，按需分配
-Xms4g //JVM初始分配的堆内存，一般和Xmx配置成一样以避免每次gc后JVM重新分配内存。
-Xmn256m //年轻代内存大小，整个JVM内存=年轻代 + 年老代 + 持久代 
-Xss512k //设置每个线程的堆栈大小
-XX:+DisableExplicitGC //忽略手动调用GC, System.gc()的调用就会变成一个空调用，完全不触发GC
-XX:+UseConcMarkSweepGC //并发标记清除（CMS）收集器
-XX:+CMSParallelRemarkEnabled //降低标记停顿
-XX:LargePageSizeInBytes=128m //内存页的大小
-XX:+UseFastAccessorMethods //原始类型的快速优化
-XX:+UseCMSInitiatingOccupancyOnly //使用手动定义初始化定义开始CMS收集
-XX:CMSInitiatingOccupancyFraction=70 //使用cms作为垃圾回收使用70％后开始CMS收集

-Duser.timezone=GMT+8 //设定GMT区域，避免CentOS坑爹的时区设置多线程

### 2.常用命令

可以使用在服务器上使用[jmap](https://links.jianshu.com/go?to=https%3A%2F%2Fyq.aliyun.com%2Fgo%2FarticleRenderRedirect%3Furl%3Dhttps%253A%252F%252Fwww.hollischuang.com%252Farchives%252F303)命令来获取堆dump，使用[jstack](https://links.jianshu.com/go?to=https%3A%2F%2Fyq.aliyun.com%2Fgo%2FarticleRenderRedirect%3Furl%3Dhttp%253A%252F%252Fwww.hollischuang.com%252Farchives%252F110)命令来获取线程的调用栈dump

## 字节码文件解析

# 线程



## 线程的基本使用

### 1.java多线程简介

以单核为例，计算机通过分配不同的时间片段来处理多个任务，让人感觉计算机可以同时处理多个任务，这个过程叫做多线程。由此可以看出多线程需要进行上下文的切换，在使用多线程中并不一定可以实现效率的提升。

java中线程间切换先切换到内核态，获取执行任务后再切回用户态来进行逻辑的处理，上下文切换过程中，对象头会记录当前线程执行片段信息。

java中实现多线程的方式有三种 1 继承thread 类  2 实现runable 接口 3 实现callable接口。这三种的区别：thread，runable 不能获取返回值，call可以获取返回值 。

#### 1.1 实现方式详解

##### 1.1.1 继承thread

```
public class ThreadDemo extends Thread {
    @Override
    public void run() {
        System.out.println("======这是一个线程=========");
    }

    public static void main(String[] args) {
        ThreadDemo demo=new ThreadDemo();
        demo.start();
    }
}
```

##### 1.1.2 实现runable

```
public class RunableDemo {
    public static void main(String[] args) {
       // ;
        RunTest r=   new RunTest();
        Thread t=new Thread(r);
        t.start();
    }
}
class RunTest implements  Runnable{

    public void run() {
        System.out.println("========创建一个线程============");
    }
}
```

##### 1.1.3 实现callable

```java
public class CallableTest {
    public static void main(String[] args) {
        CallableDemo r1=new CallableDemo("一",new Date());
        CallableDemo r2=new CallableDemo("二",new Date());
        //1.执行 Callable 方式，需要 FutureTask 实现类的支持，用于接收运算结果。
        //可以中断获取执行状态太
        FutureTask<String> result = new FutureTask<String>(r1);
        new Thread(result).start();
        //.接收线程运算后的结果
        String sum = null;  //FutureTask 可用于 闭锁 类似于CountDownLatch的作用，在所有的线程没有执行完成之后这里是不会执行的
        try {
            //result.isDone() //是否执行完成
            //result.cancel(true); //中断运行如果子任务已经开始执行了，但是还没有执行结束，根据mayInterruptIfRunning的值，如果mayInterruptIfRunning = true，那么会中断执行任务的线程，然后返回true，如果参数为false，会返回true，不会中断执行任务的线程
            sum = result.get();
            System.out.println(sum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

            System.out.println("------------------------------------");
    }

}
/*
 * 一、创建执行线程的方式三：实现 Callable 接口。 相较于实现 Runnable 接口的方式，方法可以有返回值，并且可以抛出异常。
 *
 * 二、执行 Callable 方式，需要 FutureTask 实现类的支持，用于接收运算结果。  FutureTask 是  Future 接口的实现类
 */
class  CallableDemo implements Callable{

    private String name;
    private Date date;

    public CallableDemo(String name,Date date) {
        this.date=date;
        this.name=name;
    }

    @Override
    public String call() throws Exception {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
        return "传入时间"+ sdf.format(date)+"  。线程名称："+name;
    }
}
```

###### 1.1.3.1 实现分析

FutureTask 实现了 Runable接口，在构造方法中传入callable对象，把callable变成一个成员变量，FutureTask在run方法中调用call方法

```
public void run() {
    if (state != NEW ||
        !UNSAFE.compareAndSwapObject(this, runnerOffset,
                                     null, Thread.currentThread()))
        return;
    try {
        Callable<V> c = callable;
        if (c != null && state == NEW) {
            V result;
            boolean ran;
            try {
                result = c.call();
                ran = true;
            } catch (Throwable ex) {
                result = null;
                ran = false;
                setException(ex);
            }
            if (ran)
                set(result);
        }
    } finally {
        // runner must be non-null until state is settled to
        // prevent concurrent calls to run()
        runner = null;
        // state must be re-read after nulling runner to prevent
        // leaked interrupts
        int s = state;
        if (s >= INTERRUPTING)
            handlePossibleCancellationInterrupt(s);
    }
}
```

注意：当对一个线程多次启动时会报错Exception in thread "main" java.lang.IllegalThreadStateException

![1572427654548](images/1572427654548.png)

只要重新创建Thread对象，就算重新创建一个线程

![1572427763008](images/1572427763008.png)



### 名词解释：

**死锁：**当两个线程相互等待对方资源释放才继续执行

**活锁：**两个线程相互让出资源，你让我，我让你，最后两个线程都无法使用资源。

**饥饿锁：**是指如果线程T1占用了资源R，线程T2又请求封锁R，于是T2等待。T3也请求资源R，当T1释放了R上的封锁后，系统首先批准了T3的请求，T2仍然等待。然后T4又请求封锁R，当T3释放了R上的封锁之后，系统又批准了T4的请求…，T2可能永远等待

**进程**：进程可以视为一个程序的实例，站在操作系统的角度，进程是程序运行资源分配（以内存为主）的最小单位

**线程**：线程必须依赖于进程而存在， 线程是进程中的一个实体  

**上下文切换：**

操作系统中就有上下文切换的概念，它是指 CPU(中央处理单元)**从一个进程或线程到另一个进程或线程的切换**  

引发上下文切换的原因一般包括： 线程、进程切换、 系统调用等等  

并发：单位时间内cup执行多个线程

并行：同一时间处理过个任务

### 线程的生命周期

ava 中线程的状态分为 6 种：

1. 初始(NEW)：新创建了一个线程对象，但还没有调用 start()方法。
2. 运行(RUNNABLE)： Java 线程中将就绪（ready）和运行中（running）两种状态笼统的称为“运行”。
   线程对象创建后，其他线程(比如 main 线程）调用了该对象的 start()方法。
   该状态的线程位于可运行线程池中，等待被线程调度选中，获取 CPU 的使用权，
   此时处于就绪状态（ready）。就绪状态的线程在获得 CPU 时间片后变为运行中
   状态（running）。

3. 阻塞(BLOCKED)：表示线程阻塞于锁。
4. 等待(WAITING)：进入该状态的线程需要等待其他线程做出一些特定动作（通知或中断）。

5. 超时等待(TIMED_WAITING)：该状态不同于 WAITING，它可以在指定的时间后自行返回。

6. 终止(TERMINATED)：表示该线程已经执行完毕。  

![image-20230515161237324](images\image-20230515161237324.png)



### 协程

在用户态模拟系统线程实现的一种轻量级的线程。协程机制适用于被阻塞的，且需要大量并发的场景（网络 io） ，不适合大量计算的场景，因为协程提供规模(更高的吞吐量)，而不是速度(更低的延迟)。  

目前比较出名的是Quasar  。JDK19 引入了虚拟线程  



### 管道输入输出流

可以通过管道流来实现，io流网络的传输，避免通过磁盘中转

Java 中的管道输入/输出流主要包括了如下 4 种具体实现：
PipedOutputStream、 PipedInputStream、 PipedReader 和 PipedWriter，前两种面向字节，而后两种面向字符。

### 进程间的通讯

1. 管道， 分为匿名管道（pipe）及命名管道（named pipe）：匿名管道可用于具有亲缘关系的父子进程间的通信，命名管道除了具有管道所具有的功能外，
   它还允许无亲缘关系进程间的通信。
   
2. 信号（signal）：信号是在软件层次上对中断机制的一种模拟，它是比较复杂的通信方式，用于通知进程有某事件发生，一个进程收到一个信号与处理器收到一个中断请求效果上可以说是一致的。
   
3. 消息队列（message queue）：消息队列是消息的链接表，它克服了上两种通信方式中信号量有限的缺点，具有写权限得进程可以按照一定得规则向消息队列中添加新信息；对消息队列有读权限得进程则可以从消息队列中读取信息。
   
4. 共享内存（shared memory）：可以说这是最有用的进程间通信方式。它使得多个进程可以访问同一块内存空间，不同进程可以及时看到对方进程中对共享内存中数据得更新。这种方式需要依靠某种同步操作，如互斥锁和信号量等。
   
5. 信号量（semaphore）：主要作为进程之间及同一种进程的不同线程之间得同步和互斥手段。

6. 套接字（socket）：这是一种更为一般得进程间通信机制，它可用于网络中不同机器之间的进程间通信，应用非常广泛。同一机器中的进程还可以使用
   Unix domain socket（比如同一机器中 MySQL 中的控制台 mysql shell 和 MySQL 服
   务程序的连接） ， 这种方式不需要经过网络协议栈，不需要打包拆包、计算校验
   和、维护序号和应答等，比纯粹基于网络的进程间通信肯定效率更高。  



### java 中断机制

java中断机制是一种协作机制，也就是说通过中断并不能直接终止另一个线程，而需要被中断的线程自己处理

interrupt()：将线程的中断标志位设置为true，不会中断线程

isInterrupted(): 判断当前线程的中断标志位是否为true

Thread.interrupted()：判断当前线程的中断标志位是否为true，并清除中断标志位，重置为fasle

```java
public class ThreadinterrupteTest {

    private static int i;
    public static void main(String[] args) {
        Thread thread=new Thread(()->{
           while (true){
               i++;
               System.out.println(i);
//               //判断标识位 执行完成后会清除该标识位,方法体内方法只会执行一次
//               if(Thread.interrupted()){
//                   System.out.println("================");
//                   //break;
//               }

               //判断标识位，执行完成后不会清除标识位，方法体内的方法会多次执行
               if(Thread.currentThread().isInterrupted()){
                   System.out.println("=========");
               }
           }
        });
        thread.start();
        //只是修改中断标识为true，并不会真正的中断线程，如果在线程内没有Thread.currentThread().isInterrupted() 方法进行判断处理，则没有效果
        thread.interrupt();
    }
}
```

调用thread.interrupt()后会唤醒sleep()方法，wait()方法，使第一次失效，并且报

```java
public class ThreadinterrupteTest {

    private static int i;
    public static void main(String[] args) {
        Thread thread=new Thread(()->{
           while (true){
               i++;
               System.out.println(i);
               try {
                   Thread.sleep(5000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
//               //判断标识位 执行完成后会清除该标识位,方法体内方法只会执行一次
//               if(Thread.interrupted()){
//                   System.out.println("================");
//                   //break;
//               }

               //判断标识位，执行完成后不会清除标识位，方法体内的方法会多次执行
               if(Thread.currentThread().isInterrupted()){
                   System.out.println("=========");
               }
           }
        });
        thread.start();
        //只是修改中断标识为true，并不会真正的中断线程，如果在线程内没有Thread.currentThread().isInterrupted() 方法进行判断处理，则没有效果
        thread.interrupt();
    }
}
```

```
java.lang.InterruptedException: sleep interrupted
	at java.lang.Thread.sleep(Native Method)
	at whj.thread.ThreadinterrupteTest.lambda$main$0(ThreadinterrupteTest.java:18)
	at whj.thread.ThreadinterrupteTest$$Lambda$1/1915318863.run(Unknown Source)
	at java.lang.Thread.run(Thread.java:745)
```



### 2.线程间的通讯方式

线程间通讯科通过两种方式来实现，一种是Object 的wait，notify ，notifyall 另一种 lock的 await，signal 。

通讯的意义：为了拥有锁的线程，按顺序执行，彼此之间互不影响的运行。

#### 2.1 wait，notify ，notifyall

**wait作用**：wait 阻塞当前线程，释放执行权，会在调用处中断，当调用notify或者notifyall时会重新进行竞争，再次获取执行权后会从中断出继续执行

**notify ，notifyall**：唤醒被阻塞的线程，使其拥有竞争权



当使用wait时需注意，要在synchronized中使用，使用非锁对象的wait方式时，不能对线程进行阻塞，会报IllegalMonitorStateException错，当时用的锁是类对象时，如Object.class，阻塞和唤醒都需要使用Object.class.wait和object.class.notifyall

![1572427947216](images/1572427947216.png)

##### 等待和通知的标准范式

等待方遵循如下原则。
1）获取对象的锁。
2）如果条件不满足，那么调用对象的 wait()方法，被通知后仍要检查条件。
3）条件满足则执行对应的逻辑  

![image-20230515163312777](images\image-20230515163312777.png)

通知方遵循如下原则。
1）获得对象的锁。
2）改变条件。
3）通知所有等待在对象上的线程。  

![image-20230515163340745](images\image-20230515163340745.png)





#### 2.2 lock 

###### 2.2.1 普通重入锁通讯

使用方式如下，通过lock,unlock 来控制锁定区域 ，实现synchronized 锁的功能，condition实现 wait和notify功能，condition使用时也需要在lock 和unlock中使用否则会报错 java.lang.IllegalMonitorStateException。

lock的condition可以创建多个，在多个线程中，只能对同一个进行condition阻塞和释放操作操作。Conndition使用时只能在创建出自己的lock块中使用，如果在非创建自己的lock块中时会报错 IllegalMonitorStateException。

```
ReentrantLock transt = new ReentrantLock();
Condition c= transt.newCondition();
Condition d= transt.newCondition();
 try {
             transt.lock();
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("进入X方式synchronized------》"+i);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            transt.unlock();
        }

```

#### 2.3 join 

可以通过join方法将当前线程插入，先进行执行，如果线程都调用，则后进先执行

###### 2.2.1 读写锁通讯

<https://blog.csdn.net/j080624/article/details/82790372>

① ReadWriteLock同Lock一样也是一个接口，提供了readLock和writeLock两种锁的操作机制，一个是只读的锁，一个是写锁。

读锁可以在没有写锁的时候被多个线程同时持有，写锁是独占的(排他的)。 每次只能有一个写线程，但是可以有多个线程并发地读数据。

所有读写锁的实现必须确保写操作对读操作的内存影响。换句话说，一个获得了读锁的线程必须能看到前一个释放的写锁所更新的内容。

理论上，读写锁比互斥锁允许对于共享数据更大程度的并发。与互斥锁相比，读写锁是否能够提高性能取决于读写数据的频率、读取和写入操作的持续时间、以及读线程和写线程之间的竞争。

② 使用场景

假设你的程序中涉及到对一些共享资源的读和写操作，且写操作没有读操作那么频繁。

ReadWriteLock 在使用时读读共享，读写互斥，写写互斥。read锁不支持condition条件队列

读写锁使用方式如下

```
public  class ReentWriteReadLockTest {

    public static void main(String[] args) {
        RwUtils rw = new RwUtils(0);
        for(int i = 0;i< 1; i++){
            final int n=i;
            new Thread(()->{
                rw.setNum(n);
            }).start();
        }
        for(int i = 0;i< 10; i++){
            final int n=i;
            new Thread(()->{
                rw.getNum(n);
            }).start();
        }
    }


}


class RwUtils{

    private ReentrantReadWriteLock  readWriteLock=new ReentrantReadWriteLock();
    Lock readLock=readWriteLock.readLock();
    Lock writeLock=readWriteLock.writeLock();
    Condition w=writeLock.newCondition();
    Condition r=readLock.newCondition();
    private int num=0;

    private int i;

    public RwUtils(int i){
        this.i = i;
    }
    public int getNum(int i){
        try {
            readLock.lock();
            if(i==0){
                try {
                    System.out.println("=============读开始阻塞了======================================");
                    Thread.sleep(100000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("==============="+i+"==获取"+num);
            return i;
        }finally {
            readLock.unlock();
        }
    }

    public void setNum(int num){
        try {
            readWriteLock.writeLock().lock();

            this.num = num;
            System.out.println("================="+i+"   修改值=>"+num);
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }
    }
```

#### 2.4 park，unpark

```
public class ParkTest {
    public static void main(String[] args) throws InterruptedException {
       Thread t1=new Thread(()->{
            System.out.println("======1==========");
            if(Thread.currentThread().isInterrupted()){
                System.out.println("唤醒了===========");
            }
           LockSupport.park();
           System.out.println("======2==========");
        });

        t1.start();;
        Thread.sleep(5000);
        LockSupport.unpark(t1);
        //也可将park阻塞的唤醒
        // t1.interrupt();
    }
}
```

#### 面试题

**方法和锁**
调用 yield() 、 sleep()、 wait()、 notify()等方法对锁有何影响？
yield() 、 sleep()被调用后，都不会释放当前线程所持有的锁。
调用 wait()方法后，会释放当前线程持有的锁，而且当前被唤醒后， 会重新去竞争锁，锁竞争到后才会执行 wait 方法后面的代码。调用 notify()系列方法后，对锁无影响，线程只有在 syn 同步代码执行完后才会自然而然的释放锁，所以 notify()系列方法一般都是 syn 同步代码的最后一行。  

**为什么 wait 和 notify 方法要在同步块中调用**  

1. 主要是因为 Java API 强制要求这样做，如果你不这么做，你的代码会抛出IllegalMonitorStateException 异常  
2. 一部分





## synchronized关键字的实现原理

从字节码编译后可简单得出：

1. **`同步方法：**`synchronized 方法会被翻译成普通的方法调用。在 JVM 字节码层面并没有任何特别的指令来实现被 synchronized 修饰的方法。在 Class 文件的方法表中将该方法的 access_flags 字段中的 synchronized 标志位置 1，表示该方法是同步方法并使用调用该方法的对象（对象锁）或该方法所属的 Class（类锁） 做为锁对象。

2. **同步块：**monitorenter 指令插入到同步代码块的开始位置，monitorexit 指令插入到同步代码块的结束位置，JVM 需要保证每一个 monitorenter 都有一个 monitorexit 与之相对应。任何对象都有一个 monitor 与之相关联，当且一个 monitor 被持有之后，他将处于锁定状态。线程执行到 monitorenter 指令时，将会尝试获取对象所对应的 monitor 所有权，即尝试获取对象的锁。（关于上述字节码中一个 monitorenter 指令为什么对应两 monitorexit 指令我们稍后进行说明）

3. 在 Java 早期版本中，synchronized 属于重量级锁，效率低下，因为监视器锁（monitor）是依赖于底层操作系统的 Mutex Lock 来实现的，而操作系统实现线程之间的切换时需要从用户态转换到核心态，这个状态之间的转换需要相对比较长的时间，时间成本相对较高，这也是为什么早期的 synchronized 效率低的原因。Java 6 之后 Java 官方从 JVM 层面对 synchronized 进行了较大优化，为了减少获得锁和释放锁所带来的性能消耗，引入了轻量级锁和偏向锁，

4. 当执行 monitorenter 指令时，当前线程将试图获取对象锁所对应的 monitor 的持有权，当对象锁的 monitor 的进入计数器为 0，那线程可以成功取得 monitor，并将计数器值设置为 1，取锁成功。如果当前线程已经拥有对象锁的 monitor 的持有权，那它可以重入这个 monitor，重入时计数器的值会加 1。倘若其他线程已经拥有对象锁的 monitor 的所有权，那当前线程将被阻塞，直到正在执行的线程执行完毕，即 monitorexit 指令被执行，执行线程将释放 monitor 并设置计数器值为 0，其他线程将有机会持有 monitor。值得注意的是编译器将会确保无论方法通过何种方式完成，方法中调用过的每条 monitorenter 指令都有执行其对应 monitorexit 指令，无论这个方法是正常结束还是异常结束。为了保证在方法异常完成时 monitorenter 和 monitorexit 指令依然可以正确配对执行，编译器会自动产生一个异常处理器，这个异常处理器可处理所有的异常，它的目的就是用来执行 monitorexit 指令。从字节码中也可以看出多了一个 monitorexit 指令。

   从底层语义原理

### 1.1  **对象头**

要深入了解Snchronized的原理，先来了解下对象的组成

对象在堆中由三部分组成：

1. 对象头

2. 实例对象

3. 填充对象

   **实例对象**：存放类的属性数据信息，包括父类的属性信息，如果是数组的实例的实例部分还包括数组的长度，这部分内存按4字节对齐

   **填充数据**：由于虚拟机要求对象的起始地址必须是8字节的整数倍。填充数据不是必须存在的，仅仅是为了字节对齐

   **对象头**：HotSpot虚拟机的对象头主要包括两部分数据：Mark Word（标记字段） class point （类型指针）

   其中class point 是对象指向他的类元数据的指针，虚拟机通过这个指针来确定这个对象是哪个类的实例，Mark word 用于存储对象自身运行时的数据，他是实现轻量级锁和偏向锁的关键。它还用于存储对象自身运行时的数据 如哈希码，GC分带年龄，锁状态标志，线程持有的锁，偏向线程的id，偏向时间搓等等

| 长度      | 内容                   | 说明                             |      |
| --------- | ---------------------- | -------------------------------- | ---- |
| 32/64 bit | Mark Word              | 存储对象的 hashCode 或锁信息等   |      |
| 32/64 bit | Class Metadata Address | 存储到对象类型数据的指针         |      |
| 32/64 bit | Array length           | 数组的长度（如果当前对象是数组） |      |



32 位 JVM 的 Mark Word 的默认存储结构如下：

| 锁状态   | 25bit        | 4bit         | 1bit是否是偏向锁 | 2bit锁标志位 |
| -------- | ------------ | ------------ | ---------------- | ------------ |
| 无锁状态 | 对象hashCode | 对象分代年龄 | 0                | 01           |



由于对象头的信息是与对象自身定义的数据没有关系的额外存储成本，因此考虑到jvm的效率，MARK WORD 被设计成一个非固定的数据结构，以便存储更多有效的数据，它会根据对象本身的状态复用自己的存储空间，如32位jvm下，除了上述列出的Mark Word 默认存储结构外，还有如下可能变化的结构：

![](/images/%E5%9B%BE%E7%89%871.png)

### **1.2 Monitor (管理)**

1.什么是Monitor

可以把他标记为一个同步工具，也可以描述为一种同步机制，通常被描述为一个对象。所有的java对象天生就是Monitor，在java的设计中，每一个java对象都带了一把看不见的锁，它叫做内置锁或者是Monitor锁

观察Mark Word 存储结构的那张图

这里我们主要分析重量级锁也就是通常说Synchionized的对象锁，锁标识为10，其中指针指向的是monitor对象（也被称为管程或者监视器锁）的起始地址。每一个对象都存着一个monitor与之关联，对象与其monitor之前的关系存在多种实现方式，如monitor可以与对象一起创建销毁或当前线程视图获取对象锁时自动生成，但当一个monitor被某个线程持有后，它便处于锁定状态，在java虚拟机（hotSpot），monitor是由ObjectMonitor实现的，其主要结构如下

```
ObjectMonitor() {
    _header       = NULL;
    _count        = 0;          // 记录个数
    _waiters      = 0,
    _recursions   = 0;
    _object       = NULL;
    _owner        = NULL;
    _WaitSet      = NULL;       // 处于 wait 状态的线程，会被加入到 _WaitSet
    _WaitSetLock  = 0;
    _Responsible  = NULL;
    _succ         = NULL;
    _cxq          = NULL;
    FreeNext      = NULL;
    _EntryList    = NULL;       // 处于等待锁 block 状态的线程，会被加入到该列表
    _SpinFreq     = 0;
    _SpinClock    = 0;
    OwnerIsThread = 0;
}
```

 ObjectMonitor中有两个队列，_WaitSet和_EntryList,用来保存ObjectWaiter对象列表（每个等待锁的线程都会被封装成ObjectWaiter对象），_owner指向持有ObjectMonitor对象的线程，当多个线程同时访问一段同步代码时，首先会进入_EntryList集合，当线程获取到对象的monitor后把monitor中的_owner变量设置为当前线程，同时Monitor中的计数器__count加1。若线程调用wait()方法，将释放当前持有monitor，_owner

变量恢复成null,_count自减1，同时该线程进入_WaitSet结合中等待被唤醒。若当前线程执行完毕后也将释放monitor（锁）并复位变量的值，以便其他线程获取monitor（锁）。

由此看来，monitor对象存在于每个java对象的对象头中（存储的是指针）,synchronized便是通过这种方式获取锁的，也是为什么java中任意对象可以作为锁的原因，同时也是notify,notifyAll/wait等方法存在于顶级对象Object中的原因（锁可以是任意对象，所以可以被任意对象调用的方法定义在Object中）。

### 1.3  java1.6后实现的优化方式

**1.偏向锁：**偏向锁的目标是消除数据在无竞争情况下的同步损耗

当一个线程访问同步块并获取锁时，会在对象头的帧栈中的锁记录里存储锁偏向的线程id

以后该线程在进入和退出同步块时不需要花费cas操作来加锁和解锁，而只需简单的测试下对象头的mark word里是否存储着指向当前线程的偏向锁

![](images/%E5%9B%BE%E7%89%872.png)

**2.轻量级锁**：轻量级锁的目标是在没有多线程竞争的前提下，减少传统重量锁使用操作系统互斥量带来的性能损耗

在代码进入同步块的时候，如果同步对象锁为无所状态（锁标志位01，是否偏向锁为0）

虚拟机首先将当前线程的栈帧中建立一个名为锁记录（lock Record）的空间，用于存储锁对象目前的mark word的拷贝，官方称为 Disolaced mark word

拷贝对象头中的mark word复制到锁记录中，拷贝成功后，虚拟机将使用cas操作尝试将锁对象的mark word更新为指向lockrecord的指针，并将线程栈帧中的lock record里的owner 指针指向objet的mark word，如果这个更新成功，那么这个线程就拥有了对象的锁，并且对象的mrk word的锁标志位设置为00，更新失败首先会检查对象的mark word 是够指向当前线程的栈帧，如果是就说明当前线程以及拥有了这个对象的锁，那就可以直接进入同步块继续执行。否则说明多个线程在竞争锁，轻量级锁就会膨胀为重量级锁，锁标记的状态值为10 Mark Word中存储的就是指向重量级锁（互斥量）的指针，后面等待锁的线程也要进入阻塞状态。

![](images/%E5%9B%BE%E7%89%873.png)



| 锁       | 优点                                                         | 缺点                                             | 适用场景                               |
| -------- | ------------------------------------------------------------ | ------------------------------------------------ | -------------------------------------- |
| 偏向锁   | 加锁和解锁不需要额外的消耗，和执行非同步方法比仅存在纳秒级的差距。 | 如果线程间存在锁竞争，会带来额外的锁撤销的消耗。 | 适用于只有一个线程访问同步块场景。     |
| 轻量级锁 | 竞争的线程不会阻塞，提高了程序的响应速度。                   | 如果始终得不到锁竞争的线程使用自旋会消耗CPU。    | 追求响应时间。  同步块执行速度非常快。 |
| 重量级锁 | 线程竞争不使用自旋，不会消耗CPU。                            | 线程阻塞，响应时间缓慢。                         | 追求吞吐量。  同步块执行速度较长。     |

![](images/20190323140321501.png)



## 2.AQS实现方式及应用场景

**产生原因**：1.提供一个灵活的，可扩展，实现简单的同步器

### 实现独享锁



### 实现原理

#### 1.1 设计思想

​	同步器的核心方法是acquire和release操作，其背后的思想也比较简洁明确。

**acquire**操作是这样的：

​	while(当前同步器的状态不允许获取操作){

​	           如果当前线程不在队列中，则将其插入队列

​	           阻塞当前线程	

​	}

​	如果线程位于队列中，则将其移除队列



**release**操作是这样的：

更新同步器状态

if(新的状态允许某个被阻塞的线程获取成功){

 解除队列中一个或多个线程的阻塞状态 

}

从这两个操作中的思想中我们可以提取出三大关键操作：同步器状态更新，线程阻塞和释放，插入和移出队列。所以为了实现这两个操作，引申出三个基本组件：

**同步器状态的原子性管理：**

**线程阻塞与解除阻塞**

**队列的管理**

##### 1.1.1 同步状态

AQS使用int来保存同步状态，并暴露出getStatus，setStatus 以及compareAndSet 操作来读取和更新这个同步状态。其中属性status被申明为volatile，并且通过使用cas指令来实现compareAndSetStatus，使得当且仅当同步状态拥有一个一致的期望值得时候，才会被原子的设置成新值，这样就达到了同步状态的原子性管理，确保同步状态的原子性，可见性和有序性；

- [ ] <div style="colour:red">`基于aqs的具体实现类必须根据暴露出的状态相关的方法定义tryAcquire 和tryRelease方法，以控制acquire和release操作。当同步状态满足时 ，tryAcquire必须返回true，而当新的同步状态允许后续acquire时，tryRelease方法也必须返回true，这些方法都接受一个int类型的参数用于传递想要的状态`</div>

##### 1.1.2 阻塞

j.u.c.locks包提供了LockSupport类来解决这个问题。方法LockSupport.park阻塞当前线程知道有个LockSupport.unpark方法被调用。unpark的调用时没有被技术的，因此在一个park调用前多次调用unpark方法只会解除一个park操作。另外，它们作用于每个线程而不是每个同步器。一个线程在一个新的同步器上调用park操作可能会立即返回，因为在此之前可能有多余的unpark操作。但是，在缺少一个unpark操作时，下一次调用park就会被阻塞。虽然可以现实的取消多余的unpark调用，但是这样并不值得这样做。在需要的时候多次调用park会更高效。park方法同样支持可选的相对或者绝对的超时设置，以及与jvm的thread.interrupt结合，可通过中断来unpark一个线程

##### 1.1.3 队列

整个框架的核心就是如何管理线程阻塞队列,该队列是严格的fifo,因此不支持线程优先级同步

入队操作:插入到队列尾部,同步器提供了一个cas方法,他需要传递当前线程认为的尾结点和当前节点,只有设置成功后,当前节点才正式与之前的尾节点建立联系

出队操作:遵循fifo规则,所以能成功获取到aqs同步状态的必定是首节点,首节点的线程在释放同步状态后会唤醒后续节点,而后续节点会在获取asq同步状态成功的时候将自己设置为首节点.设置首节点是由获取同步成功的线程来完成的,由于只能有一个线程获取同步状态,所以设置首节点的方法不需要像入队cas操作,只需将手机店设置为原首节点的后续节点同时断开原节点,后续节点的引用即可



##### 1.1.4 条件队列

队列除了同步队列还有条件队列.aqs只有一个同步队列,但是可以有多个条件队列.AQs框架提供了一个ConditionObject类，给维护独占同步的类以及实现lock接口的类使用。

ConditionObject类实现了Condition接口，Condition接口提供了类似Object管程式的方法如await，signal何signalall操作，还扩展了带有超时，监测和监控的方法。ConditionObject类有效的将条件与其他同步结合到了一起。该类指支持java风格管程访问规则，这些规则中，当且仅当当前线程持有锁且要操作的条件（condition）属于该锁时，条件操作才是合法的。这样一个conditionObject关联到一个ReedtrantLock上就表现的根内置管程（通过Object.wait等）一样。

ConditionObject类和aQL公用了内部接点，有自己单独的条件队列。

**signal**操作是通过将节点从条件队列转移到同步队列来实现的，没有必要在需要唤醒线程重新获取到锁之前将其唤醒

**await**操作就是将当前线程从同步队列进入到条件队列中等待

实现这些操作主要复杂在，因超时或Thread.interrupt导致取消了条件等待时，该如何处理。await和signal几乎同时发生就会有竞态问题，最终的结果遵照内置管程相关的规范。JSR133修订以后，就要求如果中断发生在signal操作之前，await方法必须在重新获取到锁后，抛出InterruptedException。但是，如果中断发生在signal后，await必须返回且不抛异常，同时设置线程的中断状态。

##### 1.1.5 实现一个cas操作

```
public class AtomicInteger {
    private volatile int value;

    public void add(int v) {
         int oldValue;
         do {
			oldValue = value;
          }while( !cas(OldValue, old+v) ;
   }
}

```



### 1.2 方法构成

如果我们理解了上一节的设计思路，我们大致就能知道AQS的主要数据结构了。

| 组件     | 数据结构           |
| -------- | ------------------ |
| 同步状态 | volatile int state |
| 阻塞     | LockSupport类      |
| 队列     | Node节点           |
| 条件队列 | ConditionObject    |

进而再来看下AQS的主要方法及其作用。

| 属性、方法                                                | 描述、作用                                                   |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| int getState()                                            | 获取当前同步状态                                             |
| void setState(int newState)                               | 设置当前同步状态                                             |
| boolean compareAndSetState(int expect, int update)        | 通过CAS设置当前状态，此方法保证状态设置的原子性              |
| boolean tryAcquire(int arg)                               | 钩子方法，独占式获取同步状态，AQS没有具体实现，具体实现都在子类中，实现此方法需要查询当前同步状态并判断同步状态是否符合预期，然后再CAS设置同步状态 |
| boolean tryRelease(int arg)                               | 钩子方法，独占式释放同步状态，AQS没有具体实现，具体实现都在子类中，等待获取同步状态的线程将有机会获取同步状态 |
| int tryAcquireShared(int arg)                             | 钩子方法，共享式获取同步状态，AQS没有具体实现，具体实现都在子类中，返回大于等于0的值表示获取成功，反之失败 |
| boolean tryReleaseShared(int arg)                         | 钩子方法，共享式释放同步状态，AQS没有具体实现，具体实现都在子类中 |
| boolean isHeldExclusively()                               | 钩子方法，AQS没有具体实现，具体实现都在子类中，当前同步器是否在独占模式下被线程占用，一般该方法表示是否被当前线程所独占 |
| void acquire(int arg)                                     | 模板方法，独占式获取同步状态，如果当前线程获取同步状态成功，则由该方法返回，否则会进入同步队列等待，此方法会调用子类重写的tryAcquire方法 |
| void acquireInterruptibly(int arg)                        | 模板方法，与acquire相同，但是此方法可以响应中断，当前线程未获取到同步状态而进入同步队列中，如果当前线程被中断，此方法会抛出InterruptedException并返回 |
| boolean tryAcquireNanos(int arg, long nanosTimeout)       | 模板方法，在acquireInterruptibly基础上增加了超时限制，如果当前线程在超时时间内没有获取到同步状态，则会返回false,如果获取到了则会返回true |
| boolean release(int arg)                                  | 模板方法，独占式的释放同步状态，该方法会在释放同步状态后，将同步队列中的第一个节点包含的线程唤醒 |
| void acquireShared(int arg)                               | 模板方法，共享式的获取同步状态，如果当前系统未获取到同步状态，将会进入同步队列等待，与acquire的主要区别在于同一时刻可以有多个线程获取到同步状态 |
| void acquireSharedInterruptibly(int arg)                  | 模板方法，与acquireShared一致，但是可以响应中断              |
| boolean tryAcquireSharedNanos(int arg, long nanosTimeout) | 模板方法，在acquireSharedInterruptibly基础上增加了超时限制   |
| boolean releaseShared(int arg)                            | 模板方法，共享式的释放同步状态                               |
| Collection<Thread> getQueuedThreads()                     | 模板方法，获取等待在同步队列上的线程集合                     |
| Node int waitStatus                                       | 等待状态1、 CANCELLED，值为1，在同步队列中等待的线程等待超时或者被中断，需要从同步队列中取消等待，节点进入该状态后将不会变化；2、 SIGNAL，值为-1，后续节点的线程处于等待状态，而当前节点的线程如果释放了同步状态或者被取消，将会通知后续节点，使后续节点的线程得以运行；3、 CONDITION，值为-2，节点在条件队列中，节点线程等待在Condition上，当其他线程对Condition调用了signal()方法后，该节点将会从条件队列中转移到同步队列中，加入到对同步状态的获取中；4、 PROPAGATE，值为-3，表示下一次共享式同步状态获取将会无条件地传播下去 |
| Node prev                                                 | 前驱节点，当节点加入同步队列时被设置                         |
| Node next                                                 | 后续节点                                                     |
| Thread thread                                             | 获取同步状态的线程                                           |
| Node nextWaiter                                           | 条件队列中的后续节点，如果当前节点是共享的，那么这个字段将是一个SHARED变量，也就是说节点类型（独占和共享）和条件队列中的后续节点共用同一个字段 |
| LockSupport void park()                                   | 阻塞当前线程，如果调用unpark方法或者当前线程被中断，才能从park方法返回 |
| LockSupport void unpark(Thread thread)                    | 唤醒处于阻塞状态的线程                                       |
| ConditionObject Node firstWaiter                          | 条件队列首节点                                               |
| ConditionObject Node lastWaiter                           | 条件队列尾节点                                               |
| void await()                                              | 当前线程进入等待状态直到signal或中断，当前线程将进入运行状态且从await方法返回的情况，包括：其他线程调用该Condition的signal或者signalAll方法，且当前线程被选中唤醒；其他线程调用interrupt方法中断当前线程；如果当前线程从await方法返回表明该线程已经获取了Condition对象对应的锁 |
| void awaitUninterruptibly()                               | 和await方法类似，但是对中断不敏感                            |
| long awaitNanos(long nanosTimeout)                        | 当前线程进入等待状态直到被signal、中断或者超时。返回值表示剩余的时间。 |
| boolean awaitUntil(Date deadline)                         | 当前线程进入等待状态直到被signal、中断或者某个时间。如果没有到指定时间就被通知，方法返回true，否则表示到了指定时间，返回false |
| void signal()                                             | 唤醒一个等待在Condition上的线程，该线程从等待方法返回前必须获得与Condition相关联的锁 |
| void signalAll()                                          | 唤醒所有等待在Condition上的线程，能够从等待方法返回的线程必须获得与Condition相关联的锁 |

　　看到这，我们对AQS的数据结构应该基本上有一个大致的认识，有了这个基本面的认识，我们就可以来看下AQS的源代码。

### 1.3 AQS的源代码实现

主要通过独占式同步状态的释放和获取，共享式同步状态的获取和释放

#### 1.3.1 独占式同步状态的获取和释放

独占式同步状态调用的方法是acquire，代码如下

```
public final void acquire(int arg) {
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            selfInterrupt();
    }
```

上诉代码主要完成了同步状态获取，节点构造，加入同步队列以及在同步队列中自旋等待的相关工作，其主要逻辑是：首先调用子类实现了tryAcquire方法，该方法保证线程安全的获取同步状态，如果状态获取失败，则构造独占式同步节点（同一时刻只能有一个线程成功获取同步状态）并通过不addWaiter方法将该节点加入到同步队列的尾部，最后调用acquireQueued方法，使得该节点以自旋的方式获取同步状态，如果获取不到则阻塞节点中的线程，而被阻塞线程的唤醒主要依靠前驱节点的出队或则塞线程被中断来实现的。

下面来首先来看下节点构造和加入同步队列是如何实现的。代码如下：

```
private Node addWaiter(Node mode) {
        // 当前线程构造成Node节点
        Node node = new Node(Thread.currentThread(), mode);
        // Try the fast path of enq; backup to full enq on failure
        // 尝试快速在尾节点后新增节点 提升算法效率 先将尾节点指向pred
        Node pred = tail;
        if (pred != null) {
            //尾节点不为空  当前线程节点的前驱节点指向尾节点
            node.prev = pred;
            //并发处理 尾节点有可能已经不是之前的节点 所以需要CAS更新
            if (compareAndSetTail(pred, node)) {
                //CAS更新成功 当前线程为尾节点 原先尾节点的后续节点就是当前节点
                pred.next = node;
                return node;
            }
        }
        //第一个入队的节点或者是尾节点后续节点新增失败时进入enq
        enq(node);
        return node;
    }
private Node enq(final Node node) {
        for (;;) {
            Node t = tail;
            if (t == null) { // Must initialize
                //尾节点为空  第一次入队  设置头尾节点一致 同步队列的初始化
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {
                //所有的线程节点在构造完成第一个节点后 依次加入到同步队列中
                node.prev = t;
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    return t;
                }
            }
        }
    }
```

节点进入同步队列后，就进入了一个自旋的过程，没有线程节点都在自省的观察，当条件满足，获取到同步状态，就可以从这个自旋过程中退出，否则依旧留在自旋过程中并会则塞节点线程代码如下：

```
final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (;;) {
                //获取当前线程节点的前驱节点
                final Node p = node.predecessor();
                //前驱节点为头节点且成功获取同步状态
                if (p == head && tryAcquire(arg)) {
                    //设置当前节点为头节点
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
                //是否阻塞
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
```

再来看看shouldParkAfterFailedAcquire和parkAndCheckInterrupt是怎么来阻塞当前线程的，代码如下：

```
private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
        //前驱节点的状态决定后续节点的行为
　　　　 int ws = pred.waitStatus;
        if (ws == Node.SIGNAL)
            /*前驱节点为-1 后续节点可以被阻塞
             * This node has already set status asking a release
             * to signal it, so it can safely park.
             */
            return true;
        if (ws > 0) {
            /*
             * Predecessor was cancelled. Skip over predecessors and
             * indicate retry.
             */
            do {
                node.prev = pred = pred.prev;
            } while (pred.waitStatus > 0);
            pred.next = node;
        } else {
            /*前驱节点是初始或者共享状态就设置为-1 使后续节点阻塞
             * waitStatus must be 0 or PROPAGATE.  Indicate that we
             * need a signal, but don't park yet.  Caller will need to
             * retry to make sure it cannot acquire before parking.
             */
            compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
        }
        return false;
    }
private final boolean parkAndCheckInterrupt() {
        //阻塞线程
        LockSupport.park(this);
        return Thread.interrupted();
    }
```

　　节点自旋的过程大致示意图如下，其实就是对图二、图三的补充。

![](/images/QQ%E6%88%AA%E5%9B%BE20190822105854.png)

​							图六  节点自旋获取队列同步状态

整个独占式获取同步状态的流程图大致如下：

![](/images/QQ%E6%88%AA%E5%9B%BE20190822105919.png)

​							图七  独占式获取同步状态

当同步状态获取成功后，当前线程从acquire方法返回，对于锁这种并发组件而言，就意味着当前线程获取了锁。有获取同步状态的方法，就存着其对应的释放方法，该方法为release，现在来看下这个方法的实现，代码如下：

```
public final boolean release(int arg) {
        if (tryRelease(arg)) {//同步状态释放成功
            Node h = head;
            if (h != null && h.waitStatus != 0)
                //直接释放头节点
                unparkSuccessor(h);
            return true;
        }
        return false;
    }
private void unparkSuccessor(Node node) {
        /*
         * If status is negative (i.e., possibly needing signal) try
         * to clear in anticipation of signalling.  It is OK if this
         * fails or if status is changed by waiting thread.
         */
        int ws = node.waitStatus;
        if (ws < 0)
            compareAndSetWaitStatus(node, ws, 0);

        /*寻找符合条件的后续节点
         * Thread to unpark is held in successor, which is normally
         * just the next node.  But if cancelled or apparently null,
         * traverse backwards from tail to find the actual
         * non-cancelled successor.
         */
        Node s = node.next;
        if (s == null || s.waitStatus > 0) {
            s = null;
            for (Node t = tail; t != null && t != node; t = t.prev)
                if (t.waitStatus <= 0)
                    s = t;
        }
        if (s != null)
            //唤醒后续节点
            LockSupport.unpark(s.thread);
    }
```

独占式锁释放非常简单而且明确。

​	总结下独占式同步状态的获取和释放：在获取同步状态时，同步器维护一个同步队列，获取状态失败的队列都会被加入到队列中并在队列中进行自旋：移除队列的条件是前驱节点为头节点且成功获取了同步状态。在释放同步状态时，同步状态调用tryReseleas方法释放同步状态，然后唤醒头节点的后继节点。

#### 1.3.2 共享式同步状态的获取和释放

共享式同步状态调用的方法是acquireShared，代码如下

```
public final void acquireShared(int arg) {
        //获取同步状态的返回值大于等于0时表示可以获取同步状态
        //小于0时表示可以获取不到同步状态  需要进入队列等待
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
private void doAcquireShared(int arg) {
        //和独占式一样的入队操作
        final Node node = addWaiter(Node.SHARED);
        boolean failed = true;
        try {
            boolean interrupted = false;
            //自旋
            for (;;) {
                final Node p = node.predecessor();
                if (p == head) {
                    int r = tryAcquireShared(arg);
                    if (r >= 0) {
                        //前驱结点为头节点且成功获取同步状态 可退出自旋
                        setHeadAndPropagate(node, r);
                        p.next = null; // help GC
                        if (interrupted)
                            selfInterrupt();
                        failed = false;
                        return;
                    }
                }
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
private void setHeadAndPropagate(Node node, int propagate) {
        Node h = head; // Record old head for check below
        //退出自旋的节点变成首节点
        setHead(node);
        /*
         * Try to signal next queued node if:
         *   Propagation was indicated by caller,
         *     or was recorded (as h.waitStatus either before
         *     or after setHead) by a previous operation
         *     (note: this uses sign-check of waitStatus because
         *      PROPAGATE status may transition to SIGNAL.)
         * and
         *   The next node is waiting in shared mode,
         *     or we don't know, because it appears null
         *
         * The conservatism in both of these checks may cause
         * unnecessary wake-ups, but only when there are multiple
         * racing acquires/releases, so most need signals now or soon
         * anyway.
         */
        if (propagate > 0 || h == null || h.waitStatus < 0 ||
            (h = head) == null || h.waitStatus < 0) {
            Node s = node.next;
            if (s == null || s.isShared())
                doReleaseShared();
        }
    }
```

与独占一样，共享获取也需要释放同步状态了，调用releaseShared方法可以释放同步状态，代码如下：

```
public final boolean releaseShared(int arg) {
        //释放同步状态
        if (tryReleaseShared(arg)) {
            //唤醒后续等待的节点
            doReleaseShared();
            return true;
        }
        return false;
    }
private void doReleaseShared() {
        /*
         * Ensure that a release propagates, even if there are other
         * in-progress acquires/releases.  This proceeds in the usual
         * way of trying to unparkSuccessor of head if it needs
         * signal. But if it does not, status is set to PROPAGATE to
         * ensure that upon release, propagation continues.
         * Additionally, we must loop in case a new node is added
         * while we are doing this. Also, unlike other uses of
         * unparkSuccessor, we need to know if CAS to reset status
         * fails, if so rechecking.
         */
        //自旋
　　　　for (;;) {
            Node h = head;
            if (h != null && h != tail) {
                int ws = h.waitStatus;
                if (ws == Node.SIGNAL) {
                    if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                        continue;            // loop to recheck cases
                    //唤醒后续节点
　　　　　　　　　　　　unparkSuccessor(h);
                }
                else if (ws == 0 &&
                         !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                    continue;                // loop on failed CAS
            }
            if (h == head)                   // loop if head changed
                break;
        }
    }
```

unparkSuccessor方法和独占时候一样的

### 1.4 AQS应用

　AQS被大量的应用在了同步工具上。

　　ReentrantLock：ReentrantLock类使用AQS同步状态来保存锁重复持有的次数。当锁被一个线程获取时，ReentrantLock也会记录下当前获得锁的线程标识，以便检查是否是重复获取，以及当错误的线程试图进行解锁操作时检测是否存在非法状态异常。ReentrantLock也使用了AQS提供的ConditionObject，还向外暴露了其它监控和监测相关的方法。

　　ReentrantReadWriteLock：ReentrantReadWriteLock类使用AQS同步状态中的16位来保存写锁持有的次数，剩下的16位用来保存读锁的持有次数。WriteLock的构建方式同ReentrantLock。ReadLock则通过使用acquireShared方法来支持同时允许多个读线程。

　　Semaphore：Semaphore类（信号量）使用AQS同步状态来保存信号量的当前计数。它里面定义的acquireShared方法会减少计数，或当计数为非正值时阻塞线程；tryRelease方法会增加计数，在计数为正值时还要解除线程的阻塞。

　　CountDownLatch：CountDownLatch类使用AQS同步状态来表示计数。当该计数为0时，所有的acquire操作（对应到CountDownLatch中就是await方法）才能通过。

　　FutureTask：FutureTask类使用AQS同步状态来表示某个异步计算任务的运行状态（初始化、运行中、被取消和完成）。设置（FutureTask的set方法）或取消（FutureTask的cancel方法）一个FutureTask时会调用AQS的release操作，等待计算结果的线程的阻塞解除是通过AQS的acquire操作实现的。

　　SynchronousQueues：SynchronousQueues类使用了内部的等待节点，这些节点可以用于协调生产者和消费者。同时，它使用AQS同步状态来控制当某个消费者消费当前一项时，允许一个生产者继续生产，反之亦然。

### 1.5 ReentrantLock

#### 1.5.1 产生原因

[synchronized](https://www.cnblogs.com/iou123lg/p/9190572.html)，这个关键字可以确保对象在并发访问中的原子性、可见性和有序性，这个关键字的底层交由了JVM通过C++来实现，既然是JVM实现，就依赖于JVM，程序员就无法在Java层面进行扩展和优化，肯定就灵活性不高，比如程序员在使用时就无法中断一个正在等待获取锁的线程，或者无法在请求一个锁时无限的等待下去。基于这样一个背景，Doug Lea构建了一个在内存语义上和synchronized一样效果的Java类，同时还扩展了其他一些高级特性，比如定时的锁等待、可中断的锁等待和公平性等，这个类就是ReentrantLock。

#### 1.5.2 源码分析

##### 1.5.2.1 可重入性原理



```
final boolean nonfairTryAcquire(int acquires) {
            final Thread current = Thread.currentThread();//当前线程
            int c = getState();
            if (c == 0) {//表示锁未被抢占
                if (compareAndSetState(0, acquires)) {//获取到同步状态
                    setExclusiveOwnerThread(current); //当前线程占有锁
                    return true;
                }
            }
            else if (current == getExclusiveOwnerThread()) {//线程已经占有锁了 重入
                int nextc = c + acquires;//同步状态记录重入的次数
                if (nextc < 0) // overflow
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            return false;
        }

        protected final boolean tryRelease(int releases) {
            int c = getState() - releases; //既然可重入 就需要释放重入获取的锁
            if (Thread.currentThread() != getExclusiveOwnerThread())
                throw new IllegalMonitorStateException();
            boolean free = false;
            if (c == 0) {
                free = true;//只有线程全部释放才返回true
                setExclusiveOwnerThread(null); //同步队列的线程都可以去获取同步状态了
            }
            setState(c); 
            return free;
        }
```

ReentrantLock类用AQS同步状态来保存锁重复持有的数。当锁被一个线程获取时，ReedtrantLock也后记录下获得锁的线程标识，以便检查是否是重复获取，以及当偶偶无的线程试图进行解锁操作时检查是否存在非法状态异常。

##### 1.5.2.2 获取和释放锁

如下是获取和释放锁的方法：

​	

```
public void lock() {
   sync.lock();//获取锁
}
public void unlock() {
   sync.release(1); //释放锁
}
```

获取锁的时候是依赖内部类Sync的lock（）方法，该方法有两个实现类，分别是非公平锁NonfairSync和公平锁FairSync。具体咱们下一小节分析。在来看下释放锁，释放锁的时候时间调用的是AQS的release方法，代码如下：

```
public final boolean release(int arg) {
        if (tryRelease(arg)) {//调用子类的tryRelease 实际就是Sync的tryRelease
            Node h = head;//取同步队列的头节点
            if (h != null && h.waitStatus != 0)//同步队列头节点不为空且不是初始状态
                unparkSuccessor(h);//释放头节点 唤醒后续节点
            return true;
        }
        return false;
}
```

Sync的tryRelease就是上一小节的重入释放方法，如果同一个线程，那么锁的重入次数就一次递减，直到重入次数为0，此方法才会返回true，此时断开头结点唤醒后继节点去获取AQS的同步状态

##### 1.5.2.3 公平锁和非公平锁

公平锁还是非公平锁取决于ReentrantLock的构造方法，默认无参的是NonfairSync，含参构造方法FairSync，入参false为NonfairSync

```
public ReentrantLock() {
   sync = new NonfairSync();
}
public ReentrantLock(boolean fair) {
   sync = fair ? new FairSync() : new NonfairSync();
}
```

再 分别看非公平和公平锁的实现

```
static final class NonfairSync extends Sync {
        private static final long serialVersionUID = 7316153563782823691L;

        /**
         * Performs lock.  Try immediate barge, backing up to normal
         * acquire on failure.
         */
        final void lock() {
            if (compareAndSetState(0, 1))//通过CAS来获取同步状态 也就是锁
                setExclusiveOwnerThread(Thread.currentThread());//获取成功线程占有锁
            else
                acquire(1);//获取失败 进入AQS同步队列排队等待 执行AQS的acquire方法 
        }

        protected final boolean tryAcquire(int acquires) {
            return nonfairTryAcquire(acquires);
        }
    }
```

在AQS的acquire方法中先调用子类tryAcquire,也就是nonfairTryacquire。可以看出非公平锁中，抢到AQS的同步状态的未必是同步队列的首节点，只要线程通过CAS抢到了同步状态或者再acquire中抢到同步状态，就优先占有锁。而相对同步队列这个严格的FIFO队列来说，所以会被认为非公平锁。

```
static final class FairSync extends Sync {
        private static final long serialVersionUID = -3000897897090466540L;

        final void lock() {
            acquire(1);//严格按照AQS的同步队列要求去获取同步状态
        }

        /**
         * Fair version of tryAcquire.  Don't grant access unless
         * recursive call or no waiters or is first.
         */
        protected final boolean tryAcquire(int acquires) {
            final Thread current = Thread.currentThread();//获取当前线程
            int c = getState();
            if (c == 0) {//锁未被抢占
                if (!hasQueuedPredecessors() &&//没有前驱节点
                    compareAndSetState(0, acquires)) {//CAS获取同步状态
                    setExclusiveOwnerThread(current);
                    return true;
                }
            }
            else if (current == getExclusiveOwnerThread()) {//锁已被抢占且线程重入
                int nextc = c + acquires;//同步状态为重入次数
                if (nextc < 0)
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            return false;
        }
    }
```

公平锁的实现直接调用AQS的acquire方法。acquire中调用tryAcquire。和非公平锁相比，这里不会执行一次cas接下来在tryAcquire去抢占锁的时候，也会先调用hasQueuedPredeessors看看前面是否有节点已经在等待获取锁了，如果存在则同步队列的前驱节点优先。

```
public final boolean hasQueuedPredecessors() {
        // The correctness of this depends on head being initialized
        // before tail and on head.next being accurate if the current
        // thread is first in queue.
        Node t = tail; // Read fields in reverse initialization order 尾节点
        Node h = head;//头节点
        Node s;
        return h != t &&//头尾节点不是一个 即队列存在排队线程
            ((s = h.next) == null || s.thread != Thread.currentThread());//头节点的后续节点为空或者不是当前线程
    }
```

#### 1.5.3 应用

##### 1.5.3.1 普通的线程锁

​	标准形式如下：

```
ReentrantLock lock = new ReentrantLock();
try {
        lock.lock();
     //……
 }finally {
     lock.unlock();
 }
```

这种用法和Synchronized效果是一样的，但是必须显示的申明lock和unlock

1.5.3.2 带限制的锁

```
public boolean tryLock()// 尝试获取锁,立即返回获取结果 轮询锁
public boolean tryLock(long timeout, TimeUnit unit)//尝试获取锁,最多等待 timeout 时长 超时锁
public void lockInterruptibly()//可中断锁,调用线程 interrupt 方法,则锁方法抛出 InterruptedException  中断锁
```

##### 1.5.3.3 等待/通知模型

内置队列存在一些缺陷，每个内置锁只能关联一个条件队列，这导致多个线程可能会在同一个条件队列上等待不同的条件，如果使用notify唤醒条件队列，可能由于唤醒错误的线程导致唤醒失败，如果使用notifyall的话，能唤醒到正确的线程因为所有的线程都会被唤醒，这也带来一个问题，就是不应该被唤醒的在被唤醒后发现不是自己等待的条件线程转而又被挂起。这样的操作会带来系统的资源浪费，降低系统性能。这时候推荐使用显示LOCK和condition来替代内置锁和条件队列，从而控制多个条件，达到精确控制线程的唤醒和挂起

##### 1.5.3.4 与Synchronized 比较

| synchronized                                      | ReentrantLock                 |
| ------------------------------------------------- | ----------------------------- |
| 使用Object本身的wait、notify、notifyAll调度机制   | 与Condition结合进行线程的调度 |
| 显式的使用在同步方法或者同步代码块                | 显式的声明指定起始和结束位置  |
| 托管给JVM执行，不会因为异常、或者未释放而发生死锁 | 手动释放锁                    |

　　Jdk1.6之前，ReentrantLock性能优于synchronized，不过1.6之后，synchronized做了大量的性能调优，而且synchronized相对程序员来说，简洁熟悉，如果不是synchronized无法实现的功能，如轮询锁、超时锁和中断锁等，推荐首先使用synchronized，而针对锁的高级功能，再使用ReentrantLock。

1.6 ReentrantReadWriteLock

#### 1.6.1产生背景

前面的ReentrantLock本质上还是互斥锁，每次最多只能有一个线程持有ReentrantLock。对于维护数据完整性来说，互斥通常是一种过于强硬的规则，因此也就不必要的限制了并发性。互斥是一种保守的加锁策略，虽然可以避免“写写”冲突，但也避免了读读冲突。和互联网的二八法则一样，大部分数据都是读数据可以存放在缓存中，数据结构的操作其实很多也是读操作，可以考虑适当的放宽加锁需求，允许多个读操作可以同时访问数据结构以提升程序的性能。在这样的需求背景下，就产生了读写锁，一个资源可以同步被多个读操作，或者被一个写访问，但是不能读写同时访问。ReadWriteLock定义了接口规范，实际读写锁控制类还是ReentrantReadWriteLock ，该类为读写锁提供了可重入的加锁语义。

#### 1.6.2 源码分析

##### 1.6.2.1 读写锁原理

 既然是读写锁。那就有两把锁，可以用AQS的同步状态表示其中的一把锁，再引入一个新的属性表示另外一把锁，但是这么做就变成了二元并发安全问题，使问题变得更加复杂。ReentrantReadWriteLock选择了用一个属性，即AQS 的同步状态来表示读写锁，怎么用一个属性来表示读写锁呢？那就是位运算

​    ReentantReadWriteLock采用“按位切割”的方式，就是将这个32位的int型state变量分为高16位和低16位，高16位代表读状态，低16位代表写状态。读锁可以共享，写锁是互斥的，对于写锁而言用低16位表示线程重入的次数，但是读锁因为可以同时有多个线程 ，所以重入次数需要通过其他的方式来记录，那就是ThreadLocal变量。从这可以总结出与ReentrantLock相比，写锁的重入次数会减少，最多不能超过65535次。读锁的线程数也有限制，最多不能超过65535个。

​      假设状态变量是c，则读状态就是c>>>16（无符号右移16位），其实就是通过无符号右移运算抹掉低的16位，剩下的就是c的高16位。写状态时c&(（1<<16）-1),其实就是00000000000000001111111111111111，与运算

之后高16位被抹掉，剩下的就是c的低16位。如果读线程申请读锁，当前写锁鸿儒次数不为0时，则等待，否则马上分配；如果是写线程申请写锁，当前状态为0则马上分配，否则等第。

##### 1.6.2.2 读锁的获取和释放

```
protected final int tryAcquireShared(int unused) {
            Thread current = Thread.currentThread();//当前线程
            int c = getState();
                //持有写锁的线程可以获取读锁
            if (exclusiveCount(c) != 0 &&  //已经分配了写锁
                getExclusiveOwnerThread() != current) //当前线程不是持有写锁的线程
                return -1;
            int r = sharedCount(c); //读锁获取次数
            if (!readerShouldBlock() && //由子类根据公平策略实现决定是否可获取读锁
                r < MAX_COUNT && //读锁获取次数小于最大值
                compareAndSetState(c, c + SHARED_UNIT)) {//更新读锁状态
                if (r == 0) {//读锁的第一个线程 此时可以不用记录到ThreadLocal
                    firstReader = current; 
                    firstReaderHoldCount = 1; //避免查找ThreadLocal 提升效率
                } else if (firstReader == current) {//读锁的第一个线程重入
                    firstReaderHoldCount++;
                } else {//非读锁的第一个线程
                    HoldCounter rh = cachedHoldCounter; //下面为重入次数更新
                    if (rh == null || rh.tid != getThreadId(current))
                        cachedHoldCounter = rh = readHolds.get();
                    else if (rh.count == 0)
                        readHolds.set(rh);
                    rh.count++;
                }
                return 1;
            }
            return fullTryAcquireShared(current); //获取读锁失败 循环重试
        }
final int fullTryAcquireShared(Thread current) {
            HoldCounter rh = null;
            for (;;) {
                int c = getState();
                if (exclusiveCount(c) != 0) {//获取到写锁
                    if (getExclusiveOwnerThread() != current) 
                        return -1; //非写锁线程获取失败
                    // else we hold the exclusive lock; blocking here
                    // would cause deadlock.
                } else if (readerShouldBlock()) {
                    // Make sure we're not acquiring read lock reentrantly
                    if (firstReader == current) {
                        // assert firstReaderHoldCount > 0;
                    } else {
                        if (rh == null) {
                            rh = cachedHoldCounter;
                            if (rh == null || rh.tid != getThreadId(current)) {
                                rh = readHolds.get();
                                if (rh.count == 0)
                                    readHolds.remove();
                            }
                        }
                        if (rh.count == 0)
                            return -1;
                    }
                }
                if (sharedCount(c) == MAX_COUNT) //读锁数量达到最大
                    throw new Error("Maximum lock count exceeded");
                if (compareAndSetState(c, c + SHARED_UNIT)) {//读锁获取成功 处理方式和之前类似
                    if (sharedCount(c) == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                    } else {
                        if (rh == null)
                            rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                        cachedHoldCounter = rh; // cache for release
                    }
                    return 1;
                }
            }
        }
```

读锁的释放方法如下：

```
protected final boolean tryReleaseShared(int unused) {
            Thread current = Thread.currentThread();
            if (firstReader == current) {//当前线程是读锁的第一个线程
                // assert firstReaderHoldCount > 0;
                if (firstReaderHoldCount == 1) //第一次占有读锁 直接清除该线程
                    firstReader = null;
                else
                    firstReaderHoldCount--;//读锁的第一个线程重入次数减少
            } else {
                HoldCounter rh = cachedHoldCounter;
                if (rh == null || rh.tid != getThreadId(current))
                    rh = readHolds.get();
                int count = rh.count;
                if (count <= 1) {
                    readHolds.remove();//读锁释放
                    if (count <= 0)
                        throw unmatchedUnlockException();
                }
                --rh.count; //重入次数减少
            }
            for (;;) {
                int c = getState();
                int nextc = c - SHARED_UNIT;
                    //减少读锁的线程数量
                if (compareAndSetState(c, nextc))
                    // Releasing the read lock has no effect on readers,
                    // but it may allow waiting writers to proceed if
                    // both read and write locks are now free.
                    return nextc == 0;
            }
        }

```

##### 1.6.2.3 写锁的获取和释放

　写锁的获取方法如下：

```
protected final boolean tryAcquire(int acquires) {
            Thread current = Thread.currentThread();
            int c = getState();
            int w = exclusiveCount(c);//写锁状态
            if (c != 0) {//表示锁已经被分配出去了 if c != 0 and w == 0表示获取读锁
                // (Note: if c != 0 and w == 0 then shared count != 0)
                    //其他线程获取到了写锁
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;
                    //写锁重入次数超过最大值
                if (w + exclusiveCount(acquires) > MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                // Reentrant acquire  更新写锁重入次数
                setState(c + acquires);
                return true;
            }
            if (writerShouldBlock() ||//子类实现写锁是否公平获取
                !compareAndSetState(c, c + acquires))
                return false;//cas获取写锁失败
            setExclusiveOwnerThread(current);//获取写锁成功 独占
            return true;
        }

```

　写锁的释放方法如下：

```
protected final boolean tryRelease(int releases) {
            if (!isHeldExclusively())//当前线程不持有写锁
                throw new IllegalMonitorStateException();
            int nextc = getState() - releases; //重入次数减少
            boolean free = exclusiveCount(nextc) == 0; //减少到0写锁释放
            if (free)
                setExclusiveOwnerThread(null); //写锁释放
            setState(nextc);
            return free;
        }

```

#### 1.6.3 锁降级

锁降级指的是写锁降级为读锁，首先持有当前写锁，然后获取到读锁，在tryAcquireShared方法中已经体现了该过程，随后再释放该写锁的过程。锁降级主要是为了保持数据的可见性，如果当前线程不获取读锁而是直接释放写锁，假设此时有另外的线程获取到了写锁并修改了数据，那么当前线程是无法知晓数据已经更新了。如果当前线程遵循锁降级的过程，则其他线程会被阻塞，直到当前线程操作完成其他线程才可以获取写锁进行数据更新。RentrantReadWriteLock不支持锁升级（把持读锁、获取写锁，最后释放读锁的过程）。目的也是保证数据可见性，如果读锁已被多个线程获取，其中任意线程成功获取了写锁并更新了数据，则其更新对其他获取到读锁的线程是不可见的。

#### 1.6.4 使用案例

```
public class MapCache<T> {
  private ReadWriteLock lock = new ReentrantReadWriteLock();
  private Map<String, T> cache = new HashMap<>();

  public void put(String key, T value) {
    lock.writeLock().lock();
    try {
      cache.put(key, value);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public T get(String key) {
    lock.readLock().lock();
    try {
      T result = cache.get(key);
      return result;
    } finally {
      lock.readLock().unlock();
    }
  }
}

```

## 3.阻塞队列

java队列示意图：

![](images/%E9%98%BB%E5%A1%9E%E9%98%9F%E5%88%97%E5%9B%BE.png)

阻塞队列（BlockingQueue）是一个支持两个附加操作的队列。这两个附加的操作支持阻塞插入和移出方法。

1.阻塞插入：当队列满时，队列会阻塞插入元素的线程，直到队列不满

2.阻塞移出：当队列空时，获取元素的线程会等待队列变为非空

### 2.1 java中的阻塞队列

![](images/java%E4%B8%AD%E9%98%BB%E5%A1%9E%E9%98%9F%E5%88%97.png)

ØArrayBlockingQueue：基于数组结构的有界阻塞队列（长度不可变）；

ØLinkedBlockingQueue：基于链表结构的有界阻塞队列（默认容量 Integer.MAX_VALUE）；

ØLinkedTransferQueue：基于链表结构的无界阻塞/传递队列；

ØLinkedBlockingDeque：基于链表结构的有界阻塞双端队列（默认容量 Integer.MAX_VALUE）；

ØSynchronousQueue：不存储元素的阻塞/传递队列；

ØPriorityBlockingQueue：支持优先级排序的无界阻塞队列；

ØDelayQueue：支持延时获取元素的无界阻塞队列; 

## 线程池

### 线程池产生原因

1.减低资源消耗，通过重复使用以创建的线程降低线程创建和销毁的消耗。

2.提高响应速度

3.提高线程的可管理性 

### 线程池的实现原理

##### 3.2.1 原理

![](images/QQ%E6%88%AA%E5%9B%BE20190823223555.png)

1.使用队列来存放新添加任务，worker重写run方法，在worker中循环获取队列中的任务调用run方法。简单实现如下：

```
package com.ty.thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 */
public class ThreadPoolExecutor {
    
    /*
     * BlockingQueue是阻塞队列，在两种情况下出现阻塞：
     *     1、当队列满了，入队列操作时；
     *     2、当队列空了，出队列操作时。
     * 阻塞队列是线程安全的，主要使用在生产/消费者的场景
     */
    private BlockingQueue<Task> blockingQueue;
    
    //线程池的工作线程数(可以认为是线程池的容量)
    private int poolSize = 0;
    
    //线程池的核心容量(也就是当前线程池中真正存在的线程个数)
    private int coreSize = 0;
    
    /*
     * 此地方使用volatile关键字，volatile的工作原理是：对于JVM维度来说，每个线程持有变量的工作副本，那对于计算机维度来说，
     * 就是这些变量的中间值会存放在高速缓存中。通过volatile关键字，告知每个线程改变此变量之后，立马更新到内存中去，并且使得
     * 缓存中的数据失效，这样来保证其中某个线程改变公有变量后，其他线程能及时读取到最新的变量值，从而保证可见性。
     * 原因如下：
     *     1、在ThreadPoolExecutorTest中操作shutDown，这是main线程操作此变量(由于变量是volatile声明，所以会立马写入内存中)；
     *     2、Worker中线程通过while(!shutDown)来判断当前线程是否应该关闭，因此需通过volatile保证可见性，使线程可以及时得到关闭。
     */
    private volatile boolean shutDown = false;
    
    public ThreadPoolExecutor(int size) {
        this.poolSize = size;
        //LinkedBlockingQueue的大小可以指定，不指定即为无边界的。
        blockingQueue = new LinkedBlockingQueue<>(poolSize);
    }
    
    public void execute(Task task) throws InterruptedException {
        if(shutDown == true) {
            return;
        }
        
        if(coreSize < poolSize) {
            /*
             * BlockingQueue中的插入主要有offer(obj)以及put(obj)两个方法，其中put(obj)是阻塞方法，如果插入不能马上进行，
             * 则操作阻塞；offer(obj)则是插入不能马上进行，返回true或false。
             * 本例中的Task不允许丢失，所以采用put(obj);
             */
            blockingQueue.put(task);
            produceWorker(task);
        }else {
            blockingQueue.put(task);
        }
    }

    private void produceWorker(Task task) throws InterruptedException {
        if(task == null) {
            throw new NullPointerException("非法参数：传入的task对象为空！");
        }

        Thread thread = new Thread(new Worker());        
        thread.start();
        coreSize++;
    }
    
    /*
     * 真正中断线程的方法，是使用共享变量发出信号，告诉线程停止运行。
     * 
     */
    public void shutDown() {
        shutDown = true;
    }
    
    /*
     * 此内部类是实际上的工作线程
     * 
     */
    class Worker implements Runnable {

        @Override
        public void run() {        
            while(!shutDown) {    
                try {
                    //
                    blockingQueue.take().doJob();
                } catch (InterruptedException e) {                    
                    e.printStackTrace();
                }
            }            
            System.out.println("线程：" + Thread.currentThread().getName() + "退出运行！");
        }    
    }
}

```

```

```

需要被执行的任务类

```
package com.ty.thread;

/**
 */
public class Task {

    //通过taskId对任务进行标识
    private int taskId;
    
    public Task(int taskId) {
        this.taskId = taskId;
    }

    public void doJob() {
        System.out.println("线程" + Thread.currentThread().getName() + "正在处理任务！");
    }

    public int getId() {        
        return taskId;
    }
}

```

测试类

```
package com.ty.thread;

/**
 * @author Taoyong
 * @date 2018年5月17日
 * 天下没有难敲的代码！
 */
public class ThreadPoolExecutorTest {

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3);
        for(int i = 0; i < 10; i++) {
            Task task = new Task(i);
            threadPoolExecutor.execute(task);                
        }        
        
        threadPoolExecutor.shutDown();
    }
}

```

##### 3.2.2 代码分析

###### 3.2.2.1 继承体系

![](images/executor%E7%BB%A7%E6%89%BF%E4%BD%93%E7%B3%BB.png)



继承体系上，最顶层只提供了一个简单的execute（Runnable command）方法，然后executorService提供了一些管理相关的方法。例如关闭，判断当前线程池的状态等。另外ExecutorService提供了一些列方法，可以将任务包装台成一个future，从而使得任务提交可以跟踪，而父类则提供了一些默认的实现

###### 3.2.2.2 构造器

ThreadPoolExecutor的构造器提供了非常多的参数，每一个参数都非常的重要，一不小心就容易踩坑，因此设置的时候，你必须要知道自己在干什么

```
public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }

```

1**.corePoolSize,maxmumPoolSize**.线程池会自动根据corePoolSize和maxIMumPool去调整当前线程池的大小。当你通过submit或者execute方法提交任务，如果当前线程池的数量小于corePoolSize。那么线程池就会创建一个新的线程去处理任务，即使其他core线程是空闲的。如果当前线程数大于corePoolSize并且小于maximumPoolSize，那么只有在队列满的时候才会创建新的线程。这里的坑很多，比如你的core和max线程数设置的不一样，希望请求积压队列的时候能够实时的扩容，但如果指定了一个无界的队列，那么就不会扩容了，因此队列不存在满的概念。

2**.keepAliveTime**。如果当前线程池中的线程超过了corePoolSize，那么如果在KeepAliveTime时间内没有新的任务需要处理，那么超过corePoolSize的这部分线程就会被销毁。默认情况下不会回收core线程的，可通过设置allowCoreThreadTimeOut改变这一行为。

3.workQueue。即实际用于存储任务的队列，可以说是最核心的一个参数了，直接决定了线程池的行为，比如时候传入一个有界队列，那么队列满的时候，线程就会根据core和max参数的设置情况决定是够需要扩容，如果传入了一个SynchronizedQueue这个队列只有在另一个线程在同步remove的时候才可以put成功，对应到线程池中，简单来说就是如果有线程池任务处理完了，吊哦用poll或者take方法获取新的任务色时候，新任务才会put成功，否则如果当前的线程都在忙着处理任务，那么就会put失败，也就会走扩容的逻辑，如果传入了一个DelayedWorkQueue ，顾名思义，任务会根据过期时间来决定什么时候弹出。

4.threadFactory，创建线程都是通过ThreadFactory来实现的，如果没有指定的话，默认会使用，executor.defaultThreadFactory,一般来说我们会在这里对线程名称，异常处理等，原因：因为线程池使用的是池中已经初始化好的线程，当使用时会忽略Thread 中设置的值

5.handler。即当任务提交失败的时候，会调用这个处理器，ThreadPoolExecutor内置了多个实现，比如抛异常，直接抛弃等。这里了需要根据业务场景进行设置。

```java
系统中默认有四种拒绝策略，这四种拒绝策略被封装在ThreadPoolExecutor中：

AbortPolicy：该策略是线程池中的默认策略。使用该策略时，如果线程池队列满了丢掉这个任务并且抛出RejectedExecutionException异常

DiscardPolicy：这个策略和AbortPolicy的slient版本，如果线程池队列满了，会直接丢掉这个任务并且不会有任何异常

DiscardOldestPolicy：这个策略从字面上也很好理解，丢弃最老的。也就是说如果队列满了，会将最早进入队列的任务删掉腾出空间，再尝试加入队列。
因为队列是队尾进，队头出，所以队头元素是最老的，因此每次都是移除对头元素后再尝试入队。

CallerRunsPolicy：使用此策略，如果添加到线程池失败，那么主线程会自己去执行该任务，不会等待线程池中的线程去执行。就像是个急脾气的人，我等不到别人来做这件事就干脆自己干。
```

###### 3.2.2.2.1 使用方法

```
public class ExecutorServiceDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int corePoolSize =1;
        int maximumPoolSize =1;
        BlockingQueue<Runnable> queue=new ArrayBlockingQueue(2);
        ThreadFactory threadFactory = new NameTreadFactory();
        RejectedExecutionHandler handler = new MyIgnorePolicy();

        //ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,1000,TimeUnit.MILLISECONDS,queue,threadFactory,handler);

        //默认的 线程工厂
        Executors.defaultThreadFactory();
        //自己创建的线程工厂
        CustomThreadFactoryBuilder customThreadFactoryBuilder;

        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,1000,TimeUnit.MILLISECONDS,queue,
                new CustomThreadFactoryBuilder().setNamePrefix("我的线程").setDaemon(false).setPriority(1).build(),new ThreadPoolExecutor.AbortPolicy());
        for (int i=0;i<10;i++){
        
         * 执行时有以下两种方式：
              executor.submit  ：如果是CallAble  可获取返回信息 Future<?> ,如果是其他类型runable，和thread 则放回null，内部实现了FutrueTask，如果是非callable 不建议使用
 * executor.execute ： 没有返回值
            executor.execute(new ThreadDeom());
            Future f= executor.submit(new ThreadDeom());
            System.out.println("  future => "+f.get());
            System.out.println();
        }

        executor.shutdown();
    }
}

class NameTreadFactory implements ThreadFactory {
     private final AtomicInteger mThreadNum = new AtomicInteger(1);


    public Thread newThread(Runnable r) {
        Thread t=new Thread(r, "my-thread-" + mThreadNum.getAndIncrement());
        System.out.println(t.getName()+" has been created");
        return t;
    }
}

class MyIgnorePolicy implements RejectedExecutionHandler {

    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        System.out.println(r.toString() +" rejected");
    }
}

class ThreadDeom implements Runnable{

    public ThreadDeom(){
        Thread.currentThread().setName("线程名称666");
    }
    public void run() {
        System.out.println("线程执行了-----------------"+ Thread.currentThread().getName());
    }
}


class CustomThreadFactoryBuilder {

    private String namePrefix = null;
    private boolean daemon = false;
    private int priority = Thread.NORM_PRIORITY;

    public CustomThreadFactoryBuilder setNamePrefix(String namePrefix) {
        if (namePrefix == null) {
            throw new NullPointerException();
        }
        this.namePrefix = namePrefix;
        return this;
    }

    public CustomThreadFactoryBuilder setDaemon(boolean daemon) {
        this.daemon = daemon;
        return this;
    }

    public CustomThreadFactoryBuilder setPriority(int priority) {
        if (priority < Thread.MIN_PRIORITY){
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be >= %s", priority, Thread.MIN_PRIORITY));
        }

        if (priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException(String.format(
                    "Thread priority (%s) must be <= %s", priority, Thread.MAX_PRIORITY));
        }

        this.priority = priority;
        return this;
    }

    public ThreadFactory build() {
        return build(this);
    }

    private static ThreadFactory build(CustomThreadFactoryBuilder builder) {
        final String namePrefix = builder.namePrefix;
        final Boolean daemon = builder.daemon;
        final Integer priority = builder.priority;
        final AtomicLong count = new AtomicLong(0);
         /*
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable);
                if (namePrefix != null) {
                    thread.setName(namePrefix + "-" + count.getAndIncrement());
                }
                if (daemon != null) {
                    thread.setDaemon(daemon);
                }
                if (priority != null) {
                    thread.setPriority(priority);
                }
                return thread;
            }
        };*/
        //jdk8中还是优先使用lamb表达式
        return (Runnable runnable) -> {
           Thread thread = new Thread(runnable);
            if (namePrefix != null) {
                thread.setName(namePrefix + "-" + count.getAndIncrement());
            }
            if (daemon != null) {
                thread.setDaemon(daemon);
            }
//                /*
//                    thread.setPriority(priority);
//                */
            return thread;
        };
    }
}

```



###### 3.2.2.3 源码分析

既然 是一个线程池，那就必须有其生命周期运行中，关闭，停止等ThreadPoolExcutor是一个用AtomicIntegr去的前三位表示这个状态，另外又重用了低29位用于表示线程数，可以支持最大大概5亿多，如果以后硬件能够启用这么多线程，该成AtimicLong就可以了 。

状态这里分为下面几种

1.RUNNING: 表示当前线程正在运行中，可以接受新的任务及处理队列中的任务

2.SHOTDOWM :不再接受新的任务，但会继续处理队列中的任务

3.STOP ： 不再接受新的任务，也不会处理队列中的任务，并且会中断正在执行的任务

4.TIDYING 所有任务都已经处理完毕，线程数为0 ，转为TIDYING状态后会调用treminated回调

5.TERMINATED:terminated已经执行完毕

同时我们可以看到所有的状态都是用二进制位表示的，并且依次递增，从而方便进行比较，比如想获取当前状态是够至少为SHUTDOWM等，同时状态之前有几种转换；

1. RUNNING -> SHUTDOWN。调用了`shutdown()`之后，或者执行了`finalize()`

2. (RUNNING 或者 SHUTDOWN) -> STOP。调用了`shutdownNow()`之后会转换这个状态

3. SHUTDOWN -> TIDYING。当线程池和队列都为空的时候

4. STOP -> TIDYING。当线程池为空的时候

5. IDYING -> TERMINATED。执行完`terminated()`回调之后会转换为这个状态


```
 private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));
    private static final int COUNT_BITS = Integer.SIZE - 3;
    private static final int CAPACITY   = (1 << COUNT_BITS) - 1;

    private static final int RUNNING    = -1 << COUNT_BITS;
    private static final int SHUTDOWN   =  0 << COUNT_BITS;
    private static final int STOP       =  1 << COUNT_BITS;
    private static final int TIDYING    =  2 << COUNT_BITS;
    private static final int TERMINATED =  3 << COUNT_BITS;

    //由于前三位表示状态，因此将CAPACITY取反，和进行与操作即可
    private static int runStateOf(int c)     { return c & ~CAPACITY; }
    private static int workerCountOf(int c)  { return c & CAPACITY; }
    
    //高三位+第三位进行或操作即可
    private static int ctlOf(int rs, int wc) { return rs | wc; }

    private static boolean runStateLessThan(int c, int s) {
        return c < s;
    }

    private static boolean runStateAtLeast(int c, int s) {
        return c >= s;
    }

    private static boolean isRunning(int c) {
        return c < SHUTDOWN;
    }
    
    //下面三个方法，通过CAS修改worker的数目
    private boolean compareAndIncrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect + 1);
    }
    
    //只尝试一次，失败了则返回，是否重试由调用方决定
    private boolean compareAndDecrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect - 1);
    }
    
    //跟上一个不一样，会一直重试
    private void decrementWorkerCount() {
        do {} while (! compareAndDecrementWorkerCount(ctl.get()));
    }

```

下面是比较核心的字段，这里works采用的是用非线程的hashset，而不是线程安全的版本，主要是因为这里有些复用的操作，比如说将worker添加到workers后，我们还需要判断是否需要跟新largestPoolSize等，workers只在获取到mainLock的情况下才会进行读写，另外这里了的mainLock也用于在中断线程的时候串行执行，否则不加锁的话，可能会造成并发去中断线程引起不必要的中断问题。

```
private final ReentrantLock mainLock = new ReentrantLock();

private final HashSet<Worker> workers = new HashSet<Worker>();

private final Condition termination = mainLock.newCondition();

private int largestPoolSize;

private long completedTaskCount;

```

**核心方法**

拿到一个线程池之后，我们可以开始提交任务，让它去执行了，那么我们看一下submit方法是如何实现的

```
public Future<?> submit(Runnable task) {
        if (task == null) throw new NullPointerException();
        RunnableFuture<Void> ftask = newTaskFor(task, null);
        execute(ftask);
        return ftask;
    }

    public <T> Future<T> submit(Callable<T> task) {
        if (task == null) throw new NullPointerException();
        RunnableFuture<T> ftask = newTaskFor(task);
        execute(ftask);
        return ftask;
    }

```

这两个方法都很简单，首先提交过来的任务（Callable，Runnable）都包装成统一的RunnableFuture，然后调用exeute方法，execute可以说是线程池最核心的一个方法。



```
  public void execute(Runnable command) {
        if (command == null)
            throw new NullPointerException();
        int c = ctl.get();
        /*
            获取当前worker的数目，如果小于corePoolSize那么就扩容，
            这里不会判断是否已经有core线程，而是只要小于corePoolSize就会直接增加worker
         */
        if (workerCountOf(c) < corePoolSize) {
            /*
                调用addWorker(Runnable firstTask, boolean core)方法扩容
                firstTask表示为该worker启动之后要执行的第一个任务，core表示要增加的为core线程
             */
            if (addWorker(command, true))
                return;
            //如果增加失败了那么重新获取ctl的快照,比如可能线程池在这期间关闭了
            c = ctl.get();
        }
        /*
             如果当前线程池正在运行中，并且将任务丢到队列中成功了，
             那么就会进行一次double check,看下在这期间线程池是否关闭了，
             如果关闭了，比如处于SHUTDOWN状态，如上文所讲的，SHUTDOWN状态的时候，
             不再接受新任务，remove成功后调用拒绝处理器。而如果仍然处于运行中的状态，
             那么这里就double check下当前的worker数，如果为0，有可能在上述逻辑的执行
             过程中，有worker销毁了，比如说任务抛出了未捕获异常等，那么就会进行一次扩容，
             但不同于扩容core线程，这里由于任务已经丢到队列中去了，因此就不需要再传递firstTask了，
             同时要注意，这里扩容的是非core线程
         */
        if (isRunning(c) && workQueue.offer(command)) {
            int recheck = ctl.get();
            if (! isRunning(recheck) && remove(command))
                reject(command);
            else if (workerCountOf(recheck) == 0)
                addWorker(null, false);
        }
        else if (!addWorker(command, false))
            /*
                如果在上一步中，将任务丢到队列中失败了，那么就进行一次扩容，
                这里会将任务传递到firstTask参数中，并且扩容的是非core线程，
                如果扩容失败了，那么就执行拒绝策略。
             */
            reject(command);
    }

```

这里要特别注意下防止队列失败的逻辑，不同的队列丢任务的逻辑也不一样，例如时候无界队列，那么就永远不会put失败，也就是说扩容永远不会执行，如果是有界队列，那么当队列满的时候会扩容非core线程，如果是SynchronizrdQueue，这个队列比较特殊，当有另一个线程正在通弄不获取任务的时候，那么后续的每次新任务都会导致扩容，当然如果worker没有任务处理了，阻塞在获取这一步的时候，新任务的提交就会直接丢到队列中去，而不会扩容。

上文中多次提都了扩容，那么我们下面看一下线程具体是如果扩容的：

```
private boolean addWorker(Runnable firstTask, boolean core) {
        retry:
        for (;;) {
            int c = ctl.get();
            //获取当前线程池的状态
            int rs = runStateOf(c);

            /*
                如果状态为大于SHUTDOWN, 比如说STOP，STOP上文说过队列中的任务不处理了，也不接受新任务，
                因此可以直接返回false不扩容了，如果状态为SHUTDOWN并且firstTask为null，同时队列非空，
                那么就可以扩容
             */
            if (rs >= SHUTDOWN &&
                ! (rs == SHUTDOWN &&
                    firstTask == null &&
                    ! workQueue.isEmpty()))
                return false;

            for (;;) {
                int wc = workerCountOf(c);
                /*
                    若worker的数目大于CAPACITY则直接返回，
                    然后根据要扩容的是core线程还是非core线程，进行判断worker数目
                    是否超过设置的值，超过则返回
                 */
                if (wc >= CAPACITY ||
                    wc >= (core ? corePoolSize : maximumPoolSize))
                    return false;
                /*
                    通过CAS的方式自增worker的数目，成功了则直接跳出循环
                 */
                if (compareAndIncrementWorkerCount(c))
                    break retry;
                //重新读取状态变量，如果状态改变了，比如线程池关闭了，那么就跳到最外层的for循环，
                //注意这里跳出的是retry。
                c = ctl.get();  // Re-read ctl
                if (runStateOf(c) != rs)
                    continue retry;
                // else CAS failed due to workerCount change; retry inner loop
            }
        }

        boolean workerStarted = false;
        boolean workerAdded = false;
        Worker w = null;
        try {
            //创建Worker
            w = new Worker(firstTask);
            final Thread t = w.thread;
            if (t != null) {
                final ReentrantLock mainLock = this.mainLock;
                mainLock.lock();
                try {
                    /*
                        获取锁，并判断线程池是否已经关闭
                     */
                    int rs = runStateOf(ctl.get());

                    if (rs < SHUTDOWN ||
                        (rs == SHUTDOWN && firstTask == null)) {
                        if (t.isAlive()) // 若线程已经启动了，比如说已经调用了start()方法，那么就抛异常，
                            throw new IllegalThreadStateException();
                        //添加到workers中
                        workers.add(w);
                        int s = workers.size();
                        if (s > largestPoolSize) //更新largestPoolSize
                            largestPoolSize = s;
                        workerAdded = true;
                    }
                } finally {
                    mainLock.unlock();
                }
                if (workerAdded) {
                    //若Worker创建成功，则启动线程，这么时候worker就会开始执行任务了
                    t.start();
                    workerStarted = true;
                }
            }
        } finally {
            if (! workerStarted)
                //添加失败
                addWorkerFailed(w);
        }
        return workerStarted;
    } 

    private void addWorkerFailed(Worker w) {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            if (w != null)
                workers.remove(w);
            decrementWorkerCount();
            //每次减少worker或者从队列中移除任务的时候都需要调用这个方法
            tryTerminate();
        } finally {
            mainLock.unlock();
        }
    }

```

这里有个貌似不起眼的方法tryTerminate，这个方法会在所有可能导致线程池终结的地方调用，比如说减少worker的数据等，如果满足条件的话，那么将线程池转换成TERMINATED状态。另外这个方法没有用private修饰，因为ScheduledThreadPoolExecutor继续子ThreadPoolExecutor，而ScheduledThreadPoolExecutor也会调用这个方法。

```
   final void tryTerminate() {
        for (;;) {
            int c = ctl.get();
            /*
                如果当前线程处于运行中、TIDYING、TERMINATED状态则直接返回，运行中的没
                什么好说的，后面两种状态可以说线程池已经正在终结了，另外如果处于SHUTDOWN状态，
                并且workQueue非空，表明还有任务需要处理，也直接返回
             */
            if (isRunning(c) ||
                runStateAtLeast(c, TIDYING) ||
                (runStateOf(c) == SHUTDOWN && ! workQueue.isEmpty()))
                return;
            //可以退出，但是线程数非0，那么就中断一个线程，从而使得关闭的信号能够传递下去,
            //中断worker后，worker捕获异常后，会尝试退出，并在这里继续执行tryTerminate()方法，
            //从而使得信号传递下去
            if (workerCountOf(c) != 0) {
                interruptIdleWorkers(ONLY_ONE);
                return;
            }

            final ReentrantLock mainLock = this.mainLock;
            mainLock.lock();
            try {
                //尝试转换成TIDYING状态，执行完terminated回调之后
                //会转换为TERMINATED状态，这个时候线程池已经完整关闭了，
                //通过signalAll方法，唤醒所有阻塞在awaitTermination上的线程
                if (ctl.compareAndSet(c, ctlOf(TIDYING, 0))) {
                    try {
                        terminated();
                    } finally {
                        ctl.set(ctlOf(TERMINATED, 0));
                        termination.signalAll();
                    }
                    return;
                }
            } finally {
                mainLock.unlock();
            }
            // else retry on failed CAS
        }
    }

    /**
     * 中断空闲的线程
     * @param onlyOne
     */
    private void interruptIdleWorkers(boolean onlyOne) {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            for (Worker w : workers) {
                //遍历所有worker，若之前没有被中断过，
                //并且获取锁成功，那么就尝试中断。
                //锁能够获取成功，那么表明当前worker没有在执行任务，而是在
                //获取任务，因此也就达到了只中断空闲线程的目的。
                Thread t = w.thread;
                if (!t.isInterrupted() && w.tryLock()) {
                    try {
                        t.interrupt();
                    } catch (SecurityException ignore) {
                    } finally {
                        w.unlock();
                    }
                }
                if (onlyOne)
                    break;
            }
        } finally {
            mainLock.unlock();
        }
    }

```

**Worker**

下面看一下Worker类，也就是这个类实际负责执行任务，WORKER类继承自AbstractQueuedSynchronizer，Aqs可以理解为一个同步框架，提供一些通用的机制，利用模板方法模式，让你能够原子的管理同步状态。

```
private final class Worker
        extends AbstractQueuedSynchronizer
        implements Runnable
    {
      
        /** Thread this worker is running in.  Null if factory fails. */
        final Thread thread;

        /** Initial task to run.  Possibly null. */
        Runnable firstTask;

        /** Per-thread task counter */
        volatile long completedTasks;

        Worker(Runnable firstTask) {
            setState(-1); // inhibit interrupts until runWorker
            this.firstTask = firstTask;
            this.thread = getThreadFactory().newThread(this);
        }

        /** Delegates main run loop to outer runWorker  */
        public void run() {
            runWorker(this);
        }
       protected boolean isHeldExclusively() {
            return getState() != 0;
        }

        protected boolean tryAcquire(int unused) {
            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        protected boolean tryRelease(int unused) {
            setExclusiveOwnerThread(null);
            setState(0);
            return true;
        }

        public void lock()        { acquire(1); }
        public boolean tryLock()  { return tryAcquire(1); }
        public void unlock()      { release(1); }
        public boolean isLocked() { return isHeldExclusively(); }

        void interruptIfStarted() {
            Thread t;
            if (getState() >= 0 && (t = thread) != null && !t.isInterrupted()) {
                try {
                    t.interrupt();
                } catch (SecurityException ignore) {
                }
            }
        }
    }

```

注意这里Worker初始化的时候，会通过setState（-1）将state设置为-1.并在runWorker（）方法中置为0，上文说过Worker是利用state这个变量来表示锁状态，那么加锁的操作通过cas将state从0改成1，那么初始化的时候改为-1，也就是表示在Worker启动之前，都不允许看锁操作，我们再interruptlfStarted（）以及interruptIdleWorkers()方法，这两个方法在尝试中断Worker之前，都会先加锁或者判断state是否大于0，因此这里的将state设置为-1，就是为了禁止中断操作，并在runWorker中置为0，也就是时候只能够在Worker启动之后才能够中断worker。

另外线程启动后，其实就是调用了runWorker方法，下面我们看下具体如何实现的。

```
final void runWorker(Worker w) {
        Thread wt = Thread.currentThread();
        Runnable task = w.firstTask;
        w.firstTask = null;
        w.unlock(); // 调用unlock()方法，将state置为0，表示其他操作可以获得锁或者中断worker
        boolean completedAbruptly = true;
        try {
            /*
                首先尝试执行firstTask，若没有的话，则调用getTask()从队列中获取任务
             */
            while (task != null || (task = getTask()) != null) {
                w.lock();
                /*
                    如果线程池正在关闭，那么中断线程。
                 */
                if ((runStateAtLeast(ctl.get(), STOP) ||
                    (Thread.interrupted() &&
                        runStateAtLeast(ctl.get(), STOP))) &&
                    !wt.isInterrupted())
                    wt.interrupt();
                try {
                    //执行beforeExecute回调
                    beforeExecute(wt, task);
                    Throwable thrown = null;
                    try {
                        //实际开始执行任务
                        task.run();
                    } catch (RuntimeException x) {
                        thrown = x; throw x;
                    } catch (Error x) {
                        thrown = x; throw x;
                    } catch (Throwable x) {
                        thrown = x; throw new Error(x);
                    } finally {
                        //执行afterExecute回调
                        afterExecute(task, thrown);
                    }
                } finally {
                    task = null;
                    //这里加了锁，因此没有线程安全的问题，volatile修饰保证其他线程的可见性
                    w.completedTasks++;
                    w.unlock();//解锁
                }
            }
            completedAbruptly = false;
        } finally {
            //抛异常了，或者当前队列中已没有任务需要处理等
            processWorkerExit(w, completedAbruptly);
        }
    }

private void processWorkerExit(Worker w, boolean completedAbruptly) {
        //如果是异常终止的，那么减少worker的数目
        if (completedAbruptly) // If abrupt, then workerCount wasn't adjusted
            decrementWorkerCount();

        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            //将当前worker中workers中删除掉，并累加当前worker已执行的任务到completedTaskCount中
            completedTaskCount += w.completedTasks;
            workers.remove(w);
        } finally {
            mainLock.unlock();
        }

        //上文说过，减少worker的操作都需要调用这个方法
        tryTerminate();

        /*
            如果当前线程池仍然是运行中的状态，那么就看一下是否需要新增另外一个worker替换此worker
         */
        int c = ctl.get();
        if (runStateLessThan(c, STOP)) {
            /*
                如果是异常结束的则直接扩容，否则的话则为正常退出，比如当前队列中已经没有任务需要处理，
                如果允许core线程超时的话，那么看一下当前队列是否为空，空的话则不用扩容。否则话看一下
                是否少于corePoolSize个worker在运行。
             */
            if (!completedAbruptly) {
                int min = allowCoreThreadTimeOut ? 0 : corePoolSize;
                if (min == 0 && ! workQueue.isEmpty())
                    min = 1;
                if (workerCountOf(c) >= min)
                    return; // replacement not needed
            }
            addWorker(null, false);
        }
    }

     private Runnable getTask() {
        boolean timedOut = false; // 上一次poll()是否超时了

        for (;;) {
            int c = ctl.get();
            int rs = runStateOf(c);

            // 若线程池关闭了(状态大于STOP)
            // 或者线程池处于SHUTDOWN状态，但是队列为空，那么返回null
            if (rs >= SHUTDOWN && (rs >= STOP || workQueue.isEmpty())) {
                decrementWorkerCount();
                return null;
            }

            int wc = workerCountOf(c);

            /*
                如果允许core线程超时 或者 不允许core线程超时但当前worker的数目大于core线程数，
                那么下面的poll()则超时调用
             */
            boolean timed = allowCoreThreadTimeOut || wc > corePoolSize;

            /*
                获取任务超时了并且(当前线程池中还有不止一个worker 或者 队列中已经没有任务了)，那么就尝试
                减少worker的数目，若失败了则重试
             */
            if ((wc > maximumPoolSize || (timed && timedOut))
                && (wc > 1 || workQueue.isEmpty())) {
                if (compareAndDecrementWorkerCount(c))
                    return null;
                continue;
            }

            try {
                //从队列中抓取任务
                Runnable r = timed ?
                    workQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS) :
                    workQueue.take();
                if (r != null)
                    return r;
                //走到这里表明，poll调用超时了
                timedOut = true;
            } catch (InterruptedException retry) {
                timedOut = false;
            }
        }
    }

```

**关闭线程池**

```
   public void shutdown() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            checkShutdownAccess();
            //通过CAS将状态更改为SHUTDOWN，这个时候线程池不接受新任务，但会继续处理队列中的任务
            advanceRunState(SHUTDOWN);
            //中断所有空闲的worker,也就是说除了正在处理任务的worker，其他阻塞在getTask()上的worker
            //都会被中断
            interruptIdleWorkers();
            //执行回调
            onShutdown(); // hook for ScheduledThreadPoolExecutor
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
        //这个方法不会等待所有的任务处理完成才返回
    }
    public List<Runnable> shutdownNow() {
        List<Runnable> tasks;
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            checkShutdownAccess();
            /*
                不同于shutdown()，会转换为STOP状态，不再处理新任务，队列中的任务也不处理，
                而且会中断所有的worker，而不只是空闲的worker
             */
            advanceRunState(STOP);
            interruptWorkers();
            tasks = drainQueue();//将所有的任务从队列中弹出
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
        return tasks;
    }

    private List<Runnable> drainQueue() {
        BlockingQueue<Runnable> q = workQueue;
        ArrayList<Runnable> taskList = new ArrayList<Runnable>();
        /*
            将队列中所有的任务remove掉，并添加到taskList中，
            但是有些队列比较特殊，比如说DelayQueue，如果第一个任务还没到过期时间，则不会弹出，
            因此这里通过调用toArray方法，然后再一个一个的remove掉
         */
        q.drainTo(taskList);
        if (!q.isEmpty()) {
            for (Runnable r : q.toArray(new Runnable[0])) {
                if (q.remove(r))
                    taskList.add(r);
            }
        }
        return taskList;
    }

```

从上文可以看出，调用shotdown（）方法后，不会等待所有任务处理完毕才返回，因此需要调用awaitTermination()来实现

```
 public boolean awaitTermination(long timeout, TimeUnit unit)
        throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            for (;;) {
                //线程池若已经终结了，那么就返回
                if (runStateAtLeast(ctl.get(), TERMINATED))
                    return true;
                //若超时了，也返回掉
                if (nanos <= 0)
                    return false;
                //阻塞在信号量上，等待线程池终结,但是要注意这个方法可能会因为一些未知原因随时唤醒当前线程，
                //因此需要重试，在tryTerminate()方法中，执行完terminated()回调后，表明线程池已经终结了,
                //然后会通过termination.signalAll()唤醒当前线程
                nanos = termination.awaitNanos(nanos);
            }
        } finally {
            mainLock.unlock();
        }
    }

```

一些统计相关的方法

```
public int getPoolSize() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            //若线程已终结则直接返回0，否则计算works中的数目
           //想一下为什么不用workerCount呢?
            return runStateAtLeast(ctl.get(), TIDYING) ? 0
                : workers.size();
        } finally {
            mainLock.unlock();
        }
    }

   public int getActiveCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            int n = 0;
            for (Worker w : workers)
                if (w.isLocked())//上锁的表明worker当前正在处理任务，也就是活跃的worker
                    ++n;
            return n;
        } finally {
            mainLock.unlock();
        }
    }


    public int getLargestPoolSize() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            return largestPoolSize;
        } finally {
            mainLock.unlock();
        }
    }

    //获取任务的总数，这个方法慎用，若是个无解队列，或者队列挤压比较严重，会很蛋疼
    public long getTaskCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            long n = completedTaskCount;//比如有些worker被销毁后，其处理完成的任务就会叠加到这里
            for (Worker w : workers) {
                n += w.completedTasks;//叠加历史处理完成的任务
                if (w.isLocked())//上锁表明正在处理任务,也算一个
                    ++n;
            }
            return n + workQueue.size();//获取队列中的数目
        } finally {
            mainLock.unlock();
        }
    }


    public long getCompletedTaskCount() {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            long n = completedTaskCount;
            for (Worker w : workers)
                n += w.completedTasks;
            return n;
        } finally {
            mainLock.unlock();
        }
    }

```



## volatile

###  作用

1.禁止指令重排序

2.忽略本地缓存，直接从主存中获取信息

**volatile只能保证数据的一致性不能保证数据的原子性**

![image-20200917112304648](images\image-20200917112304648.png)

### 4.2 分析

#### 4.2.1 缓存分析

![1572222471379](images/1572222471379.png)

在计算机中一般有三级缓存，用来提供效率，每次获取都是从高到低先去查询l1-l-l3，一级缓存用来存放运行指令，不同核之间则通过L3来进行通讯。应为有缓存行存在，在实际中会出现缓存一致性问题。常用的解决协议有 MESI协议

![1572223083935](images/1572223083935.png)

jmm与CPU模型类似，基于cpu缓存模型建立起来的。下图为java中缓存操作流程

![1572223677961](images/1572223677961.png)



java中通过volatitle 来实现java中缓存一致性问题，通过lock指令来触发cpu缓存一致性问题

#### 4.2.2 重排序分析

java语言规范规定jvm线程内部维持顺序化语义。即只要程序的最终结果与它顺序化情况的结果相等，那么指令的执行顺序可以与代码顺序不一致，此过程叫做指令的重排序。

**重排序的意义：**jvm能根据处理器性能适当的对机器指令重排序，是机器更符合cpu的性能

在编译器和cpu处理器中都能执行指令从排序优化操作。

![1572224879156](images/1572224879156.png)

### 4.3 常见面试题分析

```
class Singleton {
    private volatile static Singleton instance;
    public static Singleton getInstance() {
        if (instance == null) {
            syschronized(Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    } 
}
```

单例中的应用：

创建对象分为三步：

1.开辟一块内存空间

2.加载类型信息初始化

3.将内存地址赋值给引用

在这个过程中，2，3 可能会发生变化，如果先赋值引用就可能产生空对象

## ThreadLocal

主要在多方法中传值。使用该类会在每个线程内部生成一个副本，各个线程之间互相没有联系3

### 示例

存储方式如下：

![image-20230516084145588](images\image-20230516084145588.png)

```
public class ThreadLocalDemo {

    public static ThreadLocal<String> LOCAL_DATA=new ThreadLocal<String>();


    public void add(String str){
        LOCAL_DATA.set(str);
    }

    public String get(){
        return LOCAL_DATA.get();
    }

    public static void main(String[] args) {
        ThreadLocalDemo threadLocalDemo=new ThreadLocalDemo();
        new Thread(()->{
            threadLocalDemo.add(Thread.currentThread().getName());
            threadLocalDemo.add("111");
            threadLocalDemo.add("222");
            final String s = threadLocalDemo.get();
            System.out.println(s);

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();


        new Thread(()->{
            final String s = threadLocalDemo.get();
            System.out.println(s);

        }).start();

    }
}
```

注意：

1. 使用时一般都会将thredLocal定义为static全局的
2. 当遇到需要在当前线程的不同方法中传递值时可以使用（如日志记录，数据链接池）

### 面试问题



1. 使用threadLocal会有内存泄漏问题？
   - threadLocal使用内部定义了一个entry的数组结构来存储，entry继承了weakReaference（弱引用）
   - 弱引有 在垃圾回收时被清理的特性，threadlocal被定义为了弱引用，当使用完毕后就会将key回收，但具体值时强引用，内部存储的值并不会被回收，此时内部值就会一直存在，因此有内存泄漏的风险
   - 结果方法：使用完成后及时的清理掉

## CAS

S

## 线程中的并发工具类

### 1.Semaphore

该工具类用于流量控制，比如有一个需求要读几万个文件的数据，可以开启几十个线程并发的读取，读取完后要存入数据库，可数据库只要十个链接，只收就可以用该类来控制只有十个线程可以操作了，具体使用如下

```
public class SemaphoreTest {

    private static final int THREAD_COUNT =30;
    private static ExecutorService threadPool  =Executors.newFixedThreadPool(THREAD_COUNT);
    private static Semaphore semaphore = new Semaphore(10);

    public static void main(String[] args) {
        for (int i = 0; i <THREAD_COUNT ; i++) {
            final int num = i;

            threadPool.execute(new Thread(()->{

                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("save data ==================>"+num);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                semaphore.release();
            }));
        }
        threadPool.shutdown();
    }
}
```

#  反射

## 1.反射作用

**JAVA反射机制是在运行状态中对于任意一个类，都能够知道这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意方法和属性；这种动态获取信息以及动态调用对象方法的功能称为java语言的反射机制。**使用反射可以使用在运行时获取或修改对象的任意属性和方法

## 2.常用方法

```
  /**
  * PropertyDescriptor 用来操作get set方法的。通过 getWriteMethod 来做set操作，使用getReadMethod 来获取值
  **/
  private <K> void setValue(String fieldName, K k, Object val) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        PropertyDescriptor pd = new PropertyDescriptor(fieldName, 		   k.getClass());
        Method writeMethod = pd.getWriteMethod();
        writeMethod.invoke(k, val);
    }
```

```
 /**
     *  参数中如果使用 泛型变量 ，在方法中必须指定 如下
     * @param cls
     * @param t
     * @param <T>
     * @throws IllegalAccessException
     */
    public  <T> void  setValue(Class cls,T t) throws IllegalAccessException {
        Object o=new Object();
        //获取所有变量
       Field [] field= cls.getDeclaredFields();
        for (Field f:field
             ) {
            //设置可操作私有属性
            f.setAccessible(true);
            //第一个参数必须是 对象实体 new 出来的值，第二个是值，类型要与字段一致
            f.set(t,"sd");
        }
    }
```



# spring

导入源码时可能遇到的问题

https://www.cnblogs.com/java-chen-hao/p/11046190.html

## 1.源码编译

### 1.1 下载

git clone https://github.com/spring-projects/spring-framework.git

cd cd spring-framework/

### 1.2 编译

gradlew spring-oxm:compileTestJava

### 1.3 直接导入idea

### 1.4 在Terminal中执行

gradle objenesisRepackJar

gradle cglibRepackJar

会在spring-framework\spring-core\build\libs生成缺失的jar

## 2.IOC 容器设计理念

ioc也称为依赖注入（dependency  injection,di ）.通过将对象的生命周期和对象之间的依赖关系交由spring容器来处理

idc设计理念：通过容器统一对象的构建方式，并且维护对象的依赖关系。

线程钩子回调

### 3.Spring Ioc 的使用极其原理分析

#### 3.1 bean的装配方式

xml

```
<bean id="user"  class="bat.ke.qq.com.bean.User"/>
<bean name="userxxx" class="bat.ke.qq.com.bean.User"/>

ApplicationContext context =new ClassPahtXmlApplicationContext("spring.xml");

```

实现factoryBean

```
public class MyFactoryBean implements FactoryBean{
   
   @Override2   
   public Object getObject() throws Exception {
        	return 	new ReentrantLock();
   }   
   
   @Override
   public Class<?> getObjectType() {
   		return ReentrantLock.class;
   }	
}

context.getBean("myFactoryBean")
可以通过这种方式将bean 注册到容器中，

```

**FactoryBean和BeanFactory的区别**

https://www.cnblogs.com/aspirant/p/9082858.html

```
AbstractBeanFactory#getObjectForBeanInstance1 !(beanInstance instanceof FactoryBean) || BeanFactoryUtils.isFactoryDereference(name) 


```

**beanfactory**

BeanFactory，以Factory结尾，表示它是一个工厂类(接口)， **它负责生产和管理bean的一个工厂**。在Spring中，**BeanFactory是IOC容器的核心接口，它的职责包括：实例化、定位、配置应用程序中的对象及建立这些对象间的依赖。BeanFactory只是个接口，并不是IOC容器的具体实现，但是Spring容器给出了很多种实现，如 DefaultListableBeanFactory、XmlBeanFactory、ApplicationContext等，其中****XmlBeanFactory就是常用的一个，该实现将以XML方式描述组成应用的对象及对象间的依赖关系**。XmlBeanFactory类将持有此XML配置元数据，并用它来构建一个完全可配置的系统或应用。   

  都是附加了某种功能的实现。 它为其他具体的IOC容器提供了最基本的规范，例如DefaultListableBeanFactory,XmlBeanFactory,ApplicationContext 等具体的容器都是实现了BeanFactory，再在其基础之上附加了其他的功能。  

BeanFactory和ApplicationContext就是spring框架的两个IOC容器，现在一般使用ApplicationnContext，其不但包含了BeanFactory的作用，同时还进行更多的扩展。  

**BeanFacotry是spring中比较原始的Factory。如XMLBeanFactory就是一种典型的BeanFactory。**
原始的BeanFactory无法支持spring的许多插件，**如AOP功能、Web应用等**。ApplicationContext接口,它由BeanFactory接口派生而来， 

ApplicationContext包含BeanFactory的所有功能，通常建议比BeanFactory优先 

**FactoryBean**

一般情况下，Spring通过反射机制利用<bean>的class属性指定实现类实例化Bean，在某些情况下，实例化Bean过程比较复杂，如果按照传统的方式，则需要在<bean>中提供大量的配置信息。配置方式的灵活性是受限的，这时采用编码的方式可能会得到一个简单的方案。Spring为此提供了一个org.springframework.bean.factory.FactoryBean的工厂类接口，**用户可以通过实现该接口定制实例化Bean的逻辑**

FactoryBean是一个接口，当在IOC容器中的Bean实现了FactoryBean后，通过getBean(String BeanName)获取到的Bean对象并不是FactoryBean的实现类对象，而是这个实现类中的getObject()方法返回的对象。要想获取FactoryBean的实现类，就要getBean(&BeanName)，在BeanName之前加上&。

#### 3.2 spring常用注解

@Component ,@ComponentScan

@Component,@Repository,@Service @Controller

@Component 是通用注解，其他注解都是这三个注解的扩展，并且具有了特定的功能

@Repository 注解在持久层中，具有将数据库操作抛出的原生异常转化成spring持久层功能

@Service 是业务逻辑层注解

@Controller 是spring-mvc的注解，具有将请求进行转发，重定向的功能

@Bean 标注在方法上的，将该方法返回的对象标注为一个bean,一般会用配合Configuration 注解来用，如果类上不配置的话，会导致内部bean发生彼此依赖时会导致多例

@Configuration 标注在类上，将该类变成一个配置类

1.将Configuration配置的Appconfig由普通类转换成cglib代理类型

2.将Appconfig的beanDefinition属性赋值为full类型(不配置为lite)



```
@Configuration
public class AppConfig {
@Bean  
public User user(){
		return new User();  
			}
} 

```

**思考：配置 @Conﬁguration和不配置的区别？** 

@Conﬁguration： 作用 

1.将@Conﬁguration配置的Appconﬁg由普通类型转变为cglib代理类型 

2.将AppConﬁg的beanDeﬁnitioin属性赋值为full类型的（不配置的是lite）

3. 当内部method bean发⽣彼此依赖的时候会导致多例 




**@Import  用法**

该注解只能作用在类上，其能通过快速导入的方式实现把实例导入到spring的ioc容器中。一般有三种用法

**1.直接填class数组的方式**

```
@Import({ 类名.class , 类名.class... })
public class TestDemo {

}
```

**2.importSelector方式(重点)**

```
public class Myclass implements ImportSelector {
//既然是接口肯定要实现这个接口的方法
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[0]{"com.yc.Test.TestDemo3"};
    }
}
```

重点：1.返回值：就是我们要导入到容器中的组件全类名

​            2.AnnotationMetadata表示当前被@Import注解给标注的所有注解信息

**3 ImportBeanDefinitionRegister实现自定义加载bean**

1.可以导入其他的bean对象（实现了ImportBeanDefinitionRegistrar接口注入的bean）

```
public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
										BeanDefinitionRegistry registry) {
		//创建BeanDefinition
		RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Fox.class);
		// 注册到容器
		registry.registerBeanDefinition("fox",rootBeanDefinition);
	}
}

```

2.导入其他配置对象

```
@ComponentScan("bat.ke.qq.com")
@Configuration
@Import(value = MyImportBeanDefinitionRegistrar.class
public class AppConfig {
    @Bean5  
    public User user(){
   		 return new User();
    }

```

- 第一个参数：annotationMetadata 和之前的ImportSelector参数一样都是表示当前被@Import注解给标注的所有注解信息
- 第二个参数表示用于注册定义一个bean



**@Conditional**  有条件的装载类(例：某个类如果没有注册到bean中，其他类不能加载）

```
@ComponentScan("bat.ke.qq.com")1
@Configuration2 
public class AppConfig {3  

@Bean4  
public Cat cat(){  
	return new Cat();
}

//MyConditional 中设置过滤条件
@Bean 
@Conditional(value = MyConditional.class)
public Fox fox(){
	return new Fox()  
}




public class MyConditional implements Condition {

   @Override
   public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
      if(context.getBeanFactory().containsBean("cat"))
         return true;
      return false;
   }
}

```

#### 3.3  bean的注册原理

##### 3.3.1 bean 中关键对象

```
beanDefintion(bean定义) 承载bean的属性信息
beanDefinitionRegistry  (bean注册器) bean的id作为key注册 beanName
aliasRegistry（别名注册器） bean的name作为key注册
beanDeginitionHolder 包装beanFefinition id name (多个)

beandefinitionReader (bean定义读取) 读取spring配置文件
beandifinitionParser (bean定义解析器) parser解析 schema
beanFactory （bean工厂） 生产bean

```

##### 3.3.2 xml bean加载流程

```
//创建一个 简单的注册器
//BeanDefinitionRegistry register =new //SimpleBeanDefinitionRegistry();

//可替代上面注册器 实现以注册器的工厂
BeandefinitionRegistry registry =new DefaultListableBeanFactory()；

//创建bean定义读取器
BeanDefinitionReader reader=new XmlBeanDefinitionReader(registry);
//创建资源读取器
DefaultResourceLoader resourceLoder =new  DefaultResouceLoader();
//获取资源
Resource xmlResouce =new resouceLoader.getResource("spring.xml");
//加载资源
reader.loadBeanDefinitions(xmlResource);
// 打印构建的Bean 名称System.out.println(Arrays.toString(register.getBeanDefinitionNames());
// 工厂调用getBean方法System.out.println(register.getBean("user"))




```

##### 3.3.3 java Conﬁguration加载流程

```
AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(AppConfig.class);
//通过容器获取到beanFactory 即使工厂，又是注册器
DefaultListableBeanFactory factory =context.getDefaultListableBeanFactory();

//包装bean ，
RootBeanDefinition beanDefinition=new RootBeanDefinition(Fox.class);

//设置注入方式
//beanDefinition.setAutowireMode(2);

// spring   填充属性beanDefinition.getPropertyValues().add("name","foxxx");
System.out.println(context.getBean("fox"))

```

##### 3.3.4 bean的 依赖注入原理

**查找方式**

byType

byName

**注入方式**

- field

反射的范式，field.set（obj,value）；

- constructor  构造器方式

不配置@Autowired 情况下

1.多构造器时，有无参数构造方法则调用无参构造器，若无则当构造器大于1个时抛异常，当只有一个时会执行此构造器

2.当只有一个有参构造器时会执行此构造器

3.当该bean的beanDefiition设置了AutowireMode为3时，则会选择贪婪模式，执行参数最多的构造器，相同参数按顺序执行最后的构造参数



```
Component
public class MyBeanFactoryProcessor implements BeanFactoryPostProcessor {
        @Override3   
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        AbstractBeanDefinition beanDefinition = (AbstractBeanDefinition) beanFactory.getBeanDefinition("userServiceImpl");    beanDefinition.setAutowireMode(3);//构造器贪婪模式   }
        }

```

- setter⽅法

不配置 @ A u t o wir e d 情 况 下 通过设置AutowireMode为1或者2，会调⽤setter⽅法，通过setter⽅法注⼊bean

#### 3.4  BeanPostProcessor后置处理器分析（未完成需要分析spring中的后置处理器功能）

##### 3.4.1 BeanPostProcessor分析

该接口叫后置处理器，作用是在bean对象实例化和依赖注入完毕后，在显示调用初始化方法的前后添加我们的自己的逻辑。bean实例化完毕后及依赖注入完成后出发的。

```
public interface BeanPostProcessor {
	
	Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException;

	Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException;

}
```

| 方法                            | 说明                                                         |
| ------------------------------- | ------------------------------------------------------------ |
| postProcessBeforeInitialization | 实例化，依赖注入完毕在调用显示初始化前完成一些定制的初始化任务 |
| postProcessAfterInitialization  | 实例化，依赖注入，初始化完毕时执行                           |



## 3.bean的生命周期

```
ConfigurationClassPostProcessor #postProcessBeanDefinitionRegistry 解析注解注册bean

ClassPathScanningCandidateComponentProvider#findCandidateComponents 方法加载bean。所有被Component 修饰的类


AutowiredAnnotationBeanPostProcessor

```

![1570328960727](images/1570328960727.png)

![1570347496206](images/1570347496206.png)

![IoC](images/IoC.png)

![1570434684721](images/1570434684721.png)

循环依赖

https://cloud.tencent.com/developer/article/1497692



### 3.2 spring 中加载properties 的几种方式

#### 一、通过 context:property-placeholder 标签实现配置文件加载

1、用法示例： 在spring.xml配置文件中添加标签    
复制代码代码如下:

```
<context:property-placeholder ignore-unresolvable="true" location="classpath:redis-key.properties"/> 
```

2、在 spring.xml 中使用配置文件属性：


```
<!-- 基本属性 url、user、password -->
<property name="url" value="${jdbc.url}" /> 
<property name="username" value="${jdbc.username}" /> 
<property name="password" value="${jdbc.password}" />
```

3、在java文件中使用：

```
@Value("${jdbc_url}") 
ivate String jdbcUrl; // 注意：这里变量不能定义成static
```

#### 二、通过 util:properties 标签实现配置文件加载

1、用法示例： 在spring.xml配置文件中添加标签 

复制代码代码如下:

```
<util:properties id="util_Spring"  local-override="true" location="classpath:jeesite.properties"/>
```


2、在spring.xml 中使用配置文件属性：

```
<property name="username" value="#{util_Spring['jdbc.username']}" /> 
<property name="password" value="#{util_Spring['jdbc.password']}" /> 
```


3、在java文件中使用：

@Value(value="#{util_Spring['UTIL_SERVICE_ONE']}") 
 private String UTIL_SERVICE_ONE;

#### 三、通过 @PropertySource 注解实现配置文件加载

1、用法示例：在java类文件中使用 PropertySource 注解：

```
@PropertySource(value={"classpath:redis-key.properties"}) 
public class ReadProperties { 
@Value(value="${jdbc.username}") 
 private String USER_NAME; 
} 
```


2、在java文件中使用：


```
@Value(value="${jdbc.username}") 
 private String USER_NAME;
```

#### 四、通过 PropertyPlaceholderConfigurer 类读取配置文件

1、用法示例：在 spring.xml 中使用 <bean>标签进行配置

```
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer"> 
  <property name="locations"> 
   <list> 
    <value>classpath:redis-key.properties</value> 
   </list> 
  </property> 
  </bean> 
```


2、 PropertyPlaceholderConfigurer  配置方法，等价于 方式一，用法参考方法一。

五、 还可以使用  org.springframework.beans.factory.config.PropertiesFactoryBean  

## 4.Spring mvc

spring mvc 入口是Servlet，容器启动时先运行init方法（HttpServletBean#init），在init方法中实现了模板方法initServletBean（FrameworkServlet#initServletBean）方法通过initWebApplicationContext方法 （AbstractApplicationContext#refresh）方法来启动容器，这时就可以对外提供服务了。



### 4.1springmvc执行流程





### 4.2 常用注解

1.实现动态地址，获取动态值

![1570541443359](images/1570541443359.png)

## 5.Spring boot

### 5.1 spring boot优点

**1.独立运行spring项目**

spring boot 可以以jar包形式多里运行，运行一个spring boot项目只需要java -jar

**2.内嵌servlet容器**

Spring boot可以选择内嵌Tomcat，jetty

**3.starter简化maven配置**

spring提供里一系列的start pom来简化maven的依赖和自动配置

**4.自动配置spring**

springBoot会根据类路径中的jar包，为jar包中农的类自动配置bean，这样可以减少我们使用的配置。

#### 5.1.1 spring boot 版本说明

![1584170993875](images/1584170993875.png)

版本号说明：比如2.1.13：

-  2.主版本号，当模块功能有较大的更新
-  1:次版本号表示只是局部一些变化
-  13：修改版本号，一般是bug的修改或者是小的变动

版本后面英文的含义：

| 版本号     | 版本说明     | 用途                                                         |
| ---------- | ------------ | ------------------------------------------------------------ |
| BUILD-XXX  | 开发版本     | 一般是开发团队内部用的                                       |
| GA         | 稳定版本     | 内部开发到一定阶段了，各个模块集成后，经过全面测试，发现没有问题了，可以对外发布了，这个时候就叫GA（AenrallyAvailable）版，系统的核心功能已经可以使用。意思就是基本上可以使用了。 |
| PRE(M1,N2) | 里程碑       | 由于GA版还不属于公开的发行版，里面还有功能不完善的或者一些BUG，于是就有了milestone（里程碑）版，milestone版本主要修复一些BUG。一个GA后，一般有多个里程碑，例如 M1 M2 M3 |
| RC         | 候选发布版   | 从BUILD后GA再到M基本上系统就定型了，这个时候系统就进入RELEASEcandidates（RC候选发布）版，该阶段的软件类似于最终发行前的一个观察期，该期间只对一些发现的等级高的BUG进行修复，发布RC1,RC2等版本 |
| SR         | 正式发布版本 | 公开正式发布。正式发布版一般也有多个发布，例如SR1 SR2 SR3等等，一般是用来修复大BUG或优化。 |

#### 5.1.2 spring boot创建方式

  1.创建一个空的maven工程，导入springboot相关的jar包 父pom主要做了两件事：

​              1.所有的jar的版本统一管理

​               2.所有jar的依赖管理，其中包含springboot提供的strter启动器

```
<!‐‐ Spring Boot支持需要引入其父pom ‐‐>
2 <parent>
3 <groupId>org.springframework.boot</groupId>
4 <artifactId>spring‐boot‐starter‐parent</artifactId>
5 <version>2.1.5.RELEASE</version>
6 <relativePath/> <!‐‐ lookup parent from repository ‐‐>
7 </parent>
8 9
<dependencies>
10 <dependency>
11 <groupId>org.springframework.boot</groupId>
12 <artifactId>spring‐boot‐starter</artifactId>
13 </dependency>
14
15 <!‐‐ 实现对 SpringMVC 的自动化配置 ‐‐>
16 <dependency>
17 <groupId>org.springframework.boot</groupId>
18 <artifactId>spring‐boot‐starter‐web</artifactId>
19 </dependency>
20
21 <dependency>
22 <groupId>org.springframework.boot</groupId>
23 <artifactId>spring‐boot‐starter‐test</artifactId>
24 <scope>test</scope>
25 </dependency>
26 </dependencies>
27
28 <!‐‐ 引入一个spring boot插件，可以支持我们将web应用程序打成可运行jar包 ‐‐>
29 <build>
30 <plugins>
31 <plugin>
32 <groupId>org.springframework.boot</groupId>
33 <artifactId>spring‐boot‐maven‐plugin</artifactId>
</plugin>
35 </plugins>
36 </build>
```

2.编写入口程序，添加@SpringBootApplication注解

```
1 @SpringBootApplication
2 public class VipSpringBootDemoApplication {
3 4
public static void main(String[] args) {
5 SpringApplication.run(VipSpringBootDemoApplication.class,args);
6 }
7 }
```



### 5.2 spring boot 实现方式

#### 1.spring boot如何启动spring

![1572853308757](images/1572853308757.png)



从run方法进入后可以看到

![1572853466883](images/1572853466883.png)



![1572853496736](images/1572853496736.png)



默认是注解加载方式

#### 2.starter如何实现自动加载

![1572854972672](images/1572854972672.png)





实现自动注入的关键@EnableAutoConfiguration注解，点进去看可以看到import导入AutoConfigurationImportSelector是实现自动注入的关键。

![1572855672795](images/1572855672795.png)



![1572855909351](images/1572855909351.png)

![1572856034512](images/1572856034512.png)

![1572856051626](images/1572856051626.png)

![1572856084756](images/1572856084756.png)



```
spring boot 重写了类加载器，当加载时会使用SpringFactoriesLoader加载，固定加载META-INF/spring.factories中配置的类，然后进行条件加载将bean装入spring容器中。spring.factories 中配置了自动加载的配置类。以mybatis-spring配置文件为列
```

![1572860159128](images/1572860159128.png)



#### 3.spring boot怎么加载配置文件

假设application.yml 或者application.properties 文件 如下

```
info:
  name: xiaoming
  age: 13
  sex: 1
```

##### 1.直接读取

```
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*
* 通过@value注解的方式读取
*/
@Component
public class TechUser {
    @Value("${info.name}")
    private String name;
    @Value("${info.age}")
    private int age;
    @Value("${info.sex}")
    private int sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
```

##### 2.ConfigurationProperties读取

**第一步 需要引入pom**

```
<!‐‐ Spring Boot 配置处理器 ‐‐>
 <dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring‐boot‐configuration‐processor</artifactId>
 </dependency> 
```

引入spring-boot-configuration-processor依赖的原因是，编译项目时会扫描@configurationProperties注解生成

spring-configuration-metadata.json 配置元
数据文件（vip-spring-boot-config/target/classes/META-INF/spring-configurationmetadata.json），并交给IDEA解析。
这样做的好处是：
在 application.yaml 配置文件，添加配置项时，IDEA 会给出提示。
点击配置项时，可以跳转到对应的 @ConfigurationProperties 注解的配置类 

**第二步编写实体类**

```
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "info")
public class TechUser {
    private String name;
    private int age;
    private int sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}

```

需要引入xml文件

**注意：**

@ConfigurationProperties两种用法：

- @ConfigurationProperties&@Component
- @EnableConfigurationProperties(class:
  @ConfigurationProperties)+@Configuration & @ConfigurationProperties
  @EnableConfigurationProperties 注解，可以将指定带
  有 @ConfigurationProperties 的类，注册成 BeanDefinition ，从而创建成 Bean 对象。
  当@EnableConfigurationProperties注解应用到@Configuration时， 任何被
  @ConfigurationProperties注解的beans将自动被Environment属性配置。 

##### 3.读取指定文件

```
//不支持 yml格式文件
@ConfigurationProperties(prefix = "db") //可选设置
@PropertySource(value = { "config/db-config.properties" })
public class DBConfig1 {
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
```

#### 4.设置spring boot 不同环境加载不同的yml

****

**解决方法：**

在/src/main/resources下有application-local.yml（本地使用）、application-dev.yml（测试使用）、application-prod.yml（正式环境使用）

要想在不同环境下使用不同的配置文件可以在同目录的application.yml中添加：

spring:

 profiles:

   active: dev

其中dev代表测试，prod代表正式环境

也可通过以下方式在同一个文件中配置多个文件

```
  profiles:
    active: dev
---
spring:
  profiles: dev
server:
  port: 8081
---
spring:
  profiles: test
server:
  port: 8082
```

补充



## 6.spring cloud 

### 1.搭建一个spring boot项目 

 **环境**： jdk 1.8 ，maven 3.5 ，idea 2019.2.3

![1581669326108](images/1581669326108.png)

该网址是一个快速创建spring cloud项目的模板网站，使用时需要联网，一直点击下一步来到以下页面，左边是支持的模块，点击后会在右边显示具体的内容，选择后，会在pom文件中自动加上所需依赖

![1581669772698](images/1581669772698.png)

创建好后结构如下

![1581670061591](images/1581670061591.png)

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.4.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>day1</groupId>
    <artifactId>demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>demo</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>

```

#### 1.1spring-boot-starter-paren 作用

创建好之后，观察pom文件中的spring-boot-starter-paren，每个springboot都会默认继承，使用后的好处

**1.定义了java编译版本为1.8**

**2.使用utf-8 编码格式**

**3.继承自spring-boot-dependencies，这个里面定义了依赖的版本的，也正是因为继承了这个依赖，所以我们在写依赖时才不需要写版本号**

**4.执行打包的配置**

**5.自动化的资源过滤**

**6.自动化插件配置**

#### 1.2 Spring Boot Maven插件作用

```
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

把项目打成一个可执行的jar包，如何不加不能java -jar xxx.jar执行

#### 1.3 启动方式

1.java -jar xxx,jar --server.port=8088

2.maven插件启动 mvn spring-boot:run

3.进入jar或war目录loader启动

```
1 java org.springframework.boot.loader.JarLauncher
2 java org.springframework.boot.loader.WarLauncher
```

**为什么java -jar 可以启动应用**

查看解压后的demo/target/demo/META-INF/MANIFEST.MF 

![1584173307992](images/1584173307992.png)

如何打war包部署到外置的tomcat？

1.指定springboot pom中的打包方式由jar改为war

```
<packaging>war</packaging> 
```

2.移除spring-boot-starter-web依赖中的tomcat依赖

```
<dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring‐boot‐starter‐tomcat</artifactId>
 <!‐‐编译时依赖，但不会打包进去‐‐>
 <scope>provided</scope>
 </dependency
```

3.主启动类上实现SpringBootServletInitializer 重写confiure方法 

```
@SpringBootApplication
 public class DemoMain extends SpringBootServletInitializer {

public static void main(String[] args) {

SpringApplication.run(DemoMain.class, args);
 }

@Override
 protected SpringApplicationBuilder configure(SpringApplicationBuilder b
uilder) {
 return builder.sources(DemoMain.class);
 }
 }
```



### 2.spring boot 日志

spring boot在所有内部日志中使用Commons Longging ，但是默认配置也提供了对常用日志的支持：如 java util Logging ,log4j ,log4j2和logback。每种loger都可以通过配置使用控制台或者文件处处日志内容。

#### 2.1 logback

默认情况下spring boot会使用logback。依赖如下

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-logging</artifactId>
</dependency>
```

**spring-boot-starter**中默认添加了以上依赖，在实际使用中不需要添加，可开箱即用，常见的配置如下

```
logging:
  #日志级别
  level: 
    root: info
  file:
    # 在当前磁盘的根路径下创建spring文件夹和里面的log文件夹；使用 spring.log 作为默认文件
    path: /spring/log
  pattern:
    #  在控制台输出的日志的格式
    console: %d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n
    # 指定文件中日志输出的格式
    file: %d{yyyy-MM-dd} === [%thread] === %-5level === %logger{50} ==== %msg%n
    #指定logback文件
  config:  classpath:logging-config.xml
```

```
<?xml version="1.0" encoding="UTF-8"?>
<configuration  scan="true" scanPeriod="60 seconds" debug="false">
    <contextName>logback</contextName>
    <property name="log.path" value="/Users/tengjun/Documents/log" />
    <!--输出到控制台-->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
       <!-- <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>ERROR</level>
        </filter>-->
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} %contextName [%thread] %-5level %logger{36}.%M:%L - %msg%n</pattern>
        </encoder>
    </appender>

    <!--输出到文件-->
    <appender name="file" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${log.path}/logback.%d{yyyy-MM-dd}.log</fileNamePattern>
        </rollingPolicy>
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} %contextName [%thread] %-5level %logger{36}.%M:%L - %msg%n</pattern>
        </encoder>
    </appender>

    <root level="info">
        <appender-ref ref="console" />
        <appender-ref ref="file" />
    </root>

    <!-- logback为java中的包 -->
    <logger name="com.dudu.controller"/>
    <!--logback.LogbackDemo：类的全路径 -->
    <logger name="com.dudu.controller.LearnController" level="WARN" additivity="false">
        <appender-ref ref="console"/>
    </logger>
</configuration>


```

在指定日志时，也可以指定只打印某个类的日志

```
 logging:
 level:
 #org.springframework.web.servlet.mvc.method.annotation.RequestMappingHan
dlerMapping: trace
```



### 3.热部署

```
<!‐‐devtools热部署‐‐>
 <dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring‐boot‐devtools</artifactId>
 <optional>true</optional>
 </dependency>

// spring‐boot‐maven‐plugin 中配置
 <configuration>
 <!‐‐ 没有该配置，devtools 不生效 ‐‐>
 <fork>true</fork>
 </configuration>
```

application.yml中配置 

```
spring:
  devtools:
    restart:
      enabled: true  #设置开启热部署
      additional-paths: src/main/java #重启目录
      exclude: WEB-INF/**  #页面不加载缓存，修改即时生效 
  freemarker:
    cache: false
```

idea中配置

1）File-Settings-Compiler-Build Project automatically

![1584175956647](images/1584175956647.png)

2.ctrl + shift + alt + / ,选择Registry,勾上 Compiler autoMake allow when 

![1584176030982](images/1584176030982.png)

![1584176175362](images/1584176175362.png)

### 4.Swagger2（未完成）

### 5.集成Mybatis（未完成）

第一步向pom文件中添加

```
<mybatis.version>1.3.1</mybatis.version>
<druid.version>1.1.3</druid.version>

        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>${mybatis.version}</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>${druid.version}</version>
        </dependency>
```

```
spring:
    datasource:
        type: com.alibaba.druid.pool.DruidDataSource
        driverClassName: com.mysql.jdbc.Driver
        druid:
            url: jdbc:mysql://localhost:3306/learnboot?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8
            username: root
            password: 123456
            initial-size: 10
            max-active: 100
            min-idle: 10
            max-wait: 60000
            pool-prepared-statements: true
            max-pool-prepared-statement-per-connection-size: 20
            time-between-eviction-runs-millis: 60000
            min-evictable-idle-time-millis: 300000
            validation-query: SELECT 1
            test-while-idle: true
            test-on-borrow: false
            test-on-return: false
            stat-view-servlet:
                enabled: true
                url-pattern: /druid/*
# Mybatis配置
mybatis:
    mapperLocations: classpath:mapper/**/*.xml
    configuration:
        map-underscore-to-camel-case: true
```

配置好后在启动类上添加注解

@MapperSan("添加需要扫描的dao")



**未写如何配置慢日志，如何打印sql，缺少具体列子**

### 4.统一异常处理

springMVC自带了@ControllerAdvice ，用来定义统一异常处理列，在springboot中额外增加了@RestControllerAdvice。

使用方式如下

在方法的主街上加上@ExceptionHandler 杨阿丽指定这个方法处理哪种异常，然后处理完异常，将结果返回

```
@RestControllerAdvice
public class ExceptionHandler {
    /**
     * logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    /**
     * 处理系统自定义的异常
     *
     * @param e 异常
     * @return 状态码和错误信息
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(KCException.class)
    public ResponseEntity<String> handleKCException(KCException e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.status(e.getCode()).body(e.getMessage());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<String> handleDuplicateKeyException(DuplicateKeyException e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body("数据库中已存在该记录");
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(AuthorizationException.class)
    public ResponseEntity<String> handleAuthorizationException(AuthorizationException e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("没有权限，请联系管理员授权");
    }

    /**
     * 处理异常
     *
     * @param e 异常
     * @return 状态码
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
```

在实际使用中时，可以自己定义异常类，捕获自己的异常信息，统一返回对象

### 6.hibernate validation验证(未完成)

### 7.自定义装配参数（未完成）

可以通过自动装配类实现自定义参数接受，和预处理接收的和返回的值

### 8.RabbitMQ 消息队列（未完成）

### 9.Actuator

**作用：**完成微服务的监控，完成监控的治理。可以查看微服务之间的数据处理和调用，当它们之间出现了异常，就可以快速定位到出现问题的地方

```
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
```



```
management:
  server:
    #可选 不填写默认为项目端口
    port: 9001
  endpoints:
    web:
      ##修改访问路径  2.0之前默认是/   2.0默认是 /actuator  可以通过这个属性值修改
      base-path: /monitor
      ##开放所有页面节点  默认只开启了health、info两个节点
      exposure:
        include: '*'
  endpoint:
    health:
      ##显示健康具体信息  默认不会显示详细信息  
      show-details: always
```

可查询内容

| HTTP方法 | 路径            | 描述                       | 鉴权  |
| -------- | --------------- | -------------------------- | ----- |
| GET      | /autoconfig     | 查看自动配置的使用情况     | true  |
| GET      | /configprops    | 查看配置属性，包括默认配置 | true  |
| GET      | /beans          | 查看bean及其关系列表       | true  |
| GET      | /dump           | 打印线程栈                 | true  |
| GET      | /env            | 查看所有环境变量           | true  |
| GET      | /env/{name}     | 查看具体变量值             | true  |
| GET      | /health         | 查看应用健康指标           | false |
| GET      | / info          | 查看应用信息               | false |
| GET      | /mappings       | 查看所有url映射            | true  |
| GET      | /metrics        | 查看应用基本指标           | true  |
| GET      | /metrics/{name} | 查看具体指标               | true  |
| POST     | /shutdown       | 关闭应用                   | true  |
| GET      | /trace          | 查看基本追踪信息           | true  |

使用时<http://localhost:8080/monitor>先调用该地址（monitor为配置文件中配置），然后<http://localhost:8080/monitor/info>这样去访问，可常看以上信息

![1581690952489](images/1581690952489.png)

### 10 多数据源支持 （未完成）

### 11.spring  Security 

简介：spring security 是一个能够为基spring 的企业应用系统提供声明的安全访问控制解决方案。

#### 1.常见的权限模型

![1581691628144](images/1581691628144.png)

12.

### 13.Spring Cloud Config

### 14.Spring Cloud Hystrix

当调用的服务出现了故障，使用hystrix后对故障服务进行熔断，快速的进行反应，尽可能的减少服务的堆积，进行服务降级。

#### 1.使用步骤

在pom引入以下依赖

```
  <dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
  </dependency>
```

启动类加上

```
//要在SpringCloud中使用断路器，需要加上@EnableCircuitBreaker注解
@EnableCircuitBreaker
//开启熔断
@EnableHystrix
```

配置好后就可以在项目中使用

```
@FeignClient(value = "HYSTRIX.SERVICE", fallback = ProductClientFallback.class)
public interface ProductClient {

    @GetMapping("/product/v1/id")
    String findById(@RequestParam("id") int id);
}

```

ProductClientFallback 类如下

```
@Component
public class ProductClientFallback implements ProductClient {
    @Override
    public String findById(int id) {
        System.out.println("feign 调用 product-sdf");
        return null;
    }
}
```

除在客户端使用外，还可以在服务端使用。

```
  @HystrixCommand(fallbackMethod = "saveOrderFail")
    public Object save(@RequestParam("user_id") int userId, @RequestParam("product_id") int productId) throws Exception {


        Map<String, Object> msg = new HashMap<>();
        msg.put("code", 0);
        msg.put("data", "success");
        productOrderService.save(userId, productId);
        return msg;
    }

    private Object saveOrderFail(int userId, int productId) {
        Map<String, Object> msg = new HashMap<>();
        msg.put("code", -1);
        msg.put("data", "error");
        return msg;
    }
```

#### 2.注意事项

### 15.Spring Cloud Dashboard

Hystrix Dashboard 是hystrix指标数据的可视面板，主要依托spring-boot-actuator暴露的监控接口，将hystrixCommad所修饰的请求监控起来，几种展示

#### 1.实现方法

引入如下pom

```
<dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies

```

启动类加上 @EnableHystrixDashboard。

需要被检测的客户端需要如下配置，pom文件中添加

```
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
```

```

```

yml文件中添加如下配置

```
    #暴露全部的信息
management:
  endpoints:
    web:
      exposure:
        include: "*"
```




启动后访问<http://localhost:8899/hystrix>可看到如下页面

![1581862872255](images/1581862872255.png)

如果想检测具体服务可把具体服务地址加上 http://localhost:8104/actuator/hystrix.stream



### 16.Spring Cloud Eureka

#### 1.常见配置

| 配置参数                                               | 默认值  | 说明                                                         |
| ------------------------------------------------------ | ------- | ------------------------------------------------------------ |
| 服务注册中心配置                                       |         | Bean类：org.springframework.cloud.netflix.eureka.server.EurekaServerConfigBean |
| eureka.server.enable-self-preservation                 | false   | 关闭注册中心的保护机制，Eureka 会统计15分钟之内心跳失败的比例低于85%将会触发保护机制，不剔除服务提供者，如果关闭服务注册中心将不可用的实例正确剔除 |
| 服务实例类配置                                         |         | Bean类：org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean |
| eureka.instance.prefer-ip-address                      | false   | 不使用主机名来定义注册中心的地址，而使用IP地址的形式，如果设置了eureka.instance.ip-address 属性，则使用该属性配置的IP，否则自动获取除环路IP外的第一个IP地址 |
| eureka.instance.ip-address                             |         | IP地址                                                       |
| eureka.instance.hostname                               |         | 设置当前实例的主机名称                                       |
| eureka.instance.appname                                |         | 服务名，默认取 spring.application.name 配置值，如果没有则为 unknown |
| eureka.instance.lease-renewal-interval-in-seconds      | 30      | 定义服务续约任务（心跳）的调用间隔，单位：秒                 |
| eureka.instance.lease-expiration-duration-in-seconds   | 90      | 定义服务失效的时间，单位：秒                                 |
| eureka.instance.status-page-url-path                   | /info   | 状态页面的URL，相对路径，默认使用 HTTP 访问，如果需要使用 HTTPS则需要使用绝对路径配置 |
| eureka.instance.status-page-url                        |         | 状态页面的URL，绝对路径                                      |
| eureka.instance.health-check-url-path                  | /health | 健康检查页面的URL，相对路径，默认使用 HTTP 访问，如果需要使用 HTTPS则需要使用绝对路径配置 |
| eureka.instance.health-check-url                       |         | 健康检查页面的URL，绝对路径                                  |
| 服务注册类配置                                         |         | Bean类：org.springframework.cloud.netflix.eureka.EurekaClientConfigBean |
| eureka.client.service-url.                             |         | 指定服务注册中心地址，类型为 HashMap，并设置有一组默认值，默认的Key为 defaultZone；默认的Value为 <http://localhost:8761/eureka> ，如果服务注册中心为高可用集群时，多个注册中心地址以逗号分隔。如果服务注册中心加入了安全验证，这里配置的地址格式为：[http://:@localhost:8761/eureka](http://%3cusername%3e:%3cpassword%3e@localhost:8761/eureka) 其中 <username> 为安全校验的用户名；<password> 为该用户的密码 |
| eureka.client.fetch-registery                          | true    | 检索服务                                                     |
| eureka.client.registery-fetch-interval-seconds         | 30      | 从Eureka服务器端获取注册信息的间隔时间，单位：秒             |
| eureka.client.register-with-eureka                     | true    | 启动服务注册                                                 |
| eureka.client.eureka-server-connect-timeout-seconds    | 5       | 连接 Eureka Server 的超时时间，单位：秒                      |
| eureka.client.eureka-server-read-timeout-seconds       | 8       | 读取 Eureka Server 信息的超时时间，单位：秒                  |
| eureka.client.filter-only-up-instances                 | true    | 获取实例时是否过滤，只保留UP状态的实例                       |
| eureka.client.eureka-connection-idle-timeout-seconds   | 30      | Eureka 服务端连接空闲关闭时间，单位：秒                      |
| eureka.client.eureka-server-total-connections          | 200     | 从Eureka 客户端到所有Eureka服务端的连接总数                  |
| eureka.client.eureka-server-total-connections-per-host | 50      | 从Eureka客户端到每个Eureka服务主机的连接总数                 |

### 17.Spring Cloud turbine

### 18.spring zipkin

### 19.spring Fingn

### 20.spring Zuul

​	**作用**：    

1. 可做为项目的唯一入口，屏蔽系统内部各个微服务的细节

2. 与服务治理框架结合，实现自动化的服务实例维护以及负载均衡的路由转发

3. 通过网关的过滤器可，在个生命周期中验证请求的内容，将原本的对外服务层的校验前移

4. 对各个接口权限校验

   #### 1.基础实现

   1 引入pom文件

   ```
      <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
      </dependency>
       <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
      </dependency>
           
   ```

   遗留问题：spring-cloud-starter-netflix-zuul  和>spring-cloud-starter-zuul 区别

   2. 启动类上

   ```
   //@EnableZuulServer ：开启路由网关功能
   @SpringBootApplication
   @EnableZuulProxy
   public class ServiceZuulApplication {
   
       public static void main(String[] args) {
           SpringApplication.run(ServiceZuulApplication.class, args);
       }
   }
   ```

3.配置yml文件

```
  eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:1111/eureka/   #注册中心地址
server:
  port: 1113    //当前zuul服务端口
spring:
  application:
    name: service-zuul #服务名
zuul:
  routes:  #配置路由   如果不需要 配置其他属性可直接写 （服务名:规则）YESWAY-HONDA-API-USER: /api/** 
    api-a:  #可随意写
      path: /api-a/**  
      serviceId: consumer1   #api-a 开头的请求跳转到  consumer1服务，比如请求为localhost:8888/api-a/test会转发到 /http://localhost:8080/test
    api-b:
      path: /api-b/**
      serviceId: consumer2   #api-b 开头的请求跳转到 consumer2服务
```

通过以上三步 简单的网关就搭建好了，可以实现简单的任务分发

#### 2.过滤器

```
/**
 服务过滤
 */
@Component
public class MyFilter extends ZuulFilter {
    /**
        返回过滤器的类型，过滤器类型如下：
        pre：请求路由之前调用过滤
        routing：请求routing之时调用过滤
        post： 请求路由之后调用过滤
        error：发送错误时调用过滤
     */
    @Override
    public String filterType() {
        return "pre";
    }

    //filterOrder：过滤的顺序 数值越小越先执行
    @Override
    public int filterOrder() {
        return 0;
    }

    //shouldFilter：是否要过滤，true表示永远过滤。
    @Override
    public boolean shouldFilter() {
        return true;
    }

    //过滤逻辑可以在该方法中处理有没有权限访问等
    @Override
    public Object run() {
        //获取请求对象
        RequestContext ctx = RequestContext.getCurrentContext();
        Object pass = ctx.getRequest().getParameter("pass");
        if(pass == null) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            try {
                ctx.getResponse().getWriter().write("pass is empty");
            }catch (Exception e){}
        }
        return null;
    }
}
```

#### 3.注意事项

默认情况在请求数据时会过滤掉http请求头中的一些信息，可通过以下参数来覆盖默认值

```
zuul.sensitive-headers=  #不会过滤http头信息对所有请求起作用
#以上方法对所有请求都起作用不建议使用   YESWAY-HONDA-API-USER 为模块名称可以随意起名
zuul.routes.YESWAY-HONDA-API-USER.customSensitiveHeaders=true  #第一方法
zuul.routes.YESWAY-HONDA-API-USER.sensitive-headers=true #第二方法
```

## 7.spring 事务



# MyBites

## 1 加载流程

加载流程分分为两步：

1加载配置文件，加载sql将sql封装成MappedStatement构成出configuration 对象

2.通过configuration构建sessionFactory

3.当每次执行sql时都会创建sqlsession来运行语句

![1570545145663](images/1570545145663.png)

注意

## 2.常用配置

### 2.1 spring boot中配置

1.配置连接池

```
<dependency>
  <groupId>com.alibaba</groupId>
  <artifactId>druid</artifactId>
</dependency>
 <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.0</version>
        </dependency>
```

```
mybatis:

  mapper-locations: classpath:mapping/*Mapper.xml //扫描xml

  type-aliases-package: com.example.entity  //注册aliases
  
mybatis.mapper-locations=classpath:mapper/*.xml //扫描xml
mybatis.type-aliases-package=entity //注册aliases


spring.datasource.master.username=root
#spring.datasource.master.password=test1234
spring.datasource.master.password=wang
spring.datasource.master.driverClassName=com.mysql.jdbc.Driver
spring.datasource.master.initialSize=1
spring.datasource.master.minIdle=1
spring.datasource.master.maxActive=20
spring.datasource.master.maxWait=60000
spring.datasource.master.timeBetweenEvictionRunsMillis=60000
spring.datasource.master.minEvictableIdleTimeMillis=300000
spring.datasource.master.validationQuery=SELECT 'x'
spring.datasource.master.testWhileIdle=true
spring.datasource.master.testOnBorrow=false
spring.datasource.master.testOnReturn=false
spring.datasource.master.poolPreparedStatements=true
spring.datasource.master.maxPoolPreparedStatementPerConnectionSize=20
spring.datasource.master.filters=stat

```

![1570581548641](images/1570581548641.png)



### 1.2  SQL设置

1.结果映射分为两种： resultType 和resultmap

![1570582131165](images/1570582131165.png)

2. 获取插入后的主键id

   useGeneratedKeys=true 获取主键id   keyProperty=“id” 将id映射到哪个字段上

   ![1570582271575](images/1570582271575.png)

3.一对多

https://www.cnblogs.com/heliusKing/p/11173362.html

在resultMap中配置collection标签 property 属性为实体类型多的一方，ofType为多方类型，column为查询条件

![1570585144266](images/1570585144266.png)

![1570586773003](images/1570586773003.png)

一对多有两种写法，以下为第二种写法

![1570586834621](images/1570586834621.png)

4.一对一

https://www.cnblogs.com/heliusKing/p/11173185.html

![1570587786722](images/1570587786722.png)

![1570587808814](images/1570587808814.png)

在实际使用中可以通过以下方式来替代一对一。在property中使用对象.来进行映射

![1570588043287](images/1570588043287.png)

1.3 遍历map

```
<foreach collection="t.keys"  item="item" index="i" separator=",">
   ${item} = #{t[${item}]}
</foreach>
```

### 2.3 打印sql配置

在application.yml（.properties）中增加配置，在控制台打印sql：

```
mybatis
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl

```

# Redis

## 1.安装

1.获取文件

 wget http://download.redis.io/releases/redis-5.0.5.tar.gz
 tar xzf redis-5.0.5.tar.gz
 cd redis-5.0.5

2.make

redis是用c写的如果没有环境需要先安装 yum install gcc-c++

安装成功后执行 make install

如果报

![1568951567665](images/1568951567665.png)

执行   make MALLOC=libc



执行成功后

进入src 目录后 ./redis-server & 出现以下页面则运行成功

![1568951635826](images/1568951635826.png)

注：如果没有wget 命令，运行  yum -y install wget 进行安装

![1574677965312](images/1574677965312.png)



2.修改安装目录的配置文件

①将daemonize no  中的no 改为yes(意味着redis在后台启动)

②在bind 127.0.0.1 前面加#号   默认只有本机才能够连接 



## 2.src目录下文件作用

| **文件**         | **说明**                  |
| ---------------- | ------------------------- |
| redis-server     | 启动redis                 |
| redis-cli        | redis命令行客户端         |
| redis-benchmark  | 测试工具                  |
| redis-check-aof  | AOF持久化工具             |
| redis-check-dump | DB持久化工具              |
| redis-sentinel   | 哨兵相关                  |
| redis-trib       | Redis cluster模式管理工具 |
| redis.conf       | Redis的配置文件           |

![1568986297468](/images/1568986297468.png)

## 3.redis 优缺点

**缺点：**单进程单线程，命令慢阻塞

**优点：**1.减少了线程间切换带来的性能消耗

·	     2.数据运行在内存中，减少了io读取

## 4.基本操作

redis 有5种基本类型String ,set, hash,list,SortedSet（zset有序）

### 4.1 字符串

字符串类型：实际上可以是字符串（包括XML JSON），还有数字（整形浮点数），二进制（图片 音频视频），最大不能超过512MB

**命令：**

​          set key value

​          set name monkey ex 20  //20秒后过期 px 20000 毫秒过期

​          setnx name monkey//不存在键name时，返回1设置成功；存在的话失败0

​          set age 18 xx      //存在键age时，返回1成功

批量设值：mset name monkey sex boy

批量获取：mget name sex  （\r\n）

字符追加：append追加指令：

set name hello; 

append name  world //追加后成helloworld

字符串长度：

set monkey ’真帅’

strlen monkey//结果6，每个中文占3个字节(utf-8)

截取字符串：

  set  name monkey;

  getrange name 2 4//返回“nke”

**计数器**

CAS(Atomic原子操作)

incr age //必须为整数自加1，非整数返回错误，无age键从0自增返回1

decr age //整数age减1

incrby age 1 //整数age+1

decrby age 1//整数age -1

incrbyfloat score 3.3 //浮点型score+3.3

### 4.2 Hash 类型

哈希hash是一个string类型的field和value的映射表，hash特适合用于存储对象

| **Id** | **Username** | **Sex** | **age** |
| ------ | ------------ | ------- | ------- |
| 1      | Monkey       | Boy     | 18      |
| 2      | Fox          | Boy     |         |

 

   命令 hset key field value

   设值：hset user:1 name monkey         //成功返回1，失败返回0

   取值：hget user:1 name              //返回monkey

   删值：hdel user:1 age               //返回删除的个数

   计算个数：hset user:1 name monkey; hset user:1age 18; 

​            hlen user:1               //返回2，user:1有两个属性值

   批量设值：hmset user:2 name monkey age 18sexboy //返回OK

   批量取值：hmget user:2 name age sex   //返回三行：monkey 18 boy

   判断field是否存在：hexists user:2 name //若存在返回1，不存在返回0

   **获取所有field: hkeys user:2           //** **返回name age sex****三个field**

  **获取user:2****所有value****：hvals user:2     //** **返回monkey18 boy**

   **获取user:2****所有field****与value****：hgetall user:2 //name age sex monkey18 18 boy****值**

   增加1：hincrby user:2 age 1     //age+1

   hincrbyfloat user:2 age 2   //浮点型加2

### 4.3 List 集合

 用来存储多个有序的字符串，一个列表最多可存2的32次方减1个元素

**添加命令：**

​         rpush  monkeyc b a //从右向左插入cba, 返回值3

​         lrange monkey 0 -1 //从左到右获取列表所有元素返回 c b a

​         lpush key c b a //从左向右插入cba

​         linsert  monkey before b z//在b之前插入z, after为之后，使                                             用lrange     				monkey 0 -1 查看：c z b a

 **查找命令：**

​         lrange key start end //索引下标特点：从左到右为0到N-1

​         lindex monkey -1 //返回最右末尾a，-2返回b

​         llen monkey  //返回当前列表长度

​         lpop monkey //把最左边的第一个元素c删除

​         rpop monkey //把最右边的元素a删除

### 4.4 Set 集合

用户标签，社交，查询有共同兴趣爱好的人,智能推荐

保存多元素，与列表不一样的是不允许有重复元素，且集合是无序，一个集合最多可存2的32次方减1个元素，除了支持增删改查，还支持集合交集、并集、差集；

 exists user   //检查user键值是否存在

 sadduser a b c//向user插入3个元素，返回3

 sadduser a b  //若再加入相同的元素，则重复无效，返回0

 smembers user //获取user的所有元素,返回结果无序

 sremuser a   //返回1，删除a元素

 scard user   //返回2，计算元素个数

### 4.5 有序集合ZSET

常用于排行榜，如视频网站需要对用户上传视频做排行榜，或点赞数与集合有联系，不能有重复的成员



  **指令：**   
    
  zadd key score member [score member......]
  **例子:**
    
  zadd user:zan 200 monkey //monkey的点赞数1, 返回操作成功的条数1
    
  zadd user:zan 200 monkey 120 fox 100 lion// 返回3
    
  zadd user:zan nx 100 monkey//键test:1必须不存在，主用于添加
    
  zadd user:zan xx incr 200 monkey//键test:1必须存在，主用于修改,此时为300
     zadd
  user:zan xx ch incr -299 monkey//返回操作结果1，300-299=1
  zrange user:zan 0  -1
  withscores//查看点赞（分数）与成员名
    
  zcard user:zan    //计算成员个数， 返回1

  **排名场景：**
    
  zadd user:3 200 monkey120 fox 100 lee//先插入数据
    
  zrange user:3 0 -1 withscores //查看分数与成员

  zrank user:3 monkey//返回名次：第3名返回2，从0开始到2，共3名

  zrevrank user:3 monkey//返回0， 反排序，点赞数越高，排名越前

常用于排行榜，如视频网站需要对用户上传视频做排行榜，或点赞数与集合有联系，不能有重复的成员



  **指令：**   
    
  zadd key score member [score member......]
  **例子:**
    
  zadd user:zan 200 monkey //monkey的点赞数1, 返回操作成功的条数1
    
  zadd user:zan 200 monkey 120 fox 100 lion// 返回3
    
  zadd user:zan nx 100 monkey//键test:1必须不存在，主用于添加
    
  zadd user:zan xx incr 200 monkey//键test:1必须存在，主用于修改,此时为300
     zadd
  user:zan xx ch incr -299 monkey//返回操作结果1，300-299=1
  zrange user:zan 0  -1
  withscores//查看点赞（分数）与成员名
    
  zcard user:zan    //计算成员个数， 返回1

  **排名场景：**
    
  zadd user:3 200 monkey120 fox 100 lee//先插入数据
    
  zrange user:3 0 -1 withscores //查看分数与成员

  zrank user:3 monkey//返回名次：第3名返回2，从0开始到2，共3名

  zrevrank user:3 monkey//返回0， 反排序，点赞数越高，排名越前

## 5.Redis持久化

作用：

持久化是指将数据从内存中转移到磁盘中的过程。意义：防止突发事故导致redis宕机后数据丢失，redis持久化的方式一般有两种rdb和aof

### 5.1 RDB

把当前进程数据生成快照(.rdb)文件保存到硬盘的过程。实现方式有两种手动和自动触发

#### 5.1.1 手动触发

**save命令**:阻塞当前redis，直到rdb持久化完成为止，若内存较大会造成长时间阻塞，线上环境不建议使用。如果在BGSAVE命令的保存数据的子进程发生错误的时，用 SAVE命令保存最新的数据是最后的手段。

配置选项

```
save 900 1 // 900内,有1条写入,则产生快照
save 300 1000 // 如果300秒内有1000次写入,则产生快照
save 60 10000 // 如果60秒内有10000次写入,则产生快照

```

(这3个选项都屏蔽,则rdb禁用)    理解的话可以倒着向上看

```
stop-writes-on-bgsave-error yes // 后台备份进程出错时,主进程停不停止写入?  主进程不停止 容易造成数据不一致
rdbcompression yes // 导出的rdb文件是否压缩    如果rdb的大小很大的话建议这么做
Rdbchecksum yes // 导入rbd恢复时数据时,要不要检验rdb的完整性 验证版本是不是一致   
dbfilename dump.rdb //导出来的rdb文件名
dir ./ //rdb的放置路径  ，目录，恢复时也从该目录恢复，如果没有指定默认上一级目录

```

127.0.0.1:6379>save 

![1569214149617](images/1569214149617.png)

执行后会在指定目录下生成备份文件如果需要恢复数据，只需将备份文件 (dump.rdb) 移动到 redis 安装目录并启动服务即可。获取 redis 目录可以使用  CONFIG

bgsave命令：redis进程执行fork创建子线程，由子线程完成持久化，阻塞时间很短（微秒级）。在执行redis-cli shutdown 关闭redis服务时，如果没有开启aof持久化自动执行bgsave。

![1569215222962](images/1569215222962.png)



#### 5.1.2 save 与 bgsave 对比

| 命令   | save               | bgsave                             |
| ------ | ------------------ | ---------------------------------- |
| IO类型 | 同步               | 异步                               |
| 阻塞？ | 是                 | 是（阻塞发生在fock()，通常非常快） |
| 复杂度 | O(n)               | O(n)                               |
| 优点   | 不会消耗额外的内存 | 不阻塞客户端命令                   |
| 缺点   | 阻塞客户端命令     | 需要fock子进程，消耗内存           |

#### 5.1.3 Rdb 优缺点

**优点**

一、压缩后的二进制文，适用于备份、全量复制，用于灾难恢复

​     加载RDB恢复数据远快于AOF方式

二、与AOF相比，在恢复大的数据集的时候，RDB方式会更快一些。

**缺点**

一、无法做到实时持久化，每次都要创建子进程，频繁操作成本过高

 二、保存后的二进制文件，存在老版本不兼容新版本rdb文件的问题

 

### 5.2 AOF

快照功能（RDB）并不是非常耐久（durable）： 如果 Redis 因为某些原因而造成故障停机，那么服务器将丢失最近写入、且仍未保存到快照中的那些数据。 从 1.1 版本开始， Redis 增加了一种完全耐久的持久化方式： AOF 持久化。

打开AOF后， 每当 Redis 执行一个改变数据集的命令时（比如 SET）， 这个命令就会被追加到 AOF 文件的末尾。这样的话， 当 Redis 重新启时， 程序就可以通过重新执行 AOF 文件中的命令来达到重建数据集的目的。

#### 5.2.1 操作：

开启：redis.conf设置：appendonly yes  (默认不开启，为no) 

appendonly 有三种配置 always、everysec、no

| 命令   | 优点       | 缺点                              | 描述                                                         |
| ------ | ---------- | --------------------------------- | ------------------------------------------------------------ |
| always | 不丢失数据 | IO开销大，一般SATA磁盘只有几百TPS | 每次有新命令追加到 AOF 文件时就执行一次 fsync ：非常慢，也非常安全。 |

| everysec | 每秒进行与fsync，最多丢失1秒数据 | 可能丢失1秒数据 | 每秒 fsync 一次：足够快（和使用 RDB 持久化差不多），并且在故障时只会丢失 1 秒钟的数据。  推荐（并且也是默认）的措施为每秒 fsync 一次， 这种 fsync 策略可以兼顾速度和安全性。 |
| -------- | -------------------------------- | --------------- | ------------------------------------------------------------ |
| no       | 不用管                           | 不可控          | 从不 fsync ：将数据交给操作系统来处理，由操作系统来决定什么时候同步数据。更快，也更不安全的选择。 |

#### 5.2.2 AOF优缺点

**优点**

1.AOF 会让你的Redis更加耐久

2.AOF文件是一个只进行追加的日志文件，即使由于某些原因(磁盘空间已满，写的过程中宕机等等)未执行完整的写入命令，你也也可使用redis-check-aof工具修复这些问题。



3、Redis 可以在 AOF 文件体积变得过大时，自动地在后台对 AOF 进行重写： 重写后的新 AOF 文件包含了恢复当前数据集所需的最小命令集合。 整个重写操作是绝对安全的，因为 Redis 在创建新 AOF 文件的过程中，会继续将命令追加到现有的 AOF 文件里面，即使重写过程中发生停机，现有的 AOF 文件也不会丢失。 而一旦新 AOF 文件创建完毕，Redis 就会从旧 AOF 文件切换，并开始对新 AOF 文件进行追加操作。

4 AOF 文件有序地保存了对数据库执行的所有写入操作， 这些写入操作以 Redis 协议的格式保存，
因此 AOF 文件的内容非常容易被人读懂， 对文件进行分析（parse）也很轻松。 导出（export） AOF 文件也非常简单：
举个例子， 如果你不小心执行了 FLUSHALL 命令， 但只要 AOF 文件未被重写，
那么只要停止服务器， 移除 AOF 文件末尾的 FLUSHALL 命令， 并重启 Redis ，
就可以将数据集恢复到 FLUSHALL 执行之前的状态。



**缺点**

1. 对于相同的数据集来说，AOF 文件的体积通常要大于 RDB 文件的体积。


1. 根据所使用的 fsync 策略，AOF 的速度可能会慢于 RDB 。 在一般情况下， 每秒 fsync 的性能依然非常高， 而关闭 fsync 可以让 AOF 的速度和 RDB 一样快， 即使在高负荷之下也是如此。 不过在处理巨大的写入载入时，RDB 以提供更有保证的最大延迟时间（latency）

## 6 Redis 高可用解决方案

 https://www.cnblogs.com/qingmuchuanqi48/p/11154182.html

### 6.1主从模式

主节点负责写数据，从节点负责读数据，主节点定期把数据同步到从节点保证数据的一致性

![1569219217442](/images/1569219217442.png)



#### 6.1.1 部署

1.、新增redis7000.conf（从）, 更改配置文件相应的内容
在6379（主）启动完后再启7000，完成配置；

2.、先启动master主节点，然后在启动slave从节点

全量复制：一般用于初始化场景（第一次执行save后 全量同步）

  启动主节点：./redis-server ../redis.conf &
  启动从节点：./redis-server ../redis7000.conf --slaveof 192.168.0.31 6379 &
  客户端连接：./redis-cli -p 7000
  查看主从配置：info replication
  断开主从复制：在slave节点，执行7000:>slaveof no one

 全量复制：一般用于初始化场景（第一次执行save后 全量同步）

部分复制：网络出现问题、从再次链主节点。一部分增加同步

 

**不足：**

1、也会存在单点故障的问题。如果master宕机服务就不可用了

2、写的压力还是主库上，性能上的瓶颈

3、如果我们的master挂掉之后，没办法切换，需要手动干预

 

### 6.2 哨兵模式

#### 6.2.1 安装步骤

在解压后的Redis文件夹中，找到redis.conf文件，该文件是启动redis服务所需文件，拿出来，我们这里建三个redis服务，一主两从，将文件拷贝三份，分别命名为redis-7000.conf，redis-7001.conf，redis-7002.conf，分别编辑以下内容，注意如果配置的文件夹不存在，则需要新建好。其余配置保持默认。我举一个例子说明：

- \#bind 127.0.0.1，将bind注释掉。
- port 7000：改变其服务端口，三份文件端口分别为7000,7001,7002，注意检查端口是否被其他程序占用。
- daemonize yes：修改服务为后台运行。
- pidfile /var/run/redis_7000.pid：指定不同的pid文件，注意三份配置文件不同。
- logfile "/var/redis/7000.log"：指定log日志路径，自己配，要求不同。
- dir /opt/redis-cluster/redis-7000：这个指定rdb文件的路径配置，要求改成不同。
- \# replicaof <masterip> <masterport>：主服务这句话注释，从服务配置的两台需要开启。配置主服务的ip的port。格式 192.168.45.36 7000
- masterauth ibethfy：都配上吧，从服务到主服务的认证密码。
- requirepass ibethfy：三份文件都配置，客户端访问需要密码验证。

　　(4)、配置好三份文件后，使用redis-server redis-7000.conf，redis-server redis-7001.conf，redis-server redis-7002.conf启动服务，注意我是在配置文件路径执行的命令。

　　　　使用ps -ef|grep redis查看是否三个服务都正常启动，使用redis-cli -h ip地址 -p 端口 -a 密码，通过客户端登陆，执行info replication，可以查看到主从信息。

　　(5)、搭建哨兵。新起一台linux吧。同样执行1,2步骤，安装redis。拷贝sentinel.conf三份，分别为sentinel-26380.conf，sentinel-26381.conf，sentinel-26382.conf。

　　(6)、修改sentinel

- protected-mode no：关闭保护模式，使外网能访问。
- port 26380：修改端口。三份文件分别不同。
- daemonize yes：修改为后台运行。
- pidfile /opt/redis-cluster/redis-sentinel-26380/sentinel.pid：指定不同pid文件，注意文件夹不存在自己要新建。
- logfile "/opt/redis-cluster/redis-sentinel-26380/26380.log"：配置哨兵日志文件。
- dir /opt/redis-cluster/redis-sentinel-26380：配置哨兵工作路径。
- sentinel monitor master7000 192.167.3.145 7000 2：配置哨兵需要监控的主节点ip和端口，最后的2代表，如果有2个哨兵主观认为主节点down了，那么就客观认为主节点down掉了，开始发起投票选举新主节点的操作。多个主节点配置多个。
- sentinel auth-pass master7000 ibethfy：配置哨兵连接主节点的认证密码。（主节点配置的requirepass）。
- sentinel down-after-milliseconds master7000 5000：配置多少毫秒后没收到主节点的反馈，则主观认为主节点down了。
- sentinel failover-timeout master7000 30000：failover过期时间。当failover开始后，在此时间内仍然没有触发任何failover操作，当前sentinel将会认为此次failoer失败。  

​         注意，配置前把之前的默认的mymaster开启的配置全部注销掉！或者直接在其上修改！不然会出现问题！

　　(7)、在配置文件目录下执行redis-sentinel sentinel-26380.conf，redis-sentinel sentinel-26381.conf，redis-sentinel sentinel-26382.conf，分别启动三个哨兵。

　　(8)、测试，kill掉7000端口的redis服务，一段时间后，登陆到其他端口的客户端，查看info replication，此时是否从节点的某一个已切换成主节点，另一个从节点属于新主节点的从节点。并测试主节点再次上线后，是否是新主节点的从节点。

### 6.3 集群部署

属于多组主从

#### 6.3.1 部署步骤

选择一个目录在该目录（/root/redis-cluster）创建6个文件夹

命令：mkdir 7001

![img](images/clip_image002.jpg)

然后把redis.conf这个文件拷贝到这6个目录中

命令：cp /usr/local/redis-5.0.0/redis.conf /root/redis-cluster/7001/



**修改配置文件**

命令： vim 7001/redis.conf

| port 7001#修改成自己对应的端口号  logfile "/root/redis-cluster/redis.log"**#****事先创建好**  dir /root/redis-cluster/7001/ **#****事先创建好**  cluster-enabled yes  cluster-config-file nodes7001.conf#根据目录不同区分  cluster-node-timeout 5000  appendonly yes  #开启aof  protected-mode no #保护模式 yes改为no  #bind 127.0.0.1 #注释或者去掉这个 | #端口号  port   #指定了记录日志的文件。  logfile   #数据目录，数据库的写入会在这个目录。rdb、aof文件也会写在这个目录  dir   #是否开启集群  cluster-enabled  ##集群配置文件的名称，每个节点都有一个集群相关的配置文件，持久化保存集群的信息。这个文件并不需要手动配置，这个配置文件有Redis生成并更新，每个Redis集群节点需要一个单独的配置文件，请确保与实例运行的系统中配置文件名称不冲突  cluster-config-file nodes.conf  #节点互连超时的阀值。集群节点超时毫秒数  cluster-node-timeout  #默认redis使用的是rdb方式持久化，这种方式在许多应用中已经足够用了。但是redis如果中途宕机，会导致可能有几分钟的数据丢失，根据save来策略进行持久化，Append Only File是另一种持久化方式，可以提供更好的持久化特性。Redis会把每次写入的数据在接收后都写入 appendonly.aof 文件，每次启动时Redis都会先把这个文件的数据读入内存里，先忽略RDB文件。  appendonly |
| ------------------------------------------------------------ | :----------------------------------------------------------- |
|                                                              |                                                              |

 

**3、启动**

分别启动不同的节点

/usr/local/redis-5.0.0/src/redis-server /root/redis-cluster/7001/redis.conf

/usr/local/redis-5.0.0/src/redis-server/root/redis-cluster/7002/redis.conf

/usr/local/redis-5.0.0/src/redis-server/root/redis-cluster/7003/redis.conf

/usr/local/redis-5.0.0/src/redis-server/root/redis-cluster/7004/redis.conf

/usr/local/redis-5.0.0/src/redis-server/root/redis-cluster/7005/redis.conf

/usr/local/redis-5.0.0/src/redis-server/root/redis-cluster/7006/redis.conf

**启动成功图**：ps -ef|grep redis

![img](images/clip_image004.jpg)

**4、创建集群**

执行命令：

 /usr/local/redis-5.0.0/src/redis-cli --clustercreate 192.168.0.31:7001 192.168.0.31:7002 192.168.0.31:7003 192.168.0.31:7004192.168.0.31:7005 192.168.0.31:7006 --cluster-replicas 1

 

![img](images/clip_image006.jpg)

是否检查成功：

./redis-cli -c -p 7001 

这要注意有输入-c(表示集群模式)

然后执行cluster nodes显示如图则表示集群部署成功

![img](images/clip_image008.jpg)

 

 

###  

**高可用水平扩展:**

增加redis实例      执行mkdir 7007 7008拷贝相应的redis.conf配置文件，然后进行相应的修改配置 然后启动。

 

执行集群节点并没有返现我们的节点数据，这个时候需要把启动的节点数据增加到集群中

![img](images/clip_image010.jpg)

![img](images/clip_image012.jpg)

**增加主机点：**

执行命令：./redis-cli --cluster add-node 192.168.0.31:7007192.168.0.31:7001

 

![img](images/clip_image014.jpg)

 

查看集群状态,可以看到已知节点为7个，nodes中也显示了7007节点，但是现在没有hash槽分配到7007

 

 

**重新分配哈希槽：**

使用redis-cli命令为7007分配hash槽，找到集群中的任意一个主节点(7001)，对其进行重新分片工作

命令:./redis-cli --cluster reshard 192.168.0.31:7001

 

 

**两种方式****:**

一种是all，以将所有节点用作散列槽的源节点，

一种是done，这种是你自己选择从哪个节点上拿出来节点分给8007

**备注**:all是随机的，比如说我们要分出1000个，则3个主节点分别拿出333个，333个，334个节点分别7007，这里我们选择done，从7001拿1000个给7007

 

![img](images/clip_image016.jpg)

**增加从节点：**

命令：./redis-cli --cluster add-node 192.168.0.31:7008 192.168.0.31:7001

![img](images/clip_image018.jpg)

目前增加还是**主节点**，并且没有hash槽。

**变成从节点:**

命令：CLUSTER REPLICATE 095faf5f43085d2b8034fcb417daddbd0f5d9c2c （这个节点给哪个主节点）

![img](images/clip_image020.jpg)

**删除从节点：**

命令：

./redis-cli --cluster del-node192.168.0.31:7008 230e6e5b45e310fffafd2809f0bd5c869889a015

 

**删除主节点：**

命令：

./redis-cli --cluster reshard192.168.0.31:7007

 

 

**关闭节点：**

命令：./redis-cli-p 7001 shutdown 

#### 6.3.2 常见问题：

在安装的过程中，可能会出现以下问题。

##### 1、服务启动成功了但是telnet192.168.0.xx 7001不通

   如果telnet不通，但是ps -ef|grep redis可以看到服务或者本地客户端可以连接，远程客户端连接不了。这个时候就要修改redis.conf参数了

vim 7001(2)/redis.conf  

注释bind

![img](images/clip_image022.jpg)

关掉受保护模式

![img](images/clip_image024.jpg)

##### 2、启动redis时没有反应，查看进程没有相关进程号。

Redis.conf文件中有一个

Logfile标签，可以设置日志文件输出到这个文件中进行排查问题

![img](images/clip_image026.jpg)

 

3、注意以下的情况[ERR] Node 192.168.0.31:7001 is not empty. Either the node alreadyknows other nodes (check with CLUSTER NODES) or contains some key in database0.

解决：

这个时候需要将每个节点下的这几个文件给删掉（测试情况删掉，实际应用不要删，这是备份文件以及节点信息，按实际的情况进行处理）：

```
appendonly.aof  dump.rdb  nodes-7001.conf

```

 

# mysql

## mysql centOs安装步骤

### 1.1.安装准备 

1.1查看是否有自带的MySql 库，如果先有卸载

    [root@hadoop02 ~]# rpm -qa | grep mysql
    mysql-libs-5.1.71-1.el6.x86_64
    [root@hadoop02 ~]# rpm -e --nodeps mysql-libs-5.1.71-1.el6.x86_64
    [root@hadoop02 ~]# rpm -qa | grep mysql
    [root@hadoop02 ~]# 
### 1.2 RPM格式安装MySQL

通过SSH进入到我们刚刚上传的三个rpm安装文件的位置，然后分别执行以下的三个命令：

        rpm -ivh MySQL-server-5.6.31-1.linux_glibc2.5.x86_64.rpm
        rpm -ivh MySQL-devel-5.6.31-1.linux_glibc2.5.x86_64.rpm
        rpm -ivh MySQL-client-5.6.31-1.linux_glibc2.5.x86_64.rpm

配置mysql的配置文件
执行下述命令，将MySQL的配置文件拷贝到/etc目录下

cp /usr/share/mysql/my-default.cnf /etc/my.cnf
1
初始化mysql
分别运行下述命令，初始化MySQL及设置密码：

 mkdir -p /mnt/sdc/mysql/data     # 创建文件夹
  /usr/bin/mysql_install_db --datadir=/mnt/sdc/mysql/data   #初始化MySQL且设置数据目录 
 service mysql start        #启动MySQL 
 cat /root/.mysql_secret        #查看root账号的初始密码，会出现下述所示信息


### 1.3 设置开机启动

执行以下命令来设置开机启动

cents6 用以下命令

```
chkconfig mysql on
1
```

centos7 用

systemctl enable mysql

执行以下命令来查看是否设置成功

```
 chkconfig --list | grep mysql
```

![1572669664384](images/1572669664384.png)

### 1.4 设置远程访问权限

​        mysql -u root -p mysql    //进入mysql控制
​        若是第一次进入数数据库则要求修改密码如下：
​        mysql>set password=password('123456');
​        接着再执行以下的sql脚本
​        mysql>update user set host = '%' where user = 'root';    //这个命令执行错误时可略过 
​        mysql>flush privileges;
​        mysql>select host, user from user; //检查‘%’ 是否插入到数据库中
​        mysql>quit


### 1.5 开放端口

如果没有关闭防火墙，到了这一步一定要记得开放端口

firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload


**问题1****

如果报以下错

错误原因：这是由于yum安装了旧版本的GPG keys造成的

解决办法：后面加上--force --nodeps

![1572667413118](1572667413118.png)

  rpm -ivh MySQL-server-5.6.31-1.linux_glibc2.5.x86_64.rpm --force --nodeps 

**问题2**

报 如下错误

![1572667545254](images/1572667545254.png)

则 运行 yum install -y libaio  

**问题3**

![1572667938378](images/1572667938378.png)
这是由于perl没有安装的原因，直接安装perl，代码如下：

```
yum -y install perl
```

**问题4**

![1572668021100](images/1572668021100.png)

这是由于autoconf 没有安装的原因，直接安装autoconf ，代码如下：

yum -y install autoconf 

**问题5**

启动时报以下错，可按如下方式依次查看原因，我的问题，是没有给my.inf指定数据库

![1572669293356](images/1572669293356.png)

1、可能是/usr/local/mysql/data/mysql.pid文件没有写的权限
解决方法 ：给予权限，执行 “chown -R mysql:mysql /var/data” “chmod -R 755 /usr/local/mysql/data”  然后重新启动mysqld！

2、可能进程里已经存在mysql进程
解决方法：用命令“ps -ef|grep mysqld”查看是否有mysqld进程，如果有使用“kill -9  进程号”杀死，然后重新启动mysqld！

3、可能是第二次在机器上安装mysql，有残余数据影响了服务的启动。
解决方法：去mysql的数据目录/data看看，如果存在mysql-bin.index，就赶快把它删除掉吧，它就是罪魁祸首了。本人就是使用第三条方法解决的 ！

4、mysql在启动时没有指定配置文件时会使用/etc/my.cnf配置文件，请打开这个文件查看在[mysqld]节下有没有指定数据目录(datadir)。
解决方法：请在[mysqld]下设置这一行：datadir = /usr/local/mysql/data

5、skip-federated字段问题
解决方法：检查一下/etc/my.cnf文件中有没有没被注释掉的skip-federated字段，如果有就立即注释掉吧。

6、错误日志目录不存在
解决方法：使用“chown” “chmod”命令赋予mysql所有者及权限

7、selinux惹的祸，如果是centos系统，默认会开启selinux
解决方法：关闭它，打开/etc/selinux/config，把SELINUX=enforcing改为SELINUX=disabled后存盘退出重启机器试试。

## 1.mysql的索引类型及数据结构

### 1.1 索引类型

mysql 是一种多引擎的数据库，索引的支持与具体的引擎有关

1.从使用上来分 ：索引可分为 唯一索引，主键索引，联合索引，全文索引（目前应该就myisim支持）

2.从存储结构上来分：索引可分为b树索引 （实际上使用的是b+树） ，hash索引（只有memory索引显示的支持）

3.从数据存储文件上来分：可分为聚集索引和非聚集索引

在查询中有种特殊情况，只查询索引列的数据，无需访问其他列，这种情况叫覆盖索引

### **to do*  增加b-tree结构



增加各种数据结构图形，说明b-tree 的信息

### 1.2 索引的特点 及使用注意

#### b-tree (大多数引擎是使用b+树结构)

在使用索引时需要遵循最左匹配原则即搜索条件要与创建索引的顺序一致。列：

如在某张表中创建联合索引（1，2，3） 搜索时 使用  1 and  2 and 3 或者 2 and 3 and 1 可以用上索引。

如果 2  and  3 因为  不是从1 开始 不符合最左匹配原则。从左开始 遇到第一个条件比较 符号时停止使用索引（<,> <= ,>=）

#### hash

hash索引只有在精确查询的时候才有效，目前只有momory引擎支持。

innodb内部有一个特殊的功能叫自适应哈希索引。当innodb觉得某些索引使用非常频繁时它会在内存中基于b-tree创建一个hash索引

**缺点：**不能按照索引的顺序存储，所以也就无法用于排序。也不能进行返回 条件和like查找。

<https://www.jianshu.com/p/fa6b27d8887b>

**聚簇索引**

聚簇索引的数据的物理存放顺序与索引顺序是一致的，即：只要索引时相邻的，那么对应的数据一定也是相邻地存放在磁盘上的。聚簇索引要比非聚簇索引查询效率高很多。
聚簇索引这种主+辅索引的好处是，当发生数据行移动或者页分裂时，辅助索引树不需要更新，因为辅助索引树存储的是主索引的主键关键字，而不是具体的物理地址。

如innodb 如果聚簇索引插入时比较分散，可以用OPTIMIZE table test; 对收索引进行优化

**非聚簇索引**

B+Tree的叶子节点上的data，并不是数据本身，而是数据存放的地址。主索引和辅助索引没啥区别，只是主索引中的key一定得是唯一的



**使用索引的好处**

1.提交磁盘查找次数

2.避免服务器查询时的排序和零时表

3.将随机io变成顺序io



#### 使用索引时的一些注意事项

1.当遇到大字符串需要作为索引时，可以考虑将该列转变成hash值   列如：需要存储大量url，并且根据url进行索引查找，这时候可以将url变成一个hash值 来提高效率

crc32（url） and url ="url"

2.创建索引时避免创建过多的单列索引，尽量合并成联合索引，联合索引的创建 要尽量将 使用频率高的或者数据量大的放前面

3.搜索时 按照最左匹配原则

4.使用聚簇索引（innodb）时 使用有序主键能够提高插入效率：首先使用主键建立索引，如果没有主键则使用唯一非空索引来代替，都没有会建立一个隐式主键来作为索引。主键使用无序或者使用类似uuid索引的话插入时不能保证按照插入顺序存储，会增加构建b+的过程。使用uuid会增加索引体积

5.查询时使用覆盖索引（索引包含所需查询的所有字段的值）可以减少回表查 

6.对于varchar类型的尽量指定索引的长度，通过截取数/总数 =0.031 为佳

7.对于order by 可以单独使用最左原则，也可和where使用，如果where使用了<,>,不等于

#### 1.3 explain

https://www.cnblogs.com/wangfengming/articles/8275448.html

##### id

select 标识符。这是select的查询序列号，sql执行的顺序表示，从大到小执行，数值越大，优先级越高

1. id相同时，执行顺序由上而下，id值越大优先级越高，越先被执行

2. 如果是子查询，id的需要会递增
3. id为null的最后执行

##### select_type

查询中每个select字句的类型

1. **simple**
   1. 简单的select，不适用union或子查询等

2. **primary**
   1. 子查询中最外层查询，查询中若包含任务复杂的子部分，最外层的select被标记为primary

3. **union**
   1. union中的第二个或者后边的select语句

4. DEPENDENT 
   1. UNION(UNION中的第二个或后面的SELECT语句，取决于外面的查询)

5. union result
   1.  (union的结果，union语句中第二个select开始后面所有select)

6. **subquery**

   见下图：from前的子查询

   ```
   explain select(select 1 from actor where id=1) from (select*from film where id=1 ) der;
   ```

   ![image-20230328104121872](images\image-20230328104121872.png)

7. dependend subquery
   1. 子查询中的第一个select，依赖于外部查询

8. **derived**

   见6图，from之后的子查询

9. uncacheable subquery（
   1. 一个子查询的结果不能被缓存，必须重新评估外链接的第一行）

##### table

显示这一步锁访问数据库中表名称（显示这一行的数据是关于那张表的），有时不是正式的表可能是简称

##### type

对表访问方式，表示mysql在表中找到所需的方式，又称访问类型

常用的类型有：all，index，range，ref，eq_erf,const,system,null(从左到右，性能从差到好

)

1. **all:**
   1. Full Table Scan Mysql将遍历全表以找到匹配的行

2. **index**:
   1. Full Index Scan index和all的区别为index只遍历索引树通常比ALL快，因为索引文件通常比数据文件小。（也就是说虽然all和Index		都是读全表，但index是从索引中读取的，而all是从硬盘中读的）主要优点		是避免了排序，因为索引是排好序的。

3. **renge:**
   1. 只检索给定范围的行，使用一个索引来选择行索引范围扫描，常见于　<,<=,>,>=,between,in等操作符。　

4. **ref**:　　
   1. 这是一种索引访问（有时也叫做索引查找），它返回所有匹配某个单个值的行，然而，它可能会找到多个符合条件的行。因此，它是查找和扫描的混合体，**此类索引访问只有当使用非唯一性索引或者唯一性索引的非唯一性前缀时才会发生**。把它叫做ref是因为索引要跟某个参考值相比较。这个参考值或者是一个常数，或者是来自多表查询前一个表里的结果值。

5. **eq_ref**:
   1. 区别在于使用的索引时唯一索引，对于每个索引键值，表中只有一条记录匹配，简单来说，就是多表连接中使用了primary key 或者unique key

6. **const，system:**
   1. 在整个查询过程中这个表最多只会有一条匹配的行，比如主键 id=1 就肯定只有一行；表最多有一个匹配行，const用于比较primary key 或者unique索引。因为只匹配一行数据，所以一定是用到primary key 或者unique 情况下才会是const,看下面这条语句

7. **null：**
   1. mysql在优化过程中分解语句，执行时甚至不用访问表或索引，列如从一个索引列表中选取最小值可以通过单独索引查找完成

##### possable_key

指出mysql能使用哪个索引在表中找到记录，查询设计到的字段上若存在索引，则该索引被列出，但不一定被使用（该查询可以利用的索引，如果没有任何索引显示 null）

##### key

key列显示mysql实际决定使用的索引，必然包含在possible_key中

##### key_len

表示索引中使用的字节数，可通过该列计算查询中使用索引的长度（key_len显示的值为索引字段的最大可能长度，并非实际使用长度，即key_len是根据定义计算而得，不是通过表中检索得到的） 计算方式：

例如，sakila.film_actor的主键是两个SMALLINT列，并且每个SMALLINT列是两个字节，那么索引中的每项是4个字节。也即说明key_len通过查找表的定义而被计算出，而不是表中的数据。

##### ref

ref 列显示使用哪个列或常数与key一起从表中选择数据行。指出对 key 列所选择的索引的查找方式，常见的值有 const, func, NULL, 具体字段名。当 key 列为 NULL ，即不使用索引时 。如果值是func，则使用的值是某个函数的结果

```
create table a11(id int primary key, age int);
insert into a11 value(1, 10),(2, 10);

mysql> desc select * from a11 where age=10;
+----+-------------+-------+------+---------------+------+---------+------+------+-------------+
| id | select_type | table | type | possible_keys | key  | key_len | ref  | rows | Extra       |
+----+-------------+-------+------+---------------+------+---------+------+------+-------------+
|  1 | SIMPLE      | a11   | ALL  | NULL          | NULL | NULL    | NULL |    2 | Using where |
+----+-------------+-------+------+---------------+------+---------+------+------+-------------+
注意:当 key 列为 NULL ， ref 列也相应为 NULL 。

mysql> desc select * from a11 where id=1;
+----+-------------+-------+-------+---------------+---------+---------+-------+------+-------+
| id | select_type | table | type  | possible_keys | key     | key_len | ref   | rows | Extra |
+----+-------------+-------+-------+---------------+---------+---------+-------+------+-------+
|  1 | SIMPLE      | a11   | const | PRIMARY       | PRIMARY | 4       | const |    1 |       |
+----+-------------+-------+-------+---------------+---------+---------+-------+------+-------+

注意:这次 key 列使用了主键索引，where id=1 中 1 为常量， ref 列的 const 便是指这种常量。

```



##### rows

　这一列是mysql评估 为了找到所需的行而要读取的行数。

##### Extra

该列中包含mysqk查询的详细信息有以下几种情况

1. Using index condition:
   1. 说明使用了覆盖索引，避免了表的数据行

2. using temporary ：
   1. mysql对查询结果进行排序的时候使用了一张临时表。通常是因为GROUP BY的列没有索引，或者GROUP BY和ORDER BY的列不一样，也需要创建临时表，建议添加适当的索引。

3. using filesort：
   1.  mysql对数据不是按照表内的索引顺序进行读取,而是使用了其他字段重新排序.也有可能是因为多表连接时，排序字段不是驱动表中的字段，因此也没办法利用索引完成排序，建议添加适当的索引。

4. using filesort： 
   1. mysql对数据不是按照表内的索引顺序进行读取,而是使用了其他字段重新排序.

## 2.mysql 查询优化 

### 2.1 mysql大数据量分页

limit实现步骤：先获取到开始条数的所有数据，然后丢弃之前的数据返回固定条数，所以在查询的时候会操作大量废弃数据。可使用以下方式查询

SELECT * from bookphone a  JOIN (
		SELECT id from bookphone  LIMIT 500000,10

) b on a.id=b.id

先使用覆盖索引查找出id，连接查询来获取

注：也可以查出id后使用in查询，速度会提高，没有以上方式快

### 2.2 索引优化建议

1. 注意最左匹配原则

2. 将范围查询尽量放到最后面，遇到范围查询后将不会使用索引

3. 不要在索引列上进行计算，特别是函数运算

4. like ‘a%’可以使用索引，like '%a%'不能使用索引。遇到like 'a%'之后的索引也可以执行

5. in 可以使用到索引,相当于是范围查询，但是大多数时候in需要查询的数据比较多，会使索引失效，or 和 in要慎用

6. MySQL支持两种方式的排序filesort和index，Using index是指MySQL扫描索引本身完成排序。index
   效率高，filesort效率低。

7. 尽量在索引列上完成排序，遵循索引建立（索引创建的顺序）时的最左前缀法则。

8. 如果order by的条件不在索引列上，就会产生Using filesort。

9. 能用覆盖索引尽量用覆盖索引

10. group by与order by很类似，其实质是先排序后分组，遵照索引创建顺序的最左前缀法则。对于group
    by的优化如果不需要排序的可以加上**order by null禁止排序**。注意，where高于having，能写在where中
    的限定条件就不要去having限定了

11. order by满足两种情况会使用Using index。

    1) order by语句使用索引最左前列。
    2) 使用where子句与order by子句条件列组合满足索引最左前列。

      

12. order by 与group by 优化

    1. case1

       1. ![image-20230325153924109](images\image-20230325153924109.png)

          分析：
          利用最左前缀法则：中间字段不能断，因此查询用到了name索引，从key_len=74也能看出，a****ge索引列用**
          在排序过程中，因为Extra字段里没有using filesort**  

       2. case 2

          1. ![image-20230325155426842](images\image-20230325155426842.png)

          1. 分析：
             从explain的执行结果来看：key_len=74，查询使用了name索引，由于用了position进行排序，跳过了
             age，出现了Using filesort。  

       3. case3

          1. ![image-20230325155524733](images\image-20230325155524733.png)

             查找只用到索引name，age和position用于排序，无Using filesort。  

             

       4. case4

          1. ![image-20230325155756795](images\image-20230325155756795.png)

             分析：
             和Case 3中explain的执行结果一样，但是出现了Using filesort，因为索引的创建顺序为
             name,age,position，但是排序的时候age和position颠倒位置了

       5. case5

          ​     ![image-20230325160010834](images\image-20230325160010834.png)

          与Case 4对比，在Extra中并未出现Using filesort，因为age为常量，在排序中被优化，所以索引未颠倒，
          不会出现Using filesort  

       6. case6

          ![image-20230325161420976](images\image-20230325161420976.png)

          分析：
          虽然排序的字段列与索引顺序一样，且order by默认升序，这里position desc变成了降序，导致与索引的
          排序方式不同，从而产生Using filesort。Mysql8以上版本有降序索引可以支持该种查询方式。  

       7. case7

          ![image-20230325161515803](images\image-20230325161515803.png)

          对于排序来说，多个相等条件也是范围查询  

       8. case8

          ![image-20230325161950728](images\image-20230325161950728.png)

          ****

       

    

    

![image-20230324085645012](images\image-20230324085645012.png)

### 2.3 一些索引例子

1. 联合索引第一个字段用范围不会走索引

![image-20230325145100974](images\image-20230325145100974.png)

结论：联合索引第一个字段就用范围查找不会走索引，mysql内部可能觉得第一个字段就用范围，结果集应该很大，回表效率不高，还不
如就全表扫描  

上面的例子可以使用以下来强制走索引，但是实际运行中效率会不高

SELECT * FROM employees force index(idx_name_age_position) WHERE name > 'LiLei';  

可以考虑使用覆盖索引来进行优化

```
EXPLAIN SELECT name,age,position FROM employees WHERE name > 'LiLei' AND age = 22 AND position ='manag
er';  
```

![image-20230325150037351](images\image-20230325150037351.png)

3. in和or在表数据量比较大的情况会走索引，在表记录不多的情况下会选择全表扫描  

   ```
   EXPLAIN SELECT * FROM employees WHERE name in ('LiLei','HanMeimei','Lucy') AND age = 22 AND position
   ='manager';  
   ```

   ![image-20230325150153965](images\image-20230325150153965.png)

   ![image-20230325150240941](images\image-20230325150240941.png)

4. like 'aa%'一般都会走索引

   1. ```sql
      EXPLAIN SELECT * FROM employees WHERE name like 'LiLei%' AND age = 22 AND position ='manager';
      ```
      
      ![image-20230325150600606](images\image-20230325150600606.png)
      
      
      
   2. ```sql
      EXPLAIN SELECT * FROM employees_copy WHERE name like 'LiLei%' AND age = 22 AND position ='manager';
      ```

      ![image-20230325150617395](images\image-20230325150617395.png)

   3. like 'a%'可以使用索引是因为使用了索引下推
      1. **什么是索引下推了？**
         对于辅助的联合索引(name,age,position)，正常情况按照最左前缀原则，SELECT * FROM employees WHERE name like 'LiLei%'
         AND age = 22 AND position ='manager' 这种情况只会走name字段索引，因为根据name字段过滤完，得到的索引行里的age和
         position是无序的，无法很好的利用索引。
         在MySQL5.6之前的版本，这个查询只能在联合索引里匹配到名字是 'LiLei' 开头的索引，然后拿这些索引对应的主键逐个回表，到主键索
         引上找出相应的记录，再比对age和position这两个字段的值是否符合。
         MySQL 5.6引入了索引下推优化，可以在索引遍历过程中，对索引中包含的所有字段先做判断，过滤掉不符合条件的记录之后再回表，可以有效的减少回表次数。使用了索引下推优化后，**上面那个查询在联合索引里匹配到名字是 'LiLei' 开头的索引之后，同时还会在索引里过滤age和position这两个字段，拿着过滤完剩下的索引对应的主键id再回表查整行数据**。
         **索引下推会减少回表次数**，**对于innodb引擎的表索引下推只能用于二级索引，innodb的主键索引（聚簇索引）树叶子节点上保存的是全行数据，所以这个时候索引下推并不会起到减少查询全行数据的效果。**
         **为什么范围查找Mysql没有用索引下推优化？**
         估计应该是Mysql认为范围查找过滤的结果集过大，like KK% 在绝大多数情况来看，过滤后的结果集比较小，所以这里Mysql选择给 like
         KK% 用了索引下推优化，当然这也不是绝对的，有时like KK% 也不一定就会走索引下推  

5. mysql可以使用trace工具来查看mysql的分析过程（之后可了解，暂时不做详细说明）



### 2.3 索引小知识点

#### 1.use filesort文件排序

**单路排序：**是一次性取出满足条件行的所有字段，然后在sort buffer中进行排序；用trace工具可
以看到sort_mode信息里显示< sort_key, additional_fields >或者< sort_key,
packed_additional_fields >
**双路排序（又叫回表排序模式）**：是首先根据相应的条件取出相应的**排序字段**和**可以直接定位行
数据的行 ID**，然后在 sort buffer 中进行排序，排序完后需要再次取回其它需要的字段；用trace工具
可以看到sort_mode信息里显示< sort_key, rowid >  

MySQL 通过比较系统变量 max_length_for_sort_data(**默认1024字节**) 的大小和需要查询的字段总大小来
判断使用哪种排序模式。
如果 字段的总长度小于max_length_for_sort_data ，那么使用 单路排序模式；
如果 字段的总长度大于max_length_for_sort_data ，那么使用 双路排序模∙式 

#### 2.join关联查询优化

1. mysql的表关联常见有两种算法  
   1. 嵌套循环连接 Nested-Loop Join(NLJ) 算法  
      1. 一次一行循环地从第一张表（称为**驱动表**）中读取行，在这行数据中取到关联字段，根据关联字段在另一张表（**被驱动
         表**）里取出满足条件的行，然后取出两张表的结果合集。  
         
      2.  
   
         ```
         EXPLAIN select * from t1 inner join t2 on t1.a= t2.a;  
         ```
   
         ![image-20230325171727865](images\image-20230325171727865.png)
   
         从执行计划中可以看到这些信息：
         驱动表是 t2，被驱动表是 t1。先执行的就是驱动表(执行计划结果的id如果一样则按从上到下顺序执行sql)；优
         化器一般会优先选择**小表做驱动表**。**所以使用 inner join 时，排在前面的表并不一定就是驱动表。
         **当使用left join时，左表是驱动表，右表是被驱动表，当使用right join时，右表时驱动表，左表是被驱动表，
         当使用join时，mysql会选择数据量比较小的表作为驱动表，大表作为被驱动表。
         使用了 NLJ算法。一般 join 语句中，如果执行计划 Extra 中未出现 **Using join buffer** 则表示使用的 join 算
         法是 NLJ。 
   
         上面sql的大致流程如下：
   
         1. 从表 t2 中读取一行数据（如果t2表有查询过滤条件的，会从过滤结果里取出一行数据）；
         2. 从第 1 步的数据中，取出关联字段 a，到表 t1 中查找；
         3. 取出表 t1 中满足条件的行，跟 t2 中获取到的结果合并，作为结果返回给客户端；
         4. 重复上面 3 步。
   
         整个过程会读取 t2 表的所有数据(扫描100行)，然后遍历这每行数据中字段 a 的值，根据 t2 表中 a 的值索引扫描 t1 表
         中的对应行(扫描100次 t1 表的索引，1次扫描可以认为最终只扫描 t1 表一行完整数据，也就是总共 t1 表也扫描了100
         行)。因此整个过程扫描了 200 行。
         如果被驱动表的关联字段没索引，使用NLJ算法性能会比较低(下面有详细解释)，mysql会选择Block Nested-Loop Join
         算法  
   
   2. 基于块的嵌套循环连接 Block Nested-Loop Join(BNL)算法  
   
      把**驱动表**的数据读入到 join_buffer 中，然后扫描**被驱动表**，把**被驱动表**每一行取出来跟 join_buffer 中的数据做对比  
   
      ```sql
      EXPLAIN select * from t1 inner join t2 on t1.b= t2.b;  
      ```
   
      ![image-20230325173715341](images\image-20230325173715341.png)
   
      Extra 中 的Using join buffer (Block Nested Loop)说明该关联查询使用的是 BNL 算法。
      **上面sql的大致流程如下：
      **1. 把 t2 的所有数据放入到 **join_buffer** 中
   
      2. 把表 t1 中每一行取出来，跟 join_buffer 中的数据做对比
      3. 返回满足 join 条件的数据  
   
      整个过程对表 t1 和 t2 都做了一次全表扫描，因此扫描的总行数为10000(表 t1 的数据总量) + 100(表 t2 的数据总量) =
      **10100**。并且 join_buffer 里的数据是无序的，因此对表 t1 中的每一行，都要做 100 次判断，所以内存中的判断次数是
      100 * 10000= **100 万次**。
      这个例子里表 t2 才 100 行，要是表 t2 是一个大表，join_buffer 放不下怎么办呢？
      join_buffer 的大小是由参数 join_buffer_size 设定的，默认值是 256k。如果放不下表 t2 的所有数据话，策略很简单，
      就是**分段放**。
      比如 t2 表有1000行记录， join_buffer 一次只能放800行数据，那么执行过程就是先往 join_buffer 里放800行记录，然
      后从 t1 表里取数据跟 join_buffer 中数据对比得到部分结果，然后清空 join_buffer ，再放入 t2 表剩余200行记录，再
      次从 t1 表里取数据跟 join_buffer 中数据对比。所以就多扫了一次 t1 表。  
   
      **被驱动表的关联字段没索引为什么要选择使用 BNL 算法而不使用 Nested-Loop Join 呢？**  
   
      如果上面第二条sql使用 Nested-Loop Join，那么扫描行数为 100 * 10000 = 100万次，这个是**磁盘扫描**。
      很显然，用BNL磁盘扫描次数少很多，相比于磁盘扫描，BNL的内存计算会快得多。
      因此MySQL对于被驱动表的关联字段没索引的关联查询，一般都会使用 BNL 算法。如果有索引一般选择 NLJ 算法，有
      索引的情况下 NLJ 算法比 BNL算法性能更高  
   
   3. **重要：对于关联sql的优化**
   
      **关联字段加索引**，让mysql做join操作时尽量选择NLJ算法
      **小表驱动大表**，写多表连接sql时如果明确知道哪张表是小表可以用straight_join写法固定连接驱动方式，省去
      mysql优化器自己判断的时间  
   
      **straight_join解释**：straight_join功能同join类似，但能让左边的表来驱动右边的表，能改表优化器对于联表查询的执
      行顺序。
      比如：select * from t2 straight_join t1 on t2.a = t1.a; 代表指定mysql选着 t2 表作为驱动表。
      straight_join只适用于inner join，并不适用于left join，right join。（因为left join，right join已经代表指
      定了表的执行顺序）
      尽可能让优化器去判断，因为大部分情况下mysql优化器是比人要聪明的。使用straight_join一定要慎重，因
      为部分情况下人为指定的执行顺序并不一定会比优化引擎要靠谱。
      对于小表定义的明确
      在决定哪个表做驱动表的时候，应该是两个表按照各自的条件过滤，过滤完成之后，计算参与 join 的各个字段的总数据
      量，数据量小的那个表，就是“小表”，应该作为驱动表。  

#### 3.in和exsits优化

则：**小表驱动大表**，即小的数据集驱动大的数据集  

in：当B表的数据集小于A表的数据集时，in优于exists  

```sql
select * from A where id in (select id from B)  
 #等价于：
 for(select id from B){
	 select * from A where A.id = B.id
 }
```

exists：当A表的数据集小于B表的数据集时，exists优于in  

将主查询A的数据，放到子查询B中做条件验证，根据验证结果（true或false）来决定主查询的数据是否保留  

```sql
 select * from A where exists (select 1 from B where B.id = A.id)
 #等价于:
 for(select * from A){
 	select * from B where B.id = A.id
 }

#A表与B表的ID字段应建立索引
```

1、EXISTS (subquery)只返回TRUE或FALSE,因此子查询中的SELECT * 也可以用SELECT 1替换,官方说法是实际执行时会
忽略SELECT清单,因此没有区别
2、EXISTS子查询的实际执行过程可能经过了优化而不是我们理解上的逐条对比
3、EXISTS子查询往往也可以用JOIN来代替，何种最优需要具体问题具体分析  

#### 4. count统计

  

```sql
‐‐ 临时关闭mysql查询缓存，为了查看sql多次执行的真实时间
set global query_cache_size=0;
set global query_cache_type=0;

EXPLAIN select count(1) from employees;
EXPLAIN select count(id) from employees;
EXPLAIN select count(name) from employees;
EXPLAIN select count(*) from employees;
```

注意：以上4条sql只有根据某个字段count不会统计字段为null值的数据行  

多
字段有索引：count(*)≈count(1)>count(字段)>count(主键 id) //字段有索引，count(字段)统计走二级索引，二
级索引存储数据比主键索引少，所以count(字段)>count(主键 id)
字段无索引：count(*)≈count(1)>count(主键 id)>count(字段) //字段没有索引count(字段)统计走不了索引，
count(主键 id)还可以走主键索引，所以count(主键 id)>count(字段)
count(1)跟count(字段)执行过程类似，不过count(1)不需要取出字段统计，就用常量1做统计，count(字段)还需要取出
字段，所以理论上count(1)比count(字段)会快一点。
count(*) 是例外，mysql并不会把全部字段取出来，而是专门做了优化，不取值，按行累加，效率很高，所以不需要用
count(列名)或count(常量)来替代 count(*)。
为什么对于count(id)，mysql最终选择辅助索引而不是主键聚集索引？因为二级索引相对主键索引存储数据更少，检索
性能应该更高，mysql内部做了点优化(应该是在5.7版本才优化)。  

#### 5 .使用tinyint时如果不是标识boolean大小要设置成2

#### 6 .INT显示宽度

  我们经常会使用命令来创建数据表，而且同时会指定一个长度，如下。但是，这里的长度并非是TINYINT类型存储的最大
长度，而是显示的最大长度  

```sql
 CREATE TABLE `user`(
 	`id` TINYINT(2) UNSIGNED
 );
```

这里表示user表的id字段的类型是TINYINT，可以存储的最大数值是255。所以，在存储数据时，如果存入值小于等于
255，如200，虽然超过2位，但是没有超出TINYINT类型长度，所以可以正常保存；如果存入值大于255，如500，那么
MySQL会自动保存为TINYINT类型的最大值255。
在查询数据时，不管查询结果为何值，都按实际输出。这里TINYINT(2)中2的作用就是，当需要在查询结果前填充0时，
命令中加上ZEROFILL就可以实现，如：  

```
`id` TINYINT(2) UNSIGNED ZEROFILL
```

这样，查询结果如果是5，那输出就是05。如果指定TINYINT(5)，那输出就是00005，其实实际存储的值还是5，而且存
储的数据不会超过255，只是MySQL输出数据时在前面填充了0。
换句话说，在MySQL命令中，字段的类型长度TINYINT(2)、INT(11)不会影响数据的插入，只会在使用ZEROFILL时有
用，让查询结果前填充0。  

## 3 数据库事务

### 3.1 事务及其ACID属性

#### 事务的特性：

事务是一组操作要么全部成功，要么全部失败，目的是为了保证数据最终的一致性。
事务具有以下4个属性,通常简称为事务的ACID属性。  

- 原子性（atomicity）：当前事务的操作要么成功要么失败。原子性由undo log日志来实现。
- 一致性（Consistent）：使用事务的最终目的，由其他3个特性及其业务代码正常逻辑来实现
- 隔离性(Isolation)   ：在事务并发执行时，他们内部的操作不能互相干扰。隔离性由MySQL的各种锁以及MVCC机制来实现  
- 持久性(Durable) ：  一旦提交了事务，它对数据库的改变就应该是永久性的。持久性由redo log日志来实现  

#### 并发带来的问题

1. 更新丢失（脏读）
   1. 当多个事务选择同一行数据修改是，可能会出现更新丢失的问题，即最后的更新覆盖了前面的语句
2. 脏读
   1. 事务A读到了事务B未提交的数据
3. 不可重复读
   1. 在同一个事务中，多次执行查询时，得到的结果都不一样
4. 幻读
   1. 在事务A读取到了事务B提交的新增数据（先更新，再查询就会出现这种情况）
   2. 事务A计划更新10条，但是更新结束后发现更新了12条

#### 事务的隔离级别

脏读”、“不可重复读”和“幻读”,其实都是数据库读一致性问题,必须由数据库提供一定的事务隔离机制来解决  

| 隔离级别                     | 脏读(Dirty Read) | 不可重复读NonRepeatable                Read | 幻读(Phantom Read) |
| ---------------------------- | ---------------- | ------------------------------------------- | ------------------ |
| 读未提交（read committed）   | 可能             | 可能                                        | 可能               |
| 读已提交（read uncommitted） | 不可能           | 可能                                        | 可能               |
| 可重复读（repeatable read）  | 不可能           | 不可能                                      | 可能               |
| 串行读（serializable）       | 不可能           | 不可能                                      | 不可能             |

数据库的事务隔离越严格,并发副作用越小,但付出的代价也就越大,因为事务隔离实质上就是
使事务在一定程度上“串行化”进行,这显然与“并发”是矛盾的。
同时,不同的应用对读一致性和事务隔离程度的要求也是不同的,比如许多应用对“不可重复
读"和“幻读”并不敏感,可能更关心数据并发访问的能力。  

**Mysql默认的事务隔离级别是可重复读，用Spring开发程序时，如果不设置隔离级别默认用Mysql设置的隔离级别，如果Spring设置了就用已经设置的隔离级别 **

#### 数据库事务操作方式

  

```sql
# 查看当前数据库的事务隔离级别
show variables like 'tx_isolation';
# 设置隔离级别
set tx_isolation='read‐uncommitted';
set tx_isolation='read‐committed'
set tx_isolation='REPEATABLE-READ';
```

#### 事务定位方法

```sql
#查询执行时间超过1秒的事务，详细的定位问题方法后面讲完锁课程后会一起讲解
 SELECT * FROM
 information_schema.innodb_trx
 WHERE
 TIME_TO_SEC( timediff( now( ), trx_started ) ) > 1;
#强制结束事务
 kill 事务对应的线程id(就是上面语句查出结果里的trx_mysql_thread_id字段的值)
```

##### 锁等待分析

通过检查InnoDB_row_lock状态变量来分析系统上的行锁的争夺情况  

```sql
show status like 'innodb_row_lock%';

对各个状态量的说明如下：
 Innodb_row_lock_current_waits: 当前正在等待锁定的数量
 Innodb_row_lock_time: 从系统启动到现在锁定总时间长度
 Innodb_row_lock_time_avg: 每次等待所花平均时间
 Innodb_row_lock_time_max：从系统启动到现在等待最长的一次所花时间
 Innodb_row_lock_waits: 系统启动后到现在总共等待的次数

 对于这5个状态变量，比较重要的主要是：
 Innodb_row_lock_time_avg （等待平均时长）
 Innodb_row_lock_waits （等待总次数）
 Innodb_row_lock_time（等待总时长）
```

尤其是当等待次数很高，而且每次等待时长也不小的时候，我们就需要分析系统中为什么会有如此多的等待，然后根据分析结果着手制定优化计划  

**查看INFORMATION_SCHEMA系统库锁相关数据表**  

```sql
 ‐‐ 查看事务
select * from INFORMATION_SCHEMA.INNODB_TRX;
 ‐‐ 查看锁，8.0之后需要换成这张表performance_schema.data_locks
 select * from INFORMATION_SCHEMA.INNODB_LOCKS;
 ‐‐ 查看锁等待，8.0之后需要换成这张表performance_schema.data_lock_waits
select * from INFORMATION_SCHEMA.INNODB_LOCK_WAITS;
 ‐‐ 释放锁，trx_mysql_thread_id可以从INNODB_TRX表里查看到
kill trx_mysql_thread_id  
 ‐‐ 查看锁等待详细信息
show engine innodb status;
```

**死锁问题分析**  

```sql
 set tx_isolation='repeatable‐read';
 Session_1执行：select * from account where id=1 for update;
 Session_2执行：select * from account where id=2 for update;
 Session_1执行：select * from account where id=2 for update;
 Session_2执行：select * from account where id=1 for update;
 查看近期死锁日志信息：show engine innodb status;
```

大多数情况mysql可以自动检测死锁并回滚产生死锁的那个事务，但是有些情况mysql没法自动检测死锁，
这种情况我们可以通过日志分析找到对应事务线程id，可以通过kill杀掉  

##### 锁优化

- 尽可能让所有数据检索都通过索引来完成，避免无索引行锁升级为表锁
- 合理设计索引，尽量缩小锁的范围
- 尽可能减少检索条件范围，避免间隙锁
- 尽量控制事务大小，减少锁定资源量和时间长度，涉及事务加锁的sql尽量放在事务最后执行
- 尽可能用低的事务隔离级别  



#### 大事务的影响

- 并发情况下，数据库连接池容易被撑爆
- 锁定太多的数据，造成大量的阻塞和锁超时
- 执行时间长，容易造成主从延迟
- 回滚所需要的时间比较长
- undo log膨胀
- 容易导致死锁  

#### 事务优化

- 将查询等数据准备操作放到事务外

- 事务中避免远程调用，远程调用要设置超时，防止事务等待时间太久

- 事务中避免一次性处理太多数据，可以拆分成多个事务分次处理

- 更新等涉及加锁的操作尽可能放在事务靠后的位置

- 能异步处理的尽量异步处理

- 应用侧(业务代码)保证数据一致性，非事务执行  

### 3.2 数据库的各种锁

#### 锁分类

- 从性能上分为**乐观锁**(用版本对比或CAS机制)和**悲观锁**，乐观锁适合读操作较多的场景，悲观
  锁适合写操作较多的场景，如果在写操作较多的场景使用乐观锁会导致比对次数过多，影响性能
- 从对数据操作的粒度分，分为**表锁、页锁、行锁**
- 从对数据库操作的类型分，分为**读锁和写锁**(都属于悲观锁)，还有**意向锁**  

##### 读锁

共享锁，S锁(**S**hared)）：针对同一份数据，多个读操作可以同时进行而不会互相影响，比如：  

```sql
select * from T where id=1 lock in share mode
```

##### 写锁

（排它锁，X锁(e**X**clusive)）：当前写操作没有完成前，它会阻断其他写锁和读锁，数据修改操作都
会加写锁，查询也可以通过for update加写锁，比如：  

```sql
select * from T where id=1 for update
```

##### 意向锁（Intention Lock）

又称I锁，针对表锁，主要是为了提高加表锁的效率，是mysql数据库自己加的。**当有事务给表的数据行加了共享锁或排他锁，同时会给表设置一个标识，代表已经有行锁了，其他事务要想对表加表锁时，就不必逐行判断有没有行锁可能跟表锁冲突了，直接读这个标识就可以确定自己该不该加表锁**。特别是表中的记录很多时，逐行判断加表锁的方式效率很低。而这个**标识就是意向锁**。
意向锁主要分为：
意向共享锁，IS锁，对整个表加共享锁之前，需要先获取到意向共享锁。
意向排他锁，IX锁，对整个表加排他锁之前，需要先获取到意向排他锁  

##### 表锁

  每次操作锁住整张表。**开销小，加锁快**；不会出现死锁；锁定粒度大，发生锁冲突的概率最高，并发度最
低；一般用在整表数据迁移的场景  

```sql
 ‐‐建表SQL
 CREATE TABLE `mylock` (
 `id` INT (11) NOT NULL AUTO_INCREMENT,
 `NAME` VARCHAR (20) DEFAULT NULL,
 PRIMARY KEY (`id`)
 ) ENGINE = MyISAM DEFAULT CHARSET = utf8;

‐‐插入数据
 INSERT INTO`test`.`mylock` (`id`, `NAME`) VALUES ('1', 'a');
 INSERT INTO`test`.`mylock` (`id`, `NAME`) VALUES ('2', 'b');
 INSERT INTO`test`.`mylock` (`id`, `NAME`) VALUES ('3', 'c');
 INSERT INTO`test`.`mylock` (`id`, `NAME`) VALUES ('4', 'd');
 ‐‐手动增加表锁
 lock table 表名称 read(write),表名称2 read(write);
 ‐‐查看表上加过的锁
 show open tables;
 ‐‐删除表锁
  unlock tables;
```

##### 页锁

只有**BDB存储引擎支持页锁**，页锁就是在页的粒度上进行锁定，锁定的数据资源比行锁要多，因为一个页中
可以有多个行记录。当我们使用页锁的时候，会出现数据浪费的现象，但这样的浪费最多也就是一个页上的
数据行。页锁的开销介于表锁和行锁之间，会出现死锁。锁定粒度介于表锁和行锁之间，并发度一般。  

##### 行锁

每次操作锁住一行数据。**开销大，加锁慢**；会出现死锁；锁定粒度最小，发生锁冲突的概率最低，并发度最
高。
InnoDB相对于MYISAM的最大不同有两点：
**InnoDB支持事务（TRANSACTION）
InnoDB支持行级锁
**注意，**InnoDB的行锁实际上是针对索引加的锁(在索引对应的索引项上做标记)，不是针对整个行记录加的**
**锁。并且该索引不能失效，否则会从行锁升级为表锁。**(**RR级别会升级为表锁，RC级别不会升级为表锁**)
比如我们在RR级别执行如下sql

| 1 select * | from account where name = | 'lilei' for update; ‐‐where条件里的name字段无索引 |
| ---------- | ------------------------- | ------------------------------------------------- |
|            |                           |                                                   |

则其它Session对该表任意一行记录做修改操作都会被阻塞住。
**PS：关于RR级别行锁升级为表锁的原因分析
**因为在RR隔离级别下，需要解决不可重复读和幻读问题，所以在遍历扫描聚集索引记录时，为了防止扫描过
的索引被其它事务修改(不可重复读问题) 或 间隙被其它事务插入记录(幻读问题)，从而导致数据不一致，所
以MySQL的解决方案就是把所有扫描过的索引记录和间隙都锁上。

##### 间隙锁

间隙锁，锁的就是两个值之间的空隙，**间隙锁是在可重复读隔离级别下才会生效。
**上节课讲过，Mysql默认级别是repeatable-read，有幻读问题，间隙锁是可以解决幻读问题的。****
假设account表里数据如下： 

 ![image-20230326111620352](images\image-20230326111620352.png)

那么间隙就有 id 为 (3,10)，(10,20)，(20,正无穷) 这三个区间，在Session_1下面执行如下sql  

```sql
select * from account where id = 18 for update;
```

则其他Session没法在这个(10,20)这个间隙范围里插入任何数据。
如果执行下面这条sql：  

```sql
select * from account where id = 25 for update;
```

则其他Session没法在这个(20,正无穷)这个间隙范围里插入任何数据。  

也就是说，只要在间隙范围内锁了一条不存在的记录会锁住整个间隙范围，不锁边界记录，这样就能防止其
它Session在这个间隙范围内插入数据，就解决了可重复读隔离级别的幻读问题。  

##### 临键锁

Next-Key Locks是行锁与间隙锁的组合  ,通过一下方式可以对边界上锁

```sql
select * from account where id  => 3 and id<=25 for update;
```

### 3.3 MVCC多版本并发控制机制 

Mysql在可重复读隔离级别下如何保证事务较高的隔离性 这个隔离性就是靠MVCC(**Multi-Version Concurrency Control**)机制来保证的，对一行数据的读和写两个操作默认是不会通过加锁互斥来保证隔离性，避免了频繁加锁互斥，而在串行化隔离级别为了保证较高的隔离性是通过将所有操作加锁互斥来实现的。Mysql在读已提交和可重复读隔离级别下都实现了MVCC机制 

#### undo日志版本链与read view机制详解

undo日志版本链是指一行数据被多个事务依次修改过后，在每个事务修改完后，Mysql会保留修改前的数据undo回滚日
志，并且用两个隐藏字段trx_id和roll_pointer把这些undo日志串联起来形成一个历史记录版本链(见下图，需参考视频里的
例子理解)  

  ![image-20230326152420348](images\image-20230326152420348.png)

在**可重复读隔离级别**，当事务开启，执行任何查询sql时会生成当前事务的**一致性视图read-view，**该视图在事务结束之前永
远都不会变化(**如果是读已提交隔离级别在每次执行查询sql时都会重新生成read-view**)，这个视图由执行查询时所有未提交
事务id数组（数组里最小的id为min_id）和已创建的最大事务id（max_id）组成，事务里的任何sql查询结果需要从对应版
本链里的最新数据开始逐条跟read-view做比对从而得到最终的快照结果。
**版本链比对规则：
**1. 如果 row 的 trx_id 落在绿色部分( trx_id<min_id )，表示这个版本是已提交的事务生成的，这个数据是**可见**的；

2. 如果 row 的 trx_id 落在红色部分( trx_id>max_id )，表示这个版本是由将来启动的事务生成的，是**不可见**的(若 row 的

trx_id 就是当前自己的事务是可见的）；

3. 如果 row 的 trx_id 落在黄色部分(min_id <=trx_id<= max_id)，那就包括两种情况

a. 若 row 的 trx_id 在**视图数组**中，表示这个版本是由还没提交的事务生成的，**不可见**(若 row 的 trx_id 就是当前自己的
事务是可见的)；
b. 若 row 的 trx_id 不在**视图数组**中，表示这个版本是已经提交了的事务生成的，**可见**。  

![image-20230326163824852](images\image-20230326163824852.png)

#### 关于readview和可见性算法的原理解释 

readview和可见性算法其实就是记录了sql查询那个时刻数据库里提交和未提交所有事务的状态。
要实现RR隔离级别，**事务里每次执行查询操作readview都是使用第一次查询时生成的readview，也就是都是以第一次查询**
**时当时数据库里所有事务提交状态来比对数据是否可见**，当然可以实现每次查询的可重复读的效果了。
要实现RR隔离级别，事务里每次执行查询操作readview都会按照数据库当前状态重新生成readview，也就是每次查询都是
跟数据库里当前所有事务提交状态来比对数据是否可见，当然实现的就是每次都能查到已提交的最新数据效果了。

**注意：**begin/start transaction 命令并不是一个事务的起点，在执行到它们之后的第一个修改操作或加排它锁操作(比如
select...for update)的语句，事务才真正启动，才会向mysql申请真正的事务id，mysql内部是严格按照事务的启动顺序来分
配事务id的。  







# 4. mysql内部原理和日志机制












# ZooKeeper

## 1.安装教程

安装使用的是jdk1.7 ，zookeeper已上传百度云盘。安装之前请先确定已安装jdk

第一步：将文件上传至服务器任意目录下

![](images/QQ%E6%88%AA%E5%9B%BE20190915100041.png)

第二步 解压，命令   tar -zxvf zookeeper-3.4.6.tar.gz

解压完成后进入目录，结构如下

![](images/QQ%E6%88%AA%E5%9B%BE20190915100245.png)

第三步：进入zookeeper-3.4.6目录，创建data文件夹。命令：mkdir data

第四步 ：进入conf文件把zoo_sample.cfg 改成zoo.cfg  执行命令

vim /root/zookeeper-3.4.6/conf/zoo.cfg

修改data属性 :

dataDir=data的路径

（指定zookeeper将数据保存在哪个目录下，如果不修改，默认在/tmp下，这个目录下的数据有可能会在磁盘空间不足或服务器重启时自动被linux清理，所以一定要修改这个地址。按个人习惯将其修改为自己的管理目录。）

dataLogDir=/root/zookeeper-3.4.6/data 

，用于存放日志文件（千万不要写错 要写绝对路径）

   第四步：启动zookeeper

​           [root@localhost bin]# ./zkServer.sh start

​             关闭：[root@localhost  bin]# ./zkServer.sh stop

​           查看状态：[root@localhost bin]# ./zkServer.sh status  看到以下提示则启动成功

![](images/QQ%E6%88%AA%E5%9B%BE20190915101630%E5%90%AF%E5%8A%A8%E6%88%90%E5%8A%9F.png)

**注意：需要关闭防火墙。**

​         service iptables stop

​         永久关闭修改配置开机不启动防火墙：

​         chkconfig iptables off

​         如果不能成功启动zookeeper，需要删除data目录下的zookeeper_server.pid文件。

   若启动失败且输入jps 显示没有此命令 ，那么重新安装jdk 再启动即可 

**第五部 搭建集群**

1. 在dataDir目录中，创建一个名为myid的文件，并写入机器对应的数字值，比如我们是在zoo-1的机器上，就将该值设置为1，即集群中sever.1=zoo-1:2888:3888中server.后对应的数字。这是zookeeper用来识别是那一台集群机器的标识。这里一台机器的集群配置完成了，接下来我们在另外两台机器上进行相同的操作，安装分别zk，配置一摸一样就行，唯一一个地方需要修改的就是myid的文件。注：此处唯一不同的地方是每个dataDir下的myid中的内容要按照zoo.cfg配置文件中的集群信息设置。比如：x.x.x.2对应集群中的server.2，所以在myid中写入2
2. 将zookeeper传送到其他服务器，执行以下命令 scp -r  zookeeper-3.4.6 root@192.168.145.36:/root
3. 当集群中半数以上的节点启动后就可以使用了

## 2.zookeeper核心概念

### 什么是znode

![](images/zookeenode.jpg)

zookeeper 操作和维护的一个个数据节点，称为znode，采用类似文件系统的层级树状节点进行管理。如果znode节点包含数据则存储为字节数组（byte array）

创建znode时需要指定节点类型

znode共有4中类型分别为：持久（有序） 临时（无序） 持久有序和临时有序



### 节点类型

2大类、四种类型持久、临时、持久有序、临时有序

  PERSISTENT持久类型，如果不手动删除 是一直存在的

  PERSISTENT_SEQUENTIAL

  EPHEMERAL临时 客户端session失效就会随着删除节点没有子节点

  EPHEMERAL_SEQUENTIAL 有序 自增

#### stat 数据结构：

stat中记录了这个ZNode 的三个数据版本，分别是version（当前Znode 版本） cversion（当前znode子节点的版本）和cversion（当前znode的acl版本）

stat：状态信息，版本，权限相关

| 状态属性       | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| czxid          | 节点创建时的zxid                                             |
| mzxid          | 节点最新一次更新发生时的zxid                                 |
| ctime          | 节点创建时的时间戳.                                          |
| mtime          | 节点最新一次更新发生时的时间戳.                              |
| dataVersion    | 节点数据的更新次数.                                          |
| cversion       | 其子节点的更新次数                                           |
| aclVersion     | 节点ACL(授权信息)的更新次数.                                 |
| ephemeralOwner | 如果该节点为ephemeral节点, ephemeralOwner值表示与该节点绑定的session id. 如果该节点不是ephemeral节点, ephemeralOwner值为0. 至于什么是ephemeral节点 |
| dataLength     | 节点数据的字节数.                                            |
| numChildren    | 子节点个数.                                                  |

session 会话：

客户端创建一个和zk服务端连接的句柄。

连接状态：CONNECTING\CONNECTED\CLOSED

## 3.什么是Watcher

# Linux常用命令

https://blog.csdn.net/xiaoquantouer/article/details/76727442

## 1.开启端口

centos7开启端口
firewall-cmd --zone=public --add-port=80/tcp --permanent
命令含义：
--zone #作用域
--add-port=80/tcp  #添加端口，格式为：端口/通讯协议
--permanent   #永久生效，没有此参数重启后失效
 重启防火墙
 firewall-cmd --reload
————————————————
centos6开启端口

 vim /etc/sysconfig/iptables

-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT

service iptables restart

### 2.查看端口是否开启

2.1 lsof -l:端口

我们可以使用lsof命令来检查某一端口是否开放，基本语法如下：

lsof -l:8009

如果没有任何输出则说明没有开启该端口号

![1574991260693](images/1574991260693.png)

2.1使用netstat命令

我们可以使用netstat -atu检查linux中的开放端口

-a：所有的套接字。

-t ：节目的TCP连接

-u：节目的UDP连接

还可以添加-p标志来显示进程或程序名的相关PID。

## 3.后台开启命令

nohup  java -jar   xxxx.jar &****

## 4.文件查找命令

find  /user （查找路径） -name '文件名'

# Windows 常用命令

1、查看端口占用

**netstat -ano**

2.查看指定端口的占用情况
**netstat -aon|findstr 8080       数字为端口号**

3.查看PID对应的进程
**tasklist|findstr 5769    数字为PID**

4.结束该进程

**taskkill /f /t /im xxx.exe  杀程序**

****taskkill /f /PID 5769    杀PID****



# Nginx 

## 1.nginx安装

### 1.1 安装前的环境准备

 检查linux版本，需要linux内核2.6及以上（支持epoll）

uname -a

![1574692275704](images/1574692275704.png)

安装依赖库
Nginx中的功能是模块化的，而模块有依赖于一些软件包（如pcre库、zlib库、openssl库），因此需 要先安装依赖库。
GCC编译器: 编译C语言程序； PCRE库: 支持正则表达式； zlib库: 对http包内容进行gzip格式压缩； OpenSSL开发库：支持更安全的SSL协议，加密算法。

yum -y install make

 yum -y install zlib-devel

yum -y install gcc-c++

yum -y install libtool

yum -y install openssl-devel

yum -y install pcre-devel

### 2.nginx安装

下载nginx
官网下载地址： http://nginx.org/en/download.html

下载源码  wget http://nginx.org/download/nginx‐1.14.2.tar.gz  

解压  tar ‐zxvf nginx‐1.14.0.tar.gz

![1574692840888](images/1574692840888.png)

源码构建
基于nginx源码构建文档： http://nginx.org/en/docs/configure.html

**默认构建  ./configure**

基于参数构建  不建议

‐‐prefix设置Nginx的安装目录  #‐‐with‐http_ssl_module设置在Nginx中允许使用http_ssl_module模块的相关功能  ./configure ‐‐prefix=/usr/local/nginx ‐‐with‐http_stub_status_module ‐‐withhttp_ssl_module ‐‐with‐debug

编译安装
make && make install

启动nginx

进入安装目录/usr/local/nginx，启动nginx  ./sbin/nginx

​	帮助文档  

./sbin/nginx    ‐h

快速停止，不会管正在处理的请求  ./sbin/nginx ‐s stop  

优雅退出，会等待请求结束之后再关闭  ./sbin/nginx ‐s quit  

热装载配置文件   ./sbin/nginx ‐s reload

重新打开日志文件  ./sbin/nginx ‐s reopen



默认端口80，启动后浏览器访问http://192.168.3.14/

有时候服务启动后可能会出现不能访问的情况，这时候可能是防火墙，端口没有开



 vim /etc/sysconfig/iptables

-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT

service iptables restart

![1574694551972](images/1574694551972.png)

即可

## 2.linux使用说明

源码：<https://trac.nginx.org/nginx/browser>

官网：<http://www.nginx.org/>

linux常用的功能 1、反向代理 2正向代理 3负载均衡 ，4web缓存

### 2.1 nginx配置文件结构

默认的 nginx 配置文件 nginx.conf 内容如下

```
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }


    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


    # HTTPS server
    #
    #server {
    #    listen       443 ssl;
    #    server_name  localhost;

    #    ssl_certificate      cert.pem;
    #    ssl_certificate_key  cert.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}

}
```

**nginx 文件结构**

```
...              #全局块

events {         #events块
   ...
}

http      #http块
{
    ...   #http全局块
    server        #server块
    { 
        ...       #server全局块
        location [PATTERN]   #location块
        {
            ...
        }
        location [PATTERN] 
        {
            ...
        }
    }
    server
    {
      ...
    }
    ...     #http全局块
}
```

1.全局块：配置影响nginx全局的指令。一般有运行nginx服务器的用户组，nginx进程pid存放路径，日志存放路径，配置文件引入，允许生产worker pocess等（

nginx 启动运行后会有最少两个线程，worker pocess是用来处理请求的，一般是根据cpu来配置1比1

）

2.events块：配置影响nginx服务器与用户的网络连接。有每个进程对打连接数，选取哪种事件驱动模型处理连接请求，是否允许接多个网络连接，开启多个网络序列化等

3.http块：可以嵌套多个server，配置代理，缓存，日志定义绝大多数功能和第三方模块配置。如文件映入minme-type定义，日志定义，是否使用sendfile传输文件，连接超时时间。单连接请求数。

4.server：配置虚拟主机的相关参数，一个http可以有多个server

5.location：配置请求的路由，以及各种页面的处理情况

以下是网上找的配置文件

```
########### 每个指令必须有分号结束。#################
#user administrator administrators;  #配置用户或者组，默认为nobody nobody。
#worker_processes 2;  #允许生成的进程数，默认为1
#pid /nginx/pid/nginx.pid;   #指定nginx进程运行文件存放地址
error_log log/error.log debug;  #制定日志路径，级别。这个设置可以放入全局块，http块，server块，级别以此为：debug|info|notice|warn|error|crit|alert|emerg
events {
    accept_mutex on;   #设置网路连接序列化，防止惊群现象发生，默认为on
    multi_accept on;  #设置一个进程是否同时接受多个网络连接，默认为off
    #use epoll;      #事件驱动模型，select|poll|kqueue|epoll|resig|/dev/poll|eventport
    worker_connections  1024;    #最大连接数，默认为512
}
http {
    include       mime.types;   #文件扩展名与文件类型映射表
    default_type  application/octet-stream; #默认文件类型，默认为text/plain
    #access_log off; #取消服务日志    
    log_format myFormat '$remote_addr–$remote_user [$time_local] $request $status $body_bytes_sent $http_referer $http_user_agent $http_x_forwarded_for'; #自定义格式
    access_log log/access.log myFormat;  #combined为日志格式的默认值
    sendfile on;   #允许sendfile方式传输文件，默认为off，可以在http块，server块，location块。
    sendfile_max_chunk 100k;  #每个进程每次调用传输数量不能大于设定的值，默认为0，即不设上限。
    keepalive_timeout 65;  #连接超时时间，默认为75s，可以在http，server，location块。

    upstream mysvr {   
      server 127.0.0.1:7878;
      server 192.168.10.121:3333 backup;  #热备
    }
    error_page 404 https://www.baidu.com; #错误页
    server {
        keepalive_requests 120; #单连接请求上限次数。
        listen       4545;   #监听端口
        server_name  127.0.0.1;   #监听地址       
        location  ~*^.+$ {       #请求的url过滤，正则匹配，~为区分大小写，~*为不区分大小写。
           #root path;  #根目录
           #index vv.txt;  #设置默认页
           proxy_pass  http://mysvr;  #请求转向mysvr 定义的服务器列表
           deny 127.0.0.1;  #拒绝的ip
           allow 172.18.5.54; #允许的ip           
        } 
    }
}
```

上面是nginx的基本配置，需要注意的有以下几点：

1、几个常见配置项：

- 1.$remote_addr 与 $http_x_forwarded_for 用以记录客户端的ip地址； 
- 2.$remote_user ：用来记录客户端用户名称； 
- 3.$time_local ： 用来记录访问时间与时区；
- 4.$request ： 用来记录请求的url与http协议；
- 5.$status ： 用来记录请求状态；成功是200；
-  6.$body_bytes_s ent ：记录发送给客户端文件主体内容大小；
- 7.$http_referer ：用来记录从那个页面链接访问过来的；
- 8.$http_user_agent ：记录客户端浏览器的相关信息；

2、惊群现象：一个网路连接到来，多个睡眠的进程被同事叫醒，但只有一个进程能获得链接，这样会影响系统性能。

3、每个指令必须有分号结束。

## 3.静态文件配置

### 3.1 配置文件 

root /www/data;

​    location / {
​    }

​    location /images/ {
​    }	

 location ~ \.(log|text) {
             root /usr/local/file;
        }

可通过以上方式来访问静态文件

### 3.2 配置文件目录

location / {   
        root /usr/local/file                    //指定实际目录绝对路径；   
        autoindex on;                            //开启目录浏览功能；   
        autoindex_exact_size off;            //关闭详细文件大小统计，让文件大小显示MB，GB单位，默认为b；   
        autoindex_localtime on;              //开启以服务器本地时区显示文件修改日期！   
}

可通过以上配置来显示文件目录，配置时要注意使用 ; 结尾，不要有中文字符和空格

![1575189145934](images/1575189145934.png)

index 指令中可以列出多个文件。Nginx 会按顺序查找文件并返回第一个找到的文件。









































# 消息队列

## RabbitMq

### 1 windows 安装

1.1 安装 ERlang 

下载地址 ：erlang语言中文官网：http://www.cnerlang.com/resource

erlang语言英文官网：https://www.erlang.org/downloads

1.2 环境变量

安装完成之后创建一个名为ERLANG_HOME的系统变量，其值指向erlang的安装目录，同时将%ERLANG_HOME%\bin加入到Path中，最后打开命令行，输入erl，如果出现erlang的版本信息就表示erlang语言环境安装成功；

注意：ERLANG_HOME的指向目录是bin的上以及目录

erlang_home配置：

![1570862157192](images/1570862157192.png)

Path路径配置：

![1570862178113](images/1570862178113.png)

2.windows下安装RabbitMQ消息队列？

 步骤1：下载RabbitMQ软件，大概10M左右，速度可能非常慢，需要等待

 Rabbitmq软件下载：http://www.rabbitmq.com/   

步骤2：安装

下载完成后双击xxxxxx.exe进行安装，rabbitmq的默认安装目录是系统盘的Program file文件夹，但是rabbitmq其内部设置是不允许文件夹有中文，有空格的，加之功能也不适合在系统盘中，需要另外选择目录进行安装!。

建议：一般情况下都是将erlang与安装同一个打目录下，紧挨在一起!

步骤3：安装管理桌面的插件，可以通过浏览器登录查看消息队列状态和交换机的工作情况等信息的管理页面，类似于tomcat的管理页面；

在rabbitmq的安装目录下找到sbin目录，进入并在此目录打开cmd窗口(进入sbin的cmd窗口)。输入rabbitmq-plugins enable rabbitmq_management命令，会提示plugin安装成功!网上是默认安装6个插件，但是本身是提示了安装3个后面的使用也没有什么问题，不知道是不是版本问题，当前一般3个也能正常使用，场景图如下：
![1570866433610](images/1570866433610.png)

如果多次安装可能会出现以下问题（我没遇到)：

![1570866471012](images/1570866471012.png)

步骤4：打开Web管理页面

在浏览器输入http://localhost:15672进行验证，你会看到下面界面，输入用户名：guest，密码：guest你就可以进入管理界面！一般情况下，页面是每隔几秒刷新一次，也可以自己设置!

**注意：安装是要核对 ERlang  和mq的版本 要不会出现奇怪的错误**

### 2.linux安装

### 3.RabbitMq基本概念

![1570935073308](images/1570935073308.png)



RabbitMq是AMQP（高级消息队列协议）的标准实现。

Queue，exchang和binding构成了AMQP协议的核心

Producer：消息生产者，即投递消息的程序

Broker:消息队列服务实体

​	Exchange：消息交换机，它指定消息按什么规则，路由到哪个队列

​	Binding：绑定。它的作用就是把Exchange和Queue按照规则绑定起来。

​	Queue：消息队列的载体，每个消息都会被投放到一个或多个队列

Consumer：消息消费者，即接受消息的程序。

#### 3.1 Exchange简介

那么为什么我们需要 Exchange 而不是直接将消息发送至队列呢？

AMQP 协议中的核心思想就是生产者和消费者的解耦，生产者从不直接将消息发送给队列。生产者通常不知道是否一个消息会被发送到队列中，只是将消息发送到一个交换机。先由 Exchange 来接收，然后 Exchange 按照特定的策略转发到 Queue 进行存储。Exchange 就类似于一个交换机，将各个消息分发到相应的队列中。

在实际应用中我们只需要定义好 Exchange 的路由策略，而生产者则不需要关心消息会发送到哪个 Queue 或被哪些 Consumer 消费。在这种模式下生产者只面向 Exchange 发布消息，消费者只面向 Queue 消费消息，Exchange 定义了消息路由到 Queue 的规则，将各个层面的消息传递隔离开，使每一层只需要关心自己面向的下一层，降低了整体的耦合度。

#### 3.2 Exchange4种模式

Exchange通过binding和RoutingKey将消息与Queue建立连接。

Binding表示Exchange与Queue之间的关系，我们也可以简单的认为队列对该交换机上的消息感兴趣，绑定可以附带一个额外的参数RoutingKey.Exchange就是根据这个RoutingKey和当前Exchange所有绑定的Binding做匹配，如果满足就往Exchange所绑定的Queue发送消息，这样就解决了我们向RabbitMQ发送一次消息，可以分发到不同Queue。Routingkey的意义依赖于交换机的类型。

**Exchange主要有三种类型**：Fanout，Direct和topick

##### 3.2.1 Fanout

![1570955528204](images/1570955528204.png)

Fanout Exchange 会忽略 RoutingKey 的设置，直接将 Message 广播到所有绑定的 Queue 中。

##### 应用场景

以日志系统为例：假设我们定义了一个 Exchange 来接收日志消息，同时定义了两个 Queue 来存储消息：一个记录将被打印到控制台的日志消息；另一个记录将被写入磁盘文件的日志消息。我们希望 Exchange 接收到的每一条消息都会同时被转发到两个 Queue，这种场景下就可以使用 Fanout Exchange 来广播消息到所有绑定的 Queue。

##### 3.2.2 Direct

![1570955650546](images/1570955650546.png)

Direct是RabbitMQ默认的Exchange，完全根据RoutingKey来路由消息。设置Exchange个Queue的Binding时需指定RoutingKey（一般用Queue name），法消息时也指定一样的RoutingKey，消息就会被路由对应的Queue。

##### 应用场景

现在我们考虑只把重要的日志消息写入磁盘文件，例如只把 Error 级别的日志发送给负责记录写入磁盘文件的 Queue。这种场景下我们可以使用指定的 RoutingKey（例如 error）将写入磁盘文件的 Queue 绑定到 Direct Exchange 上

##### 3.3.3 topick

![1570960052907](images/1570960052907.png)

Topic Exchange 和 Direct Exchange 类似，也需要通过 RoutingKey 来路由消息，区别在于Direct Exchange 对 RoutingKey 是精确匹配，而 Topic Exchange 支持模糊匹配。分别支持`*`和`#`通配符，`*`表示匹配一个单词，`#`则表示匹配没有或者多个单词。

##### 应用场景

假设我们的消息路由规则除了需要根据日志级别来分发之外还需要根据消息来源分发，可以将 RoutingKey 定义为 `消息来源.级别` 如 `order.info`、`user.error`等。处理所有来源为 `user` 的 Queue 就可以通过 `user.*` 绑定到 Topic Exchange 上，而处理所有日志级别为 `info` 的 Queue 可以通过 `*.info` 绑定到 Exchange上。

#### 两种特殊的 Exchange

#### Headers Exchange

Headers Exchange 会忽略 RoutingKey 而根据消息中的 Headers 和创建绑定关系时指定的 Arguments 来匹配决定路由到哪些 Queue。

Headers Exchange 的性能比较差，而且 Direct Exchange 完全可以代替它，所以不建议使用。

#### Default Exchange

Default Exchange 是一种特殊的 Direct Exchange。当你手动创建一个队列时，后台会自动将这个队列绑定到一个名称为空的 Direct Exchange 上，绑定 RoutingKey 与队列名称相同。有了这个默认的交换机和绑定，使我们只关心队列这一层即可，这个比较适合做一些简单的应用。

#### 3.3 集群的搭建

<https://www.jianshu.com/p/6376936845ff>

#### 3.4 confirm模式

<https://blog.csdn.net/supercmd/article/details/93189617>

#### 3.5 限流

<https://www.cnblogs.com/haixiang/p/10959551.html>

## Rocketmq







# 分库分表中间件

## 1.分布分表原因

当网站快速发展，单库或者单标，负载不够时就需要进行优化，优化方式一般有以下几种

1.换数据库

2.sql，索引，字段

3.读写分离

4.分库分表

5.分区

## 2.什么是分库分表

即把存于一个库的数据分散到多个库中，把存于一个表的数据分散到多个表中。

一个库一个表 拆分为 N个库N个表

## 3.分库分表的常见方式

### 3.1 垂直分

通俗的叫法叫做“大表拆小表”，拆分是基于关系型数据库中的列（字段）进行的

**特点**：

1. 每个库（表）的结构都不一样
2. 每个库（表）的数据都（至少有一列）一样
3. 每个库（表）的并集是全量数据

总结：按拆分字段（多表字段拆成少表字段）

**缺点**

1. 如果单表的数据量大，写压力大
2. 受业务限制。一个业务往往会影响到数据库的瓶颈。（比如需要增减字段时会影响所有表）
3. 部分数据无法关联join，只能通过java程序接口去调用，提供了开发复杂度

### 3.2 水平分

以某个字段按照一定规律（取模，或者日期）将一个表的数据分到多个库中。

**特点：**

1. 每个表（库）的结构都是一样的

2. 每个表（库）的数据都是不一样的

3. 每个库（表）的并集是全部数据

   **优点**

   1、单库（表）的数据保持在一定的量（减少），有助于性能提高
   2、提高了系统的稳定性和负载能力。
   3、切分的表的结构相同、程序改造较少

**缺点：**

1、数据的扩容很有难度维护量大
2、拆分规则很难抽象出来
3、分片事务的一致性的问题部分业务无法关联 join、只能通过java程序接口去调用

## 4. 分库分表的中间件

常用的有 Proxy、jdbc两种方式

**Proxy****（代理层）**：mycat代理层、atlas（360）mysql-proxy

**优点**：1.开发无感知  2.增加节点程序不需要重新启动 3.跨语言

**缺点：**性能下降 不支持跨库

jd**bc直接连**：shardingsphere（sharding-jdbc）、TDDL（淘宝部分开源）

**优点**：性能好 支持跨数据库的jdbc

**缺点**：

  1、增加了开发难度  2、不支持跨语言（java）

### 4.1 ShardingSphere

1.



# 错误记录

## 1.maven 操作问题

1.[ERROR] C:/response/work1.01/work/sjs_project_service/AuditAnalysis/src/main/java/com/icss/model/gather/DasExelog.java:[107,37] 编码UTF-8的不可映射字符

2.idea启动tomcat乱码

<https://blog.csdn.net/lk1822791193/article/details/88317140>

## 2.将本地包加入maven仓库



​       命令解释：mvn install:install-file -Dfile=”jar包的绝对路径” -Dpackaging=”文件打包方式” -DgroupId=groupid名 -DartifactId=artifactId名 -Dversion=jar版本 （artifactId名对应之后maven配置的依赖名）。

```
 例子 将sqljdbc42.jar 包加入： maven安装jar包命令：mvn install:install-file -Dfile=sqljdbc42.jar -Dpackaging=jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc4 -Dversion=3.0　　

<dependency>
   <groupId>com.microsoft.sqlserver</groupId>
   <artifactId>sqljdbc4</artifactId>
   <version>3.0</version>
</dependency>
```

## 3.spring boot项目打包后 java -jar 报无主清单属性

![1582436798002](images/1582436798002.png)

修改插件的打包方式可解决

```
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <executions>
                <execution>
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## 4.使用ConfigurationProperties 时报not found in classpath

![1582986539894](images/1582986539894.png)



<dependency>  

​          <groupId>org.springframework.boot</groupId>  

​          <artifactId>spring-boot-configuration-processor</artifactId>  

​          <optional>true</optional>  

</dependency>  

引用这个依赖，官方中对于spring-boot-configuration-processor是这么说明的：

通过使用spring-boot-configuration-processor jar， 你可以从被@ConfigurationProperties注解的节点轻松的产生自己的配置元数据文件。

自定义的元数据文件使用注解方式获取，需要先引入这个依赖。

##5.spring boot项目启动时不加载配置文件

1.可能原因:

![1590288692086](images/1590288692086.png)

可在下图中配置

![1590288783537](images/1590288783537.png)

## 5. Error:(1, 1) java: 非法字符: '\ufeff'”错误解决办法

### 原因

用Windows记事本打开并修改.java文件保存后重新编译运行项目出现“Error:(1, 1) java: 非法字符: '\ufeff'”错误，如下图所示：

![image-20200715100003126](images\image-20200715100003126.png)



原来这是因为Windows记事本在修改UTF-8文件时自作聪明地在文件开头添加BOM导致的，所以才会导致IDEA不能正确读取.java文件从而程序出错。

### 解决办法

在编辑器IDEA中将文件编码更改为UTF-16，再改回UTF-8即可，其实就相当于刷新了一下文件编码。

### idea新导入项目报gbk异常

见链接

https://blog.csdn.net/yinying293/article/details/128569778

### 配置nacos config报错

![image-20240307102913399](images\image-20240307102913399.png)**二、解决方法**

Spring Cloud 新版本默认将 Bootstrap 禁用，需要将 spring-cloud-starter-bootstrap 依赖引入到工程中：

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bootstrap</artifactId>
</dependency>
```



# 爬虫

## 1.http中的一些概念

User-Agent：user-Agent是Http协议中的一部分，属于头域的组成部分，User Agent也简称UA。用较为普通的一点来说，是一种向访问网站提供你所使用的浏览器类型、[操作系统](http://lib.csdn.net/base/operatingsystem)及版本、CPU 类型、浏览器渲染引擎、浏览器语言、浏览器插件等信息的标识。**UA字符串**在每次浏览器 HTTP 请求时发送到服务器！

浏览器UA 字串的**标准格式**为： 浏览器标识 (操作系统标识; 加密等级标识; 浏览器语言) 渲染引擎标识 版本信息

做移动APP开发时，有个网站在你手机端APP打开之后和直接在浏览器中打开之后看到的东西不一样，网页会根据UA判断你是app打开的还是直接浏览器打开的，如果没有找到事先商定的UA信息就认定这个链接是在浏览器打开的



# Netty

## 1.七层网络模型的作用和功能

**1 物理层**：主要定义物理设备的标准，如网线的接口类型，光纤的接口类型，各种传输介质的传输速率等。它的主要作用是传输比特流（就是由 1,0 转化为电流强弱来进行传输，到达目的地后再转化为1,0，也就是我们常说的数模转换和模数转换）这一层的数据叫做比特

**2.数据链路层**：定义了如何让格式化数据以进行传输，以及如何让控制对物理介质的访问，这一层通常还提供错误检测和纠正，以保证数据的可靠传输

**3.网络层：**在位于不同地理位置的网络中的两个主机系统之间提供链接和路径选择，Internet的发展使得从世界各个站点访问数据的用户数大大增加，而网络正式管理这种连接的层

**4.传输层** 定义了一些传输数据的的协议和端口（www端口80等） 如：TCP,udp 等

**5.会话层：**：通过传输层（端口号与接收端口）建立数据传输的通路，主要在你的系统之间发起会话或者接收会话请求（通过ip或者mac或者主机名）

**6.表示层**:确保一个系统的应用层所发的信息可以被另一个系统应用读取（如p程序与另一台计算机进行通讯，其中一台计算机使用扩展二十一进制交换码，而另一台则使用美国信息交换标准码来表示相同的字符，如有必要，表示层会通过使用一种通格式来实现多种数据格式之间的交换）

**7.应用层**：最靠近用户的osi层，这一层为应用程序提供网络服务

以下列表时一些协议的归类：

**应用层**

　　· DHCP(动态主机分配协议)
　　· DNS (域名解析）
　　· FTP（File Transfer Protocol）文件传输协议
　　· Gopher （英文原义：The Internet Gopher Protocol 中文释义：（RFC-1436）网际Gopher协议）
　　· HTTP （Hypertext Transfer Protocol）超文本传输协议
　　· IMAP4 (Internet Message Access Protocol 4) 即 Internet信息访问协议的第4版本
　　· IRC （Internet Relay Chat ）网络聊天协议
　　· NNTP （Network News Transport Protocol）RFC-977）网络新闻传输协议
　　· XMPP 可扩展消息处理现场协议
　　· POP3 (Post Office Protocol 3)即邮局协议的第3个版本
　　· SIP 信令控制协议
　　· SMTP （Simple Mail Transfer Protocol）即简单邮件传输协议
　　· SNMP (Simple Network Management Protocol,简单网络管理协议)
　　· SSH （Secure Shell）安全外壳协议

​      . SSL: 安全套接字层协议；

　　· TELNET 远程登录协议
　　· RPC （Remote Procedure Call Protocol）（RFC-1831）远程过程调用协议
　　· RTCP （RTP Control Protocol）RTP 控制协议
　　· RTSP （Real Time Streaming Protocol）实时流传输协议
　　· TLS （Transport Layer Security Protocol）传输层安全协议

　　· SDP( Session Description Protocol）会话描述协议
　　· SOAP （Simple Object Access Protocol）简单对象访问协议
　　· GTP 通用数据传输平台
　　· STUN （Simple Traversal of UDP over NATs，NAT 的UDP简单穿越）是一种网络协议
　　· NTP （Network Time Protocol）网络校时协议
**传输层**
　　·TCP（Transmission Control Protocol）传输控制协议
　　· UDP (User Datagram Protocol）用户数据报协议
　　· DCCP （Datagram Congestion Control Protocol）数据报拥塞控制协议
　　· SCTP（STREAM CONTROL TRANSMISSION PROTOCOL）流控制传输协议
　　· RTP(Real-time Transport Protocol或简写RTP）实时传送协议
　　· RSVP （Resource ReSer Vation Protocol）资源预留协议
　　· PPTP ( Point to Point Tunneling Protocol）点对点隧道协议
**网络层**
IP(IPv4 · IPv6) Internet Protocol（网络之间互连的协议）
ARP : Address Resolution Protocol即地址解析协议，实现通过IP地址得知其物理地址。
RARP :Reverse Address Resolution Protocol 反向地址转换协议允许局域网的物理机器从网关服务器的 ARP 表或者缓存上请求其 IP 地址。
ICMP :（Internet Control Message Protocol）Internet控制报文协议。它是TCP/IP协议族的一个子协议，用于在IP主机、路由器之间传递控制消息。
ICMPv6:
IGMP :Internet 组管理协议（IGMP）是因特网协议家族中的一个组播协议，用于IP 主机向任一个直接相邻的路由器报告他们的组成员情况。
RIP : 路由信息协议（RIP）是一种在网关与主机之间交换路由选择信息的标准。
OSPF : (Open Shortest Path First开放式最短路径优先).
BGP :（Border Gateway Protocol ）边界网关协议，用来连接Internet上独立系统的路由选择协议
IS-IS:（Intermediate System to Intermediate System Routing Protocol）中间系统到中间系统的路由选择协议.
IPsec:“Internet 协议安全性”是一种开放标准的框架结构，通过使用加密的安全服务以确保在 Internet 协议 (IP) 网络上进行保密而安全的通讯。
**数据链路层**
　　802.11 · 802.16 · Wi-Fi · WiMAX · ATM · DTM · 令牌环 · 以太网 · FDDI · 帧中继 · GPRS · EVDO · HSPA · HDLC · PPP · L2TP · ISDN
**物理层**
　　以太网物理层 · 调制解调器 · PLC · SONET/SDH · G.709 · 光导纤维 · 同轴电缆 · 双绞线

![img](images/20160731161720376)



## 2.七层网络模型与四层之间的关系

| OSI七层网络模型         | TCP/IP四层概念模型                   | 对应网络协议                            |
| ----------------------- | ------------------------------------ | --------------------------------------- |
| 应用层（Application）   | 应用层                               | HTTP、TFTP, FTP, NFS, WAIS、SMTP        |
| 表示层（Presentation）  | Telnet, Rlogin, SNMP, Gopher         |                                         |
| 会话层（Session）       | SMTP, DNS                            |                                         |
| 传输层（Transport）     | 传输层                               | TCP, UDP                                |
| 网络层（Network）       | 网络层                               | IP, ICMP, ARP, RARP, AKP, UUCP          |
| 数据链路层（Data Link） | 数据链路层                           | FDDI, Ethernet, Arpanet, PDN, SLIP, PPP |
| 物理层（Physical）      | IEEE 802.1A, IEEE 802.2到IEEE 802.11 |                                         |

## 3.应用程序获取数据的两个阶段

**数据准备**：应用程序无法直接操作硬件资源，需要操作系统资源时，先通知我们的内核，内核检查是够就绪资源，如果有则先把对应数据加载到内存空间。

**数据拷贝**：把数据资源从内核空间复制到应用数据的用户空间

**零拷贝**：cpu不执行拷贝数据从一个存储区域到到另一个存储区域的任务，这通常用于通过网络传输一个文件时以减少cpu周期和内存带宽

**详细见**：<https://blog.51cto.com/12182612/2424692?source=dra

https://www.cnblogs.com/myseries/p/11756335.html

## 4.io 模型

io中的一些概念：

阻塞：同时只能处理一个请求，处理完后才能处理下一个请求

非阻塞：同时可以处理多个请求

异步：发送一个请求后，不会同步等待结果的返回，一般都会通过事件回调的方式来

同步：发送一个请求后 ，会等待结果返回后才处理其他逻辑





​	io的模型指的是从内核态到用户态不同的拷贝过程，常用的模式有五种如下图所示

![1596409109936](./images/1596409109936.png)

### 阻塞型io

同时只能为一个客户端提供服务。

![1596409797593](images/1596409797593.png)

### 非阻塞型io

非阻塞型io指的用户态查询内核态是否存在数据的过程是非阻塞的，这个过程 通过轮询的方式获取，当查询到有数据时就返回，读取的过程是阻塞的。模型的缺点：需要一直去循环查看是否存在数据。

### i/o多路复用

多路复用是指一个线程检查多个文件描述符（相当与在内核中维护了一个socket列表），查询后如果存在就返回，不存在则阻塞待超时后返回

### 信号驱动

当一个线程检查内核中是否有数据时，会在内核态中标记后返回。当收到数据时回调，通知应用程序读取内核天中数据。应用程序接收到回调事件后主动进行读取。

### aio 异步io

当一个线程检查内核中是否有数据时，会在内核态中标记后返回。

当收到数据时回调.推送数据到应用程序。

数据流转的过程：

## 5.JAVA中的io模型

在java中有三种io模型，BIO,NIO,AIO

三种模型的区别：bio为阻塞io，nio为非阻塞io（信号驱动），bio为异步io

### BIO

BIO 是阻塞io，一个线程只能处理一个客户端请求，通常可以通过多线程的方式来实现同时处理多个客户端，具体实现见代码

### NIO

nio中基本就是对三大组件的使用channel,selector，buffer

**channel:**

相当于bio 中的serverSocket，用来建立连接，接受客户端连接

通道类似与流，但又有区别：

1.通道可以同时进行读写，而流只能读或者只能写

2.通道可以实现异步的读写数据

3.通道可以从缓存中读数据，也可以写

**selector：**

相当于 bio中的socket集合，通过Selector.open可获取，

通过selector可以获取所有可连接事件。selector可以监听多个channel实现一个线程监听多个

**buffer(缓冲区):**

用于当数据传输的载体,可读写数据的内存块可以理解为是一个容器对象，该对象提供了一组方法，可以操作内存块。

java 的nio 是io多路复用模型，

服务端一个线程可以处理多个客户端为了更高效率处理请求，nio有多线程模型，

nio面向缓存区，或者面向块进行编程，

**三者之间的关系**

1.每个channel对应一个buffer

2.selector对应一个线程，一个线程对应多个channel连接

3.切换到哪个channel是由时间决定的，event是一个很重要的概念

5.selector会根据不同的事件，在各个通道上切换

6.Buffer就是一个内存块，底层是由一个数组构成的

7.数据的读取，写入都是通过buffer来实现的，buffer是双向的可读可写，通过flip可以时间，和bio不同

8.channel是双向的，可以返回底层操作系统的情况，比如linux，底层的操作系统通道就湿双向的

#### 反应堆模型

反应堆模型有点像观察者模式，都是根据某一种

### AIO



### netty 中的概念：

以站点为基准，向站点流出叫出站，向站点流入叫入站。在出站中可以对数据进行编码，入站中可以对数据进行解码

# Maven

<https://blog.csdn.net/weixin_38569499/article/details/91456988>

## 1.setting.xml文件节点说明

```
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
               
  <localRepository>D:\\develop\\mavenRepository</localRepository>
  <interactiveMode>true</interactiveMode>
  <offline>false</offline>
  <pluginGroups>
 
  </pluginGroups>
 
  <proxies>
    <proxy>
      <id>optional</id>
      <active>true</active>
      <protocol>http</protocol>
      <username>proxyuser</username>
      <password>proxypass</password>
      <host>proxy.host.net</host>
      <port>80</port>
      <nonProxyHosts>local.net|some.host.com</nonProxyHosts>
    </proxy>
  </proxies>
 
  <servers>
    <server>
      <id>deploymentRepo</id>
      <username>repouser</username>
      <password>repopwd</password>
    </server>
  </servers>
 
  <mirrors>
    <mirror>
      <id>mirrorId</id>
      <mirrorOf>repositoryId</mirrorOf>
      <name>Human Readable Name for this Mirror.</name>
      <url>http://my.repository.com/repo/path</url>
    </mirror>
  </mirrors>
 
  <profiles>
    <profile>
      <id>jdk-1.5</id>
      <activation>
        <jdk>1.5</jdk>
      </activation>
      <repositories>
        <repository>
          <id>jdk15</id>
          <name>jdk1.5</name>
          <url>http://www.myhost.com/maven/jdk15</url>
          <layout>default</layout>
          <snapshotPolicy>always</snapshotPolicy>
        </repository>
      </repositories>
    </profile>
  </profiles>
  <activeProfiles>
    <activeProfile>jdk-1.5</activeProfile>
  </activeProfiles>
</settings>
```

1. localRepository：用来在本地存储信息的本地仓库，默认.m2/repository目录下

2. interactiveMode:表示交互模式，默认为true

3. offine 表示是否离线，默认shifalse 这个属性表示在maven进行项目编译的时是否允许maven进行联网下载需要的信息

4. pluginGroups：在pluginGroups元素下面可以定义一系列的pluginGroup元素。表示当通过plugin的前缀来解析plugin的时候到哪里寻找。pluginGroup元素指定的是plugin的groupId。默认情况下，Maven会自动把org.apache.maven.plugins和org.codehaus.mojo添加到pluginGroups下。

5. proxies：：其下面可以定义一系列的proxy子元素，表示Maven在进行联网时需要使用到的代理。当设置了多个代理的时候第一个标记active为true的代理将会被使用。下面是一个使用代理的例子：

   ```
   <proxies>
     <proxy>
         <id>xxx</id>
         <active>true</active>
         <protocol>http</protocol>
         <username>用户名</username>
         <password>密码</password>
         <host>代理服务器地址</host>
         <port>代理服务器的端口</port>
         <nonProxyHosts>不使用代理的主机</nonProxyHosts>
     </proxy>
   </proxies>
   ```

   6. servers：其下面可以定义一系列server子元素，表示当需要连接到一个远程服务器的时候需要使用到的验证方式。这主要有username/password和privateKey/passphrase这两种方式。以下是一个使用servers的示例：

      ``` 
        <servers>
          <server>
            <id>id</id>
            <username>用户名</username>
            <password>密码</password>
          </server>
        </servers>

      ```

   7. mirrors ：用于定义一系列远程镜像。我们可以在pom中定义一个下载工件时所需的远程仓库。当有时候这个远程仓库会比较忙，所以这个时候人们就想着给它创建镜像以缓解远程仓库的压力，也就是说会把对远程仓库的请求转换到对其镜像地址的请求。每个远程仓库都会有一个id，这样我们就可以创建自己的mirror来关联到该仓库，那么以后需要从远程仓库下载工件的时候Maven就可以从我们定义好的mirror站点来下载，这可以很好的缓解我们远程仓库的压力。在我们定义的mirror中每个远程仓库都只能有一个mirror与它关联，也就是说你不能同时配置多个mirror的mirrorOf指向同一个repositoryId。

      ```
      <mirrors>
          <mirror>
            <id>mirrorId</id>
            <mirrorOf>repositoryId</mirrorOf>
            <name>定义一个容易看懂的名称 </name>
            <url>http://my.repository.com/repo/path</url>
          </mirror>
      </mirrors>
      ```

      ## 2.pom文件 解析

      <https://blog.csdn.net/weixin_38569499/article/details/91456988>

      ### 2.1 根元素和必要配置

```

<project xmlns = "http://maven.apache.org/POM/4.0.0"
    xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation = "http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/xsd/maven-4.0.0.xsd">
 
    <!-- 模型版本 -->
    <modelVersion>4.0.0</modelVersion>
    <!-- 公司或者组织的唯一标志，也是打包成jar包路径的依据 -->
    <!-- 例如com.companyname.project-group，maven打包jar包的路径：/com/companyname/project-group -->
    <groupId>com.companyname.project-group</groupId>
 
    <!-- 项目的唯一ID，一个groupId下面可能多个项目，就是靠artifactId来区分的 -->
    <artifactId>project</artifactId>
 
    <!-- 项目当前版本，格式为:主版本.次版本.增量版本-限定版本号 -->
    <version>1.0</version>
 
    <!--项目产生的构件类型，包括jar、war、ear、pom等 -->
    <packaging>jar</packaging>
</project>

```

project是pom文件的根元素，project下有modeVersion，groupid，artifactid，version，packaging等重要元素。其中groupid，artifactid，version三个元素又来定位一个项目的坐标，在 maven仓库中用来唯一定位

project:整个pom配置文件的根元素，所有配置都在project元素中

modelVersion：制动了当前pom模型的版本，对于maven2及maven3来说只能是4.0.0

groupid：这是组织的标识，它在项目中时唯一的**（如果是子项目怎么定义）**

artifactid：只是项目标识，通常是工程的名称，它在一个项目组（groupid）下是唯一的

version：这是项目的版本号，用来区分同一个artifact的不同版本

packaging：这是项目产生的构建类型，即项目通过maven打包的输出文件的后缀名包括（jar。pom，war，rar）**（怎么使用**）

### 2.2 父项目和parent元素

```
 <!--父项目的坐标，坐标包括group ID，artifact ID和version。 -->
    <!--如果项目中没有规定某个元素的值，那么父项目中的对应值即为项目的默认值 -->
    <parent>
 
        <!--被继承的父项目的构件标识符 -->
        <artifactId>com.companyname.project-group</artifactId>
        <!--被继承的父项目的全球唯一标识符 -->
        <groupId>base-project</groupId>
        <!--被继承的父项目的版本 -->
        <version>1.0.1-RELEASE</version>
 
        <!-- 父项目的pom.xml文件的相对路径,默认值是../pom.xml。 -->
        <!-- 寻找父项目的pom：构建当前项目的地方--)relativePath指定的位置--)本地仓库--)远程仓库 -->
        <relativePath>../pom.xml</relativePath>
    </parent>

```

所有的pom都继承自一个父pom（super pom）。父pom包含了一些可以被继承的默认设置，如果项目的pom中没有设置这些元素，就会使用父ppom中设置。例如：super pom中配置了默认仓库，哪怕项目中米有配置，也会从这个默认的仓库中去下载。

pom：用于指定父项目；

groupid parent的子元素，父项目的groupid，用于定位父项目

artifatid：parent的子元素，父项目的artifactid，用于定位父项目；

version：parent的子元素，父项目的version，用于定位父项目

relativePath:parent的子元素，用于s定位父项目pom的位置

### 3.项目依赖相关信息

pom文件中通过depencyManagement来声明依赖，通过dependencies元素来管理依赖。dependencyManagement下的子元素只有一个直接的子元素dependencice，其配置和dependencies子元素是完全一致的；而dependencies下只有一类直接的子元素：dependency。一个dependency子元素表示一个依赖项目。

```
  <!--该元素描述了项目相关的所有依赖。 这些依赖自动从项目定义的仓库中下载 -->
    <dependencies>
        <dependency>
            <!------------------- 依赖坐标 ------------------->
            <!--依赖项目的坐标三元素：groupId + artifactId + version -->
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-artifact</artifactId>
            <version>3.8.1</version>
 
            <!------------------- 依赖类型 ------------------->
            <!-- 依赖类型，默认是jar。通常表示依赖文件的扩展名，但有例外。一个类型可以被映射成另外一个扩展名或分类器 -->
            <!-- 类型经常和使用的打包方式对应，尽管这也有例外，一些类型的例子：jar，war，ejb-client和test-jar -->
            <!-- 如果设置extensions为true，就可以在plugin里定义新的类型 -->
            <type>jar</type>
            <!-- 依赖的分类器。分类器可以区分属于同一个POM，但不同构建方式的构件。分类器名被附加到文件名的版本号后面 -->
            <!-- 如果想将项目构建成两个单独的JAR，分别使用Java 4和6编译器，就可以使用分类器来生成两个单独的JAR构件 -->
            <classifier></classifier>
 
            <!------------------- 依赖传递 ------------------->
            <!--依赖排除，即告诉maven只依赖指定的项目，不依赖该项目的这些依赖。此元素主要用于解决版本冲突问题 -->
            <exclusions>
                <exclusion>
                    <artifactId>spring-core</artifactId>
                    <groupId>org.springframework</groupId>
                </exclusion>
            </exclusions>
            <!-- 可选依赖，用于阻断依赖的传递性。如果在项目B中把C依赖声明为可选，那么依赖B的项目中无法使用C依赖 -->
            <optional>true</optional>
            
            <!------------------- 依赖范围 ------------------->
            <!--依赖范围。在项目发布过程中，帮助决定哪些构件被包括进来
                - compile：默认范围，用于编译;  - provided：类似于编译，但支持jdk或者容器提供，类似于classpath 
                - runtime: 在执行时需要使用;    - systemPath: 仅用于范围为system。提供相应的路径 
                - test: 用于test任务时使用;    - system: 需要外在提供相应的元素。通过systemPath来取得 
                - optional: 当项目自身被依赖时，标注依赖是否传递。用于连续依赖时使用 -->
            <scope>test</scope>
            <!-- 该元素为依赖规定了文件系统上的路径。仅供scope设置system时使用。但是不推荐使用这个元素 -->
            <!-- 不推荐使用绝对路径，如果必须要用，推荐使用属性匹配绝对路径，例如${java.home} -->
            <systemPath></systemPath>
        </dependency>
    </dependencies>
 
    <!-- 继承自该项目的所有子项目的默认依赖信息，这部分的依赖信息不会被立即解析。 -->
    <!-- 当子项目声明一个依赖，如果group ID和artifact ID以外的一些信息没有描述，则使用这里的依赖信息 -->
    <dependencyManagement>
        <dependencies>
            <!--参见dependencies/dependency元素 -->
            <dependency>
                ......
            </dependency>
        </dependencies>
    </dependencyManagement>
```

dependency元素详解

**1、依赖坐标**
    依然是通过groupId + artifactId + version来在仓库中定位一个项目：

groupId：parent的子元素，父项目的groupId，用于定位父项目；
artifactId：parent的子元素，父项目的artifactId，用于定位父项目；
version：parent的子元素，父项目的version，用于定位父项目；
**2、依赖类型**
    这个分类主要包括两个元素，分别是依赖类型和依赖的分类器。同一个项目，即使打包成同一种类型，也可以有多个文件同时存在，因为它们的分类器可能是不同的。

type：依赖类型，默认是jar。通常表示依赖文件的扩展名，但也有例外。一个类型可以被映射成另外一个扩展名或分类器。类型经常和使用的打包方式对应，尽管这也有例外，一些类型的例子：jar，war，ejb-client和test-jar。如果设置extensions为true，就可以在plugin里定义新的类型。
classifier：依赖的分类器。分类器可以区分属于同一个POM，但不同构建方式的构件。分类器名被附加到文件名的版本号后面，如果想将项目构建成两个单独的JAR，分别使用Java 4和6编译器，就可以使用分类器来生成两个单独的JAR构件
**3、依赖传递**
    依赖传递相关的子元素主要有两个，用于依赖排除的exclusions和设置依赖是否可选的optional。

exclusions：排除该项目中的一些依赖，即本项目A依赖该dependency指示的项目B，但是不依赖项目B中依赖的这些依赖；
optional：可选依赖，用于阻断依赖的传递性，即本项目不会依赖父项目中optional设置为true的依赖。
**4、依赖范围**
    还有一些其他元素：

scope：依赖范围。在项目发布过程中，帮助决定哪些构件被包括进来
                - compile：默认范围，用于编译;   - provided：类似于编译，但支持jdk或者容器提供，类似于classpath 

                - runtime: 在执行时需要使用;         - systemPath: 仅用于范围为system，提供相应的路径 
    
                - test: 用于test任务时使用;             - system: 需要外在提供相应的元素，通过systemPath来取得 
    
                - optional: 当项目自身被依赖时，标注依赖是否传递。用于连续依赖时使用
  systemPath：该元素为依赖规定了文件系统上的绝对路径。仅在scope设置成system时才会使用。不推荐使用这个元素。不推荐使用绝对路径，如果必须要用，推荐使用属性匹配绝对路径，例如${java.home}

  ### 4.远程仓库列表

  ​	远程仓库列表的配置，包括依赖和扩展的远程仓库配置，以及插件的远程仓库配置。在本地仓库找不到的情况下，maven下载依赖，扩展和插件就可以从远程仓库中下载。

  release和snapshot两者的区别。release是稳定版本，一经发布不在修改，想要发布修改后的项目，只能升级项目版本。snapshot是不稳定的，一个snapshot的版本是可以不算的改变的。

```
 <!--发现依赖和扩展的远程仓库列表。 -->
    <repositories>
        <!--包含需要连接到远程仓库的信息 -->
        <repository>
            <!--如何处理远程仓库里发布版本的下载 -->
            <releases>
                <!--值为true或者false，表示该仓库是否为下载某种类型构件（发布版，快照版）开启。 -->
                <enabled />
                <!--该元素指定更新发生的频率。Maven会比较本地POM和远程POM的时间戳 -->
                <!--选项：always，daily（默认），interval：X（X单位为分钟），或者never。 -->
                <updatePolicy />
                <!--当Maven验证构件校验文件失败时该怎么做。选项：ignore，fail，或者warn -->
                <checksumPolicy />
            </releases>
            <!-- 如何处理远程仓库里快照版本的下载 -->
            <!-- 有了releases和snapshots这两组配置，就可以在每个单独的仓库中，为每种类型的构件采取不同的策略 -->
            <snapshots>
                <enabled />
                <updatePolicy />
                <checksumPolicy />
            </snapshots>
 
            <!--远程仓库唯一标识符。可以用来匹配在settings.xml文件里配置的远程仓库 -->
            <id>nanxs-repository-proxy</id>
            <!--远程仓库名称 -->
            <name>nanxs-repository-proxy</name>
            <!--远程仓库URL，按protocol://hostname/path形式 -->
            <url>http://192.168.1.169:9999/repository/</url>
            <!-- 用于定位和排序构件的仓库布局类型。可以是default或者legacy -->
            <layout>default</layout>
        </repository>
    </repositories>
    
    <!--发现插件的远程仓库列表，这些插件用于构建和报表 -->
    <pluginRepositories>
        <!--包含需要连接到远程插件仓库的信息。参见repositories/repository元素 -->
        <pluginRepository>
            ......
        </pluginRepository>
    </pluginRepositories>

```

### 4.如何继承父项目pom

父项目中modules节点，表明一下模块为子项目

```
<modelVersion>4.0.0</modelVersion>  
<groupId>demo</groupId>              *
<artifactId>Parent</artifactId>         *
<version>1.0.2</version>            *
<packaging>pom</packaging>  
<name>demo</name>
<modules>
   <module>childA</module>
   <module>childB</module>
</modules>
```

父项目写好后如果子项目中想要使用父项目中的依赖，需要加入如下代码块

```
<parent>
   <groupId>demo</groupId>
   <artifactId>parent</artifactId>
   <version>1.0.2</version>
   <relativePath>../pom.xml</relativePath>  <!--本例中此处是可选的-->
</parent
```

# 分布式概念

## 1.分布式锁

### 1.应用场景

抢卷，解决超卖，解决幂等性

### 2.分布式锁的特点

1. 互斥性

2. 锁超时

3. 支持阻塞和非阻塞

4. 可重入

5. 高可用

   分布式锁需要满足以上5个特点业界常用的解决方案有使用mysql，redis，zookeeper。其中redis使用量最大以下使用redis来分析锁的实现

### **3.redis实现**

redis使用setnx来实现 。 setnx如果不存在添加返回1，如果存在操作返回0 。

redis实现五个锁五个特点的解决方案

1. **互斥性** (使用setnx来判断该区域是否已被锁定)

2. **锁超时** （为锁添加超时时间，添加时间后需要实现锁续命，否则可能会出现被锁区域还没有执行完，锁已经失效）

3. **支持阻塞和非阻塞**（

    阻塞：获取锁时改为自旋的方式

4. **可重入** 为当前线程添加一个计数器，如果当前线程计数器不为0可重复进入，需注意释放锁时，也需要按顺序释放。（可使用ThreadLocal）

5. **高可用**  使用redis集群

#### 3.1 具体实现



## 2 配置中心

## 3.分布式事务

**分布式事务产生原因**：在一个服务中，需要对多个数据库进行操作或者分布式服务中有链式调用在以上的操作中需要保持数据的原子性。

**解决方法**：

1. xa协议 实现方式有JTA

		   2. tcc 补偿性事务（实际上也算是对xa协议思想的一种延伸）

### 1.XA协议

常用来解决一个服务中调用多个数据库的事务性问题。XA协议的基本思想是两阶段提交和事务管理器协调：

1. 第一阶段预提交，该阶段会预提交，事务管理器确定没问题后开启第二阶段（问题：是客户端向数据库进行请求锁定资源后，向事务管理器提交结果吗）
2. 第二阶段：commit

该方案有个问题如果在commit出现问题后，可能会导致数据不一致。可以在本地进行记录，事后补偿来缓解矛盾，该方案效率较低，不适合并发量高的项目。

#### 1.1 java实现

使用Atomikos来分析实现方式。Atomikos是实现了JTA(Java Transaction API,java 根据xa协议实现的java规范) 规范。



### 2.TCC

https://www.cnblogs.com/jajian/p/10014145.html （文章讲的很好，对tcc，及对大部分解决方案做了分析，复习

查阅）

tcc补偿性事务：服务发起者来控制执行，如果发起者发现有问题，会通过具体服务提供者提供的cancel操作来取消已完成动作。

tcc的步骤为try:获取资源并锁定 commit 提交 如果失败 cancel

基本思路如下：以电商扣款 成功后加积分为例子。这个流程涉及三个微服务，订单系统，支付系统，积分系统。

![1583050459833](images/1583050459833.png)

try阶段时 先调用pay 和add（不直接在原数据上进行操作，新加一个表，先把中间数据存在该表）接口当接口都调用成功后，通过定时任务 或者中间件将中间数据进行持久化 。该方案采用 数据最终一致性，中间可能会有部分延时

tcc解决方案有：tcc-transaction 

源码git地址：https://github.com/changmingxie/tcc-transaction 有详细使用方案

## 4.分布式session 解决方案

### 1.tomcat session复制

### 2.负载时iphash

### 3.redis

### 4.使用jwt

### 5.cas

## 5.分布式id

## 6.分布式分库分表

常用的分库分表方案有region和hash两种方案各有优缺点；

**hash**:

​	**优点:**可以做到均衡的将数据分布到不同的库表中，可以解决热点数据问题

​	**缺点**:不容易扩容，扩容时有hash变化的问题

**region:**

​       **优点:**可随意扩容

​        **缺点：**不能解决热点数据的问题

一般在实际开发中会将两种方案进行结合，先根据id或者有序的标记对数据进行分组，在分组内进行hash这样可以解决以上问题。

# git

## 1.常用命令

1.将文件添加到暂存区

   git add *.txt test.java

   git add . 添加所有修改过的文件到暂存区，不包含忽略文件

2.比较工作目录和暂存区之间的差异，也就是修改后没有add的内容

git diff

3.查看repository

git status

4 忽略某些文件

创建名字为.gitignore的文件，将需要忽略的文件添加进去

5.提交更新

git commit -m '描述信息'

6.删除参控中的某个文件

​    6.1 直接删除

  		使用git rm xxxx.txt 之后commit即可删除

   6.2 强制删除add到暂存区的文件，使用参数-f

 		git rm -f xxx.txt

 7.将某些文件从追踪文件列表中移除，需要递归的话使用\

​        git rm --cached  xxx.txt

​	git rm --cached  \*.class 将当前目录及其子文件目录中的class结尾的文件从跟踪列表中移除

​        git rm --cached -r 文件名递归移除

8.重命名文件

 git mv delete.txt add.txt

9查看log

git log –p -2 
　　-p 选项展开显示每次提交的内容差异 
　　-<n> 仅显示最近的若干条提交
　　--stat 仅显示简要的增改行数统计
　　--pretty 自己制定日志显示格式
　　=oneline 将每个提交放在一行显示
　　= format
　　--graph开头多出一些 ASCII 字符串表示的简单图形，形象地展示了每个提交所在的分支及其分化衍合情况

10.提交完了才发现漏掉几个文件没有加，或者提交信息写错了

git commit -amend

11.去掉已暂存的文件

git reset HEAD xxx.txt

14版本回退

git reset --hard id

## 2.远程仓库相关

1.从指定远程库下载项目

git clone URL

 查看关联远程仓库信息

git remote 或者 git remote -v

2.添加一个新的远程仓库

git remote add origin git@server-name:path/repo-name.git

3.推送到远程仓库

第一次推送用这个命令

git push -u origin master

以后推送可用以下命令

git push origin master

# java基础

## 1. jdk1.8新特性

  1.8新增了Lambda表达式 和Stream操作方法

### 1.1 Lambda

lambda替换匿名内部类，使用() -> {} 替换了匿名内部类的实现 当接口中只有一个方法时用@FunctionalInterface修饰接口后 可以用()->{}来替换 内部类的实现。java内置了多个接口

| 消费型接口 | Consumer<T>     | void  accept(T t);  |
| ---------- | --------------- | ------------------- |
| 供给型接口 | Supplier<T>     | T  get();           |
| 函数型接口 | Function<T,  R> | R  app1y(T t);      |
| 断言型接口 | Predicate<T>    | boolean  test(T t); |

1.内部没有返回值

```java
   /**
     * 内部没有返回值
     */
    @Test
    public void test1(){
        Thread thread=new Thread(()->{
            System.out.println("测试");
        });
        thread.start();
    }
```

2.消费型接口例子

```java
/**
 * 消费型接口
 */
@Test
public void test1(){
    Consumer<String> customer = x-> {
        System.out.println(x + "6666");
    };
    customer.accept("88");

}
```

3.供给型接口例子

```java
/**
 * 供给型接口
 *
 */
public void test5(){
    Supplier<String> supplier=String::new;
    System.out.println(supplier.get());
}
```

4.断言型接口例子

```java
/**
 * 断言型接口
 */
public void test7(){
    BiPredicate<String,String> bp=(x,y)->x.equals(y);
    System.out.println(bp.test("a","b"));
}
```

```
public void test(){
    /*****************************方法的引用******************************/
    //类::静态方法名称
    Comparator<Integer> cam1=(x,y)->x.compareTo(y);
    cam1.compare(1,2);
    // 类：：实例方法名
    BiPredicate<String,String> bp=(x,y)->x.equals(y);
    System.out.println(bp.test("a","b"));
    BiPredicate<String,String> bp1=String::equals;
    bp1.test("a","b");
    //对象：：实例方法名
    Consumer<String> con1=x-> System.out.println(x);
    con1.accept("sdfsdf");

    Student student=new Student(1,"b");
    Supplier<String> supplier=() ->student.getName();
    System.out.println(supplier.get());
    Supplier<String> supplier1= student::getName;
    System.out.println(supplier1.get());

    /*************** 构造器的引用 ****************/
    Supplier <Student> supplier2=()->new Student();
    Supplier<Student> supplier3=Student::new;
    Student student1=supplier3.get();
    //一个参数
    Function<String,Student > supplier4=Student::new;
    Student student2=supplier3.get();
    //两个参数
    BiFunction<Integer,String,Student> bFun1=Student::new;
    Student student3=bFun1.apply( 18,"xiaohong");
}
```

### 1.2 Stream流

以下是一篇特别好的博客 https://blog.csdn.net/y_k_y/article/details/84633001

```java
package test;

import org.junit.Test;
import test.entity.Student;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stream流问题
 */
public class StreamTest {

    /**
     * 创建流
     */
    public void test1() {
        String[] dd = {"a", "b", "c"};
        Arrays.stream(dd).forEach(System.out::print);// abc
        System.out.println();
        Stream.of(dd).forEach(System.out::print);// abc
        System.out.println();
        Arrays.asList(dd).stream().forEach(System.out::print);// abc
        System.out.println();
        Stream.iterate(0, x -> x + 1).limit(10).forEach(System.out::print);// 0123456789
        System.out.println();
        Stream.generate(() -> "x").limit(10).forEach(System.out::print);// xxxxxxxx
    }

    /**
     * map操作，filter操作
     */
    @Test
    public void test2() {
        String[] t = {"1", "2", "3", "4"};
        Stream<String> stream = Arrays.stream(t);
        //参数为 Predicate接口，用来做断言，设置条件
        stream.filter(s -> s.equals("2")).forEach(System.out::println);
        //Map=======================================================================
        Function<String, Student> function = Student::new;
        List<Student> list = new ArrayList<>(10);
        for (int i = 0; i < 5; i++) {
            list.add(function.apply("i=>" + i));
        }
        //传入参数为Function接口，1个入参，有一个返回值，设置可返回参数
        list.stream().map(x -> x.getName()).forEach(System.out::println);
    }

    /**
     * flatMap操作
     * 与map参数一致 具体的作用是把两个流，变成一个流返回
     */
    @Test
    public void test3() {
        String[] strs = {"aaa", "bbb", "ccc"};
        Arrays.stream(strs).map(str -> str.split("")).forEach(System.out::println);// Ljava.lang.String;@53d8d10a
        System.out.println("=================================================================");
        Arrays.stream(strs).map(str -> str.split("")).flatMap(Arrays::stream).forEach(System.out::println);// aaabbbccc
        System.out.println("=================================================================");
        Arrays.stream(strs).map(str -> str.split("")).flatMap(str -> Arrays.stream(str)).forEach(System.out::println);// aaabbbccc
    }

    /**
     * 常用方法
     * <p>
     * //去重复
     * Stream<T> distinct();
     * //排序
     * Stream<T> sorted();
     * //根据属性排序
     * Stream<T> sorted(Comparator<? super T> comparator);
     * //对对象的进行操作
     * Stream<T> peek(Consumer<? super T> action);
     * //截断--取先maxSize个对象
     * Stream<T> limit(long maxSize);
     * //截断--忽略前N个对象
     * Stream<T> skip(long n);
     */
    @Test
    public void test4() {
        List<Emp> list = new ArrayList<>();
        list.add(new Emp("xiaoHong1", 20, 1000.0));
        list.add(new Emp("xiaoHong2", 25, 2000.0));
        list.add(new Emp("xiaoHong3", 30, 3000.0));
        list.add(new Emp("xiaoHong4", 35, 4000.0));
        list.add(new Emp("xiaoHong5", 38, 5000.0));
        list.add(new Emp("xiaoHong6", 45, 9000.0));
        list.add(new Emp("xiaoHong7", 55, 10000.0));
        list.add(new Emp("xiaoHong8", 42, 15000.0));
        // 对数组流，先过滤重复，在排序，再控制台输出 1，2，3
        Arrays.asList(3, 1, 2, 1).stream().distinct().sorted().forEach(str -> {
            System.out.println(str);
        });
        // 对list里的emp对象，取出薪水，并对薪水进行排序，然后输出薪水的内容，map操作，改变了Strenm的泛型对象
        System.out.println("=====================================对list里的emp对象，取出薪水，并对薪水进行排序，然后输出薪水的内容，map操作，改变了Strenm的泛型对象=================================");

        list.stream().map(e -> e.salary).sorted().forEach(System.out::println);
        // 根据emp的属性name，进行排序
        System.out.println("=====================================根据emp的属性name，进行排序=================================");
        list.stream().sorted(Comparator.comparing(x -> x.name)).forEach(x -> System.out.println(x.name));
        //给年纪大于30岁的人，薪水提升1.5倍，并输出结果
        System.out.println("=====================================给年纪大于30岁的人，薪水提升1.5倍，并输出结果=================================");
        // 给年纪大于30岁的人，薪水提升1.5倍，并输出结果
        Stream<Emp> stream = list.stream().filter(emp ->
                emp.getAge() > 30
        //对对象的进行操作peek 为消费型参数 ,不会改变返回类型，只会对传入值进行修改
        ).peek(emp -> {
            emp.setSalary(emp.getSalary() * 1.5);
        });
        // Stream<Emp> stream1 =  list.stream().filter(e->e.age>30).map(emp -> emp.name).forEach(System.out::println);
        println(stream);

        System.out.println("数字从1开始迭代（无限流），下一个数字，是上个数字+1，忽略前5个 ，" +
                "并且只取10个数字,原本1-无限，忽略前5个，就是1-5数字，不要，从6开始，截取10个，就是6-15");
        // 数字从1开始迭代（无限流），下一个数字，是上个数字+1，忽略前5个 ，并且只取10个数字
        // 原本1-无限，忽略前5个，就是1-5数字，不要，从6开始，截取10个，就是6-15
        //截断--忽略前N个对象
        Stream.iterate(1, x -> ++x).skip(5).limit(10).forEach(System.out::println);
        //对用户进行分组统计并且，以分组条件返回
        // list.stream().
    }

    /**
     * toArray操作  ，将集合转换成数组
     */
    @Test
    public void test5(){

        List<String> strs = Arrays.asList("a", "b", "c");
        String[] dd = strs.stream().toArray(str -> new String[strs.size()]);
        String[] dd1 = strs.stream().toArray(String[]::new);
        Object[] obj = strs.stream().toArray();
        String[] dd2 = strs.toArray(new String[strs.size()]);
        Object[] obj1 = strs.toArray();
    }

    /**
     * stream的 count，anyMatch，allMatch，noneMatch 操作
     */
    @Test
    public void  test6(){
        List<String> strs = Arrays.asList("a", "a", "a", "a", "b");
        boolean aa = strs.stream().anyMatch(str -> str.equals("a"));
        boolean bb = strs.stream().allMatch(str -> str.equals("a"));
        boolean cc = strs.stream().noneMatch(str -> str.equals("a"));
        long count = strs.stream().filter(str -> str.equals("a")).count();
        System.out.println(aa);// TRUE
        System.out.println(bb);// FALSE
        System.out.println(cc);// FALSE
        System.out.println(count);// 4
    }

    /**
     * reduce操作
     * reduce是一种归约操作，将流归约成一个值的操作叫做归约操作
     *
     *
     * 案例需要学习
     */
    @Test
    public void test7(){
        List<Integer> numbers = Stream.iterate(1, x -> x + 1).limit(10).collect(Collectors.toList());
        Integer aa = 0;
        for (Integer i : numbers) {
            aa += i;
        }
        //第一个表示初始值，第二个值表示计算规则
        Integer dd = numbers.stream().reduce(0, (a, b) -> a + b, (a, b) -> a - b);

        Optional<Integer> dd1 = numbers.stream().reduce((a, b) -> a + b);
        System.out.println(aa);
        System.out.println(dd);
        System.out.println(dd1.get());
    }




    public static void println(Stream<Emp> stream) {
        stream.forEach(emp -> {
            System.out.println(String.format("名字：%s，年纪：%s，薪水：%s", emp.getName(), emp.getAge(), emp.getSalary()));
        });
    }

    class Emp {
        private String name;

        private Integer age;

        private Double salary;

        public Emp(String name, Integer age, Double salary) {
            super();
            this.name = name;
            this.age = age;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Double getSalary() {
            return salary;
        }

        public void setSalary(Double salary) {
            this.salary = salary;
        }

    }

}
```

```java
package test;

import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamTest1 {
    public static List<Emp> list = new ArrayList<>();
    static {
        list.add(new Emp("上海", "小名", 17));
        list.add(new Emp("北京", "小红", 18));
        list.add(new Emp("深圳", "小蓝", 19));
        list.add(new Emp("广州", "小灰", 20));
        list.add(new Emp("杭州", "小黄", 21));
        list.add(new Emp("贵阳", "小白", 22));
    }

    /**
     * collect操作
     */
    @Test
    public void test8(){
        //转list
        System.out.println("==============================转list=======================================================");
        List<String> names=list.stream().map(emp -> emp.getName()).collect(Collectors.toList());
        // 转set
        System.out.println("==============================转set=======================================================");
        Set<String> address=list.stream().map(emp -> emp.getAddress()).collect(Collectors.toSet());
        // 转map，需要指定key和value，Function.identity()表示当前的Emp对象本身
        System.out.println("=========转map，需要指定key和value，Function.identity()表示当前的Emp对象本身=======================================================");
        Map<String,Emp> map=list.stream().collect(Collectors.toMap(Emp::getName, Function.identity()));
        System.out.println("=========根据名称进行分组，并根据分组条件转成map=======================================================");
        //根据名称进行分组，并根据分组条件转成map
        Map<String, List<String>> result =
                list.stream().collect(Collectors.groupingBy(Emp::getName,
                        Collectors.mapping(Emp::getName, Collectors.toList())));
        System.out.println("=========转map，需要指定key和value，转换成Map<List>=======================================================");
        Map<String, List<Emp>> result1 =
                list.stream().collect(Collectors.groupingBy(Emp::getName));

        // 计算元素中的个数
        System.out.println("========= 计算元素中的个数=======================================================");
        Long count=list.stream().collect(Collectors.counting());
        /// 数据求和 summingInt summingLong，summingDouble
        System.out.println("========= 数据求和 summingInt summingLong，summingDouble=======================================================");
        Integer sumAges = list.stream().collect(Collectors.summingInt(Emp::getAge));
        // 平均值 averagingInt,averagingDouble,averagingLong
        Double aveAges = list.stream().collect(Collectors.averagingInt(Emp::getAge));
        // 综合处理的，求最大值，最小值，平均值，求和操作
        // summarizingInt，summarizingLong,summarizingDouble
        IntSummaryStatistics intSummary = list.stream().collect(Collectors.summarizingInt(Emp::getAge));
        System.out.println(intSummary.getAverage());// 19.5
        System.out.println(intSummary.getMax());// 22
        System.out.println(intSummary.getMin());// 17
        System.out.println(intSummary.getSum());// 117
        // 连接字符串,当然也可以使用重载的方法，加上一些前缀，后缀和中间分隔符
        String strEmp = list.stream().map(emp -> emp.getName()).collect(Collectors.joining());
        String strEmp1 = list.stream().map(emp -> emp.getName()).collect(Collectors.joining("-中间的分隔符-"));
        String strEmp2 = list.stream().map(emp -> emp.getName()).collect(Collectors.joining("-中间的分隔符-", "前缀*", "&后缀"));
        System.out.println(strEmp);// 小名小红小蓝小灰小黄小白
// 小名-中间的分隔符-小红-中间的分隔符-小蓝-中间的分隔符-小灰-中间的分隔符-小黄-中间的分隔符-小白
        System.out.println(strEmp1);
        // 前缀*小名-中间的分隔符-小红-中间的分隔符-小蓝-中间的分隔符-小灰-中间的分隔符-小黄-中间的分隔符-小白&后缀
        System.out.println(strEmp2);
        // maxBy 按照比较器中的比较结果刷选 最大值
        Optional<Integer> maxAge = list.stream().map(emp -> emp.getAge())
                .collect(Collectors.maxBy(Comparator.comparing(Function.identity())));
        // 最小值
        Optional<Integer> minAge = list.stream().map(emp -> emp.getAge())
                .collect(Collectors.minBy(Comparator.comparing(Function.identity())));
        System.out.println("max:" + maxAge);
        System.out.println("min:" + minAge);

        // 归约操作
        System.out.println("================================== 归约操作====================================================");
        Optional<Integer> count1= list.stream().map(emp -> emp.getAge()).collect(Collectors.reducing((x, y) -> x + y));
        Integer count2= list.stream().map(emp -> emp.getAge()).collect(Collectors.reducing(0, (x, y) -> x + y));
        System.out.println("count===>"+count+"============count2=====>"+count2);
        // 分操作 groupingBy 根据地址，把原list进行分组
        System.out.println("==================================分操作 groupingBy 根据地址，把原list进行分组====================================================");
        Map<String, List<Emp>> mapGroup = list.stream().collect(Collectors.groupingBy(Emp::getAddress));
        // partitioningBy 分区操作 需要根据类型指定判断分区
        //partitioningBy会根据值是否为true，把集合分割为两个列表，一个true列表，一个false列表。
        Map<Boolean, List<Integer>> partitioningMap = list.stream().map(emp -> emp.getAge())
                .collect(Collectors.partitioningBy(emp -> emp > 20));
        System.out.println("==================================partitioningBy会根据值是否为true，把集合分割为两个列表，一个true列表，一个false列表====================================================");
        System.out.println(partitioningMap);

    }
    static class Emp {
        private String address;

        private String name;

        private Integer age;

        public Emp() {

        }

        public Emp(String address) {
            this.address = address;
        }

        public Emp(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public Emp(String address, String name, Integer age) {
            super();
            this.address = address;
            this.name = name;
            this.age = age;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Emp [address=" + address + ", name=" + name + ", age=" + age + "]";
        }

    

}

```

## 





# 设计模式

设计模式是被大多数人使用的代码经验总结

1.单例模式

获取对象时保证获取的都是同一个。适用与对象在整个生命周期中是无状态的，生成后不会被修改，查多的场景。

单例有两种：懒汉和饿汉模式

懒汉：一启动就加载

饿汉：当使用时再加载

具体实现见代码

2.建造者模式

3.工厂模式

4.代理模式

# Nacos

1 注册中心

2.服务配置  

# ElasticSearch

## 1.概念

### 1.ES 与关系型数据库对比

| 关系型数据库 | database（数据库）        | table（表）     | Row（行） | column（列） |
| ------------ | ------------------------- | --------------- | --------- | ------------ |
| ES           | type（类型，7之后统一_doc | Index（索引库） | document  | field        |
|              |                           |                 |           |              |

**index(索引库)**

索引库类似于数据库中的表，是字段集合，索引库名称必须全部是小写字母的

**映射mapping** 

mapping是处理数据的方式和规则做一些显示，如某个字段，数据类型，默认值，分词器，首付被索引等 

**字段field**

相当于数据库的字段

**字段类型type**

每个字段都应该对应一个类型，例如：text，keyword，byte等

### 2.倒排索引

通过分词内容关联id，与mysql等数据库中的

修改默认分词方法(这里修改school_index索引的默认分词为：ik_max_word)： 



```
PUT /school_index 

{
"settings" : { 

"index" : {

"analysis.analyzer.default.type": "ik_max_word" } } } 
```



## 2.基本语法

### 索引操作

```
#测试分词器
POST _analyze 
 { "analyzer": "ik_max_word",  "text": "中华人民共和国测试"  }
#添加信息
 PUT /es_db/_doc/1
 {
   "name":"刘六",
   "sex":1,
   "age":25,
   "address":"广州天河公园",
   "remark":"sdjfh"
 }
 #按id获取
 GET /es_db/_doc/1
 
 #修改文档
  PUT /es_db/_doc/1
 {
   "name":"白起老师",
   "sex":2,
   "age":55,
   "address":"电饭锅",
   "remark":"尔特人儿"
 }
 #删除文档
格式: DELETE /索引名称/类型/id 
举例: DELETE /es_db/_doc/1
```



### 查询语句

```
格式: GET /索引名称/类型/_search
举例: GET /es_db/_doc/_search
SQL:  select * from student

格式: GET /索引名称/类型/_search?q=*:***
举例: GET /es_db/_doc/_search?q=age:28
SQL:  select * from student where age = 28


格式: GET /索引名称/类型/_search?q=***[25 TO 26]
举例: GET /es_db/_doc/_search?q=age[25 TO 26]
SQL:  select * from student where age between 25 and 26

格式: GET /索引名称/类型/_mget
举例: GET /es_db/_doc/_mget 
{
 "ids":["1","2"]  
 }
SQL:  select * from student where id in (1,2)


格式: GET /索引名称/类型/_search?q=age:<=**
举例: GET /es_db/_doc/_search?q=age:<=28
SQL:  select * from student where age <= 28

格式: GET /索引名称/类型/_search?q=age:>**
举例: GET /es_db/_doc/_search?q=age:>28
SQL:  select * from student where age > 28


格式: GET /索引名称/类型/_search?q=age[25 TO 26]&from=0&size=1
举例: GET /es_db/_doc/_search?q=age[25 TO 26]&from=0&size=1
SQL:  select * from student where age between 25 and 26 limit 0, 1 

格式: GET /索引名称/类型/_search?_source=字段,字段
举例: GET /es_db/_doc/_search?_source=name,age
SQL:  select name,age from student

格式: GET /索引名称/类型/_search?sort=字段 desc
举例: GET /es_db/_doc/_search?sort=age:desc
SQL:  select * from student order by age desc
```

### 批量操作

#### 1.批量获取文档数据

批量获取文档数据是通过   \_mget的API来实现.

(1)在url中不指定index和type

- 请求方式：GET
- 请求地址：_mget
- 功能说明：可以通过ID批量获取不同index和type的数据
  请求参数：
  - docs :文档数组参数
    - _index:指定index
    - _type:指定type
    - _id：指定id
    - _source:指定要查询的字段



```
GET _mget
{
"docs": [
    {
        "_index": "es_db",
        "_type": "_doc",
        "_id": 1
    },
    {
        "_index": "es_db",
        "_type": "_doc",
        "_id": 2
    }
		]
}
```

(2)在URL 中指定index

- 请求方式：GET

- 请求地址：/{{indexName}}/_mget

- 功能说明：可以通过ID批量获取不同的index和type的数据 

- 请求数据：
  - docs：文档数组参数
    - _index:指定index
    - _type:指定type
    - _id：指定id
    - _source：指定要查询的字段

```
GET /es_db/_mget
{
  "docs":[
      {
        "_type":"_doc",
        "_id":6,
        "_source":["name","sex","age"]
      }
    ]
}
```

（3）在url中指定index和type

- 请求方式：GET

- 请求地址：/{{indexName}}/{{typeName}}/_mget

- 功能请求：可以通过ID批量获取不同index和type的数据

- 请求参数：
  - docs:文档数组参数
    - _index:指定index
    - _type:指定ype
    - _id:指定id
    - _source:指定 查询的字段

```
GET /es_db/_doc/_mget
{
"docs": [
{
"_id": 1
},
{
"_id": 2
}
]
}
```

#### 2.批量操作文档数据

批量对文档进行写操作通过_bulk的API来实现的

- 请求方式：POST

- 请求地址：_bulk

- 请求参数：通过_bulk操作文档，一般至少有两行参数（或偶数行参数）
  - 第一行参数为指定操作的类型及操作的对象（index,type和id）
  - 第二行参数才是操作的数据

参数类似于：

```
{"actionName":{"_index":"indexName","_type":"typeName","_id":"id"}}
{"field1":"value","field2":"value2"}
```

**actionName**:标识操作类型，主要有create，index,delete和update

- (1)批量创建文档create

```
POST _bulk
{"create":{"_index":"article", "_type":"_doc", "_id":3}}
{"id":3,"title":"白起老师1","content":"白起老师666","tags":["java", "面向对象"],"create_time":1554015482530}
{"create":{"_index":"article", "_type":"_doc", "_id":4}}
{"id":4,"title":"白起老师2","content":"白起老师NB","tags":["java", "面向对象"],"create_time":1554015482530}
```

- (2)普通创建或全量替换index
  - 如何原文档不存在，则是创建
  - 如果原文档存在，则是替换

```
POST _bulk
{"index":{"_index":"article", "_type":"_doc", "_id":3}}
{"id":3,"title":"图灵徐庶老师(一)","content":"图灵学院徐庶老师666","tags":["java", "面向对象"],"create_time":1554015482530}
{"index":{"_index":"article", "_type":"_doc", "_id":4}}
{"id":4,"title":"图灵诸葛老师(二)","content":"图灵学院诸葛老师NB","tags":["java", "面向对象"],"create_time":1554015482530}
```

   



- (3)批量删除delete

```
POST _bulk
{"delete":{"_index":"article", "_type":"_doc", "_id":3}}
{"delete":{"_index":"article", "_type":"_doc", "_id":4}}
```

- (4)批量修改update

```
POST _bulk
{"update":{"_index":"article", "_type":"_doc", "_id":3}}
{"doc":{"title":"ES大法必修内功"}}
{"update":{"_index":"article", "_type":"_doc", "_id":4}}
{"doc":{"create_time":1554018421008}}
```



## 3.指定分词器

​     

```json
POST _analyze
{ "analyzer":"standard", "text":"我爱你中国" }              
```

## 4.DSL语言高级查询

DSL 被称为领域专用语言

DSL提供基于json的dsl来定义查询，dsl有叶子查询字句和复合字句构成

![images](images/clipboard.png)

### 1.无条件查询

无查询条件是查询所有，默认是查询所有，或者使用match_all表示所有

```
GET /es_db/_doc/_search
{
"query":{
"match_all":{}
}
}
```

### 2.有条件查询

#### 2.1叶子条件查询（单字段查询条件）

##### 2.1.1模糊匹配

模糊匹配主要是针对文本类型的字段，稳定版类型的字段会对内容分词，对查询时也会对搜索条件进行分词，然后通过倒排索引查找到匹配的数据，模模糊糊匹配主要通过match等参数来实现

- match: 通过match关键词模糊匹配条件内容

- prefix:前缀匹配

- regexp:通过正则表达式来匹配数据

match的复杂用法

match条件还支持以下参数

- query 指定匹配的值

- operator 匹配条件类型
  - and 条件分词后都要匹配
  - or 条件分词后有一个匹配即可
- minum_should_match:指定最小匹配的数量



2.1.2 精确匹配

- term:单个条件匹配
- terms：单个字段属于某个值数组内的值
- range：字段属于某个范围的值
- exists：某个字段的值是否存在
- ids:通过ID批量查询



#### 2.2 组合条件查询（多条件查询）

组合条件查询时将叶子条件查询语句进行组合而形成的一个完成的组合查询条件

- bool：各条件之间有and ，or或not的关系
  - must：各条件都必须满足，即各条件是and的关系
  - should:各个条件有一个满足即可，即各条件是or的关系
  - must_not:不满足所有条件即各条件是not的关系
  - filter:不计算相关度评分，它不计算_score即相关度评分，效率更高
- constant_score:不计算相关度评分

must/filter/shoud/must_not等的子条件是通过term/terms/range/ids/exists/match等叶子条件为参数的

注：以上参数，当只有一个搜索条件时，must等对应的是一个对象，当时多个条件，对应的是一个数组



### 3.查询关键词区别

#### 1.match

match:模糊匹配，需要指定字段名称，输入会进行分词，比如“hello world”会进行拆分为hellow和world，然后匹配，如果字段中

### 2.term

### 3.match_phrase

### 4.query_string



4.常用功能

4.1高亮显示

fragment_size: 某字段的值，长度是1万，但是我们一般不会在页面展示这么长，可能只是展示一部分。设置要显示出来的fragment文本判断的长度，默认是100

 

number_of_fragments：高亮的文本片段有多个，你希望展示几个

no_match_size: 可能没有高亮的文本片段，这个参数可以设置从该字段的开始制定长度为多少，然后作为默认的显示
以上词没明白什么意思

4.2推荐词

### 5.文件搜索

### 6.父子级关系搜索

### 7.分页

#### 1.使用from和size进行分页



在查询时，可以指定from（从第几条数据开始查起）和size(每页返回多少条)数据（超过1w条数据后会失效）

```json
POST /es_db/_doc/_search
{
"from": 0,
"size": 2,
"query": {
"match": {
"address": "广州天河"
}
}
}
```



#### 2、使用scroll方式进行分页

前面使用from和size方式，查询在1W条数据以内都是OK的，但如果数据比较多的时候，会出现性能问题。Elasticsearch做了一个限制，不允许查询的是10000条以后的数据。如果要查询1W条以后的数据，需要使用Elasticsearch中提供的scroll游标来查询。

在进行大量分页时，每次分页都需要将要查询的数据进行重新排序，这样非常浪费性能。使用scroll是将要用的数据一次性排序好，然后分批取出。性能要比from + size好得多。使用scroll查询后，排序后的数据会保持一定的时间，后续的分页查询都从该快照取数据即可。

**2.1、第一次使用scroll分页查询**

此处，我们让排序的数据保持1分钟，所以设置scroll为1m

```
GET /es_db/_search?scroll=1m { "query": { "multi_match":{ "query":"广州长沙张三", "fields":["address","name"] } }, "size":100 }
```

​           

执行后，我们注意到，在响应结果中有一项：

"_scroll_id": "DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAAZEWY2VQZXBia1JTVkdhTWkwSl9GaUYtQQ=="

后续，我们需要根据这个_scroll_id来进行查询

**2.2、第二次直接使用scroll id进行查询**

​                GET _search/scroll?scroll=1m { "scroll_id":"DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAAZoWY2VQZXBia1JTVkdhTWkwSl9GaUYtQQ==" }        

这时候会按上面的查询条件返回下一批数据，如果第一次查询时已将所有数据返回，这时则是空      

### 8.地理查询



# webFlux

## 常用方法说明

### map

### flatMap

# DDD



设计要刚刚好，不过度设计

实体：（充血模型）除了基础的属性外，还有一些基础的业务数据操作，只包含对属性状态发生变化的方法

贫血模型：包含属性和get，set方法

值对象：实体中属性是一个对象就是一个值对象

数据库操作：使用仓库来调用数据库操作（repository),数据库对应对象使用DTO 

与其他微服务交互：抽象接口 叫防腐层（一般会将与第三方依赖的信息抽象出来）

领域服务：将跨实体的动作抽象出来，调用实体

![image-20221222214540333](images/image-20221222214540333.png)



领域划分，可以按照controller进行划分，一个controller可以当成一个领域。

领域划分完成后，在领域内部按照四层模型进行开发

entity ，infrastruture，repository，service，adapter用来多个领域之间相互调用，adapter暴露出单前领域对外提供的能力

 ## 实体，值对象





## 贫血模型

基于基础pojo构建实体，

没有具体的业务操作只作为数据传输使用

## 充血模型

简化了service，在实体中会有具体的操作 

## 仓库与工厂

![1664091968596](images/1664091968596.png)



![1664092652322](images/1664092652322.png)

![1664094893313](images/1664094893313.png)

![1664095453536](images/1664095453536.png)





值对象：是可以变化的



变化大的用充血模型

![1664097554337](images/1664097554337.png)

![1664098334179](images/1664098334179.png)

![1664098571108](images/1664098571108.png)

 ![1664098654900](images/1664098654900.png)

![1664098779521](images/1664098779521.png)

![1664098913040](images/1664098913040.png)

![1664099000592](images/1664099000592.png)

![1664099055134](images/1664099055134.png)

![1664099373853](images/1664099373853.png)

![1664100023178](images/1664100023178.png)

![1664100439501](images/1664100439501.png)

![1664100848204](images/1664100848204.png)



![1664101056179](images/1664101056179.png)

![1664111985754](images/1664111985754.png)

![1664112143805](images/1664112143805.png)

![1664112287557](images/1664112287557.png)

![1664113590828](images/1664113590828.png)

![1664113767234](images/1664113767234.png)

![1664114057074](images/1664114057074.png)

![1664114101694](images/1664114101694.png)

 