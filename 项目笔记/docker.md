1.  容器与虚拟机有什么不同
   1. 容器更小，只包含最小运行资源
2. docker
   1. 镜像
   2. 容器
   3. 仓库
3. docker-hub 镜像容器仓库

架构

1. c/s结构系统，docker

# 安装docker步骤

1. 确定centos的版本，

   cat /etc/redhat-release

2. 如果有旧版本可以卸载掉旧版本

   yum remove docker

3. 安装gcc依赖

   1. yum -y install gcc
   2. yum -y install gcc-c++

4. 安装软件包

   yum install -y yum-utils

5. 设置阿里云源

   1. yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

6. 更新索引包

   1. yum makecache fast

7. 安装docker ce 

   1. yum -y install docker-ce docker-ce-cli containerd.io

8. 启动docker

   1. systemctl start docker

9. 测试

   1. docker version
   2. docker run hello-world

# 设置阿里云加速

1. 打开阿里云网站
   1. https://promotion.aliyun.com/ntms/act/kubernetes.html

![image-20240202104731806](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202104731806.png)

2. 点击控制台，找到容器下容器镜像服务

![image-20240202105021773](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202105021773.png)

3. 获取加速器地址

![image-20240202105244070](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202105244070.png)

在机器上执行以下代码

```
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://oa2hhojs.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

# 常用命令

## docker操作命令





## 镜像命令

1. 查看镜像
   1. 
2. 下载镜像
   1. 88888888888888
3. 删除镜像



docker ps  罗列出运行的容器

docker run -it --name=myu1 ubuntu bash

docker run help

docker ps   -a  正在运行+历史运行

exit：   退出容器

ctrl+q+p 退出后还活着

docker restart id   启动

docker stop id       停止容器

docker rm id/name 删除停止的容器

docker rm -f id/name 强制删除

docker rm -f $(docker ps -a -q)  删除所有运行的镜像

## 进入容器

docker run -d 容器名   后台运行

docker logs 37ebe599bd9f 看日志

docker top id 显示容器内部资源

docker exec -it id  /bin/bash  进入容器内部。退出后不会导致容器停止

docker attache id  停止容器运行，退出会导致容器停止

docker cp id:容器内路径   机器路径. 将某个文件拷贝到及其上

docker export id > abcd.tar 导出容器‘

cat   abcd.tar | docker  import atguigu/ubuntu:3.7 导入容器

## 创建镜像

docker comit n='说明' -a ’zuozhe‘  id  新名字:标签 

## 发布镜像到阿里云

 

## 私有仓库创建

使用docker registry创建私有仓库

1. 下载docker registry
   1. docker pull registry 
2. 运行私有库Registry，相当于本地有个私有Docker hub
   1. docker run -d -p 5000:5000  -v /zzyyuse/myregistry/:/tmp/registry --privileged=true registry
      默认情况，仓库被创建在容器的/var/lib/registry目录下，建议自行用容器卷映射，方便于宿主机联调

3. 将镜像修改为符合私服规范的tag

   ```
   按照公式： docker   tag   镜像:Tag   Host:Port/Repository:Tag
   自己host主机IP地址，填写同学你们自己的，不要粘贴错误，O(∩_∩)O
   使用命令 docker tag 将zzyyubuntu:1.2 这个镜像修改为192.168.111.162:5000/zzyyubuntu:1.2
    
   docker tag  zzyyubuntu:1.2  192.168.111.162:5000/zzyyubuntu:1.2
   ```

4. push推送到私服库

```
docker push 192.168.111.162:5000/zzyyubuntu:1.2
```

![image-20240202223727170](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202223727170.png)

5. 验证是否上传成功

   ```
   curl -XGET http://192.168.111.162:5000/v2/_catalog
   ```

   ![image-20240202223659862](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202223659862.png)

6. 使用上传的镜像

   ```
   docker pull 192.168.111.162:5000/zzyyubuntu:1.2
   ```

**问题一：如果出现截图情况，需要修改配置问题支持http上传方式**

![image-20240202222557283](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240202222557283.png)

vim命令新增如下内容：vim /etc/docker/daemon.json

{
  "registry-mirrors": ["https://aa25jngu.mirror.aliyuncs.com"],
  **"insecure-registries": ["192.168.111.162:5000"]**
}

# 容器卷

1. 容器与主机的映射

   1. docker run -it **--privileged=true** -v /宿主机绝对路径目录:/容器内目录      镜像名

   2. Docker挂载主机目录访问如果出现cannot open directory .: Permission denied
      解决办法：在挂载目录后多加一个--privileged=true参数即可

      如果是CentOS7安全模块会比之前系统版本加强，不安全的会先禁止，所以目录挂载的情况被默认为不安全的行为，
      在SELinux里面挂载目录被禁止掉了额，如果要开启，我们一般使用--privileged=true命令，扩大容器的权限解决挂载目录没有权限的问题，也即
      使用该参数，container内的root拥有真正的root权限，否则，container内的root只是外部的一个普通用户权限。

   3. 样例命令

      ```
      docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录      镜像名
      
      docker run -it --name myu3 --privileged=true -v /tmp/myHostData:/tmp/myDockerData ubuntu /bin/bash
      ```

   4. 通过：docker inspect 容器ID 可以查看是否挂载成功，出现以下内容说明挂载成功![image-20240203175645857](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240203175645857.png)

      

2. 当容器停止后，重新启动映射内容依旧会同步（在宿主机修改的文件会同步到容器内）

3. 容器的读写规则

# Docker 常用常规软件安装

## 安装tomcat

```
docker run -d -p 8080:8080 --name mytomcat8 billygoo/tomcat8-jdk8
```

## 安装mysql

1. 新建mysql镜像

```
docker run -d -p 3306:3306 --privileged=true -v /zzyyuse/mysql/log:/var/log/mysql -v /zzyyuse/mysql/data:/var/lib/mysql -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=123456  --name mysql mysql:5.7
```

2. 设置配置文件

   进入cd /zzyyuse/mysql/conf 目录新建 vi  my.cnf 

   ```
   [client]
   default_character_set=utf8
   [mysqld]
   collation_server = utf8_general_ci
   character_set_server = utf8
   ```

3. 重启docker

   ```
   docker restart mysql
   ```

## 安装redis 

1. 安装redis

```
docker run  -p 6379:6379 --name myr3 --privileged=true -v /app/redis/redis.conf:/etc/redis/redis.conf -v /app/redis/data:/data -d redis:6.0.8 redis-server /etc/redis/redis.conf
```

2. 在宿主机下新建目录 /app/redis

   ```
   cd /app/redis
   mkdir -p /app/redis
   ```

3. 将redis.conf复制到/app/redis目录下

   ```
   cp /myredis/redis.conf /app/redis/
   ```

   

## 安装nginx



# 复杂安装

## mysql 主从复制

1. 新建服务器实例3307

```
docker run -p 3307:3306 --name mysql-master \
-v /mydata/mysql-master/log:/var/log/mysql \
-v /mydata/mysql-master/data:/var/lib/mysql \
-v /mydata/mysql-master/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root  \
-d mysql:5.7
```

2. 进入/mydata/mysql-master/conf目录下新建my.cnf

```
[mysqld]
## 设置server_id，同一局域网中需要唯一
server_id=101 
## 指定不需要同步的数据库名称
binlog-ignore-db=mysql  
## 开启二进制日志功能
log-bin=mall-mysql-bin  
## 设置二进制日志使用内存大小（事务）
binlog_cache_size=1M  
## 设置使用的二进制日志格式（mixed,statement,row）
binlog_format=mixed  
## 二进制日志过期清理时间。默认值为0，表示不自动清理。
expire_logs_days=7  
## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
slave_skip_errors=1062

```

3. 修改完配置后重启master实例

```
docker restart mysql-master
```

4. 进入mysql-master容器

   ```
   docker exec -it mysql-master /bin/bash
   mysql -uroot -proot
   ```

   

5. master容器实例内创建数据同步用户

   ```
   CREATE USER 'slave'@'%' IDENTIFIED BY '123456';
   GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'slave'@'%';
   ```

   

6. 新建从服务容器实例3308

   ```
   docker run -p 3308:3306 --name mysql-slave \
   -v /mydata/mysql-slave/log:/var/log/mysql \
   -v /mydata/mysql-slave/data:/var/lib/mysql \
   -v /mydata/mysql-slave/conf:/etc/mysql \
   -e MYSQL_ROOT_PASSWORD=root  \
   -d mysql:5.7
   ```

   

7. 进入/mydata/mysql-slave/conf目录下新建my.cnf

   ```
   [mysqld]
   ## 设置server_id，同一局域网中需要唯一
   server_id=102
   ## 指定不需要同步的数据库名称
   binlog-ignore-db=mysql  
   ## 开启二进制日志功能，以备Slave作为其它数据库实例的Master时使用
   log-bin=mall-mysql-slave1-bin  
   ## 设置二进制日志使用内存大小（事务）
   binlog_cache_size=1M  
   ## 设置使用的二进制日志格式（mixed,statement,row）
   binlog_format=mixed  
   ## 二进制日志过期清理时间。默认值为0，表示不自动清理。
   expire_logs_days=7  
   ## 跳过主从复制中遇到的所有错误或指定类型的错误，避免slave端复制中断。
   ## 如：1062错误是指一些主键重复，1032错误是因为主从数据库数据不一致
   slave_skip_errors=1062  
   ## relay_log配置中继日志
   relay_log=mall-mysql-relay-bin  
   ## log_slave_updates表示slave将复制事件写进自己的二进制日志
   log_slave_updates=1  
   ## slave设置为只读（具有super权限的用户除外）
   read_only=1
   ```

   8. 修改玩配置后重启逆slave实例

      ```
      docker restart mysql-slave
      ```

   9. 在主数据库中查看主从同步状态

      ```
      show master status;
      ```

   10. 进入mysql-slave容器

       ```
       docker exec -it mysql-slave /bin/bash
       mysql -uroot -proot
       ```

   11. 在从数据库中配置主从复制

```sql
master_host：主数据库的IP地址；
master_port：主数据库的运行端口；
master_user：在主数据库创建的用于同步数据的用户账号；
master_password：在主数据库创建的用于同步数据的用户密码；
master_log_file：指定从数据库要复制数据的日志文件，通过查看主数据的状态，获取File参数；
master_log_pos：指定从数据库从哪个位置开始复制数据，通过查看主数据的状态，获取Position参数；
master_connect_retry：连接失败重试的时间间隔，单位为秒。

change master to master_host='主ip',master_user='主账号',master_password='123456',master_port=主端口,master_log_file='mall-mysql-bin.000001',master_log_pos=617,master_connect_retry=30;

# 执行语句
change master to master_host='172.17.0.1',master_user='slave',master_password='123456',master_port=3307,master_log_file='mall-mysql-bin.000001',master_log_pos=617,master_connect_retry=30;

```

```

```



12. 查看状态 show slave status \G;

    ```
    show slave status \G;
    ```

    ![image-20240204095649319](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240204095649319.png)

13. 插入数据查看效果

```
开启端口
firewall-cmd --zone=public --add-port=3307/tcp --permanent
firewall-cmd --zone=public --add-port=3308/tcp --permanent
firewall-cmd --zone=public --add-port=6381/tcp --permanent
firewall-cmd --reload

```

## 安装Redis集群



 客户端使用redis-cli  -p  6981 -c    集群连接  

redis-cli --cluster check 172.17.0.1:6381 验证哈希槽

集群新增节点后，会重新分配槽点，旧的槽点会每个均一点给新增的节点

1. **启动多台redis**

   ```
   docker run -d --name redis-node-1 --net host --privileged=true -v /data/redis/share/redis-node-1:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6381
    
   docker run -d --name redis-node-2 --net host --privileged=true -v /data/redis/share/redis-node-2:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6382
    
   docker run -d --name redis-node-3 --net host --privileged=true -v /data/redis/share/redis-node-3:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6383
    
   docker run -d --name redis-node-4 --net host --privileged=true -v /data/redis/share/redis-node-4:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6384
    
   docker run -d --name redis-node-5 --net host --privileged=true -v /data/redis/share/redis-node-5:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6385
    
   docker run -d --name redis-node-6 --net host --privileged=true -v /data/redis/share/redis-node-6:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6386
   ```

   ![image-20240205092621084](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205092621084.png)

2. **进入容器redis-node-1并为6台机器构建集群关系**

   1. 进入容器

      ```
      docker exec -it redis-node-1 /bin/bash
      ```

   2. 构建主从关系

      ```
      //注意，进入docker容器后才能执行一下命令，且注意自己的真实IP地址
      
      redis-cli --cluster create 172.17.0.1:6381 172.17.0.1:6382 172.17.0.1:6383 172.17.0.1:6384 172.17.0.1:6385 172.17.0.1:6386 --cluster-replicas 1
      
      //--cluster-replicas 1 表示为每个master创建一个slave节点
      
      ```

      

      ![image-20240205093804245](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205093804245.png)

   3. 运行完成后，主从配置完成

      ```
      redis-cli --cluster check 172.17.0.1:6381
      ```

   4. 链接进入6381

      ```
      redis-cli  -p  6981 -c # 链接集群，不加c的话可能链接不上其他节点
      redis-cli --cluster check 172.17.0.1:6381
      cluster info
      cluster nodes
      ```

## redis主从扩容

1. **新建6387,6388两个节点+启动+查看**

   ```
   docker run -d --name redis-node-7 --net host --privileged=true -v /data/redis/share/redis-node-7:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6387
   
   docker run -d --name redis-node-8 --net host --privileged=true -v /data/redis/share/redis-node-8:/data redis:6.0.8 --cluster-enabled yes --appendonly yes --port 6388
   
   docker ps
   
   ```

2. **进入节点内部**

```
docker exec -it redis-node-7 /bin/bash
```

3. **将新增的6387作为你master节点加入集群**

   ```
   将新增的6387作为master节点加入集群
   redis-cli --cluster add-node 自己实际IP地址:6387 自己实际IP地址:6381
   6387 就是将要作为master新增节点
   6381 就是原来集群节点里面的领路人，相当于6387拜拜6381的码头从而找到组织加入集群
   
   # 将新增节点加入集群
   redis-cli --cluster add-node 172.17.0.1:6387 172.17.0.1:6381
   # 查看（在任意节点查看都会转到主节点上6381节点）
   redis-cli --cluster check 172.17.0.1:6381
   ```

4. **重新分配槽号**

```
重新分派槽号：
命令:redis-cli --cluster reshard IP地址:端口号

redis-cli --cluster reshard 172.17.0.1:6381


```

![image-20240205105953161](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205105953161.png)

![image-20240205110011580](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205110011580.png)

**为什么6387是3个新的区间，以前的还是连续？**
重新分配成本太高，所以前3家各自匀出来一部分，从6381/6382/6383三个旧节点分别匀出1364个坑位给新节点6387

5. **为主节点6387分配从节点6388**

```
命令：redis-cli --cluster add-node ip:新slave端口 ip:新master端口 --cluster-slave --cluster-master-id 新主机节点ID
 
redis-cli --cluster add-node 172.17.0.1:6388 172.17.0.1:6387 --cluster-slave --cluster-master-id e4781f644d4a4e4d4b4d107157b9ba8144631451
-------这个是6387的编号，按照自己实际情况


redis-cli --cluster add-node 172.17.0.1:6388 1172.17.0.1:6387 --cluster-slave --cluster-master-id 718afcd647404ac90c5edf17b089b25cf70bc66d



```

![image-20240205113717548](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205113717548.png)

![image-20240205113732897](D:\文档\王海晶个人文档\笔记\node\项目笔记\images\image-20240205113732897.png)

## 主从缩容案例

1. 6387和6388下线

   查看集群情况

2. 

```
命令：redis-cli --cluster del-node ip:从机端口 从机6388节点ID
 redis-cli --cluster del-node 172.17.0.1:6388 5d149074b7e57b802287d1797a874ed7a1a284a8
```



# DockerFile





# 部署Docker微服务





# Docker容器网络





# 容器编排

1. 是什么：负责实现对docker容器集群的快速编排 
2. 干什么：能一键部署素项目
