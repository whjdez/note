# nacos

## 1.什么是nacos

Nacos 致力于帮助您发现、配置和管理微服务。Nacos 提供了一组简单易用的特性集，帮助您快速实现动态服务发现、服务配置、服务元数据及流量管理。

Nacos 的关键特性包括:

- 服务发现和服务健康监测
- 动态配置服务
- 动态 DNS 服务
- 服务及其元数据管理

官方文档：https://nacos.io/zh-cn/docs/architecture.html

## 2.Nacos Server部署

**下载安装包**

下载地址：https://github.com/alibaba/Nacos/releases

下载解压后可看到如下目录

![1598779077652](nacos/1598779077652.png)

**单机模式：**

进入目录执行

bin/startup.sh -m standalone

