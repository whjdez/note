# 1.spring boot

# 2.eureka

## 2.1客户端代码分析

### 2.1.1 注解扫盲

```
@ConditionalOnClass
```

```
@Singleton
```

```
@AutoConfigureAfter
```

```
@ConditionalOnMissingBean
```

```
@Inject
```

```
@VisibleForTesting
```
```
@EventListener 
```

```
@ConditionalOnProperty
```

```
@ImplementedBy
```

```
@Singleton
```

```
@Bean(destroyMethod = "shutdown")
```

# 3.Feign

# 4.Ribbon

# 5.Hystrix 

# 6.zuul

## 5.1 作用

Zuul是Netflix开源的微服务网关，可以Eureka、Ribbon、Hystrix使用。通过Zuul可以对外统一出口，隐藏服务名称，对内聚合服务。Zuul使用filter实现其功能。常用的用途有

-  认证服务，拒绝非法请求
- 性能监控，在服务便捷追踪并统计计算
- 动态路由，做负载均衡
- 负载卸载 预先为每种请求分配容量，当请求超过容量时自动丢弃 
- 静态资源处理，直接在返回某些响应

## 5.2 ZUUL使用

### 1.引入依赖

```
<dependency>
 <groupId>org.springframework.cloud</groupId>
 <artifactId>spring‐cloud‐starter‐netflix‐zuul</artifactId>
 </dependency>
<dependency>
 <groupId>org.springframework.cloud</groupId>
 <artifactId>spring‐cloud‐starter‐netflix‐eureka‐client</artifactId>
 </dependency>
```



### 2.配置启动类

添加@EnableZuulProxy 注解

```
@SpringBootApplication
@EnableZuulProxy
public class GatwayApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatwayApiApplication.class, args);
    }

}
```

### 3.配置文件

```
#指定注册中心地址
eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/

#/order-service/api/v1/order/save?user_id=2&product_id=1
#自定义路由映射
zuul:
  routes:
    order-service: /apigateway/order/**   #项目名称可自定义
    product-name: /apigateway/product/**
  #统一入口为上面的配置，其他入口忽略
  ignored-patterns: /*-service/**
  #处理http请求头为空的问题
  sensitive-headers:

```



配置之后就可以通过网关的端口和项目名称访问具体的微服务了



```
1 # 关闭通过微服务名称访问(暴露了微服务)
2 #zuul.ignored‐services=*
3 #指定关闭的微服务
4 zuul.ignored‐services=service‐hystrix‐consumer,service‐member
5 6
# 自定义指定微服务访问路径
7 # zuul.routes.指定微服务serviceId = 指定路径
8 # http://localhost:9000/user‐service/user/getById/1
9 zuul.routes.service‐hystrix‐consumer=/user‐service/**
10
11 # 同时指定微服务的serviceId和访问路径path
12 # user‐route只是给路由一个名称，可以任意起名
13 # http://localhost:9000/user‐consumer/user/getById/1
14 zuul.routes.user‐route.serviceId=service‐hystrix‐consumer
15 zuul.routes.user‐route.path=/user‐service/**
16
17 # 同时指定path和URL
18 #使用这种方式配置的路由不会作为HystrixCommand执行，
19 # 同时也不能使用Ribbon来负载均衡多个URL
20 zuul.routes.user‐route.url=http://localhost:8100/
21 zuul.routes.user‐route.path=/user‐service/**
22
23 #同时执行path和URL，并且不破坏Zuul的Hystrix、Ribbon特性
24 zuul.routes.user‐route.serviceId=service‐hystrix‐consumer
25 zuul.routes.user‐route.path=/user‐service/**
26 #禁用在Ribbon中使用Eureka
27 ribbon.eureka.enabled=false
28 service‐hystrix‐consumer.ribbon.listOfServers=http://localhost:8100/,
29 http://localhost:8101/
30
31 # 路由前缀，当请求匹配前缀时会进行代理
32 #zuul.prefix= /api
33 # 代理前缀默认会从请求路径中移除，通过stripPrefix=false可以关闭移除功能34 # 当下游服务server.servlet.context‐path=/ 映射
localhost:8103/user/getById/1
35 # true: localhost:9000/api/user‐service/user/getById/1
36 #‐‐> localhost:8103/user/getById/1
37 # false: localhost:9000/api/user‐service/user/getById/1
38 #‐‐> localhost:8103/api/user/getById/1
39 # 如果为false，需要下游服务的server.servlet.context‐path=/api才能正常访问
40 #zuul.strip‐prefix=false
41 #zuul.routes.service‐hystrix‐consumer=/user‐service/**
42
43 # 局部配置
44 #zuul.routes.user‐route.serviceId=service‐hystrix‐consumer
45 #zuul.routes.user‐route.path=/user‐service/**
46 # true localhost:9000/user‐service/user/getById/1
47 #‐‐> localhost:8103/user/getById/1
48 # false localhost:9000/user‐service/user/getById/1
49 #‐‐> localhost:8103/user‐service/user/getById/1
50 #zuul.routes.user‐route.strip‐prefix=false
51
52
53 # 忽略某些路径
54 # 想让Zuul代理某个微服务，同时又想保护该微服务某些敏感路径。
55 zuul.ignored‐patterns=/**/admin/**
56
57 #过滤敏感头信息,若不设置,cookie setcookie和 authrotion不会传递给下游服务
58 #zuul.sensitive‐headers=
59 #局部配置
60 zuul.routes.user‐route.sensitive‐headers=
61 zuul.routes.user‐route.serviceId=service‐order
62 zuul.routes.user‐route.path=/order‐service/**
63
```

### 4.过滤器的使用



```
1 public class MyFilter extends ZuulFilter {
2 /**
3 * filterType：返回一个字符串代表过滤器的类型，
4 * 在zuul中定义了四种不同生命周期的过滤器类型，具体如下：
5 * pre：可以在请求被路由之前调用
6 * route：在路由请求时候被调用
7 * post：在route和error过滤器之后被调用
8 * error：处理请求时发生错误时被调用
9 * @return
10 */
11 @Override
12 public String filterType() {
13 // pre类型
14 return PRE_TYPE;
15 }
16
17 @Override
18 public int filterOrder() {
19 //排序
20 return 1;
21 }
22
23 @Override
24 public boolean shouldFilter() {
25 // 返回true才会调用run方法
26 return true;27 }
28
29 @Override
30 public Object run() throws ZuulException {
31 RequestContext ctx = RequestContext.getCurrentContext();
32 System.out.println("执行 MyFilter 过滤器");
33
34 // HttpServletRequest request = ctx.getRequest();
35 // String token = request.getHeader("token");
36 // if (StringUtils.isBlank(token)) {
37 // token = request.getParameter("token");
38 // }
39 // //登陆校验逻辑
40 // if (StringUtils.isBlank(token)) {
41 // ctx.setSendZuulResponse(false);
42 // ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
43 // }
44
45 return null;
46 }
47 }
48
49 @Configuration
50 public class FilterConfig {
51
52 @Bean
53 public MyFilter myFilter() {
54 return new MyFilter();
55 }
56 }
```

### 5.3 源码分析

# 7.config

## 1.作用

> - 统一对文件进行管理
> - 热更新配置文件
> - 加密敏感信息

## 2.使用方法

spring cloud分为服务端和客户端。服务端用于集中管理配置文件（可用git或者svn来存文件以下例子采用git），客户端用于与服务端交互获取配置文件

![1586956502461](images/1586956502461.png)

### 1.引入pom

```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>
```

### 2.添加注解

```
@EnableConfigServer
```

### 3.配置文件

```
server:
  port: 9100
spring:
  application:
    name: config-service
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/whjdez/resource #git地址
          username: 1023120902@qq.com
          password: wangjing7654321
          timeout: 5
          default-label: master #配置分支
          search-paths: config #配置文件所在根目录
```

配置完成后，可通过以下几种方式访问

```
#通过application‐{profiles}.yml访问
2 http://localhost:9100/config‐single‐client‐dev.yml
3 # 通过/lable/application‐{profiles}.yml访问
4 http://localhost:9100/master/config‐single‐client‐dev.yml
5 #通过/{application}/{profiles}/{lable}访问
6 http://localhost:9100/config‐single‐client/prod/master
```

### 4.配置客户端pom

```
<dependency>
 <groupId>org.springframework.cloud</groupId>
 <artifactId>spring‐cloud‐starter‐config</artifactId>
 </dependency>

<dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring‐boot‐starter‐web</artifactId>
</dependency
```

### 5.添加配置文件

```
   配置中心的配置，如果不需要可以删除改节点下的配置
    cloud:
      config:
        # 分支
        label: master
        # 项目名称
        name: ${spring.application.name}
        # 基于注册中心发现
        discovery:
          enabled: true
          service-id: YESWAY-PROJECT-FRAMEWORK-SERVER-CONFIG
        # 默认的环境
        profile: dev
```

## 3.配置热更新文件

### 4.配置加密

